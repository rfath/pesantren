/*
Navicat MySQL Data Transfer

Source Server         : SERVER_KPP
Source Server Version : 50723
Source Host           : operation.kpptechnology.co.id:3306
Source Database       : kpptech_pesantren

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2018-08-26 23:00:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_access_group
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group`;
CREATE TABLE `app_access_group` (
  `access_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_group_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `access_group_status` tinyint(1) DEFAULT NULL COMMENT '1 = "Active", 0 = "Non Active"',
  PRIMARY KEY (`access_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group
-- ----------------------------
INSERT INTO `app_access_group` VALUES ('1', 'Administrator', '1', '2018-08-21 10:51:51', '1', '2018-08-26 22:55:47', '1');
INSERT INTO `app_access_group` VALUES ('2', 'Kesiswaan', '1', '2018-08-21 14:42:36', '1', '2018-08-22 23:25:36', '1');
INSERT INTO `app_access_group` VALUES ('3', 'Tesetting', '1', '2018-08-21 18:51:40', null, null, '1');

-- ----------------------------
-- Table structure for app_access_group_moduldetail
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group_moduldetail`;
CREATE TABLE `app_access_group_moduldetail` (
  `access_group_id` int(11) NOT NULL,
  `access_module_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group_moduldetail
-- ----------------------------

-- ----------------------------
-- Table structure for app_access_group_modulDetail
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group_modulDetail`;
CREATE TABLE `app_access_group_modulDetail` (
  `access_group_id` int(11) NOT NULL,
  `access_module_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group_modulDetail
-- ----------------------------
INSERT INTO `app_access_group_modulDetail` VALUES ('1', '6');
INSERT INTO `app_access_group_modulDetail` VALUES ('2', '1');
INSERT INTO `app_access_group_modulDetail` VALUES ('1', '5');
INSERT INTO `app_access_group_modulDetail` VALUES ('1', '3');
INSERT INTO `app_access_group_modulDetail` VALUES ('1', '2');
INSERT INTO `app_access_group_modulDetail` VALUES ('1', '1');
INSERT INTO `app_access_group_modulDetail` VALUES ('1', '8');

-- ----------------------------
-- Table structure for app_access_group_submodule
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group_submodule`;
CREATE TABLE `app_access_group_submodule` (
  `access_group_id` int(11) NOT NULL,
  `access_submodule_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group_submodule
-- ----------------------------
INSERT INTO `app_access_group_submodule` VALUES ('1', '11');
INSERT INTO `app_access_group_submodule` VALUES ('1', '10');
INSERT INTO `app_access_group_submodule` VALUES ('1', '13');
INSERT INTO `app_access_group_submodule` VALUES ('1', '9');
INSERT INTO `app_access_group_submodule` VALUES ('2', '1');
INSERT INTO `app_access_group_submodule` VALUES ('2', '2');
INSERT INTO `app_access_group_submodule` VALUES ('2', '3');
INSERT INTO `app_access_group_submodule` VALUES ('2', '4');
INSERT INTO `app_access_group_submodule` VALUES ('1', '8');
INSERT INTO `app_access_group_submodule` VALUES ('1', '7');
INSERT INTO `app_access_group_submodule` VALUES ('1', '6');
INSERT INTO `app_access_group_submodule` VALUES ('1', '5');
INSERT INTO `app_access_group_submodule` VALUES ('1', '14');
INSERT INTO `app_access_group_submodule` VALUES ('1', '12');
INSERT INTO `app_access_group_submodule` VALUES ('1', '4');
INSERT INTO `app_access_group_submodule` VALUES ('1', '3');
INSERT INTO `app_access_group_submodule` VALUES ('1', '2');
INSERT INTO `app_access_group_submodule` VALUES ('1', '1');

-- ----------------------------
-- Table structure for app_city
-- ----------------------------
DROP TABLE IF EXISTS `app_city`;
CREATE TABLE `app_city` (
  `city_id` int(3) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `province_id` int(2) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_city
-- ----------------------------
INSERT INTO `app_city` VALUES ('1', 'Kabupaten Aceh Barat', '1');
INSERT INTO `app_city` VALUES ('2', 'Kabupaten Aceh Barat Daya', '1');
INSERT INTO `app_city` VALUES ('3', 'Kabupaten Aceh Besar', '1');
INSERT INTO `app_city` VALUES ('4', 'Kabupaten Aceh Jaya', '1');
INSERT INTO `app_city` VALUES ('5', 'Kabupaten Aceh Selatan', '1');
INSERT INTO `app_city` VALUES ('6', 'Kabupaten Aceh Singkil', '1');
INSERT INTO `app_city` VALUES ('7', 'Kabupaten Aceh Tamiang', '1');
INSERT INTO `app_city` VALUES ('8', 'Kabupaten Aceh Tengah', '1');
INSERT INTO `app_city` VALUES ('9', 'Kabupaten Aceh Tenggara', '1');
INSERT INTO `app_city` VALUES ('10', 'Kabupaten Aceh Timur', '1');
INSERT INTO `app_city` VALUES ('11', 'Kabupaten Aceh Utara', '1');
INSERT INTO `app_city` VALUES ('12', 'Kabupaten Bener Meriah', '1');
INSERT INTO `app_city` VALUES ('13', 'Kabupaten Bireuen', '1');
INSERT INTO `app_city` VALUES ('14', 'Kabupaten Gayo Lues', '1');
INSERT INTO `app_city` VALUES ('15', 'Kabupaten Nagan Raya', '1');
INSERT INTO `app_city` VALUES ('16', 'Kabupaten Pidie', '1');
INSERT INTO `app_city` VALUES ('17', 'Kabupaten Pidie Jaya', '1');
INSERT INTO `app_city` VALUES ('18', 'Kabupaten Simeulue', '1');
INSERT INTO `app_city` VALUES ('19', 'Kota Banda Aceh', '1');
INSERT INTO `app_city` VALUES ('20', 'Kota Langsa', '1');
INSERT INTO `app_city` VALUES ('21', 'Kota Lhokseumawe', '1');
INSERT INTO `app_city` VALUES ('22', 'Kota Sabang', '1');
INSERT INTO `app_city` VALUES ('23', 'Kota Subulussalam', '1');
INSERT INTO `app_city` VALUES ('24', 'Kabupaten Asahan', '2');
INSERT INTO `app_city` VALUES ('25', 'Kabupaten Batu Bara', '2');
INSERT INTO `app_city` VALUES ('26', 'Kabupaten Dairi', '2');
INSERT INTO `app_city` VALUES ('27', 'Kabupaten Deli Serdang', '2');
INSERT INTO `app_city` VALUES ('28', 'Kabupaten Humbang Hasundutan', '2');
INSERT INTO `app_city` VALUES ('29', 'Kabupaten Karo', '2');
INSERT INTO `app_city` VALUES ('30', 'Kabupaten Labuhanbatu', '2');
INSERT INTO `app_city` VALUES ('31', 'Kabupaten Labuhanbatu Selatan', '2');
INSERT INTO `app_city` VALUES ('32', 'Kabupaten Labuhanbatu Utara', '2');
INSERT INTO `app_city` VALUES ('33', 'Kabupaten Langkat', '2');
INSERT INTO `app_city` VALUES ('34', 'Kabupaten Mandailing Natal', '2');
INSERT INTO `app_city` VALUES ('35', 'Kabupaten Nias', '2');
INSERT INTO `app_city` VALUES ('36', 'Kabupaten Nias Barat', '2');
INSERT INTO `app_city` VALUES ('37', 'Kabupaten Nias Selatan', '2');
INSERT INTO `app_city` VALUES ('38', 'Kabupaten Nias Utara', '2');
INSERT INTO `app_city` VALUES ('39', 'Kabupaten Padang Lawas', '2');
INSERT INTO `app_city` VALUES ('40', 'Kabupaten Padang Lawas Utara', '2');
INSERT INTO `app_city` VALUES ('41', 'Kabupaten Pakpak Bharat', '2');
INSERT INTO `app_city` VALUES ('42', 'Kabupaten Samosir', '2');
INSERT INTO `app_city` VALUES ('43', 'Kabupaten Serdang Bedagai', '2');
INSERT INTO `app_city` VALUES ('44', 'Kabupaten Simalungun', '2');
INSERT INTO `app_city` VALUES ('45', 'Kabupaten Tapanuli Selatan', '2');
INSERT INTO `app_city` VALUES ('46', 'Kabupaten Tapanuli Tengah', '2');
INSERT INTO `app_city` VALUES ('47', 'Kabupaten Tapanuli Utara', '2');
INSERT INTO `app_city` VALUES ('48', 'Kabupaten Toba Samosir', '2');
INSERT INTO `app_city` VALUES ('49', 'Kota Binjai', '2');
INSERT INTO `app_city` VALUES ('50', 'Kota Gunung Sitoli', '2');
INSERT INTO `app_city` VALUES ('51', 'Kota Medan', '2');
INSERT INTO `app_city` VALUES ('52', 'Kota Padang Sidempuan', '2');
INSERT INTO `app_city` VALUES ('53', 'Kota Pematangsiantar', '2');
INSERT INTO `app_city` VALUES ('54', 'Kota Sibolga', '2');
INSERT INTO `app_city` VALUES ('55', 'Kota Tanjung Balai', '2');
INSERT INTO `app_city` VALUES ('56', 'Kota Tebing Tinggi', '2');
INSERT INTO `app_city` VALUES ('57', 'Kabupaten Bengkulu Selatan', '3');
INSERT INTO `app_city` VALUES ('58', 'Kabupaten Bengkulu Tengah', '3');
INSERT INTO `app_city` VALUES ('59', 'Kabupaten Bengkulu Utara', '3');
INSERT INTO `app_city` VALUES ('60', 'Kabupaten Benteng', '3');
INSERT INTO `app_city` VALUES ('61', 'Kabupaten Kaur', '3');
INSERT INTO `app_city` VALUES ('62', 'Kabupaten Kepahiang', '3');
INSERT INTO `app_city` VALUES ('63', 'Kabupaten Lebong', '3');
INSERT INTO `app_city` VALUES ('64', 'Kabupaten Mukomuko', '3');
INSERT INTO `app_city` VALUES ('65', 'Kabupaten Rejang Lebong', '3');
INSERT INTO `app_city` VALUES ('66', 'Kabupaten Seluma', '3');
INSERT INTO `app_city` VALUES ('67', 'Kota Bengkulu', '3');
INSERT INTO `app_city` VALUES ('68', 'Kabupaten Batang Hari', '4');
INSERT INTO `app_city` VALUES ('69', 'Kabupaten Bungo', '4');
INSERT INTO `app_city` VALUES ('70', 'Kabupaten Kerinci', '4');
INSERT INTO `app_city` VALUES ('71', 'Kabupaten Merangin', '4');
INSERT INTO `app_city` VALUES ('72', 'Kabupaten Muaro Jambi', '4');
INSERT INTO `app_city` VALUES ('73', 'Kabupaten Sarolangun', '4');
INSERT INTO `app_city` VALUES ('74', 'Kabupaten Tanjung Jabung Barat', '4');
INSERT INTO `app_city` VALUES ('75', 'Kabupaten Tanjung Jabung Timur', '4');
INSERT INTO `app_city` VALUES ('76', 'Kabupaten Tebo', '4');
INSERT INTO `app_city` VALUES ('77', 'Kota Jambi', '4');
INSERT INTO `app_city` VALUES ('78', 'Kota Sungai Penuh', '4');
INSERT INTO `app_city` VALUES ('79', 'Kabupaten Bengkalis', '5');
INSERT INTO `app_city` VALUES ('80', 'Kabupaten Indragiri Hilir', '5');
INSERT INTO `app_city` VALUES ('81', 'Kabupaten Indragiri Hulu', '5');
INSERT INTO `app_city` VALUES ('82', 'Kabupaten Kampar', '5');
INSERT INTO `app_city` VALUES ('83', 'Kabupaten Kuantan Singingi', '5');
INSERT INTO `app_city` VALUES ('84', 'Kabupaten Pelalawan', '5');
INSERT INTO `app_city` VALUES ('85', 'Kabupaten Rokan Hilir', '5');
INSERT INTO `app_city` VALUES ('86', 'Kabupaten Rokan Hulu', '5');
INSERT INTO `app_city` VALUES ('87', 'Kabupaten Siak', '5');
INSERT INTO `app_city` VALUES ('88', 'Kota Pekanbaru', '5');
INSERT INTO `app_city` VALUES ('89', 'Kota Dumai', '5');
INSERT INTO `app_city` VALUES ('90', 'Kabupaten Kepulauan Meranti', '5');
INSERT INTO `app_city` VALUES ('91', 'Kabupaten Agam', '6');
INSERT INTO `app_city` VALUES ('92', 'Kabupaten Dharmasraya', '6');
INSERT INTO `app_city` VALUES ('93', 'Kabupaten Kepulauan Mentawai', '6');
INSERT INTO `app_city` VALUES ('94', 'Kabupaten Lima Puluh Kota', '6');
INSERT INTO `app_city` VALUES ('95', 'Kabupaten Padang Pariaman', '6');
INSERT INTO `app_city` VALUES ('96', 'Kabupaten Pasaman', '6');
INSERT INTO `app_city` VALUES ('97', 'Kabupaten Pasaman Barat', '6');
INSERT INTO `app_city` VALUES ('98', 'Kabupaten Pesisir Selatan', '6');
INSERT INTO `app_city` VALUES ('99', 'Kabupaten Sijunjung', '6');
INSERT INTO `app_city` VALUES ('100', 'Kabupaten Solok', '6');
INSERT INTO `app_city` VALUES ('101', 'Kabupaten Solok Selatan', '6');
INSERT INTO `app_city` VALUES ('102', 'Kabupaten Tanah Datar', '6');
INSERT INTO `app_city` VALUES ('103', 'Kota Bukittinggi', '6');
INSERT INTO `app_city` VALUES ('104', 'Kota Padang', '6');
INSERT INTO `app_city` VALUES ('105', 'Kota Padangpanjang', '6');
INSERT INTO `app_city` VALUES ('106', 'Kota Pariaman', '6');
INSERT INTO `app_city` VALUES ('107', 'Kota Payakumbuh', '6');
INSERT INTO `app_city` VALUES ('108', 'Kota Sawahlunto', '6');
INSERT INTO `app_city` VALUES ('109', 'Kota Solok', '6');
INSERT INTO `app_city` VALUES ('110', 'Kabupaten Banyuasin', '7');
INSERT INTO `app_city` VALUES ('111', 'Kabupaten Empat Lawang', '7');
INSERT INTO `app_city` VALUES ('112', 'Kabupaten Lahat', '7');
INSERT INTO `app_city` VALUES ('113', 'Kabupaten Muara Enim', '7');
INSERT INTO `app_city` VALUES ('114', 'Kabupaten Musi Banyuasin', '7');
INSERT INTO `app_city` VALUES ('115', 'Kabupaten Musi Rawas', '7');
INSERT INTO `app_city` VALUES ('116', 'Kabupaten Ogan Ilir', '7');
INSERT INTO `app_city` VALUES ('117', 'Kabupaten Ogan Komering Ilir', '7');
INSERT INTO `app_city` VALUES ('118', 'Kabupaten Ogan Komering Ulu', '7');
INSERT INTO `app_city` VALUES ('119', 'Kabupaten Ogan Komering Ulu Selatan', '7');
INSERT INTO `app_city` VALUES ('120', 'Kabupaten Ogan Komering Ulu Timur', '7');
INSERT INTO `app_city` VALUES ('121', 'Kota Lubuklinggau', '7');
INSERT INTO `app_city` VALUES ('122', 'Kota Pagar Alam', '7');
INSERT INTO `app_city` VALUES ('123', 'Kota Palembang', '7');
INSERT INTO `app_city` VALUES ('124', 'Kota Prabumulih', '7');
INSERT INTO `app_city` VALUES ('125', 'Kabupaten Lampung Barat', '8');
INSERT INTO `app_city` VALUES ('126', 'Kabupaten Lampung Selatan', '8');
INSERT INTO `app_city` VALUES ('127', 'Kabupaten Lampung Tengah', '8');
INSERT INTO `app_city` VALUES ('128', 'Kabupaten Lampung Timur', '8');
INSERT INTO `app_city` VALUES ('129', 'Kabupaten Lampung Utara', '8');
INSERT INTO `app_city` VALUES ('130', 'Kabupaten Mesuji', '8');
INSERT INTO `app_city` VALUES ('131', 'Kabupaten Pesawaran', '8');
INSERT INTO `app_city` VALUES ('132', 'Kabupaten Pringsewu', '8');
INSERT INTO `app_city` VALUES ('133', 'Kabupaten Tanggamus', '8');
INSERT INTO `app_city` VALUES ('134', 'Kabupaten Tulang Bawang', '8');
INSERT INTO `app_city` VALUES ('135', 'Kabupaten Tulang Bawang Barat', '8');
INSERT INTO `app_city` VALUES ('136', 'Kabupaten Way Kanan', '8');
INSERT INTO `app_city` VALUES ('137', 'Kota Bandar Lampung', '8');
INSERT INTO `app_city` VALUES ('138', 'Kota Metro', '8');
INSERT INTO `app_city` VALUES ('139', 'Kabupaten Bangka', '9');
INSERT INTO `app_city` VALUES ('140', 'Kabupaten Bangka Barat', '9');
INSERT INTO `app_city` VALUES ('141', 'Kabupaten Bangka Selatan', '9');
INSERT INTO `app_city` VALUES ('142', 'Kabupaten Bangka Tengah', '9');
INSERT INTO `app_city` VALUES ('143', 'Kabupaten Belitung', '9');
INSERT INTO `app_city` VALUES ('144', 'Kabupaten Belitung Timur', '9');
INSERT INTO `app_city` VALUES ('145', 'Kota Pangkal Pinang', '9');
INSERT INTO `app_city` VALUES ('146', 'Kabupaten Bintan', '10');
INSERT INTO `app_city` VALUES ('147', 'Kabupaten Karimun', '10');
INSERT INTO `app_city` VALUES ('148', 'Kabupaten Kepulauan Anambas', '10');
INSERT INTO `app_city` VALUES ('149', 'Kabupaten Lingga', '10');
INSERT INTO `app_city` VALUES ('150', 'Kabupaten Natuna', '10');
INSERT INTO `app_city` VALUES ('151', 'Kota Batam', '10');
INSERT INTO `app_city` VALUES ('152', 'Kota Tanjung Pinang', '10');
INSERT INTO `app_city` VALUES ('153', 'Kabupaten Lebak', '11');
INSERT INTO `app_city` VALUES ('154', 'Kabupaten Pandeglang', '11');
INSERT INTO `app_city` VALUES ('155', 'Kabupaten Serang', '11');
INSERT INTO `app_city` VALUES ('156', 'Kabupaten Tangerang', '11');
INSERT INTO `app_city` VALUES ('157', 'Kota Cilegon', '11');
INSERT INTO `app_city` VALUES ('158', 'Kota Serang', '11');
INSERT INTO `app_city` VALUES ('159', 'Kota Tangerang', '11');
INSERT INTO `app_city` VALUES ('160', 'Kota Tangerang Selatan', '11');
INSERT INTO `app_city` VALUES ('161', 'Kabupaten Bandung', '12');
INSERT INTO `app_city` VALUES ('162', 'Kabupaten Bandung Barat', '12');
INSERT INTO `app_city` VALUES ('163', 'Kabupaten Bekasi', '12');
INSERT INTO `app_city` VALUES ('164', 'Kabupaten Bogor', '12');
INSERT INTO `app_city` VALUES ('165', 'Kabupaten Ciamis', '12');
INSERT INTO `app_city` VALUES ('166', 'Kabupaten Cianjur', '12');
INSERT INTO `app_city` VALUES ('167', 'Kabupaten Cirebon', '12');
INSERT INTO `app_city` VALUES ('168', 'Kabupaten Garut', '12');
INSERT INTO `app_city` VALUES ('169', 'Kabupaten Indramayu', '12');
INSERT INTO `app_city` VALUES ('170', 'Kabupaten Karawang', '12');
INSERT INTO `app_city` VALUES ('171', 'Kabupaten Kuningan', '12');
INSERT INTO `app_city` VALUES ('172', 'Kabupaten Majalengka', '12');
INSERT INTO `app_city` VALUES ('173', 'Kabupaten Purwakarta', '12');
INSERT INTO `app_city` VALUES ('174', 'Kabupaten Subang', '12');
INSERT INTO `app_city` VALUES ('175', 'Kabupaten Sukabumi', '12');
INSERT INTO `app_city` VALUES ('176', 'Kabupaten Sumedang', '12');
INSERT INTO `app_city` VALUES ('177', 'Kabupaten Tasikmalaya', '12');
INSERT INTO `app_city` VALUES ('178', 'Kota Bandung', '12');
INSERT INTO `app_city` VALUES ('179', 'Kota Banjar', '12');
INSERT INTO `app_city` VALUES ('180', 'Kota Bekasi', '12');
INSERT INTO `app_city` VALUES ('181', 'Kota Bogor', '12');
INSERT INTO `app_city` VALUES ('182', 'Kota Cimahi', '12');
INSERT INTO `app_city` VALUES ('183', 'Kota Cirebon', '12');
INSERT INTO `app_city` VALUES ('184', 'Kota Depok', '12');
INSERT INTO `app_city` VALUES ('185', 'Kota Sukabumi', '12');
INSERT INTO `app_city` VALUES ('186', 'Kota Tasikmalaya', '12');
INSERT INTO `app_city` VALUES ('187', 'Kabupaten Administrasi Kepulauan Seribu', '13');
INSERT INTO `app_city` VALUES ('188', 'Kota Administrasi Jakarta Barat', '13');
INSERT INTO `app_city` VALUES ('189', 'Kota Administrasi Jakarta Pusat', '13');
INSERT INTO `app_city` VALUES ('190', 'Kota Administrasi Jakarta Selatan', '13');
INSERT INTO `app_city` VALUES ('191', 'Kota Administrasi Jakarta Timur', '13');
INSERT INTO `app_city` VALUES ('192', 'Kota Administrasi Jakarta Utara', '13');
INSERT INTO `app_city` VALUES ('193', 'Kabupaten Banjarnegara', '14');
INSERT INTO `app_city` VALUES ('194', 'Kabupaten Banyumas', '14');
INSERT INTO `app_city` VALUES ('195', 'Kabupaten Batang', '14');
INSERT INTO `app_city` VALUES ('196', 'Kabupaten Blora', '14');
INSERT INTO `app_city` VALUES ('197', 'Kabupaten Boyolali', '14');
INSERT INTO `app_city` VALUES ('198', 'Kabupaten Brebes', '14');
INSERT INTO `app_city` VALUES ('199', 'Kabupaten Cilacap', '14');
INSERT INTO `app_city` VALUES ('200', 'Kabupaten Demak', '14');
INSERT INTO `app_city` VALUES ('201', 'Kabupaten Grobogan', '14');
INSERT INTO `app_city` VALUES ('202', 'Kabupaten Jepara', '14');
INSERT INTO `app_city` VALUES ('203', 'Kabupaten Karanganyar', '14');
INSERT INTO `app_city` VALUES ('204', 'Kabupaten Kebumen', '14');
INSERT INTO `app_city` VALUES ('205', 'Kabupaten Kendal', '14');
INSERT INTO `app_city` VALUES ('206', 'Kabupaten Klaten', '14');
INSERT INTO `app_city` VALUES ('207', 'Kabupaten Kudus', '14');
INSERT INTO `app_city` VALUES ('208', 'Kabupaten Magelang', '14');
INSERT INTO `app_city` VALUES ('209', 'Kabupaten Pati', '14');
INSERT INTO `app_city` VALUES ('210', 'Kabupaten Pekalongan', '14');
INSERT INTO `app_city` VALUES ('211', 'Kabupaten Pemalang', '14');
INSERT INTO `app_city` VALUES ('212', 'Kabupaten Purbalingga', '14');
INSERT INTO `app_city` VALUES ('213', 'Kabupaten Purworejo', '14');
INSERT INTO `app_city` VALUES ('214', 'Kabupaten Rembang', '14');
INSERT INTO `app_city` VALUES ('215', 'Kabupaten Semarang', '14');
INSERT INTO `app_city` VALUES ('216', 'Kabupaten Sragen', '14');
INSERT INTO `app_city` VALUES ('217', 'Kabupaten Sukoharjo', '14');
INSERT INTO `app_city` VALUES ('218', 'Kabupaten Tegal', '14');
INSERT INTO `app_city` VALUES ('219', 'Kabupaten Temanggung', '14');
INSERT INTO `app_city` VALUES ('220', 'Kabupaten Wonogiri', '14');
INSERT INTO `app_city` VALUES ('221', 'Kabupaten Wonosobo', '14');
INSERT INTO `app_city` VALUES ('222', 'Kota Magelang', '14');
INSERT INTO `app_city` VALUES ('223', 'Kota Pekalongan', '14');
INSERT INTO `app_city` VALUES ('224', 'Kota Salatiga', '14');
INSERT INTO `app_city` VALUES ('225', 'Kota Semarang', '14');
INSERT INTO `app_city` VALUES ('226', 'Kota Surakarta', '14');
INSERT INTO `app_city` VALUES ('227', 'Kota Tegal', '14');
INSERT INTO `app_city` VALUES ('228', 'Kabupaten Bangkalan', '15');
INSERT INTO `app_city` VALUES ('229', 'Kabupaten Banyuwangi', '15');
INSERT INTO `app_city` VALUES ('230', 'Kabupaten Blitar', '15');
INSERT INTO `app_city` VALUES ('231', 'Kabupaten Bojonegoro', '15');
INSERT INTO `app_city` VALUES ('232', 'Kabupaten Bondowoso', '15');
INSERT INTO `app_city` VALUES ('233', 'Kabupaten Gresik', '15');
INSERT INTO `app_city` VALUES ('234', 'Kabupaten Jember', '15');
INSERT INTO `app_city` VALUES ('235', 'Kabupaten Jombang', '15');
INSERT INTO `app_city` VALUES ('236', 'Kabupaten Kediri', '15');
INSERT INTO `app_city` VALUES ('237', 'Kabupaten Lamongan', '15');
INSERT INTO `app_city` VALUES ('238', 'Kabupaten Lumajang', '15');
INSERT INTO `app_city` VALUES ('239', 'Kabupaten Madiun', '15');
INSERT INTO `app_city` VALUES ('240', 'Kabupaten Magetan', '15');
INSERT INTO `app_city` VALUES ('241', 'Kabupaten Malang', '15');
INSERT INTO `app_city` VALUES ('242', 'Kabupaten Mojokerto', '15');
INSERT INTO `app_city` VALUES ('243', 'Kabupaten Nganjuk', '15');
INSERT INTO `app_city` VALUES ('244', 'Kabupaten Ngawi', '15');
INSERT INTO `app_city` VALUES ('245', 'Kabupaten Pacitan', '15');
INSERT INTO `app_city` VALUES ('246', 'Kabupaten Pamekasan', '15');
INSERT INTO `app_city` VALUES ('247', 'Kabupaten Pasuruan', '15');
INSERT INTO `app_city` VALUES ('248', 'Kabupaten Ponorogo', '15');
INSERT INTO `app_city` VALUES ('249', 'Kabupaten Probolinggo', '15');
INSERT INTO `app_city` VALUES ('250', 'Kabupaten Sampang', '15');
INSERT INTO `app_city` VALUES ('251', 'Kabupaten Sidoarjo', '15');
INSERT INTO `app_city` VALUES ('252', 'Kabupaten Situbondo', '15');
INSERT INTO `app_city` VALUES ('253', 'Kabupaten Sumenep', '15');
INSERT INTO `app_city` VALUES ('254', 'Kabupaten Trenggalek', '15');
INSERT INTO `app_city` VALUES ('255', 'Kabupaten Tuban', '15');
INSERT INTO `app_city` VALUES ('256', 'Kabupaten Tulungagung', '15');
INSERT INTO `app_city` VALUES ('257', 'Kota Batu', '15');
INSERT INTO `app_city` VALUES ('258', 'Kota Blitar', '15');
INSERT INTO `app_city` VALUES ('259', 'Kota Kediri', '15');
INSERT INTO `app_city` VALUES ('260', 'Kota Madiun', '15');
INSERT INTO `app_city` VALUES ('261', 'Kota Malang', '15');
INSERT INTO `app_city` VALUES ('262', 'Kota Mojokerto', '15');
INSERT INTO `app_city` VALUES ('263', 'Kota Pasuruan', '15');
INSERT INTO `app_city` VALUES ('264', 'Kota Probolinggo', '15');
INSERT INTO `app_city` VALUES ('265', 'Kota Surabaya', '15');
INSERT INTO `app_city` VALUES ('266', 'Kabupaten Bantul', '16');
INSERT INTO `app_city` VALUES ('267', 'Kabupaten Gunung Kidul', '16');
INSERT INTO `app_city` VALUES ('268', 'Kabupaten Kulon Progo', '16');
INSERT INTO `app_city` VALUES ('269', 'Kabupaten Sleman', '16');
INSERT INTO `app_city` VALUES ('270', 'Kota Yogyakarta', '16');
INSERT INTO `app_city` VALUES ('271', 'Kabupaten Badung', '17');
INSERT INTO `app_city` VALUES ('272', 'Kabupaten Bangli', '17');
INSERT INTO `app_city` VALUES ('273', 'Kabupaten Buleleng', '17');
INSERT INTO `app_city` VALUES ('274', 'Kabupaten Gianyar', '17');
INSERT INTO `app_city` VALUES ('275', 'Kabupaten Jembrana', '17');
INSERT INTO `app_city` VALUES ('276', 'Kabupaten Karangasem', '17');
INSERT INTO `app_city` VALUES ('277', 'Kabupaten Klungkung', '17');
INSERT INTO `app_city` VALUES ('278', 'Kabupaten Tabanan', '17');
INSERT INTO `app_city` VALUES ('279', 'Kota Denpasar', '17');
INSERT INTO `app_city` VALUES ('280', 'Kabupaten Bima', '18');
INSERT INTO `app_city` VALUES ('281', 'Kabupaten Dompu', '18');
INSERT INTO `app_city` VALUES ('282', 'Kabupaten Lombok Barat', '18');
INSERT INTO `app_city` VALUES ('283', 'Kabupaten Lombok Tengah', '18');
INSERT INTO `app_city` VALUES ('284', 'Kabupaten Lombok Timur', '18');
INSERT INTO `app_city` VALUES ('285', 'Kabupaten Lombok Utara', '18');
INSERT INTO `app_city` VALUES ('286', 'Kabupaten Sumbawa', '18');
INSERT INTO `app_city` VALUES ('287', 'Kabupaten Sumbawa Barat', '18');
INSERT INTO `app_city` VALUES ('288', 'Kota Bima', '18');
INSERT INTO `app_city` VALUES ('289', 'Kota Mataram', '18');
INSERT INTO `app_city` VALUES ('290', 'Kabupaten Kupang', '19');
INSERT INTO `app_city` VALUES ('291', 'Kabupaten Timor Tengah Selatan', '19');
INSERT INTO `app_city` VALUES ('292', 'Kabupaten Timor Tengah Utara', '19');
INSERT INTO `app_city` VALUES ('293', 'Kabupaten Belu', '19');
INSERT INTO `app_city` VALUES ('294', 'Kabupaten Alor', '19');
INSERT INTO `app_city` VALUES ('295', 'Kabupaten Flores Timur', '19');
INSERT INTO `app_city` VALUES ('296', 'Kabupaten Sikka', '19');
INSERT INTO `app_city` VALUES ('297', 'Kabupaten Ende', '19');
INSERT INTO `app_city` VALUES ('298', 'Kabupaten Ngada', '19');
INSERT INTO `app_city` VALUES ('299', 'Kabupaten Manggarai', '19');
INSERT INTO `app_city` VALUES ('300', 'Kabupaten Sumba Timur', '19');
INSERT INTO `app_city` VALUES ('301', 'Kabupaten Sumba Barat', '19');
INSERT INTO `app_city` VALUES ('302', 'Kabupaten Lembata', '19');
INSERT INTO `app_city` VALUES ('303', 'Kabupaten Rote Ndao', '19');
INSERT INTO `app_city` VALUES ('304', 'Kabupaten Manggarai Barat', '19');
INSERT INTO `app_city` VALUES ('305', 'Kabupaten Nagekeo', '19');
INSERT INTO `app_city` VALUES ('306', 'Kabupaten Sumba Tengah', '19');
INSERT INTO `app_city` VALUES ('307', 'Kabupaten Sumba Barat Daya', '19');
INSERT INTO `app_city` VALUES ('308', 'Kabupaten Manggarai Timur', '19');
INSERT INTO `app_city` VALUES ('309', 'Kabupaten Sabu Raijua', '19');
INSERT INTO `app_city` VALUES ('310', 'Kota Kupang', '19');
INSERT INTO `app_city` VALUES ('311', 'Kabupaten Bengkayang', '20');
INSERT INTO `app_city` VALUES ('312', 'Kabupaten Kapuas Hulu', '20');
INSERT INTO `app_city` VALUES ('313', 'Kabupaten Kayong Utara', '20');
INSERT INTO `app_city` VALUES ('314', 'Kabupaten Ketapang', '20');
INSERT INTO `app_city` VALUES ('315', 'Kabupaten Kubu Raya', '20');
INSERT INTO `app_city` VALUES ('316', 'Kabupaten Landak', '20');
INSERT INTO `app_city` VALUES ('317', 'Kabupaten Melawi', '20');
INSERT INTO `app_city` VALUES ('318', 'Kabupaten Pontianak', '20');
INSERT INTO `app_city` VALUES ('319', 'Kabupaten Sambas', '20');
INSERT INTO `app_city` VALUES ('320', 'Kabupaten Sanggau', '20');
INSERT INTO `app_city` VALUES ('321', 'Kabupaten Sekadau', '20');
INSERT INTO `app_city` VALUES ('322', 'Kabupaten Sintang', '20');
INSERT INTO `app_city` VALUES ('323', 'Kota Pontianak', '20');
INSERT INTO `app_city` VALUES ('324', 'Kota Singkawang', '20');
INSERT INTO `app_city` VALUES ('325', 'Kabupaten Balangan', '21');
INSERT INTO `app_city` VALUES ('326', 'Kabupaten Banjar', '21');
INSERT INTO `app_city` VALUES ('327', 'Kabupaten Barito Kuala', '21');
INSERT INTO `app_city` VALUES ('328', 'Kabupaten Hulu Sungai Selatan', '21');
INSERT INTO `app_city` VALUES ('329', 'Kabupaten Hulu Sungai Tengah', '21');
INSERT INTO `app_city` VALUES ('330', 'Kabupaten Hulu Sungai Utara', '21');
INSERT INTO `app_city` VALUES ('331', 'Kabupaten Kotabaru', '21');
INSERT INTO `app_city` VALUES ('332', 'Kabupaten Tabalong', '21');
INSERT INTO `app_city` VALUES ('333', 'Kabupaten Tanah Bumbu', '21');
INSERT INTO `app_city` VALUES ('334', 'Kabupaten Tanah Laut', '21');
INSERT INTO `app_city` VALUES ('335', 'Kabupaten Tapin', '21');
INSERT INTO `app_city` VALUES ('336', 'Kota Banjarbaru', '21');
INSERT INTO `app_city` VALUES ('337', 'Kota Banjarmasin', '21');
INSERT INTO `app_city` VALUES ('338', 'Kabupaten Barito Selatan', '22');
INSERT INTO `app_city` VALUES ('339', 'Kabupaten Barito Timur', '22');
INSERT INTO `app_city` VALUES ('340', 'Kabupaten Barito Utara', '22');
INSERT INTO `app_city` VALUES ('341', 'Kabupaten Gunung Mas', '22');
INSERT INTO `app_city` VALUES ('342', 'Kabupaten Kapuas', '22');
INSERT INTO `app_city` VALUES ('343', 'Kabupaten Katingan', '22');
INSERT INTO `app_city` VALUES ('344', 'Kabupaten Kotawaringin Barat', '22');
INSERT INTO `app_city` VALUES ('345', 'Kabupaten Kotawaringin Timur', '22');
INSERT INTO `app_city` VALUES ('346', 'Kabupaten Lamandau', '22');
INSERT INTO `app_city` VALUES ('347', 'Kabupaten Murung Raya', '22');
INSERT INTO `app_city` VALUES ('348', 'Kabupaten Pulang Pisau', '22');
INSERT INTO `app_city` VALUES ('349', 'Kabupaten Sukamara', '22');
INSERT INTO `app_city` VALUES ('350', 'Kabupaten Seruyan', '22');
INSERT INTO `app_city` VALUES ('351', 'Kota Palangka Raya', '22');
INSERT INTO `app_city` VALUES ('352', 'Kabupaten Berau', '23');
INSERT INTO `app_city` VALUES ('353', 'Kabupaten Bulungan', '23');
INSERT INTO `app_city` VALUES ('354', 'Kabupaten Kutai Barat', '23');
INSERT INTO `app_city` VALUES ('355', 'Kabupaten Kutai Kartanegara', '23');
INSERT INTO `app_city` VALUES ('356', 'Kabupaten Kutai Timur', '23');
INSERT INTO `app_city` VALUES ('357', 'Kabupaten Malinau', '23');
INSERT INTO `app_city` VALUES ('358', 'Kabupaten Nunukan', '23');
INSERT INTO `app_city` VALUES ('359', 'Kabupaten Paser', '23');
INSERT INTO `app_city` VALUES ('360', 'Kabupaten Penajam Paser Utara', '23');
INSERT INTO `app_city` VALUES ('361', 'Kabupaten Tana Tidung', '23');
INSERT INTO `app_city` VALUES ('362', 'Kota Balikpapan', '23');
INSERT INTO `app_city` VALUES ('363', 'Kota Bontang', '23');
INSERT INTO `app_city` VALUES ('364', 'Kota Samarinda', '23');
INSERT INTO `app_city` VALUES ('365', 'Kota Tarakan', '23');
INSERT INTO `app_city` VALUES ('366', 'Kabupaten Boalemo', '24');
INSERT INTO `app_city` VALUES ('367', 'Kabupaten Bone Bolango', '24');
INSERT INTO `app_city` VALUES ('368', 'Kabupaten Gorontalo', '24');
INSERT INTO `app_city` VALUES ('369', 'Kabupaten Gorontalo Utara', '24');
INSERT INTO `app_city` VALUES ('370', 'Kabupaten Pohuwato', '24');
INSERT INTO `app_city` VALUES ('371', 'Kota Gorontalo', '24');
INSERT INTO `app_city` VALUES ('372', 'Kabupaten Bantaeng', '25');
INSERT INTO `app_city` VALUES ('373', 'Kabupaten Barru', '25');
INSERT INTO `app_city` VALUES ('374', 'Kabupaten Bone', '25');
INSERT INTO `app_city` VALUES ('375', 'Kabupaten Bulukumba', '25');
INSERT INTO `app_city` VALUES ('376', 'Kabupaten Enrekang', '25');
INSERT INTO `app_city` VALUES ('377', 'Kabupaten Gowa', '25');
INSERT INTO `app_city` VALUES ('378', 'Kabupaten Jeneponto', '25');
INSERT INTO `app_city` VALUES ('379', 'Kabupaten Kepulauan Selayar', '25');
INSERT INTO `app_city` VALUES ('380', 'Kabupaten Luwu', '25');
INSERT INTO `app_city` VALUES ('381', 'Kabupaten Luwu Timur', '25');
INSERT INTO `app_city` VALUES ('382', 'Kabupaten Luwu Utara', '25');
INSERT INTO `app_city` VALUES ('383', 'Kabupaten Maros', '25');
INSERT INTO `app_city` VALUES ('384', 'Kabupaten Pangkajene dan Kepulauan', '25');
INSERT INTO `app_city` VALUES ('385', 'Kabupaten Pinrang', '25');
INSERT INTO `app_city` VALUES ('386', 'Kabupaten Sidenreng Rappang', '25');
INSERT INTO `app_city` VALUES ('387', 'Kabupaten Sinjai', '25');
INSERT INTO `app_city` VALUES ('388', 'Kabupaten Soppeng', '25');
INSERT INTO `app_city` VALUES ('389', 'Kabupaten Takalar', '25');
INSERT INTO `app_city` VALUES ('390', 'Kabupaten Tana Toraja', '25');
INSERT INTO `app_city` VALUES ('391', 'Kabupaten Toraja Utara', '25');
INSERT INTO `app_city` VALUES ('392', 'Kabupaten Wajo', '25');
INSERT INTO `app_city` VALUES ('393', 'Kota Makassar', '25');
INSERT INTO `app_city` VALUES ('394', 'Kota Palopo', '25');
INSERT INTO `app_city` VALUES ('395', 'Kota Parepare', '25');
INSERT INTO `app_city` VALUES ('396', 'Kabupaten Bombana', '26');
INSERT INTO `app_city` VALUES ('397', 'Kabupaten Buton', '26');
INSERT INTO `app_city` VALUES ('398', 'Kabupaten Buton Utara', '26');
INSERT INTO `app_city` VALUES ('399', 'Kabupaten Kolaka', '26');
INSERT INTO `app_city` VALUES ('400', 'Kabupaten Kolaka Utara', '26');
INSERT INTO `app_city` VALUES ('401', 'Kabupaten Konawe', '26');
INSERT INTO `app_city` VALUES ('402', 'Kabupaten Konawe Selatan', '26');
INSERT INTO `app_city` VALUES ('403', 'Kabupaten Konawe Utara', '26');
INSERT INTO `app_city` VALUES ('404', 'Kabupaten Muna', '26');
INSERT INTO `app_city` VALUES ('405', 'Kabupaten Wakatobi', '26');
INSERT INTO `app_city` VALUES ('406', 'Kota Bau-Bau', '26');
INSERT INTO `app_city` VALUES ('407', 'Kota Kendari', '26');
INSERT INTO `app_city` VALUES ('408', 'Kabupaten Banggai', '27');
INSERT INTO `app_city` VALUES ('409', 'Kabupaten Banggai Kepulauan', '27');
INSERT INTO `app_city` VALUES ('410', 'Kabupaten Buol', '27');
INSERT INTO `app_city` VALUES ('411', 'Kabupaten Donggala', '27');
INSERT INTO `app_city` VALUES ('412', 'Kabupaten Morowali', '27');
INSERT INTO `app_city` VALUES ('413', 'Kabupaten Parigi Moutong', '27');
INSERT INTO `app_city` VALUES ('414', 'Kabupaten Poso', '27');
INSERT INTO `app_city` VALUES ('415', 'Kabupaten Tojo Una-Una', '27');
INSERT INTO `app_city` VALUES ('416', 'Kabupaten Toli-Toli', '27');
INSERT INTO `app_city` VALUES ('417', 'Kabupaten Sigi', '27');
INSERT INTO `app_city` VALUES ('418', 'Kota Palu', '27');
INSERT INTO `app_city` VALUES ('419', 'Kabupaten Bolaang Mongondow', '28');
INSERT INTO `app_city` VALUES ('420', 'Kabupaten Bolaang Mongondow Selatan', '28');
INSERT INTO `app_city` VALUES ('421', 'Kabupaten Bolaang Mongondow Timur', '28');
INSERT INTO `app_city` VALUES ('422', 'Kabupaten Bolaang Mongondow Utara', '28');
INSERT INTO `app_city` VALUES ('423', 'Kabupaten Kepulauan Sangihe', '28');
INSERT INTO `app_city` VALUES ('424', 'Kabupaten Kepulauan Siau Tagulandang Biaro', '28');
INSERT INTO `app_city` VALUES ('425', 'Kabupaten Kepulauan Talaud', '28');
INSERT INTO `app_city` VALUES ('426', 'Kabupaten Minahasa', '28');
INSERT INTO `app_city` VALUES ('427', 'Kabupaten Minahasa Selatan', '28');
INSERT INTO `app_city` VALUES ('428', 'Kabupaten Minahasa Tenggara', '28');
INSERT INTO `app_city` VALUES ('429', 'Kabupaten Minahasa Utara', '28');
INSERT INTO `app_city` VALUES ('430', 'Kota Bitung', '28');
INSERT INTO `app_city` VALUES ('431', 'Kota Kotamobagu', '28');
INSERT INTO `app_city` VALUES ('432', 'Kota Manado', '28');
INSERT INTO `app_city` VALUES ('433', 'Kota Tomohon', '28');
INSERT INTO `app_city` VALUES ('434', 'Kabupaten Majene', '29');
INSERT INTO `app_city` VALUES ('435', 'Kabupaten Mamasa', '29');
INSERT INTO `app_city` VALUES ('436', 'Kabupaten Mamuju', '29');
INSERT INTO `app_city` VALUES ('437', 'Kabupaten Mamuju Utara', '29');
INSERT INTO `app_city` VALUES ('438', 'Kabupaten Polewali Mandar', '29');
INSERT INTO `app_city` VALUES ('439', 'Kabupaten Buru', '30');
INSERT INTO `app_city` VALUES ('440', 'Kabupaten Buru Selatan', '30');
INSERT INTO `app_city` VALUES ('441', 'Kabupaten Kepulauan Aru', '30');
INSERT INTO `app_city` VALUES ('442', 'Kabupaten Maluku Barat Daya', '30');
INSERT INTO `app_city` VALUES ('443', 'Kabupaten Maluku Tengah', '30');
INSERT INTO `app_city` VALUES ('444', 'Kabupaten Maluku Tenggara', '30');
INSERT INTO `app_city` VALUES ('445', 'Kabupaten Maluku Tenggara Barat', '30');
INSERT INTO `app_city` VALUES ('446', 'Kabupaten Seram Bagian Barat', '30');
INSERT INTO `app_city` VALUES ('447', 'Kabupaten Seram Bagian Timur', '30');
INSERT INTO `app_city` VALUES ('448', 'Kota Ambon', '30');
INSERT INTO `app_city` VALUES ('449', 'Kota Tual', '30');
INSERT INTO `app_city` VALUES ('450', 'Kabupaten Halmahera Barat', '31');
INSERT INTO `app_city` VALUES ('451', 'Kabupaten Halmahera Tengah', '31');
INSERT INTO `app_city` VALUES ('452', 'Kabupaten Halmahera Utara', '31');
INSERT INTO `app_city` VALUES ('453', 'Kabupaten Halmahera Selatan', '31');
INSERT INTO `app_city` VALUES ('454', 'Kabupaten Kepulauan Sula', '31');
INSERT INTO `app_city` VALUES ('455', 'Kabupaten Halmahera Timur', '31');
INSERT INTO `app_city` VALUES ('456', 'Kabupaten Pulau Morotai', '31');
INSERT INTO `app_city` VALUES ('457', 'Kota Ternate', '31');
INSERT INTO `app_city` VALUES ('458', 'Kota Tidore Kepulauan', '31');
INSERT INTO `app_city` VALUES ('459', 'Kabupaten Asmat', '32');
INSERT INTO `app_city` VALUES ('460', 'Kabupaten Biak Numfor', '32');
INSERT INTO `app_city` VALUES ('461', 'Kabupaten Boven Digoel', '32');
INSERT INTO `app_city` VALUES ('462', 'Kabupaten Deiyai', '32');
INSERT INTO `app_city` VALUES ('463', 'Kabupaten Dogiyai', '32');
INSERT INTO `app_city` VALUES ('464', 'Kabupaten Intan Jaya', '32');
INSERT INTO `app_city` VALUES ('465', 'Kabupaten Jayapura', '32');
INSERT INTO `app_city` VALUES ('466', 'Kabupaten Jayawijaya', '32');
INSERT INTO `app_city` VALUES ('467', 'Kabupaten Keerom', '32');
INSERT INTO `app_city` VALUES ('468', 'Kabupaten Kepulauan Yapen', '32');
INSERT INTO `app_city` VALUES ('469', 'Kabupaten Lanny Jaya', '32');
INSERT INTO `app_city` VALUES ('470', 'Kabupaten Mamberamo Raya', '32');
INSERT INTO `app_city` VALUES ('471', 'Kabupaten Mamberamo Tengah', '32');
INSERT INTO `app_city` VALUES ('472', 'Kabupaten Mappi', '32');
INSERT INTO `app_city` VALUES ('473', 'Kabupaten Merauke', '32');
INSERT INTO `app_city` VALUES ('474', 'Kabupaten Mimika', '32');
INSERT INTO `app_city` VALUES ('475', 'Kabupaten Nabire', '32');
INSERT INTO `app_city` VALUES ('476', 'Kabupaten Nduga', '32');
INSERT INTO `app_city` VALUES ('477', 'Kabupaten Paniai', '32');
INSERT INTO `app_city` VALUES ('478', 'Kabupaten Pegunungan Bintang', '32');
INSERT INTO `app_city` VALUES ('479', 'Kabupaten Puncak', '32');
INSERT INTO `app_city` VALUES ('480', 'Kabupaten Puncak Jaya', '32');
INSERT INTO `app_city` VALUES ('481', 'Kabupaten Sarmi', '32');
INSERT INTO `app_city` VALUES ('482', 'Kabupaten Supiori', '32');
INSERT INTO `app_city` VALUES ('483', 'Kabupaten Tolikara', '32');
INSERT INTO `app_city` VALUES ('484', 'Kabupaten Waropen', '32');
INSERT INTO `app_city` VALUES ('485', 'Kabupaten Yahukimo', '32');
INSERT INTO `app_city` VALUES ('486', 'Kabupaten Yalimo', '32');
INSERT INTO `app_city` VALUES ('487', 'Kota Jayapura', '32');
INSERT INTO `app_city` VALUES ('488', 'Kabupaten Fakfak', '33');
INSERT INTO `app_city` VALUES ('489', 'Kabupaten Kaimana', '33');
INSERT INTO `app_city` VALUES ('490', 'Kabupaten Manokwari', '33');
INSERT INTO `app_city` VALUES ('491', 'Kabupaten Maybrat', '33');
INSERT INTO `app_city` VALUES ('492', 'Kabupaten Raja Ampat', '33');
INSERT INTO `app_city` VALUES ('493', 'Kabupaten Sorong', '33');
INSERT INTO `app_city` VALUES ('494', 'Kabupaten Sorong Selatan', '33');
INSERT INTO `app_city` VALUES ('495', 'Kabupaten Tambrauw', '33');
INSERT INTO `app_city` VALUES ('496', 'Kabupaten Teluk Bintuni', '33');
INSERT INTO `app_city` VALUES ('497', 'Kabupaten Teluk Wondama', '33');
INSERT INTO `app_city` VALUES ('498', 'Kota Sorong', '33');

-- ----------------------------
-- Table structure for app_module
-- ----------------------------
DROP TABLE IF EXISTS `app_module`;
CREATE TABLE `app_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `module_order` int(10) DEFAULT NULL COMMENT 'Order Menu',
  `module_icon` varchar(255) DEFAULT NULL,
  `module_url` varchar(255) DEFAULT NULL COMMENT 'URL when click, if parent give it "#"',
  `isParent` int(1) DEFAULT NULL COMMENT '1 = "Is Parent" (If 1 it means add sub module), 0 = "No Sub Module"',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `module_status` tinyint(1) DEFAULT '1' COMMENT '1 = "Active", 0 = "Non Active"',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_module
-- ----------------------------
INSERT INTO `app_module` VALUES ('1', 'Tata Usaha', '1', 'fa fa-list-alt', '#', '1', '1', '2018-08-22 19:56:34', '1', '2018-08-23 10:12:13', '1', '0');
INSERT INTO `app_module` VALUES ('2', 'User', '99', 'fa fa-user', 'user', '0', '1', '2018-08-23 09:05:29', '1', '2018-08-26 22:48:22', '1', '0');
INSERT INTO `app_module` VALUES ('3', 'Settings', '100', 'fa fa-cog', '#', '1', '1', '2018-08-23 11:02:32', '1', '2018-08-24 09:38:30', '1', '0');
INSERT INTO `app_module` VALUES ('4', 'Group Module', '101', 'fa fa-anchor', 'groupmodule', '0', '1', '2018-08-23 11:03:25', '1', '2018-08-23 11:04:28', '1', '1');
INSERT INTO `app_module` VALUES ('5', 'Pelanggaran & Prestasi', '2', 'fa fa-anchor', '#', '1', '1', '2018-08-23 11:06:43', '1', '2018-08-26 22:51:32', '1', '0');
INSERT INTO `app_module` VALUES ('6', 'Kemanan', '3', 'fa fa-lock', '#', '1', '1', '2018-08-24 09:40:25', '1', '2018-08-26 22:49:21', '1', '0');
INSERT INTO `app_module` VALUES ('7', 'Transaksi', '4', 'fas fa-exchange-alt', '#', '1', '1', '2018-08-26 22:54:03', null, null, '1', '0');
INSERT INTO `app_module` VALUES ('8', 'Bendahara', '5', 'fa fa-money', 'bendahara', '0', '1', '2018-08-26 22:55:05', '1', '2018-08-26 22:56:27', '1', '0');

-- ----------------------------
-- Table structure for app_province
-- ----------------------------
DROP TABLE IF EXISTS `app_province`;
CREATE TABLE `app_province` (
  `province_id` int(2) NOT NULL,
  `province_name` varchar(50) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_province
-- ----------------------------
INSERT INTO `app_province` VALUES ('1', 'Aceh', '101');
INSERT INTO `app_province` VALUES ('2', 'Sumatera Utara', '101');
INSERT INTO `app_province` VALUES ('3', 'Bengkulu', '101');
INSERT INTO `app_province` VALUES ('4', 'Jambi', '101');
INSERT INTO `app_province` VALUES ('5', 'Riau', '101');
INSERT INTO `app_province` VALUES ('6', 'Sumatera Barat', '101');
INSERT INTO `app_province` VALUES ('7', 'Sumatera Selatan', '101');
INSERT INTO `app_province` VALUES ('8', 'Lampung', '101');
INSERT INTO `app_province` VALUES ('9', 'Kepulauan Bangka Belitung', '101');
INSERT INTO `app_province` VALUES ('10', 'Kepulauan Riau', '101');
INSERT INTO `app_province` VALUES ('11', 'Banten', '101');
INSERT INTO `app_province` VALUES ('12', 'Jawa Barat', '101');
INSERT INTO `app_province` VALUES ('13', 'DKI Jakarta', '101');
INSERT INTO `app_province` VALUES ('14', 'Jawa Tengah', '101');
INSERT INTO `app_province` VALUES ('15', 'Jawa Timur', '101');
INSERT INTO `app_province` VALUES ('16', 'Daerah Istimewa Yogyakarta', '101');
INSERT INTO `app_province` VALUES ('17', 'Bali', '101');
INSERT INTO `app_province` VALUES ('18', 'Nusa Tenggara Barat', '101');
INSERT INTO `app_province` VALUES ('19', 'Nusa Tenggara Timur', '101');
INSERT INTO `app_province` VALUES ('20', 'Kalimantan Barat', '101');
INSERT INTO `app_province` VALUES ('21', 'Kalimantan Selatan', '101');
INSERT INTO `app_province` VALUES ('22', 'Kalimantan Tengah', '101');
INSERT INTO `app_province` VALUES ('23', 'Kalimantan Timur', '101');
INSERT INTO `app_province` VALUES ('24', 'Gorontalo', '101');
INSERT INTO `app_province` VALUES ('25', 'Sulawesi Selatan', '101');
INSERT INTO `app_province` VALUES ('26', 'Sulawesi Tenggara', '101');
INSERT INTO `app_province` VALUES ('27', 'Sulawesi Tengah', '101');
INSERT INTO `app_province` VALUES ('28', 'Sulawesi Utara', '101');
INSERT INTO `app_province` VALUES ('29', 'Sulawesi Barat', '101');
INSERT INTO `app_province` VALUES ('30', 'Maluku', '101');
INSERT INTO `app_province` VALUES ('31', 'Maluku Utara', '101');
INSERT INTO `app_province` VALUES ('32', 'Papua', '101');
INSERT INTO `app_province` VALUES ('33', 'Papua Barat', '101');

-- ----------------------------
-- Table structure for app_submodule
-- ----------------------------
DROP TABLE IF EXISTS `app_submodule`;
CREATE TABLE `app_submodule` (
  `submodule_id` int(11) NOT NULL AUTO_INCREMENT,
  `submodule_name` varchar(255) DEFAULT NULL,
  `submodule_order` int(10) DEFAULT NULL COMMENT 'Order Menu',
  `submodule_url` varchar(255) DEFAULT NULL COMMENT 'URL when click',
  `module_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `submodule_status` tinyint(1) DEFAULT '1' COMMENT '1 = "Active", 0 = "Non Active"',
  `isDeleted` tinyint(1) NOT NULL COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`submodule_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_submodule
-- ----------------------------
INSERT INTO `app_submodule` VALUES ('1', 'Tahun Pelajaran', '1', 'tahunpelajaran', '1', '1', '2018-08-22 19:57:05', '1', '2018-08-22 22:15:48', '1', '0');
INSERT INTO `app_submodule` VALUES ('2', 'Kelas', '2', 'kelas', '1', '1', '2018-08-22 19:57:40', '1', '2018-08-22 23:08:58', '1', '0');
INSERT INTO `app_submodule` VALUES ('3', 'Santri', '3', 'santri', '1', '1', '2018-08-22 19:58:07', '1', '2018-08-23 09:53:16', '1', '0');
INSERT INTO `app_submodule` VALUES ('4', 'Asatidz', '4', 'asatidz', '1', '1', '2018-08-22 19:58:47', '1', '2018-08-23 10:36:54', '1', '0');
INSERT INTO `app_submodule` VALUES ('5', 'Module & Submodule', '1', 'module', '3', '1', '2018-08-23 11:05:06', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('6', 'Group Module', '2', 'groupmodule', '3', '1', '2018-08-23 11:05:22', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('7', 'Pelanggaran', '1', 'pelanggaran', '5', '1', '2018-08-23 11:06:58', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('8', 'Prestasi', '2', 'prestasi', '5', '1', '2018-08-23 11:07:14', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('9', 'Pelanggaran Santri', '3', 'pelanggaransantri', '5', '1', '2018-08-23 13:18:59', '1', '2018-08-23 13:20:53', '1', '0');
INSERT INTO `app_submodule` VALUES ('10', 'Perizinan Keluar', '1', 'perkeluar', '6', '1', '2018-08-24 09:43:45', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('11', 'Perizinan Pulang', '2', 'perpulang', '6', '1', '2018-08-24 09:44:08', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('12', 'Pembagian Kelas', '5', 'pembagiankelas', '1', '1', '2018-08-24 10:22:05', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('13', 'Prestasi Siswa', '8', 'prestasisiswa', '5', '1', '2018-08-24 17:12:50', null, null, '1', '0');
INSERT INTO `app_submodule` VALUES ('14', 'Posting Naik Kelas', '6', 'penaikankelas', '1', '1', '2018-08-24 21:34:17', null, null, '1', '0');

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_full_name` varchar(255) DEFAULT NULL,
  `user_username` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL COMMENT 'Password min 6 character',
  `access_group_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL COMMENT '1 = "Active", 2 = "Non Active"',
  `isDeleted` varchar(255) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('1', 'Wanda Riswanda', 'admin', 'admin@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '0', '2018-08-26 22:57:37');
INSERT INTO `app_user` VALUES ('2', 'Latif', 'bendahara', 'alatief432@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', '1', '1', '2018-08-21 14:25:31');
INSERT INTO `app_user` VALUES ('3', 'Wayang', 'Kesiswaan', 'wayang@gmail.com', '4297f44b13955235245b2497399d7a93', '2', '1', '0', '2018-08-24 10:16:31');
INSERT INTO `app_user` VALUES ('4', 'Nona Muda', 'nonamuda', 'nonamuda@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '1', '2018-08-21 15:19:49');
INSERT INTO `app_user` VALUES ('5', 'Nana', 'annana', 'nonamuda@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '1', '2018-08-21 15:20:31');
INSERT INTO `app_user` VALUES ('6', 'faf', 'dasfdf', 'alatief432@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '1', '2018-08-21 16:44:35');
INSERT INTO `app_user` VALUES ('7', 'werfwerw', 'latif', 'latif@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '1', '2018-08-21 16:53:49');
INSERT INTO `app_user` VALUES ('8', 'zsetwe', 'latif12', 'naon@gmail.com', '4297f44b13955235245b2497399d7a93', '3', '1', '0', '2018-08-21 16:54:19');
INSERT INTO `app_user` VALUES ('9', 'srtyre', 'ertyrty', 'alatief43@gmail.com', '2f9ecf80b22d731110e785bc4cc4b2dc', '2', '1', '0', '2018-08-21 16:54:39');
INSERT INTO `app_user` VALUES ('10', 'dewrwr', 'latif45453', 'alatief43254@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '0', '2018-08-21 16:58:50');
INSERT INTO `app_user` VALUES ('11', 'ertwtyter', 'latif', 'werewr@gmail.com', '4297f44b13955235245b2497399d7a93', '1', '1', '0', '2018-08-21 17:08:27');
INSERT INTO `app_user` VALUES ('12', 'fgsdfdsf', 'sadfsdf', 'sdfgdfg@gmail.com', '20838a8df7cc0babd745c7af4b7d94e2', '1', '1', '0', '2018-08-21 17:19:53');
INSERT INTO `app_user` VALUES ('13', 'fsdhfdsf', 'edfdfsfesfe', 'ksjbvfhsdfdv@gmail.com', '2f8742e8080fdb525f62d4f52ea6528d', '1', '1', '0', '2018-08-21 17:23:30');
INSERT INTO `app_user` VALUES ('14', null, null, null, 'd41d8cd98f00b204e9800998ecf8427e', null, '1', '0', '2018-08-23 11:30:24');

-- ----------------------------
-- Table structure for asatidz
-- ----------------------------
DROP TABLE IF EXISTS `asatidz`;
CREATE TABLE `asatidz` (
  `asatidz_id` int(11) NOT NULL AUTO_INCREMENT,
  `asatidz_no_induk` varchar(255) DEFAULT NULL,
  `asatidz_nuptk` varchar(255) DEFAULT NULL,
  `asatidz_name` varchar(255) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `asatidz_address` varchar(255) DEFAULT NULL,
  `asatidz_phone` varchar(255) DEFAULT NULL,
  `asatidz_tmt` varchar(255) DEFAULT NULL,
  `asatidz_lama_pengambian` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1 = aktif, 0 = tidak aktif',
  `isDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`asatidz_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of asatidz
-- ----------------------------
INSERT INTO `asatidz` VALUES ('1', '11304188', '11605522', 'Wanda Riswanda', 'Bogor', '1997-05-26', 'Jl. Raya Puncak', '0895396584306', '2017', '1', 'images/asatidz/AST-18230801154397.png', '2018-08-23 11:35:15', '1', '2018-08-23 13:18:52', '1', '1', '0');
INSERT INTO `asatidz` VALUES ('6', '1140001', '1140001', 'ucok', 'Bogor', '1997-05-26', 'Jl. Raya Puncak', '089516500999', '2017', '1', 'images/img/default.png', '2018-08-23 14:42:21', '1', null, null, '1', '0');
INSERT INTO `asatidz` VALUES ('7', '1140002', '1140002', 'ucok', 'Bogor', '1997-05-26', 'Jl. Raya Puncak', '089516500598', '2017', '1', 'images/img/default.png', '2018-08-23 14:42:21', '1', null, null, '1', '0');

-- ----------------------------
-- Table structure for izin_keluar
-- ----------------------------
DROP TABLE IF EXISTS `izin_keluar`;
CREATE TABLE `izin_keluar` (
  `izin_keluar_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_id` int(11) DEFAULT NULL,
  `keperluan` text,
  `description` text,
  `time_limit` datetime DEFAULT NULL COMMENT 'Kapan siswa harus kembali, di tentukan manual',
  `return_date` datetime DEFAULT NULL COMMENT 'Isi waktu siswa kembali',
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `isDeleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`izin_keluar_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of izin_keluar
-- ----------------------------
INSERT INTO `izin_keluar` VALUES ('1', '49', 'Beli Kecap Baru', null, '2018-08-24 12:59:00', '2018-08-24 16:08:00', '2018-08-24 15:06:39', '1', '2018-08-24 16:08:40', '1', '1', '0');
INSERT INTO `izin_keluar` VALUES ('3', '50', 'Beli Sarung', null, '2018-08-24 11:50:00', null, '2018-08-24 11:50:51', '1', null, null, '1', '1');
INSERT INTO `izin_keluar` VALUES ('4', '50', 'jajan Bakso', null, '2018-08-24 16:30:00', '2018-08-24 16:42:00', '2018-08-24 16:10:50', '1', '2018-08-24 16:16:04', '1', '1', '0');

-- ----------------------------
-- Table structure for izin_pulang
-- ----------------------------
DROP TABLE IF EXISTS `izin_pulang`;
CREATE TABLE `izin_pulang` (
  `izin_pulang_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_id` int(11) DEFAULT NULL,
  `keperluan` text,
  `description` text,
  `time_limit` datetime DEFAULT NULL COMMENT 'Kapan siswa harus kembali, di tentukan manual',
  `return_date` datetime DEFAULT NULL COMMENT 'Isi waktu siswa kembali',
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`izin_pulang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of izin_pulang
-- ----------------------------

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`kelas_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES ('1', '1', '2018-08-22 23:15:15', '1', '2018-08-23 13:17:57', '1', '1', '0');
INSERT INTO `kelas` VALUES ('2', '2', '2018-08-22 23:22:39', '1', '2018-08-22 23:22:45', '1', '1', '0');

-- ----------------------------
-- Table structure for kelas_rombel
-- ----------------------------
DROP TABLE IF EXISTS `kelas_rombel`;
CREATE TABLE `kelas_rombel` (
  `kelas_rombel_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_id` int(11) DEFAULT NULL,
  `kelas_rombel_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1 = "Active", 0 = "Non Active"',
  `isDeleted` tinyint(255) DEFAULT NULL COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`kelas_rombel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelas_rombel
-- ----------------------------
INSERT INTO `kelas_rombel` VALUES ('1', '1', 'RPL 1', '2018-08-24 09:53:25', '1', null, null, '1', '0');
INSERT INTO `kelas_rombel` VALUES ('2', '1', 'RPL 2', '2018-08-24 09:56:18', '1', '2018-08-24 10:07:40', '1', '1', '0');

-- ----------------------------
-- Table structure for pelanggaran
-- ----------------------------
DROP TABLE IF EXISTS `pelanggaran`;
CREATE TABLE `pelanggaran` (
  `pelanggaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggaran_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pelanggaran_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelanggaran
-- ----------------------------
INSERT INTO `pelanggaran` VALUES ('1', 'Kabur', '2018-08-23 12:03:47', '1', '2018-08-23 12:05:30', '1', '1', '1');
INSERT INTO `pelanggaran` VALUES ('2', 'Bolos', '2018-08-23 12:05:56', '1', null, null, '1', '0');

-- ----------------------------
-- Table structure for pelanggaran_siswa
-- ----------------------------
DROP TABLE IF EXISTS `pelanggaran_siswa`;
CREATE TABLE `pelanggaran_siswa` (
  `pelanggaran_siswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggaran_id` int(11) DEFAULT NULL,
  `siswaId` int(11) DEFAULT NULL,
  `hukuman` text COMMENT 'Berbentuk json contoh {"Hukuman" : "Bersih bersih wc", "Status" : "1" dst} sesuai yang di butuhkan',
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pelanggaran_siswa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelanggaran_siswa
-- ----------------------------
INSERT INTO `pelanggaran_siswa` VALUES ('6', '2', '1', '{\"hukuman\":[\"Cuci gudang\",\"Cuci piring\",\"wfewrfe\"],\"status\":[\"masih berjalan\",\"selesai\"],\"Status\":[\"selesai\"]}', '2018-08-23 16:40:53', '1', '2018-08-23 18:03:55', '1');
INSERT INTO `pelanggaran_siswa` VALUES ('7', '2', '1', '{\"hukuman\":[\"Cuci piring\"],\"status\":[\"masih berjalan\"]}', '2018-08-23 17:14:38', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('5', '1', '1', '{\"hukuman\":[\"Cuci gudang\"],\"status\":[\"masih berjalan\"]}', '2018-08-23 16:30:31', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('8', '2', '50', '{\"hukuman\":[\"Cuci Muka\"],\"status\":[\"masih berjalan\"]}', '2018-08-24 15:18:04', '1', '2018-08-24 17:02:42', '1');
INSERT INTO `pelanggaran_siswa` VALUES ('9', '2', '50', '{\"hukuman\":[\"Cuci gudang\"],\"status\":[\"masih berjalan\"]}', '2018-08-24 16:04:50', '1', '2018-08-24 17:04:55', '1');
INSERT INTO `pelanggaran_siswa` VALUES ('10', '2', '50', '{\"hukuman\":[\"Cuci piring\"],\"status\":[\"masih berjalan\"]}', '2018-08-24 16:05:14', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('11', '2', '50', '{\"hukuman\":[\"Cuci piring\",\"Cuci gudang\",\"shretyery\",\"dtutyuityuitujfj\"],\"status\":[\"selesai\",\"masih berjalan\",\"selesai\",\"masih berjalan\"]}', '2018-08-24 16:22:09', '1', '2018-08-24 19:29:45', '1');
INSERT INTO `pelanggaran_siswa` VALUES ('12', '2', '50', '{\"hukuman\":[\"ertert\"],\"status\":[\"masih berjalan\"]}', '2018-08-24 16:47:42', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('21', '2', '52', 'null', '2018-08-25 11:45:36', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('22', '2', '52', '{\"hukuman\":[\"ertert\"],\"status\":[\"masih berjalan\"]}', '2018-08-25 11:46:08', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('23', '2', null, 'null', '2018-08-25 11:50:04', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('24', '2', null, 'null', '2018-08-25 11:50:37', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('25', '2', null, 'null', '2018-08-25 11:50:43', '1', null, null);
INSERT INTO `pelanggaran_siswa` VALUES ('26', '2', null, 'null', '2018-08-25 11:58:31', '1', null, null);

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `pembayaran_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pembayaran_title` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran_install_siswa
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_install_siswa`;
CREATE TABLE `pembayaran_install_siswa` (
  `pembayaran_id` int(11) NOT NULL,
  `siswa_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran_install_siswa
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran_istall_item
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_istall_item`;
CREATE TABLE `pembayaran_istall_item` (
  `pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembayaran_item_id` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran_istall_item
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran_item
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_item`;
CREATE TABLE `pembayaran_item` (
  `pembarayan_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pembarayan_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran_item
-- ----------------------------

-- ----------------------------
-- Table structure for prestasi
-- ----------------------------
DROP TABLE IF EXISTS `prestasi`;
CREATE TABLE `prestasi` (
  `prestasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `prestasi_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`prestasi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prestasi
-- ----------------------------
INSERT INTO `prestasi` VALUES ('1', 'kESENIAN', '2018-08-23 11:30:57', '1', '2018-08-23 11:42:50', '1', '1', '1');
INSERT INTO `prestasi` VALUES ('2', 'Olahraga', '2018-08-23 11:46:44', '1', null, null, '1', '0');
INSERT INTO `prestasi` VALUES ('3', 'kESENIANwerwe', '2018-08-23 13:03:14', '1', '2018-08-23 13:03:22', '1', '1', '1');

-- ----------------------------
-- Table structure for prestasi_siswa
-- ----------------------------
DROP TABLE IF EXISTS `prestasi_siswa`;
CREATE TABLE `prestasi_siswa` (
  `prestasi_siswa_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `siswaId` bigint(20) DEFAULT NULL,
  `prestasi_id` int(11) DEFAULT NULL,
  `prestasi_date_achive` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`prestasi_siswa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prestasi_siswa
-- ----------------------------
INSERT INTO `prestasi_siswa` VALUES ('1', '49', '2', '0000-00-00 00:00:00', '2018-08-25 10:17:34', '1', '2018-08-25 11:08:09', '1', '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('2', '1', '2', '0000-00-00 00:00:00', '2018-08-25 11:04:28', '1', '2018-08-25 11:07:41', '1', '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('3', '1', '2', '0000-00-00 00:00:00', '2018-08-25 11:05:19', '1', '2018-08-25 11:07:29', '1', '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('4', '49', '2', '0000-00-00 00:00:00', '2018-08-25 11:14:27', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('5', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:24:48', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('6', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:25:24', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('7', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:26:05', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('8', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:26:05', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('9', '1', '2', '0000-00-00 00:00:00', '2018-08-25 11:26:10', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('10', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:26:51', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('11', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:27:06', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('12', '1', '2', '0000-00-00 00:00:00', '2018-08-25 11:27:51', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('13', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:28:02', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('14', '0', '2', '0000-00-00 00:00:00', '2018-08-25 11:28:27', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('15', '50', '2', '0000-00-00 00:00:00', '2018-08-25 12:17:27', '1', null, null, '1', '0');
INSERT INTO `prestasi_siswa` VALUES ('16', '1', '2', '0000-00-00 00:00:00', '2018-08-25 12:17:44', '1', null, null, '1', '0');

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_name` varchar(255) DEFAULT NULL,
  `siswa_nomor_induk` varchar(255) DEFAULT NULL,
  `siswa_nisn` varchar(255) DEFAULT NULL,
  `siswa_tempat_lahir` varchar(255) DEFAULT NULL,
  `siswa_tanggal_lahir` date DEFAULT NULL,
  `siswa_no_hp` varchar(25) DEFAULT NULL,
  `siswa_alamat` varchar(255) DEFAULT NULL,
  `siswa_province_id` int(11) DEFAULT NULL,
  `siswa_kota_id` int(11) DEFAULT NULL,
  `siswa_kecamatan` varchar(255) DEFAULT NULL,
  `siswa_kelurahan` varchar(255) DEFAULT NULL,
  `siswa_additional` text COMMENT 'Data berbentuk json {"Nama Ayah" : "Contoh 1", "Pendidikan" : "S1" dst}',
  `photo` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `is_graduate` tinyint(4) DEFAULT NULL COMMENT '0 = "Still studen", 1 ="Graduated"',
  `year_graduate` year(4) DEFAULT NULL,
  `date_graduate` datetime DEFAULT NULL,
  `graduate_tahun_pelajaran_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1 = "Active", 0 = "Mutasi"',
  `isDeleted` tinyint(4) DEFAULT NULL COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`siswa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa
-- ----------------------------
INSERT INTO `siswa` VALUES ('1', 'asep', '1234', '1234', 'bogor', '2001-08-09', '089516500999', 'bogor', '12', '50', '111', '1111', 'indonesia', null, '2018-08-23 13:46:03', '1', null, null, '0', null, null, null, '1', '0');
INSERT INTO `siswa` VALUES ('2', 'Wanda Riswanda', '11304188', '11505522', 'Bogor', '1997-05-26', '0895396584306', 'Jl. Raya Puncak', '12', '181', 'Cisarua', 'Bogor', '{\"nama_ayah\":\"Arief\",\"pekerjaan_ayah\":\"Wiraswasta\",\"pendidikan_ayah\":\"SLTA\",\"nama_ibu\":\"Hayat\",\"pekerjaan_ibu\":\"IRT\",\"pendidikan_ibu\":\"SLTA\",\"kontak_ortu\":\"089516500899\",\"alamat_ortu\":\"Jl. Raya Puncak\",\"nama_wali\":\"Wawan\",\"kontak_wali\":\"089516500989\",\"alamat_wali\":\"Jl. Raya Puncak\"}', 'images/santri/SNT-18230806005186.png', '2018-08-23 17:18:28', '1', '2018-08-23 18:19:49', '1', '0', null, null, null, '1', '0');
INSERT INTO `siswa` VALUES ('49', 'Ndaradiansyah', '11304101', '10140311', 'Bogor', '1997-05-26', '089516500999', 'Jl. Raya Puncak', '12', '181', 'Cisarua', 'Citeko', '{\"nama_ayah\":\"Arief\",\"pekerjaan_ayah\":\"Pekerjaan Ayah\",\"pendidikan_ayah\":\"Pendidikan Ayah\",\"nama_ibu\":\"ibu\",\"pekerjaan_ibu\":\"Pekerjaan Ibu\",\"pendidikan_ibu\":\"Pendidikan Ibu\",\"kontak_ortu\":\"0895163736\",\"alamat_ortu\":\"Alamat Ortu\",\"nama_wali\":\"Nama Wali\",\"kontak_wali\":\"089518343\",\"alamat_wali\":\"Alamat Wali\"}', 'images/img/default.png', '2018-08-24 08:40:35', '1', null, null, '0', null, null, null, '1', '0');
INSERT INTO `siswa` VALUES ('50', 'Wayang', '11304102', '10140312', 'Bogor', '1997-05-26', '089516500999', 'Jl. Raya Puncak', '12', '181', 'Cisarua', 'Citeko', '{\"nama_ayah\":\"Dhimas\",\"pekerjaan_ayah\":\"Pekerjaan Ayah\",\"pendidikan_ayah\":\"Pendidikan Ayah\",\"nama_ibu\":\"ibu\",\"pekerjaan_ibu\":\"Pekerjaan Ibu\",\"pendidikan_ibu\":\"Pendidikan Ibu\",\"kontak_ortu\":\"0895163736\",\"alamat_ortu\":\"Alamat Ortu\",\"nama_wali\":\"Nama Wali\",\"kontak_wali\":\"089518343\",\"alamat_wali\":\"Alamat Wali\"}', 'images/img/default.png', '2018-08-24 08:40:35', '1', null, null, '0', null, null, null, '1', '0');
INSERT INTO `siswa` VALUES ('52', 'Rahmat Fathi', '999999', '999999', 'Jakarta', '1972-08-18', '0878776247634', 'Jl. Baru no 12', '13', '190', 'Kecamatan', 'Mana mana', '{\"nama_ayah\":\"Ayah 123\",\"pekerjaan_ayah\":\"Pegawai\",\"pendidikan_ayah\":\"S10\",\"nama_ibu\":\"Ibu 123\",\"pekerjaan_ibu\":\"IRT\",\"pendidikan_ibu\":\"S20\",\"kontak_ortu\":\"0857878\",\"alamat_ortu\":\"Jakarta\",\"nama_wali\":\"\",\"kontak_wali\":\"\",\"alamat_wali\":\"\"}', 'images/santri/', '2018-08-24 19:21:58', '1', null, null, '0', null, null, null, '1', '0');
INSERT INTO `siswa` VALUES ('53', 'Miftahuddin', '1801001', '0983567456', 'Nganjuk', '1994-08-03', '085790385206', 'Sumber', '15', '243', 'Sawahan', 'Sidorejo', '{\"nama_ayah\":\"Moh. Ilham Basori\",\"pekerjaan_ayah\":\"Guru\",\"pendidikan_ayah\":\"S1\",\"nama_ibu\":\"Wasilaturrohmah\",\"pekerjaan_ibu\":\"Tidak Bekerja\",\"pendidikan_ibu\":\"SMA\",\"kontak_ortu\":\"08175264323\",\"alamat_ortu\":\"Sumber\",\"nama_wali\":\"Moh. Ilham Basori\",\"kontak_wali\":\"08175264323\",\"alamat_wali\":\"Sumber\"}', 'images/santri/', '2018-08-25 19:48:10', '1', null, null, '0', null, null, null, '1', '0');

-- ----------------------------
-- Table structure for siswa_kelas
-- ----------------------------
DROP TABLE IF EXISTS `siswa_kelas`;
CREATE TABLE `siswa_kelas` (
  `siswa_kelas_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `siswa_id` bigint(11) DEFAULT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `kelas_rombel_id` bigint(11) DEFAULT NULL,
  `tahun_pelajaran_id` bigint(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(255) DEFAULT NULL,
  `isDeleted` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`siswa_kelas_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa_kelas
-- ----------------------------
INSERT INTO `siswa_kelas` VALUES ('1', '50', '1', '1', '1', '2018-08-24 21:31:13', '1', null, null, '1', '0');
INSERT INTO `siswa_kelas` VALUES ('2', '1', '1', '2', '1', '2018-08-25 09:42:07', '1', null, null, '1', '0');

-- ----------------------------
-- Table structure for siswa_kelas_history
-- ----------------------------
DROP TABLE IF EXISTS `siswa_kelas_history`;
CREATE TABLE `siswa_kelas_history` (
  `siswa_kelas_history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `siswa_id` bigint(11) NOT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `kelas_rombel_id` bigint(11) DEFAULT NULL,
  `tahun_pelajaran_id` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(255) DEFAULT NULL,
  `isDeleted` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`siswa_kelas_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa_kelas_history
-- ----------------------------
INSERT INTO `siswa_kelas_history` VALUES ('1', '50', '1', '1', '1', '2018-08-24 21:31:13', '1', null, null, '1', '0');
INSERT INTO `siswa_kelas_history` VALUES ('2', '1', '1', '2', '1', '2018-08-25 09:42:07', '1', null, null, '1', '0');

-- ----------------------------
-- Table structure for tahun_pelajaran
-- ----------------------------
DROP TABLE IF EXISTS `tahun_pelajaran`;
CREATE TABLE `tahun_pelajaran` (
  `tahun_pelajaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_pelajaran_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`tahun_pelajaran_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tahun_pelajaran
-- ----------------------------
INSERT INTO `tahun_pelajaran` VALUES ('1', '2017-2018', '2018-08-22 21:55:44', '1', '2018-08-22 22:43:36', '1', '1', '0');
INSERT INTO `tahun_pelajaran` VALUES ('2', '2018-2019', '2018-08-22 22:34:21', '1', null, null, '1', '0');
INSERT INTO `tahun_pelajaran` VALUES ('3', '2019-2020', '2018-08-22 22:44:14', '1', '2018-08-22 22:46:02', '1', '1', '0');

-- ----------------------------
-- Table structure for uang_kas
-- ----------------------------
DROP TABLE IF EXISTS `uang_kas`;
CREATE TABLE `uang_kas` (
  `kas_id` bigint(20) NOT NULL,
  `kas_name` varchar(255) DEFAULT NULL,
  `kas_type` varchar(4) DEFAULT NULL COMMENT 'IN = "Masuk", OUT = "Keluar"',
  `kas_from` varchar(255) DEFAULT NULL,
  `kas_transaction` decimal(10,2) DEFAULT NULL,
  `pembayaran_id` int(11) DEFAULT NULL,
  `pembayaran_siswa_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`kas_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uang_kas
-- ----------------------------
