/*
Navicat MySQL Data Transfer

Source Server         : SERVER_KPP
Source Server Version : 50723
Source Host           : operation.kpptechnology.co.id:3306
Source Database       : kpptech_pesantren

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2018-08-20 14:07:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_access_group
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group`;
CREATE TABLE `app_access_group` (
  `access_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_group_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `access_group_status` tinyint(1) DEFAULT NULL COMMENT '1 = "Active", 0 = "Non Active"',
  PRIMARY KEY (`access_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group
-- ----------------------------
INSERT INTO `app_access_group` VALUES ('1', 'Administrator', '0', '2018-08-20 10:32:26', null, null, '1');

-- ----------------------------
-- Table structure for app_access_group_modulDetail
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group_modulDetail`;
CREATE TABLE `app_access_group_modulDetail` (
  `access_group_id` int(11) NOT NULL,
  `access_module_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group_modulDetail
-- ----------------------------

-- ----------------------------
-- Table structure for app_access_group_submodule
-- ----------------------------
DROP TABLE IF EXISTS `app_access_group_submodule`;
CREATE TABLE `app_access_group_submodule` (
  `access_group_id` int(11) NOT NULL,
  `access_submodule_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_access_group_submodule
-- ----------------------------

-- ----------------------------
-- Table structure for app_module
-- ----------------------------
DROP TABLE IF EXISTS `app_module`;
CREATE TABLE `app_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `module_order` int(10) DEFAULT NULL COMMENT 'Order Menu',
  `module_icon` varchar(255) DEFAULT NULL,
  `module_url` varchar(255) DEFAULT NULL COMMENT 'URL when click, if parent give it "#"',
  `isParent` int(1) DEFAULT NULL COMMENT '1 = "Is Parent" (If 1 it means add sub module), 0 = "No Sub Module"',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `module_status` tinyint(1) DEFAULT '1' COMMENT '1 = "Active", 0 = "Non Active"',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_module
-- ----------------------------

-- ----------------------------
-- Table structure for app_submodule
-- ----------------------------
DROP TABLE IF EXISTS `app_submodule`;
CREATE TABLE `app_submodule` (
  `submodule_id` int(11) NOT NULL AUTO_INCREMENT,
  `submodule_name` varchar(255) DEFAULT NULL,
  `submodule_order` int(10) DEFAULT NULL COMMENT 'Order Menu',
  `submodule_url` varchar(255) DEFAULT NULL COMMENT 'URL when click',
  `module_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `submodule_status` tinyint(1) DEFAULT '1' COMMENT '1 = "Active", 0 = "Non Active"',
  `isDeleted` tinyint(1) NOT NULL COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`submodule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_submodule
-- ----------------------------

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_full_name` varchar(255) DEFAULT NULL,
  `user_username` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL COMMENT 'Password min 6 character',
  `access_group_id` int(11) DEFAULT NULL COMMENT '1 = "Active", 2 = "Non Active"',
  `status` varchar(255) DEFAULT NULL,
  `isDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_user
-- ----------------------------

-- ----------------------------
-- Table structure for astidz
-- ----------------------------
DROP TABLE IF EXISTS `astidz`;
CREATE TABLE `astidz` (
  `astidz_id` int(11) NOT NULL,
  `astidz_no_induk` varchar(255) DEFAULT NULL,
  `astidz_nuptk` varchar(255) DEFAULT NULL,
  `astidz_name` varchar(255) DEFAULT NULL,
  `astidz_address` varchar(255) DEFAULT NULL,
  `astidz_phone` varchar(255) DEFAULT NULL,
  `astidz_tmt` varchar(255) DEFAULT NULL,
  `astidz_lama_pengambian` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`astidz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of astidz
-- ----------------------------

-- ----------------------------
-- Table structure for izin_keluar
-- ----------------------------
DROP TABLE IF EXISTS `izin_keluar`;
CREATE TABLE `izin_keluar` (
  `izin_keluar_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_id` int(11) DEFAULT NULL,
  `keperluan` text,
  `description` text,
  `time_limit` datetime DEFAULT NULL COMMENT 'Kapan siswa harus kembali, di tentukan manual',
  `return_date` datetime DEFAULT NULL COMMENT 'Isi waktu siswa kembali',
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`izin_keluar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of izin_keluar
-- ----------------------------

-- ----------------------------
-- Table structure for izin_pulang
-- ----------------------------
DROP TABLE IF EXISTS `izin_pulang`;
CREATE TABLE `izin_pulang` (
  `izin_pulang_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_id` int(11) DEFAULT NULL,
  `keperluan` text,
  `description` text,
  `time_limit` datetime DEFAULT NULL COMMENT 'Kapan siswa harus kembali, di tentukan manual',
  `return_date` datetime DEFAULT NULL COMMENT 'Isi waktu siswa kembali',
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`izin_pulang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of izin_pulang
-- ----------------------------

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`kelas_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelas
-- ----------------------------

-- ----------------------------
-- Table structure for kelas_rombel
-- ----------------------------
DROP TABLE IF EXISTS `kelas_rombel`;
CREATE TABLE `kelas_rombel` (
  `kelas_rombel_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_id` int(11) DEFAULT NULL,
  `kelas_rombel_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1 = "Active", 0 = "Non Active"',
  `isDeleted` tinyint(255) DEFAULT NULL COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`kelas_rombel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelas_rombel
-- ----------------------------

-- ----------------------------
-- Table structure for pelanggaran
-- ----------------------------
DROP TABLE IF EXISTS `pelanggaran`;
CREATE TABLE `pelanggaran` (
  `pelanggaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggaran_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pelanggaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelanggaran
-- ----------------------------

-- ----------------------------
-- Table structure for pelanggaran_siswa
-- ----------------------------
DROP TABLE IF EXISTS `pelanggaran_siswa`;
CREATE TABLE `pelanggaran_siswa` (
  `pelanggaran_id` int(11) NOT NULL,
  `siswa_id` int(11) DEFAULT NULL,
  `hukuman` text COMMENT 'Berbentuk json contoh {"Hukuman" : "Bersih bersih wc", "Status" : "1" dst} sesuai yang di butuhkan',
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pelanggaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelanggaran_siswa
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `pembayaran_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pembayaran_title` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran_install_siswa
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_install_siswa`;
CREATE TABLE `pembayaran_install_siswa` (
  `pembayaran_id` int(11) NOT NULL,
  `siswa_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran_install_siswa
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran_istall_item
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_istall_item`;
CREATE TABLE `pembayaran_istall_item` (
  `pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembayaran_item_id` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran_istall_item
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran_item
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran_item`;
CREATE TABLE `pembayaran_item` (
  `pembarayan_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pembarayan_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran_item
-- ----------------------------

-- ----------------------------
-- Table structure for prestasi
-- ----------------------------
DROP TABLE IF EXISTS `prestasi`;
CREATE TABLE `prestasi` (
  `prestasi_id` int(11) NOT NULL,
  `prestasi_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`prestasi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prestasi
-- ----------------------------

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL,
  `siswa_nomor_induk` varchar(255) NOT NULL,
  `siswa_nisn` varchar(255) DEFAULT NULL,
  `siswa_tempat_lahir` varchar(255) DEFAULT NULL,
  `siswa_tanggal_lahir` date DEFAULT NULL,
  `siswa_alamat` varchar(255) DEFAULT NULL,
  `siswa_province_id` int(11) DEFAULT NULL,
  `siswa_kota_id` int(11) DEFAULT NULL,
  `siswa_kecamatan` varchar(255) DEFAULT NULL,
  `siswa_kelurahan` varchar(255) DEFAULT NULL,
  `siswa_additional` text COMMENT 'Data berbentuk json {"Nama Ayah" : "Contoh 1", "Pendidikan" : "S1" dst}',
  `photo` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `is_graduate` tinyint(4) DEFAULT NULL COMMENT '0 = "Still studen", 1 ="Graduated"',
  `year_graduate` year(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1 = "Active", 0 = "Mutasi"',
  `isDeleted` tinyint(4) DEFAULT NULL COMMENT '1 = "Deletede", 0 = "Active" (Remove from UI table if its 1)',
  PRIMARY KEY (`siswa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa
-- ----------------------------

-- ----------------------------
-- Table structure for siswa_kelas
-- ----------------------------
DROP TABLE IF EXISTS `siswa_kelas`;
CREATE TABLE `siswa_kelas` (
  `siswa_id` bigint(11) NOT NULL,
  `kelas_id` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(255) DEFAULT NULL,
  `isDeleted` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`siswa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa_kelas
-- ----------------------------

-- ----------------------------
-- Table structure for siswa_kelas_history
-- ----------------------------
DROP TABLE IF EXISTS `siswa_kelas_history`;
CREATE TABLE `siswa_kelas_history` (
  `siswa_id` bigint(11) NOT NULL,
  `kelas_id` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(255) DEFAULT NULL,
  `isDeleted` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`siswa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa_kelas_history
-- ----------------------------

-- ----------------------------
-- Table structure for tahun_pelajaran
-- ----------------------------
DROP TABLE IF EXISTS `tahun_pelajaran`;
CREATE TABLE `tahun_pelajaran` (
  `tahun_pelajaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_pelajaran_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`tahun_pelajaran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tahun_pelajaran
-- ----------------------------

-- ----------------------------
-- Table structure for uang_kas
-- ----------------------------
DROP TABLE IF EXISTS `uang_kas`;
CREATE TABLE `uang_kas` (
  `kas_id` bigint(20) NOT NULL,
  `kas_name` varchar(255) DEFAULT NULL,
  `kas_type` varchar(4) DEFAULT NULL COMMENT 'IN = "Masuk", OUT = "Keluar"',
  `kas_from` varchar(255) DEFAULT NULL,
  `pembayaran_id` int(11) DEFAULT NULL,
  `pembayaran_siswa_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`kas_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uang_kas
-- ----------------------------
