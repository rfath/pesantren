<!doctype html>
<html lang="en">

<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="" />
		<link rel="shortcut icon" href="<?= base_url('assets_ticketing/back_end/') ?>img/favicon.ico" />
		<title>Signin</title>
		
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
		
		<!-- Common CSS -->
		<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>fonts/icomoon/icomoon.css" />

		<!-- Mian and Login css -->
		<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>css/main.css" />

	</head>  

	<body class="login-bg">
			
		<div class="container">
			<div class="login-screen row align-items-center">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<?php echo return_custom_notif(); ?>
					<form action="<?= base_url("signin/doReset/"); ?>" method="post">
						<div class="login-container">
							<div class="row no-gutters">
								<div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
									<div class="login-box">
										<h4>Reset your password</h4>
										<br>
										<div class="input-group">
											<span class="input-group-addon" id="email"><i class="icon-account_circle"></i></span>
											<input type="text" class="form-control" placeholder="Email" aria-label="email" aria-describedby="email" name="email">
										</div>
										<div class="actions clearfix" >
											<button type="submit" class="btn btn-primary" style="width: 100%;">Reset Your password</button>
										</div>
										<hr>
										<div class="mt-4">
											<a href="<?= base_url('signin'); ?>" class="additional-link">Already have an account? <span>Login Now</span></a>
										</div>
										<div class="mt-4">
											<a href="<?= base_url(); ?>" class="additional-link" style="margin:0px;">Back to home</a>
										</div>
									</div>
								</div>
								<div class="col-xl-8 col-lg-7 col-md-6 col-sm-12">
									<div class="login-slider"></div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script src="<?= base_url("assets_ticketing/back_end/"); ?>js/jquery.js"></script>
		<script src="<?= base_url("assets_ticketing/back_end/"); ?>js/tether.min.js"></script>
		<script src="<?= base_url("assets_ticketing/back_end/"); ?>js/bootstrap.min.js"></script>
		<script src="<?= base_url("assets_ticketing/back_end/"); ?>vendor/notify/notify.js"></script>
		<script src="<?= base_url("assets_ticketing/back_end/"); ?>vendor/notify/notify-custom.js"></script>
		<footer class="main-footer no-bdr fixed-btm">
			<div class="container">
				
			</div>
		</footer>
	</body>
</html>