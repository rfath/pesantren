<style type="text/css">
	#customers {
	    
	    border-collapse: collapse;
	    width: 100%;
	}

	#customers td, #customers th {
	    border-top: 1px solid #ddd;
	    padding: 8px;
	}

	#customers tr:nth-child(even){background-color: #f2f2f2;}

	#customers th {
	    padding-top: 12px;
	    padding-bottom: 12px;
	    text-align: left;
	    background-color: #4CAF50;
	    color: white;
	}
</style>
<div style="background-color: #f6f6f6;padding: 30px; width: 100%; display: inline-block;">
	<div style="background-color: #fff;padding: 13px; width: 40%; display: inline-block; margin-left: 25%; margin-right: 30%;">
		<p> Hi <b><?php echo $detailOrder->member_name ?></b><br><br>
		Thank you for completed participant data<br><br>
		Your Registration Detail Below  <br><br>
		<table width="100%;" id="customers">
			<tr>
				<td>E-ticket Number</td>
				<td>:</td>
				<td><?php echo $detailOrder->eticket ?></td>
			</tr>
			<tr>
				<td>Order Number</td>
				<td>:</td>
				<td><?php echo orderNumberFormat($detailOrder->order_time, $detailOrder->order_id); ?></td>
			</tr>
			<tr>
				<td style="width: 40%;">Event Name</td>
				<td style="width: 2%;">:</td>
				<td><?php echo $detailOrder->event_name ?></td>
			</tr>
			<tr>
				<td>Participant Name</td>
				<td>:</td>
				<td><?php echo $detailOrder->member_name ?></td>
			</tr>
			<?php if($additional <> ""){ ?>
				<tr>
					<td>Additional Participant</td>
					<td>:</td>
					<td><?= $additional ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td>Total Seat</td>
				<td>:</td>
				<td><?= $seat ?></td>
			</tr>
			<tr>
				<td>Package</td>
				<td>:</td>
				<td><?= $detailOrder->package_name; ?></td>
			</tr>
			<tr>
				<td>Payment Type</td>
				<td>:</td>
				<td><?= $payment; ?></td>
			</tr>
			<tr>
				<td>Total Payment</td>
				<td>:</td>
				<td>Rp. <?= number_format($detailOrder->order_price) ?></td>
			</tr>
		</table>
		</p>
		
	</div>
</div>