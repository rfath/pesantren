<div class="blog single">
	<div class="blog-title">
		<h3>Order Payment</h3>
	</div>

	<div class="event-item">
		<div class="col-md-12">
			<?php echo flashdata_notif("is_success","Yes"); ?>
			<form method="POST" enctype="multipart/form-data" action="<?= base_url('event/uploadpayment/'.$id); ?>">
				<input type="hidden" name="param" value="<?= $id; ?>">

				<div class="form-group row">
					<label class="col-md-2">Order Number</label>
					<div class="col-md-10">
						<input type="text" class="form-control" value="<?= orderNumberFormat($dataOrder->order_time, $dataOrder->order_id); ?>" readonly="" required="">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-2">E-ticket Number</label>
					<div class="col-md-10">
						<input type="text" class="form-control" value="<?= $dataOrder->eticket; ?>" readonly="" required="">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-2">Total Price</label>
					<div class="col-md-10">
						<input type="text" class="form-control" value="<?= number_format($dataOrder->order_price); ?>" readonly="" required="">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-md-2">Confirmation File</label>
					<div class="col-md-10">
						<input type="file" class="form-control" value="" name="fileconfirm" required="">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-2">Remarks</label>
					<div class="col-md-10">
						<input type="text" class="form-control" name="remarks" value="<?= $dataOrder->payment_remarks; ?>" placeholder="Remarks">
					</div>
				</div>
				<hr>
				<div class="form-group row">
					<div class="col-md-2"></div>
					<div class="col-md-10">					
						<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Upload confirmation</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>