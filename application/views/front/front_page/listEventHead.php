<?php 
$dataPage = $this->frontpage->getDateEventHead();
?>

<?php
if (empty($dataPage)) { 
	?>
	<div class="item active" id="empty-event-main">
		<div id="empty-event-div">
			There's no event right now
		</div>
	</div>
<?php }else{ ?>
<?php
$no = 0;
foreach ($dataPage as $dpkey => $dpvalue) {
	$no++;
	if ($no == 1) {
		$active = "active";
	}else{
		$active = "";
	}
	$date  = date("d",strtotime($dpvalue->date_event));
	$month = date("M", strtotime($dpvalue->date_event));
	$fulldate  = date("Y-m-d", strtotime($dpvalue->date_event));

	$eventList = $this->frontpage->getListDataEventByDate($dpvalue->date_event);	
	?>
	<div class="item <?= $active ?>">
		<a href="#" class="opener"><strong><?= $date; ?></strong> <?= $month; ?></a>
		<?php foreach ($eventList as $ekey => $evalue) { ?>
			<?php if($ekey == 0){ ?>
			<div class="slide">
				<a href="<?= base_url('event/detail/'.$evalue->event_id); ?>">
					<span class="meta"><?= date("d M Y",strtotime($evalue->event_start_date))." - ".date("d M Y",strtotime($evalue->event_end_date)) ?>
					<strong class="title"><?= $evalue->event_name; ?></strong>
					<span class="place"><?= $evalue->place_name; ?></span>
				</a>
			</div>
			<?php }else{ ?>
			<div class="wrap_cont">
				<div class="wrap_cont_item">
					<?php foreach ($eventList as $skey => $svalue) { if ($skey <> 0) { ?>
						<ul>
							<li>
								<a href="<?= base_url('event/detail/'.$evalue->event_id); ?>">
									<div class="date">
										<?= "<span>".date("d M Y",strtotime($svalue->event_start_date))."</span> - <span>".date("d M Y",strtotime($evalue->event_end_date))."</span>" ?>
									</div>
									<?= $svalue->event_name; ?>
									<p><?= $svalue->place_name; ?></p>
								</a>
							</li>
						</ul>
					<?php } } ?>
					<a href="<?= base_url('listevent?date='.$fulldate); ?>" class="botton_cont_item">MORE EVENTS IN <?= $month." ".$date ?></a>
				</div>
			</div>
			<?php } ?>
		<?php } ?>
	</div>
	<?php
}
?>
<?php } ?>