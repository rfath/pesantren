<div class="blog single">
	<div class="blog-title">
		<h3>Event Registration</h3>
	</div>
	<div class="event-item">
		<?php if($isRegistered <> 0){ //Check if member alredy register (DISABLED FOR NOW) ?>
			<div class="col-md-12">
				<div class="alert alert-info">
				  <strong>Info!</strong> This member already registered in this event.
				</div>
			</div>
		<?php }else{ ?>
			<form method="POST" action="<?= base_url('event/doManualRegister/'); ?>">
				<div class="col-md-12">
					<div class="form-group row">
						<label class="col-md-2">Event Name</label>
						<div class="col-md-10">
							<input type="hidden" name="param" class="form-control" value="<?= $detailEvent->event_id; ?>" required="" readonly="">
							<input type="text" class="form-control" value="<?= $detailEvent->event_name; ?>" readonly="">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Name Participant</label>
						<div class="col-md-10">
							<input type="hidden" name="member_param" value="<?= $dataUser->member_id; ?>">
							<input type="text" class="form-control" value="<?= $dataUser->member_name; ?>" readonly="">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Name Participant</label>
						<div class="col-md-10">
							<input type="text" class="form-control" value="<?= $dataUser->member_name; ?>" readonly="">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Gender</label>
						<div class="col-md-10">
							<select name="gender" class="form-control" id="gender" required="">
								<option value="Male" <?= check_selected($dataUser->gender, "Male"); ?> >Male</option>
								<option value="Female" <?= check_selected($dataUser->gender, "Female"); ?> >Female</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Age</label>
						<div class="col-md-10">
							<input type="text" class="form-control" value="" name="age" required="" placeholder="Age">
						</div>
					</div>
					<div id="additionaldata">

					</div>
					<hr>
					<div class="form-group row">
						<label class="col-md-2"></label>
						<div class="col-md-10">
							<button type="button" class="btn btn-info btn-sm" id="additional"><i class="fa fa-plus"></i> Add Additional Participant</button>
						</div>
					</div>
					<hr>
					<div class="form-group row">
						<label class="col-md-2">Seat</label>
						<div class="col-md-10">
							<input type="text" class="form-control" value="1" id="qtyseta" readonly="">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Package</label>
						<div class="col-md-10">
							<select name="package" class="form-control" id="package" required="">
								<option value="">Chose Package</option>
								<?php foreach ($package as $pkkey => $pkvalue) { ?>
									<option value="<?= $pkvalue->event_package_id ?>" data-pr="<?= number_format($pkvalue->package_price); ?>"><?= $pkvalue->package_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2"></label>
						<div class="col-md-10">
							<span style="color: #e64614;" id="pricetext"></span>
							<span id="detailPackage"></span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Total Price</label>
						<div class="col-md-10">
							<input type="text" id="totalPriceInfo" class="form-control" readonly="">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-md-2">Payment</label>
						<div class="col-md-10">
							<select name="payment" class="form-control" id="payment" required="">
								<option value="">Chose Payment</option>
								<option value="1">Online Payment Channel</option>
								<?php if(!empty($offlineBank)){ ?>
									<option value="2">Manual Transfer</option>
								<?php } ?>
								<option value="3">Offline Payment (Cash)</option>
							</select>
						</div>
					</div>
					<div class="form-group row" id="bank" style="display: none;">
						<label class="col-md-2">Bank</label>
						<div class="col-md-10">
							<select name="bank" class="form-control">
								<?php foreach ($offlineBank as $ofkey => $ofvalue) { ?>
									<option value="<?php echo $ofvalue->name.'|'.$ofvalue->number.'|'.$ofvalue->accname ?>"><?= $ofvalue->name." (Acc Number : ".$ofvalue->number." , Acc Name : ".$ofvalue->accname.")" ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Remarks</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="remarks" placeholder="Remarks">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Terms & Condition</label>
						<div class="col-md-10">
							<textarea class="form-control" readonly=""><?= $detailEvent->tac; ?></textarea>
							<br>
							<input id="okgo" type="checkbox"> I've read terms and condition<br>
						</div>
					</div>
					<hr>
					<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<input type="hidden" id="number" value="1">
							<button type="submit" id="regbtn" class="btn btn-primary" style="display: none;">Register</button>
						</div>				
					</div>
				</div>
			</form>
		<?php } ?>
	</div>
</div>

<script type="text/javascript">
	$("#package").change(function(event) {
		/*var pr = $(this).find(':selected').data('pr');
		$("#pricetext").html("Rp. "+pr+"/Person");*/
		$("#detailPackage").html("<i>Getting Package Detail</i>");
		$.ajax({
			url: '<?= base_url('event/detailPackage/'); ?>'+$("#package").val(),
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#detailPackage").html(e);
			//====== START PROCCESS CALCULATION =====//
			proccessTotalPrice();
			//====== END PROCCESS CALCULATION =====//
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

	$("#additional").click(function(event) {
		var number = $("#number").val();
		var _html = '<div id="additional_pr'+number+'">\
			<hr>\
			<h5><button type="button" class="btn" onClick="removeAdd('+number+')"><i class="fa fa-times"></i></button> Additional Participant</h5>\
			<div class="form-group row">\
				<label class="col-md-2">Name</label>\
				<div class="col-md-10">\
					<input type="text" name="additionalp['+number+'][name]" class="form-control" placeholder="Name Additional Participant" required="">\
				</div>\
			</div>\
			<div id="additionaldata">\
				<div class="form-group row">\
					<label class="col-md-2">Email</label>\
					<div class="col-md-10">\
						<input type="text" name="additionalp['+number+'][email]" class="form-control" placeholder="Email Additional Participant" required="">\
					</div>\
				</div>\
			</div>\
			<div id="additionaldata">\
				<div class="form-group row">\
					<label class="col-md-2">Phone Number</label>\
					<div class="col-md-10">\
						<input type="text" name="additionalp['+number+'][phone]" class="form-control" placeholder="Phone Additional Participant" required="">\
					</div>\
				</div>\
			</div>\
			<div class="form-group row">\
				<label class="col-md-2">Gender</label>\
				<div class="col-md-10">\
					<select class="form-control" name="additionalp['+number+'][gender]">\
						<option value="Male">Male</option>\
						<option value="Female">Female</option>\
					</select>\
				</div>\
			</div>\
			<div id="additionaldata">\
				<div class="form-group row">\
					<label class="col-md-2">Age</label>\
					<div class="col-md-10">\
						<input type="text" name="additionalp['+number+'][age]" class="form-control" placeholder="Age Additional Participant" required="">\
					</div>\
				</div>\
			</div>\
		</div>';
		$("#additionaldata").append(_html);
		var number_add = parseInt(number) + 1;
		$("#number").val(number_add);
		//====== START QTY =====//
		var _qtyseat = $("#qtyseta").val();
		var _plusQty = parseInt(_qtyseat) + 1;
		$("#qtyseta").val(_plusQty);
		//====== END QTY =====//
		jcf.replaceAll();
		//====== START PROCCESS CALCULATION =====//
		proccessTotalPrice();
		//====== END PROCCESS CALCULATION =====//
	});

	function removeAdd(number){
		$("#additional_pr"+number).remove();
		//====== START QTY =====//
		var _qtyseat = $("#qtyseta").val();
		var _plusQty = parseInt(_qtyseat) - 1;
		$("#qtyseta").val(_plusQty);
		//====== END QTY =====//
		//====== START PROCCESS CALCULATION =====//
		proccessTotalPrice();
		//====== END PROCCESS CALCULATION =====//
	}

	function proccessTotalPrice(){
		var _qtyseat = $("#qtyseta").val();
		var _prc = $("#prc").val();
		var _total = $.number(parseInt(_qtyseat) * parseInt(_prc));
		$("#totalPriceInfo").val(_total);
	}
	$("#payment").change(function(event) {
		var _payval = $("#payment").val();
		if (_payval == "2") {
			$("#bank").fadeIn("fast", function() {});
		}else{
			$("#bank").fadeOut("fast", function() {});
		}
	});

	$("#okgo").change(function(event) {
		if ($('#okgo').is(':checked')) {
			$("#regbtn").fadeIn('fast', function() {});
		}else{
			$("#regbtn").fadeOut('fast', function() {});
		}
	});
</script>