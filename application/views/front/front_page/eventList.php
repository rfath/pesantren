<section class="calendar">
	<header>
		<h1>event calendar</h1>
		<!-- <div class="sort">
			<form method="get">
				<select name="date" class="location-select">
					<option value="">All Date</option>
				</select>
				<select name="location" class="location-select">
					<option value="">All Location</option>
				</select>
				<input class="location-select form-control col-md-3" name="searchkey" placeholder="Search Event">
				<button type="submit" class="btn btn-primary btn-sm" style="margin-left: 10px; margin-top: -2px;"><i class="fa fa-filter"></i> Filter</button>
			</form>
		</div> -->
	</header>
	<div class="body">
		<div id="resullist">
		<?php foreach ($list_event as $lekey => $levalue) { ?>
			<div class="event-item">
				<span class="date"><strong><?= date("d",strtotime($levalue->event_start_date)) ?></strong><?= date("M",strtotime($levalue->event_start_date)) ?></span>
				<span class="time"><span class="h"><?= date("d M Y",strtotime($levalue->event_start_date)) ?><span class="sep"></span> <?= date("d M Y",strtotime($levalue->event_end_date)) ?></span></span>
				<div class="frame">
					<h2><a href="<?= base_url('event/detail/'.$levalue->event_id); ?>"><?= $levalue->event_name ?></a></h2>
					<span class="meta"><a href="#"><?= $levalue->place_name; ?></a></span>
				</div>
				<a href="<?= base_url('event/detail/'.$levalue->event_id); ?>" class="more"><i class="fa fa-angle-right"></i></a>
			</div>
		<?php } ?>
		</div>
		<input type="text" class="dsnone" id="pages" value="5">
		<a href="javascript:void(0);" id="loadmorelist" class="btn btn-primary btn-more">LOAD MORE</a>
		<a href="javascript:void(0);" id="loadingcontetn" class="btn btn-warning btn-more" style="display: none;">Loading Content...</a>
		<div class="ds-none" id="nomoreevent">No more event Available</div>
	</div>
</section>

<script type="text/javascript">
	$("#loadmorelist").click(function(event) {
		var _pages = $("#pages").val();
		var _dates = '<?= $date; ?>';
		$("#loadmorelist").fadeOut('fast', function() {
			$("#loadingcontetn").fadeIn('fast', function() {
				$.ajax({
					url: '<?= base_url('listevent/loadmore/'); ?>'+ _pages + "?date=" + _dates,
					type: 'GET',
					dataType: 'HTML',
					async: true,
					processData: false,
					contentType: false
				})
				.done(function(e) {
					if (e != "") {
						var _newpages = parseInt(_pages) + 5;
						$("#pages").val(_newpages);
						$("#resullist").append(e);

						$("#loadingcontetn").fadeOut('fast', function() {
							$("#loadmorelist").fadeIn('fast', function() {
							});
						});
					}else{
						$("#loadingcontetn").fadeOut('fast', function() {
							$("#loadmorelist").fadeOut('fast', function() {
								$("#nomoreevent").fadeIn('fast', function() {
									
								});
							});
						});
					}
					
				})
				.fail(function() {
					$("#loadingcontetn").fadeOut('fast', function() {
						$("#loadmorelist").fadeIn('fast', function() {
						});
					});
				})
				.always(function() {
					
				});
			});
		});
	});
</script>