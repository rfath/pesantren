<div style="padding: 10px; border:1px solid #ff4a02; border-radius: 20px;">
	<h5>Detail Package</h5>
	<input type="hidden" id="prc" value="<?= $packageDetail->package_price; ?>" readonly="">
	<table class="table">
		<tr>
			<td style="width: 20%;">Package Name</td>
			<td style="width: 1%;">:</td>
			<td><?= $packageDetail->package_name ?></td>
		</tr>
		<tr>
			<td>Price</td>
			<td>:</td>
			<td>Rp. <?= number_format($packageDetail->package_price) ?> / Person</td>
		</tr>
		<tr>
			<td>Schedule</td>
			<td>:</td>
			<td>
				<?php 
					foreach ($schedule as $sckey => $scvalue) {
						echo "<li style='margin-bottom:5px;'>".$scvalue->schedule_alias." <small>(".date("d/m/Y H:i",strtotime($scvalue->schedule_start_time))." - ".date("d/m/Y H:i",strtotime($scvalue->schedule_end_time)).")</small></li>";
					} 
				?>
			</td>
		</tr>
	</table>
</div>