<div class="blog single">
   <div class="blog-title">
      <h3>History Order</h3>
   </div>
   <div class="event-item">
      <div class="col-md-12">
         <div class="form-group">
            <div class="row">
               <div class="col-md-3">
                  <select class="form-control selectpicker col-md-2 oCh" id="event" data-live-search="true">
                     <option value="">All Event</option>
                     <?php foreach ($listEvent as $key => $value) { ?>
                        <option value="<?= $value->event_id ?>"><?= $value->event_name; ?></option>
                     <?php } ?>
                  </select>
               </div>
               <div class="col-md-3">
                  <select class="form-control selectpicker col-md-2 oCh" id="status" data-live-search="true">
                     <option value="">All Status</option>
                     <option value="0">Waiting for Payment</option>
                     <option value="1">Waiting Admin Approve</option>
                     <option value="2">Accepted</option>
                     <option value="3">Rejected</option>
                  </select>
               </div>
            </div>
         </div>
         <?php echo flashdata_notif("is_success","Yes"); ?>
         <div class="table-responsive">
            <table width="100%" id="datatable" class="table table-striped">
               <thead>
                  <tr>
                     <th style="width: 10px;">No</th>
                     <th>Order Number</th>
                     <th>Name Participant</th>
                     <th>Event</th>
                     <th>Package</th>
                     <th>Payment Status</th>
                     <th></th>
                  </tr>
               </thead>
            </table>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(document).ready(function() {
      var dataTable = $("#datatable").DataTable({
         "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
         "processing": true,
         "serverSide": true,
         "pagingType": "full_numbers",
         "ajax": { 
            url: "<?php echo base_url('order/get_list_order_by_member'); ?>", 
            type: "POST",
            data: function (d) {
               d.event = $("#event").val();
               d.status = $("#status").val();
            },
            error: function () {

            }
         },
      });
      $(".oCh").on('change',function(event) {
         reload_table();
      });
      function reload_table(){
         dataTable.ajax.reload(null,false); 
      }
   });
</script>