<div class="blog single">
	<div class="event-titles">
		<span class="date-t">Date</span>
		<span class="time-t">Time</span>
		<span class="event-t">Event</span>
	</div>
	<div class="event-item">
		<span class="date"><strong><?= date("d",strtotime($detailEvent->event_start_date)) ?></strong> <?= date("M",strtotime($detailEvent->event_start_date)) ?></span>
		<span class="time"><span class="h"><?= date("d M Y",strtotime($detailEvent->event_start_date)) ?><span class="sep"></span> <?= date("d M Y",strtotime($detailEvent->event_end_date)) ?></span></span>
		<div class="frame">
			<h2><a href="#"><?= $detailEvent->event_name ?></a></h2>
			<span class="meta"><a href="#"><?= $detailEvent->place_name; ?></a></span>
		</div>
		<div class="action">
			<?php if(empty($loginData)){ ?>
				<a href="<?= base_url('signin'); ?>" class="btn btn-warning">LOGIN TO FOLLOW THIS EVENT</a>
			<?php }else{ ?>
				<a href="<?= base_url('event/register/'.$detailEvent->event_id); ?>" class="btn btn-primary">Register this event</a>
			<?php } ?>
		</div>
	</div>
	<div class="text">
		<p><?= nl2br($detailEvent->event_description) ?></p>
	</div>
	<hr>
	<div class="event-schedule-detail">
		<h3>Event Schedule</h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Schedule</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($listSchedule as $sckey => $scvalue) { ?>
					<tr>
						<td><?= $scvalue->schedule_alias ?></td>
						<td><?= date("d M Y H:i",strtotime($scvalue->schedule_start_time)); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<hr>
	<div class="contact-event">
		<h3>Contact Info</h3>
		<table>
			<tr>
				<td><i class="fa fa-whatsapp"></i> Whatsapp</td>
				<td>&nbsp:&nbsp</td>
				<td><?= $detailEvent->event_contact_whatsapp ?></td>
			</tr>
			<tr>
				<td><i class="fa fa-phone"></i> Phone</td>
				<td>&nbsp:&nbsp</td>
				<td><?= $detailEvent->event_contact_telephone ?></td>
			</tr>
			<tr>
				<td><i class="fa fa-envelope"></i> Email</td>
				<td>&nbsp:&nbsp</td>
				<td><?= $detailEvent->event_contact_email ?></td>
			</tr>
		</table>
	</div>
	<div class="social">
		<?php 
			if($detailEvent->event_social_twitter <> "" AND $detailEvent->event_social_twitter <> "#"){
				echo '<a target="_blank" href="'.$detailEvent->event_social_twitter.'"><i class="fa fa-twitter"></i></a>';
			}

			if($detailEvent->event_social_ig <> "" AND $detailEvent->event_social_ig <> "#"){
				echo '<a target="_blank" href="'.$detailEvent->event_social_ig.'"><i class="fa fa-instagram"></i></a>';
			}

			if($detailEvent->event_social_fb <> "" AND $detailEvent->event_social_fb <> "#"){
				echo '<a target="_blank" href="'.$detailEvent->event_social_fb.'"><i class="fa fa-facebook"></i></a>';
			}
		?>
	</div>
	<div class="map-section">
		<div class="heading">
			<div class="loc"><span>Location</span> <?= $detailEvent->place_name; ?></div>
			<a href="#" class="link">CONTACT US</a>
		</div>

		<div id="map" class="map"></div>

	</div>
</div>

<script>
	function initMap() {
		// variables init
		var mapHolder = document.getElementById('map'), // map holder element
			coords = {lat: <?= $latlong[0] ?>, lng: <?= $latlong[1] ?>}, // center coordinates
			pinPath = '<?= base_url('assets_ticketing/front_end/') ?>images/map-pointer.png', // path to pointer icon

		// Pointer image create
		image = {
				url: pinPath, // path to image
				size: new google.maps.Size(21, 32), // image size
				origin: new google.maps.Point(0, 0), // origin position
				anchor: new google.maps.Point(10, 32) // anchor position
			},

		// Load map to holder element
		map = new google.maps.Map(mapHolder, {
				center: coords, // center coordinated
				zoom: 15, // map scale
				//scrollwheel: false, // disable mouse wheel scrolling
				mapTypeControl: false,
				streetViewControl: false,
				zoomControl: false
			}),

		// Place marker to the map
		marker = new google.maps.Marker({
				position: coords, // marker coordinates
				icon: image, // marker image
				map: map // map to place in
			});
	}
</script>