<div class="blog single">
	<div class="blog-title">
		<h3>Prepare Payment Page</h3>
	</div>

	<div class="event-item">
		<div class="col-md-12">
			<?php echo flashdata_notif("is_success","Yes"); ?>
			<input type="hidden" name="param" value="<?= $id; ?>">
			<center><img src="<?= base_url('assets_ticketing/front_end/images/loading.gif'); ?>"></center>
			<center><span>Please wait. We're Preparing for your payment page.</span></center>
		</div>
	</div>
</div>
<?php 
	$AMOUNT          = $dataOrder->order_price.".00";
	$BASKET          = $dataOrder->event_name.",".$AMOUNT.",".$dataOrder->qty_seat.",".$AMOUNT;
	$STOREID         = $doku_config->store_id;
	$TRANSIDMERCHANT = $dataOrder->order_id;
	
	$URL             = base_url();
	$SAHRED_KEY      = $doku_config->shared_key;
	$WORDS           = sha1($AMOUNT.$SAHRED_KEY.$TRANSIDMERCHANT);
	$CNAME           = $member->member_name;
	$CEMAIL          = $member->member_email;
	$CWPHONE         = $member->member_phone;
	$CHPHONE         = $member->member_phone;
	$CMPHONE         = $member->member_phone;
	$CCAPHONE        = "";
	$CADDRESS        = $member->member_address;
?>

<FORM NAME="order" METHOD="Post" ACTION="<?= $doku_config->url; ?>" >
	<input type ="hidden" name="BASKET" value="<?= $BASKET; ?>">
	<input type ="hidden" name="STOREID" value="<?= $STOREID; ?>">
	<input type ="hidden" name="TRANSIDMERCHANT" value="<?= $TRANSIDMERCHANT; ?>">
	<input type ="hidden" name="AMOUNT" value="<?= $AMOUNT; ?>">
	<input type ="hidden" name="URL" value="<?= $URL; ?>">
	<input type ="hidden" name="WORDS" value="<?= $WORDS; ?>">
	<input type ="hidden" name="CNAME" value="<?= $CNAME; ?>">
	<input type ="hidden" name="CEMAIL" value="<?= $CEMAIL ?>">
	<input type ="hidden" name="CWPHONE" value="<?= $CWPHONE; ?>">
	<input type ="hidden" name="CHPHONE" value="<?= $CHPHONE; ?>">
	<input type ="hidden" name="CMPHONE" value="<?= $CMPHONE; ?>">
	<input type ="hidden" name="CCAPHONE" value="">
	<input type ="hidden" name="CADDRESS" value="<?= $CADDRESS ?>">
	<input type ="hidden" name="CZIPCODE" value="">
	<input type ="hidden" name=”SADDRESS” value="">
	<input type ="hidden" name=”SZIPCODE” value="">
	<input type ="hidden" name=”SCITY” value="">
	<input type ="hidden" name=”SSTATE” value="">
	<input type ="hidden" name=”SCOUNTRY” value="784">
	<input type ="hidden" name="BIRTHDATE" value="">
	<button id="btnsubmit" class="ds-none">submit</button>
</FORM>
<?php //debugCode($doku_config); ?>
<script>
	$(document).ready(function() {
		$("#btnsubmit").trigger('click');
	});
</script>