<div class="blog single">
  <div class="blog-title">
    <h3>My Profile</h3>
  </div>
  <div class="event-item">
    <form method="POST" action="<?= base_url('event/doregister/'); ?>">
      <div class="col-md-12">
        <div class="form-group row">
          <label class="col-md-2">Name</label>
          <div class="col-md-10">
            <input type="text" class="form-control" value="<?= $dataUser->member_name ?>" readonly="">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-2">Email</label>
          <div class="col-md-10">
            <input type="text" class="form-control" value="<?= $dataUser->member_email ?>" readonly="">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-2">phone</label>
          <div class="col-md-10">
            <input type="text" class="form-control" value="<?= $dataUser->member_phone ?>" readonly="">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-2">Birth Date</label>
          <div class="col-md-10">
            <input type="text" class="form-control" value="<?= date('Y-m-d', strtotime($dataUser->member_birthdate)) ?>" readonly="">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-2">Address</label>
          <div class="col-md-10">
            <textarea class="form-control" readonly=""><?= $dataUser->member_address; ?></textarea>
          </div>
        </div>  
        <!-- <div class="form-group row">
          <label class="col-md-2">Province</label>
          <div class="col-md-10">
            <select name="package" class="form-control" id="province" required="" readonly="">
              <option value="">Choose Province</option>
              <?php foreach ($province as $dataProvince) { ?>
                <?php if ($dataUser->province_id == $dataProvince->province_id): ?>
                    <option value="<?= $dataProvince->province_id ?>"><?= $dataProvince->province_name; ?></option>
                <?php endif ?>
              <?php } ?>
            </select>
          </div>
        </div> -->
       <!--  <div class="form-group row">
          <label class="col-md-2">City</label>
          <div class="col-md-10">
            <select name="package" class="form-control" id="city" required="" readonly="">
              <option value="">Choose City</option>
            </select>
          </div>
        </div> -->
        <!-- <hr> -->
        <div class="form-group row">
          <div class="col-md-2"></div>
          <div class="col-md-10">
            <a href="<?= base_url('myprofile/editprofile/'.$dataUser->member_id) ?>" id="regbtn" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
          </div>        
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  $("#province").change(function(event) {
    _myval = $("#province").val();
    $.ajax({
      url: '<?= base_url('myprofile/getCityByProvince/'); ?>'+_myval,
      type: 'GET',
      dataType: 'HTML',
      async: true,
      processData: false,
      contentType: false
    })
    .done(function(e) {
      $("#city").html(e);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
</script>