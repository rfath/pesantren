<div class="blog single">
	<div class="blog-title">
		<h3>Order Detail</h3>
	</div>

	<div class="event-item">
		<div class="col-md-12">
			<?php echo flashdata_notif("is_success","Yes","Registration success"); ?>

			<div class="form-group row">
				<label class="col-md-2">Order Number</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="OD-<?= date('Ymd',strtotime($dataOrder->order_time)).'-'.$dataOrder->order_id; ?>" readonly="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">E-ticket Number</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $dataOrder->eticket; ?>" readonly="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">Status</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $status_payment; ?>" readonly="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">Event Name</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $dataOrder->event_name; ?>" readonly="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">Name Participant</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $dataOrder->member_name; ?>" readonly="">
				</div>
			</div>
			<div id="additionaldata">
				<?php foreach ($additional as $key => $value) { ?>
					<hr>
					<h5>Additional Participant</h5>
					<div class="form-group row">
						<label class="col-md-2">Name</label>
						<div class="col-md-10">
							<input type="text" value="<?= $value->name ?>" class="form-control" placeholder="Name Additional Participant" required="" readonly="">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">E-ticket Number</label>
						<div class="col-md-10">
							<input type="text" value="<?= $value->eticket ?>" class="form-control" placeholder="E-ticket Additional Participant" required="" readonly="">
						</div>
					</div>
					<div id="additionaldata">
						<div class="form-group row">
							<label class="col-md-2">Email</label>
							<div class="col-md-10">
								<input type="text" value="<?= $value->email ?>" class="form-control" placeholder="Email Additional Participant" required=""  readonly="">
							</div>
						</div>
					</div>
					<div id="additionaldata">
						<div class="form-group row">
							<label class="col-md-2">Phone Number</label>
							<div class="col-md-10">
								<input type="text" value="<?= $value->phone ?>" class="form-control" placeholder="Phone Additional Participant" required="" readonly="">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2">Gender</label>
						<div class="col-md-10">
							<input type="text" class="form-control" value="<?= $value->gender ?>" readonly="">
						</div>
					</div>
				<?php } ?>
			</div>
			<hr>
			<div class="form-group row">
				<label class="col-md-2">Seat</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $seat ?>" id="qtyseta" readonly="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">Package</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $dataOrder->package_name; ?>" name="" readonly="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2"></label>
				<div class="col-md-10">
					<span style="color: #e64614;" id="pricetext"></span>
					<span id="detailPackage"></span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">Total Price</label>
				<div class="col-md-10">
					<input type="text" value="<?= number_format($dataOrder->order_price); ?>" id="totalPriceInfo" class="form-control" readonly="">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-md-2">Payment</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $payment ?>" readonly="" name="">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2">Remarks</label>
				<div class="col-md-10">
					<input type="text" class="form-control" value="<?= $dataOrder->payment_remarks ?>" name="remarks" placeholder="Remarks" readonly="">
				</div>
			</div>
			<hr>
			<div class="form-group row">
				<div class="col-md-2"></div>
				<div class="col-md-10">
					<a href="<?= base_url('order/historyorder/'); ?>" type="button" class="btn btn-primary"><i class="fa fa-arrow-left"></i> History Order</a>
					<?php if($dataOrder->payment_status == 0){ ?>
						<a href="<?= base_url('event/payment/'.$dataOrder->order_id); ?>" type="button" class="btn btn-info"><i class="fa fa-money"></i> Go To Payment Proccess</a>
					<?php } ?>
					
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#detailPackage").html("<i>Getting Package Detail</i>");
		$.ajax({
			url: '<?= base_url('event/detailPackage/'); ?>'+<?= $dataOrder->package_id; ?>,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#detailPackage").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});
</script>