<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<link rel="shortcut icon" href="<?= base_url('assets_ticketing/back_end/') ?>img/favicon.ico" />
	<title>Edit Profile</title>
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	
	<!-- Common CSS -->
	<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>fonts/icomoon/icomoon.css" />
	<!-- Date Picker -->
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<!-- Time Picker -->
	<link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
	<!-- Mian and Login css -->
	<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>css/main.css" />
	<script src="<?= base_url("assets_ticketing/back_end/"); ?>js/jquery.js"></script>
	<script src="<?= base_url("assets_ticketing/back_end/"); ?>js/tether.min.js"></script>
</head>  

<body class="login-bg">
		
	<div class="container">
		<div class="login-screen row align-items-center" style="padding-top: 10px;">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
				<?php echo flashdata_notif("is_success","Yes","Register Success, please login to continue."); ?>
				<form action="<?= base_url('myprofile/updateprofile/'.$dataUser->member_id); ?>" method="post">
					<div class="login-container">
						<div class="row no-gutters">
							<div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
								<div class="login-box" style="padding: 25px 25px;">
									<a href="#" class="login-logo">
										<!-- <img src="<?= base_url('assets_ticketing/back_end/') ?>img/unify.png" alt="Unify Admin Dashboard" /> -->
									</a>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Name" name="member_name" value="<?= $dataUser->member_name ?>" required="">
									</div>
									<br>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Name" name="member_email" value="<?= $dataUser->member_email ?>" required="">
									</div>
									<br>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Name" name="member_phone" value="<?= $dataUser->member_phone ?>" required="">
									</div>
									<br>
									<div class="input-group">
										Birthday : &nbsp<input type="text" class="form-control datetimepicker" placeholder="Birthday" aria-label="birthday" name="member_birthdate" value="<?= date('m/d/Y', strtotime($dataUser->member_birthdate))?>" required="">
									</div>
									<br>
									<div class="input-group">
										<textarea class="form-control" placeholder="Address" name="member_address"><?= $dataUser->member_address ?></textarea>
									</div>
									<br>
									<div class="input-group">
										<select class="form-control" id="province" name="province">
											<option value="">Select Province</option>
											<?php foreach ($province as $key => $value) { ?>												
												<option value="<?= $value->province_id ?>" <?= check_selected($value->province_id, $dataUser->province_id); ?> ><?= $value->province_name ?></option>
											<?php } ?>
										</select>
									</div>
									<br>
									<div class="input-group">
										<select class="form-control" id="city" name="city">
											<option value="">Select City</option>
											<?php foreach ($city as $ckey => $cvalue) { ?>
												<option value="<?= $cvalue->city_id ?>" <?= check_selected($cvalue->city_id, $dataUser->city_id); ?> ><?= $cvalue->city_name; ?></option>
											<?php } ?>
										</select>
									</div>
									<br>
									<div class="input-group">
										<select class="form-control" id="gender" name="gender">
											<option value="Male" <?= check_selected($dataUser->gender, "Male"); ?>>Male</option>
											<option value="Female" <?= check_selected($dataUser->gender, "Female"); ?>>Female</option>
										</select>
									</div>
									<br>
									<span class="red">* Change Password (Optional)</span>
									<div class="input-group">
										<span class="input-group-addon" id="password"><i class="icon-verified_user"></i></span>
										<input type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="password" name="member_password">
									</div>
									<div class="actions clearfix">
										<button type="submit" class="btn btn-primary">Update</button>
									</div>
									<hr>
									<div class="mt-4">
										<a href="<?= base_url('myprofile/user/'.$dataUser->member_id); ?>" class="additional-link" style="margin:0px;">Back to My Profile</a>
									</div>
								</div>
							</div>
							<div class="col-xl-8 col-lg-7 col-md-6 col-sm-12">
								<div class="login-slider"></div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<script src="<?= base_url("assets_ticketing/back_end/"); ?>js/bootstrap.min.js"></script>
	<script src="<?= base_url("assets_ticketing/back_end/"); ?>vendor/notify/notify.js"></script>
	<script src="<?= base_url("assets_ticketing/back_end/"); ?>vendor/notify/notify-custom.js"></script>
	<script src="<?= base_url('assets_ticketing/back_end/') ?>vendor/unifyMenu/unifyMenu.js"></script>
	<script src="<?= base_url('assets_ticketing/back_end/') ?>vendor/onoffcanvas/onoffcanvas.js"></script>
	<script src="<?= base_url('assets_ticketing/back_end/') ?>js/moment.js"></script>
	
	<!-- Time Picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
	<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js"></script>

	<!-- Date Time Picker -->
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.datetimepicker').daterangepicker({
			    "singleDatePicker": true,
			    "showDropdowns": true,
			    "autoApply": true,
			    "locale": {
			        "format": "MM/DD/YYYY",
			        "separator": " - ",
			        "firstDay": 1
			    }
			}, function(start, end, label) {
			  
			});
		});

		$("#province").change(function(event) {
			_myval = $("#province").val();
			$.ajax({
				url: '<?= base_url('signin/getCityByProvince/'); ?>'+_myval,
				type: 'GET',
				dataType: 'HTML',
				async: true,
				processData: false,
				contentType: false
			})
			.done(function(e) {
				$("#city").html(e);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});
	</script>
</body>
</html>