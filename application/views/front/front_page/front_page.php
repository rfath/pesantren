<section class="calendar">
	<header>
		<h1>event calendar</h1>
	</header>
	<div class="body">
		<?php foreach ($list_event as $lekey => $levalue) { ?>
			<div class="event-item">
				<span class="date"><strong><?= date("d",strtotime($levalue->event_start_date)) ?></strong><?= date("M",strtotime($levalue->event_start_date)) ?></span>
				<span class="time"><span class="h"><?= date("d M Y H:i",strtotime($levalue->event_start_date)) ?><span class="sep"></span> <?= date("d M Y H:i",strtotime($levalue->event_end_date)) ?></span></span>
				<div class="frame">
					<h2><a href="<?= base_url('event/detail/'.$levalue->event_id); ?>"><?= $levalue->event_name ?></a></h2>
					<span class="meta"><a href="#"><?= $levalue->place_name; ?></a></span>
				</div>
				<a href="<?= base_url('event/detail/'.$levalue->event_id); ?>" class="more"><i class="fa fa-angle-right"></i></a>
			</div>
		<?php } ?>
		<a href="<?= base_url('listevent'); ?>" class="btn btn-primary btn-more">SHOW MORE</a>
	</div>
</section>