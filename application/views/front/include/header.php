<div id="sidebar-wrapper" class="mCustomScrollbar" data-mcs-theme="minimal-dark">
	<a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
	<div id='cssmenu'>
		<ul>
			<li><a href='index.html'>Home</a></li>
			<!-- <li><a href='event-single.html'>Events</a></li> -->
			<!-- <li class='has-sub offset'>
				<a href='#'>Blog Page
					<i class="fa fa-sort-desc" aria-hidden="true"></i>
				</a>
				<ul>
					<li class='offset'><a href='blog.html'>Blog</a></li>
					<li class='offset'><a href='blogpost.html'>Blog Post</a></li>
				</ul>
			</li> -->
			<!-- <li class='has-sub offset'>
				<a href='#' class='offset'>Organizers
					<i class="fa fa-sort-desc" aria-hidden="true"></i>
				</a>
				<ul>
					<li><a href='organizer.html'>Organizers</a></li>
				</ul>
			</li>
			<li><a href='404.html'>About</a></li>
			<li class='has-sub offset'>
				<a href='#'>Pages
					<i class="fa fa-sort-desc" aria-hidden="true"></i>
				</a>
				<ul>
					<li class='offset'><a href='404.html'>About</a></li>
					<li class='offset'><a href='404.html'>Terms</a></li>
					<li class='offset'><a href='404.html'>Privacy</a></li>
					<li class='offset'><a href='404.html'>Help Center</a></li>
				</ul>
			</li>
			<li><a href='#'>Contact</a></li>
			<li><a href='404.html'>404 Page</a></li> -->
		</ul>
	</div>
</div>
<header id="header">
	<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle">
		<i class="fa fa-bars"></i>
	</a>
	<a href="<?= base_url() ?>" class="logo">
		<img src="<?= base_url('assets_ticketing/front_end/'); ?>images/logo.png" alt="UNITY">
	</a>
	<a href="#" class="menu-opener"><span></span></a>
	<div class="menu-slide">
		<!-- <div class="search-block">
			<a href="#" class="btn search-opener">
				<i class="fa fa-search"></i>
			</a>
			<form action="#" class="search-slide">
				<input type="search" placeholder="Fests in Ber|" class="form-control">
				<button class="btn"><i class="fa fa-search"></i></button>
			</form>
		</div> -->
		<div class="menu-slide">
			<div class="language">
				<label>
					<a href="<?= base_url('cms/signin/'); ?>" class="btn">Sign In (Admin)</a>
				</label>
				<?php if(empty($this->session->sessionMember)){ ?>
					<label>
						<a href="<?= base_url('signin/'); ?>" class="btn">Sign In</a>
					</label>
					<label>
						<a href="<?= base_url('signin/signup/'); ?>" class="btn">Register</a>
					</label>
				
				<?php } ?>
			</div>
			<?php if(!empty($this->session->sessionMember)): ?>
			<select class="location-select position-select" id="location">
				<option>
					<a href="<?= base_url('signin/'); ?>" class="btn dropdown"><?= $this->session->sessionMember['member_name'] ?></a>
				</option>
				<option value="profile">My Profile</option>
				<option value="history">History Order</option>
				<option value="signout">SignOut</option>
			</select>
			
			<?php endif ?>
		</div>
	</div>
	<div class="shedule">
		<?php $this->load->view('front/front_page/listEventHead'); ?>
	</div>
</header>