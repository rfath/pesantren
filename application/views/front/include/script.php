
<script src="<?= base_url('assets_ticketing/front_end/'); ?>js/jquery-ui.min.js"></script>
<script src="<?= base_url('assets_ticketing/front_end/'); ?>js/main.js"></script>
<script src="<?= base_url('assets_ticketing/front_end/'); ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7Mr1SnmpnkqSqZZcFDXOPK0LVGJ1aqV4&callback=initMap" async defer></script>
<script src="https://use.fontawesome.com/3272ab6501.js"></script>

<script src="<?= base_url('assets_ticketing/front_end/'); ?>js/jquery.number.js"></script>
<script src="<?= base_url('assets_ticketing/front_end/'); ?>js/jquery.number.min.js"></script>

<!-- Data Tables -->
<script src="<?= base_url('assets_ticketing/back_end/') ?>vendor/datatables/dataTables.min.js"></script>
<script src="<?= base_url('assets_ticketing/back_end/') ?>vendor/datatables/dataTables.bootstrap.min.js"></script>

<!-- Custom Data tables -->
<script src="<?= base_url('assets_ticketing/back_end/') ?>vendor/datatables/custom/custom-datatables.js"></script>
<script src="<?= base_url('assets_ticketing/back_end/') ?>vendor/datatables/custom/fixedHeader.js"></script>
<script type="text/javascript">
	$("#location").on("change", function(){
		var location = $("#location").val();
		if (location == "profile") {
			window.location.href = "<?= base_url('myprofile/user/'.$this->session->sessionMember['member_id']) ?>";
		}else if(location == "history"){
			window.location.href = "<?= base_url('order/historyorder') ?>";
		}else if(location == "signout"){
			window.location.href = "<?= base_url('signin/signout/'); ?>";
		}

	});
</script>