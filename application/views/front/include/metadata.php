<!DOCTYPE html>
<html>

<!-- Mirrored from unity.wpmix.net/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Jun 2018 04:34:32 GMT -->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Event</title>
	<link rel="shortcut icon" href="<?= base_url('assets_ticketing/back_end/img/ifgf.png'); ?>" type="image/x-icon"/>
	<link media="all" rel="stylesheet" href="<?= base_url('assets_ticketing/front_end/'); ?>css/main.css">
	<link media="all" rel="stylesheet" href="<?= base_url('assets_ticketing/front_end/'); ?>css/jquery.mCustomScrollbar.css">
	<link media="all" rel="stylesheet" href="<?= base_url('assets_ticketing/front_end/'); ?>css/custom.css">

	<!-- Data Tables -->
	<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>vendor/datatables/dataTables.bs4.css" />
	<link rel="stylesheet" href="<?= base_url('assets_ticketing/back_end/') ?>vendor/datatables/dataTables.bs4-custom.css" />

	<script src="<?= base_url('assets_ticketing/front_end/'); ?>js/jquery-1.11.2.min.js"></script>
</head>