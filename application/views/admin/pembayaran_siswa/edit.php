<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Add Prestasi Siswa</h5>
					<h6 class="sub-heading">Prestasi Siswa</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
			<form method="post" action="<?= base_url('cms/pembayaransiswa/doUpdate/'.$detailPemSiswa->pembayaran_install_siswa_id); ?>"  role="form" enctype="multipart/form-data">
				<div class="card">
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Prestasi</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="title" required="">
									<option value="" >Pilih Tile Pembayaran</option>
									<?php
			                            foreach ($dataPembayaran as $vTitlePembayaran) {
			                        ?>
				                        <option value="<?php echo $vTitlePembayaran->pembayaran_id ?>" <?= check_selected($detailPemSiswa->pembayaran_id, $vTitlePembayaran->pembayaran_id) ?>><?php echo $vTitlePembayaran->pembayaran_title ?></option>
			                        <?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Santri</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="santri" id="namasantri" placeholder="Select Santri" value="<?= $detailPemSiswa->siswa_name ?>"  required="" readonly="">
								<input type="hidden" class="form-control" name="idsantri" id="idsantri" placeholder="Select Santri" value="<?= encrypt_decrypt("encrypt", $detailPemSiswa->siswa_id) ?>"  required="" readonly="">
								<button type="button" id="cardisantri" class="btn btn-success btn-sm" style="margin-top: 5px;">Cari Santri</button>
							</div>
						</div>

					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- D MODAL -->
<div class="modal fade" id="csantri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form id="delmodalForm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Pilih Santri</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-responsive" width="100%" id="datatable">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Santri</th>
									<th>NIK</th>
									<th>NISN</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/perkeluar/get_list_siswa'); ?>", 
                type: "POST",
				data: function (d) {
					
                },
                error: function () {
                	
                }
            },
		});
	});

	$("#cardisantri").click(function(event) {
		$("#csantri").modal("show");
	});

	function selectsantri(par1,par2){
		$("#namasantri").val(par2);
		$("#idsantri").val(par1);
		$("#csantri").modal("toggle");
	}
	
</script>