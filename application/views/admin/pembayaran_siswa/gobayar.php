<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Bayar</h5>
					<h6 class="sub-heading">Pembayaran</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->
<form method="post" action="<?= base_url('cms/pembayaransiswa/doBayar/'.$id); ?>" enctype="multipart/form-data">
	<!-- BEGIN .main-content -->
	<div class="main-content">
		<div class="row gutters">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header main-head">Detail Pembayaran</div>
					<div class="card-body">
						<?php echo return_custom_notif(); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pembayaran</label>
							<div class="col-md-10">
								<input type="text" name="pembayaran" class="form-control" placeholder="Pembayaran"  required="" readonly="" value="<?= $dataPembayaran->pembayaran_title; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nomor Induk</label>
							<div class="col-md-10">
								<input type="text" name="noinduk" class="form-control" placeholder="Nomor Induk"  required="" readonly="" value="<?= $dataPembayaran->siswa_nomor_induk; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama Siswa</label>
							<div class="col-md-10">
								<input type="text" name="nama_siswa" class="form-control" placeholder="Nama Siswa"  required="" readonly="" value="<?= $dataPembayaran->siswa_name; ?>">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Kelas</label>
							<div class="col-md-10">
								<input type="text" name="kelas" class="form-control" placeholder="Kelas"  required="" readonly="" value="<?= $dataPembayaran->kelas_name; ?>">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tanggal Bayar</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Tanggal Bayar"  required="" readonly="" value="<?php echo date("d M Y") ?>">
							</div>
						</div>
						<center><h3>DETAIL PEMBAYARAN</h3></center>
						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No</th>
										<th>Keterangan</th>
										<th>Besar</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; $total; foreach ($detail_pembayaran as $key => $value) { $total += $value->price; ?>
										<tr>
											<td style="width: 3px;"><?php echo $no++; ?></td>
											<td><?php echo $value->title; ?></td>
											<td style="text-align: right;">Rp. <?php echo number_format($value->price); ?></td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr style="font-weight: bold;">
										<td colspan="2">TOTAL</td>
										<td style="text-align: right;">Rp. <?php echo number_format($total); ?></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<input type="hidden" name="totalz" value="<?= $total; ?>">
					<input type="hidden" name="sidz" value="<?= $dataPembayaran->siswa_id; ?>">
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="button" id="bayar" class="btn btn-success"><i class="fa fa-check"></i> Bayar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- D MODAL -->
	<div class="modal fade" id="confirm_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Are you sure ?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					Are you sure you want to change status to <b>Bayar</b>, dengan nominal sebesar <b>Rp.<?php echo number_format($total); ?></b> ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">OK, Bayar</button>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	$("#bayar").click(function(event) {
		$("#confirm_modal").modal("show");
	});
</script>