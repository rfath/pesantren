<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Export Card</h5>
					<h6 class="sub-heading">Card</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Export card to PDF</div>
				<div class="card-body">
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Kelas</label>
						<div class="col-md-10">
							<select class="form-control selectpicker" data-live-search="true" id="kelas">
								<option value="">Pilih Kelas</option>
								<?php foreach ($kelas as $kkey => $kvalue) { ?>
									<option value="<?= $kvalue->kelas_id ?>"><?= $kvalue->kelas_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Rombel</label>
						<div class="col-md-10">
							<select class="form-control selectpicker" data-live-search="true" id="rombel">
								<option value="">Pilih Rombel</option>
							</select>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" id="doprint"><i class="fa fa-print"></i> Print to PDF</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#kelas").change(function(event) {
		var _kelas = $("#kelas").val();
		$.ajax({
			url: '<?= base_url('cms/exportcard/getrombel/'); ?>'+_kelas,
			type: 'GET',
			dataType: 'HTML',
			async:true,
			processData:false,
			contentType:false
		})
		.done(function(e) {
			$("#rombel").html(e).selectpicker("refresh");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

	$("#doprint").click(function(event) {
		var _kelas = $("#kelas").val();
		var _rombel = $("#rombel").val();
		if (_kelas.length > 0 & _rombel.length > 0) {
			window.open('<?= base_url('cms/exportcard/topdf/') ?>'+_kelas+'/'+_rombel, '_blank'); 
		}
	});
</script>