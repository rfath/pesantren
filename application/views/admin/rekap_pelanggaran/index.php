<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Rekap Pelanggaran</h5>
					<h6 class="sub-heading">Rekap Pelanggaran Siswa</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Siswa Melanggar</div>
				<div class="card-body">
					<div class="form-group">
						<!-- <a href="<?= base_url('cms/pembagiankelas/add/'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a> -->
					</div>
					<!-- <div class="form-group">
						<label>Filter :</label>
						<select class="form-control selectpicker col-md-3 oCh" id="status" data-live-search="true">
						<option value="">Pilih Tahun Pelajaran</option>
						<option value="<?= $ltp->tahun_pelajaran_id ?>" <?= check_selected($k_tp, 0) ?>><?= $ltp->tahun_pelajaran_name ?></option>
						</select>
					</div> -->
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Nomor Induk</th>
									<th>Nama Siswa</th>
									<!-- <th>Kelas</th> -->
									<!-- <th>Rombel</th>
									<th>Tahun Pelajar</th> -->
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/rekappelanggaran/get_list_siswa_melanggar'); ?>", 
                type: "POST",
				data: function (d) {
					d.status = $("#status").val()
                },
                error: function () {
                	
                }
            },
		});

		function reload_table(){
			dataTable.ajax.reload(null,false); 
		}

		$('.oCh').change(function(event) {
			reload_table();
		});
	});
</script>