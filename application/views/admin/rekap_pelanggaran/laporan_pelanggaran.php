<div style="max-width: 720px;">
	<table width="100%">
		<tr>
            <td style="font-weight: bold;">
                <img src="<?php echo base_url("assets_ticketing/back_end/img/pesantren_logo_med.png") ?>" style="width: 110px;max-width: 150px;margin-top: -5px;">
            </td>
			<td>
                <h4>YAYASAN AL-ISLAM NGANJUK</h4>
                <h5 style="margin-top: -16px;">PONDOK MODERN "AL-ISLAM" NGANJUK</h5>
                <h5 style="margin-top: -16px;">NSPP: 510035180109</h5>
				<p style=";margin-top: -16px;">
					Alamat: Lingkungan Jatirejo Jl. Raya Sukomoro - Pace Km.1 Kapas, Sukomoro, Nganjuk, Jawa timur, Indonesia<br>
                    Kode Pos: 64481 Telp: (0358) 325096 e-mail: sekretarispondok@gmail.com
				</p>
			</td>
		</tr>
	</table>
	<div style="border: 1px solid;margin-top: 10px;"></div>
    <div style="text-align: center;"><h3><u>Laporan Pelanggaran</u></h3></div>
    <?php 
        if (!empty($this->uri->segment(5))) {
    ?>
    <div style="text-align: center;">Dari Tanggal : <?= date("m/d/Y", strtotime($this->uri->segment(5))) ?> | Sampai Tanggal : <?= date("m/d/Y", strtotime($this->uri->segment(6))) ?></div><br>
    <?php 
        }
    ?>
	<div style="border: 1px solid;"></div>
	<div style="min-height: 400px;">
		<table width="100%" class="table table-bordered" id="diagnosCopy" border="1" style="border-collapse: collapse;">
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Pelanggaran</th>
                <th>Hukuman</th>
                <th>Tanggal Pelanggaran</th>
            </tr>
   <?php 
        if (!empty($listPelanggaran)) {
    ?>
        <?php 
            $no = 0;
            foreach ($listPelanggaran as $value) {
            $no++;
        ?>
            <tr>
                <td align="center"><?= $no ?></td>
                <td><?= $value->siswa_name ?></td>
                <td><?= $value->pelanggaran_name ?></td>
                <td>
                    <?php
                        $hukuman = array();
                        $hukuman = json_decode($value->hukuman,true);

                        foreach ($hukuman['hukuman'] as $skey => $svalue) {
                            // debugCode($value);
                            echo "<b>Hukuman</b> : ".$svalue." (".$hukuman['status'][$skey].")<br/>";
                        }
                    ?>
                </td>
                <td align="center"><?= date("Y-m-d", strtotime($value->created_date)) ?></td>
            </tr>
        <?php
            }
        ?>
    <?php }else{ ?>
        <tr>
           <td colspan="5" align="center">Tidak ada data.</td>
        </tr>
    <?php } ?>
        </table>
	</div>
</div>
<!-- <script type="text/javascript">
	window.print();
</script> -->