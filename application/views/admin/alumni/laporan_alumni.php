<div style="max-width: 720px;">
    <table width="100%">
        <tr>
            <td style="font-weight: bold;">
                <img src="<?php echo base_url("assets_ticketing/back_end/img/pesantren_logo_med.png") ?>" style="width: 110px;max-width: 150px;margin-top: -5px;">
            </td>
            <td>
                <h4>YAYASAN AL-ISLAM NGANJUK</h4>
                <h5 style="margin-top: -16px;">PONDOK MODERN "AL-ISLAM" NGANJUK</h5>
                <h5 style="margin-top: -16px;">NSPP: 510035180109</h5>
                <p style=";margin-top: -16px;">
                    Alamat: Lingkungan Jatirejo Jl. Raya Sukomoro - Pace Km.1 Kapas, Sukomoro, Nganjuk, Jawa timur, Indonesia<br>
                    Kode Pos: 64481 Telp: (0358) 325096 e-mail: sekretarispondok@gmail.com
                </p>
            </td>
        </tr>
    </table>
    <div style="border: 1px solid;margin-top: 10px;"></div>
    <div style="text-align: center;"><h3><u>Daftar Alumni</u></h3></div>
    <div style="text-align: center;">Tahun Pelajaran : <?= $tp ?></div><br>
    <div style="border: 1px solid;"></div>
    <div style="min-height: 400px;">
        <table width="100%" class="table table-bordered" id="diagnosCopy" border="1" style="border-collapse: collapse;">
            <tr>
                <th>No</th>
                <th>NISN</th>
                <th>Nomor Induk</th>
                <th>Nama</th>
                <th>No HP</th>
                <th>Alamat</th>
                <th>Tahun Pelajaran</th>
            </tr>
   <?php 
        if (!empty($listData)) {
    ?>
        <?php 
            $no = 0;
            foreach ($listData as $value) {
            $no++;
        ?>
            <tr>
                <td align="center"><?= $no ?></td>
                <td><?= $value->siswa_nisn ?></td>
                <td><?= $value->siswa_nomor_induk ?></td>
                <td><?= $value->siswa_name ?></td>
                <td><?= $value->siswa_no_hp ?></td>
                <td><?= $value->siswa_alamat ?></td>
                <td align="center"><?= $value->tahun_pelajaran_name ?></td>
            </tr>
        <?php
            }
        ?>

    <?php }else{ ?>
        <tr>
           <td colspan="8" align="center">Tidak ada data.</td>
        </tr>
    <?php } ?>
        </table>
    </div>
</div>