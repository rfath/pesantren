<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Alumni</h5>
					<h6 class="sub-heading">List ALumni</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Alumni</div>
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label>Filter :</label>
								<select class="form-control selectpicker oCh" id="tp" data-live-search="true">
									<option value="">Pilih Tahun Pelajaran</option>
								<?php 
									foreach ($tp as $k_tp => $ltp) {
								?>
									<option value="<?= $ltp->tahun_pelajaran_id ?>" <?= check_selected($k_tp, "0") ?>><?= $ltp->tahun_pelajaran_name ?></option>
								<?php 
									}
								?>
								</select>
							</div>
							<div class="col-md-3">
								<a href="<?= base_url('cms/alumni/'); ?>" target="_blank" class="btn btn-primary" id="printjurnal"  style="margin-top: 30px;"><i class="fas fa-print"></i> Print</a>
							</div>
						</div>
					</div>
					<?php echo return_custom_notif(); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>NISN</th>
									<th>Nomor Induk</th>
									<th>Nama</th>
									<th>No HP</th>
									<th>Alamat</th>
									<th>Tahun Pelajaran</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODAL 1 -->
<div class="modal fade" id="csantri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form id="delmodalForm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Santri Kembali Masuk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="dataresult">
					
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/alumni/get_list_alumni'); ?>", 
                type: "POST",
				data: function (d) {
					d.tp = $("#tp").val();

					// Display link href
					document.getElementById("printjurnal").href="<?= base_url() ?>cms/alumni/laporanalumni/"+$("#tp").val();
                },
                error: function () {
                	
                }
            },
		});

		function reload_table(){
			dataTable.ajax.reload(null,false); 
		}

		$('.oCh').change(function(event) {
			reload_table();
		});
	});
</script>