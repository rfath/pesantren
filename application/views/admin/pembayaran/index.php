<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Pembayaran</h5>
					<h6 class="sub-heading">List Pembayaran</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Pembayaran</div>
				<div class="card-body">
					<div class="form-group">
						<a href="<?= base_url('cms/pembayaran/add/'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable_pembayaran" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Title Pembayaran</th>
									<th>Detial Pembayaran</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$no = 0;
								
								foreach ($listData as $value) {
								$pembayaran_item = [];
								$no++;
							?>
								<tr>
									<td><?= $no ?></td>
									<td><?= $value->pembayaran_title ?></td>
									<td>
										<?php 
											// Mencari Detail Group Pembayaran
											$dataInstallItem 	= $this->M_pembayaran->getPembayaranInstallItem("pembayaran_id", $value->pembayaran_id);
											foreach ($dataInstallItem as $valItem) {
													$pembayaran_item[] = "*) ".$valItem->title."<br/>";
											}

											echo implode("", $pembayaran_item);
										?>
									</td>
									<td>
										<a href="<?= base_url('cms/pembayaran/edit/'.$value->pembayaran_id) ?>" class="btn btn-primary btn-sm" title="Edit / Detail"><i class="fa fa-edit"></i></a>
										<!-- <button type="button" onclick="carpembayaranitem(<?= $value->pembayaran_id ?>)" class="btn btn-info btn-sm" style="margin-top: 5px;"><i class="fa fa-plus"></i> Add</button> -->
										<button class="btn btn-danger btn-sm" onClick="is_delete('<?= base_url('cms/pembayaran/doDelete/'.$value->pembayaran_id) ?>')" title="Delete"><i class="fa fa-trash"></i></button>
									</td>
								</tr>
							<?php 
								}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		var dataTable = $("#datatable_pembayaran").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		});
	});
</script>