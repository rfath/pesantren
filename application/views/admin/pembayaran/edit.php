<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Pembayaran</h5>
					<h6 class="sub-heading">Pembayaran</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">

				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/pembayaran/doUpdate/'.$detailPembayaran->pembayaran_id); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Pembayaran</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Title Pembayaran</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="title" placeholder="Title Pembayaran" value="<?= $detailPembayaran->pembayaran_title ?>" required="">
							</div>
						</div>
						<div class="table-responsive">
							<table width="100%" id="datatable" class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Pembayaran</th>
										<th>Nominal</th>
										<th><input type="checkbox" id="checkall"></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($listData as $value) {
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $value->title ?></td>
											<td><?= number_format($value->price) ?></td>
											<td>
												<div class="form-check form-check-inline">
													<label class="form-check-label">
														<?php 
														$detailInstallItem = $this->M_pembayaran->getInstallItemDetail("pembayaran_item_id", $value->pembayaran_item_id, $this->uri->segment(4));
														?>
														<input class="form-check-input datacheck" type="checkbox" name="pembayaran_item[]" id="pembayaran_item" value="<?= $value->pembayaran_item_id ?>" <?php (!empty($detailInstallItem) ? print_r("checked") : print_r("")) ?>>
													</label>
												</div>
											</td>
										</tr>
										<?php 
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[-1], ["All"]],

		});
	});

	$('#checkall').change(function(){
	    if(this.checked == false){
	        $(".datacheck").prop('checked', false);
	    }else{
	    	$(".datacheck").prop('checked', true);
	    }
	});
</script>