<?php
$name = "data_inout_schedule".str_replace(" ", "_", $schedule->schedule_name).".xls";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename='".$name."'");
header("Pragma: no-cache");
header("Expires: 0");
?>
<h3>Data IN/OUT Schedule : <?= $schedule->schedule_name; ?></h3>
<table border="1">
	<tr>
		<th>Attendance Name</th>
		<th>Type</th>
		<th>Date In / Out</th>
		<th>Region</th>
		<th>DPW</th>
		<th>Satelit Church</th>
		<th>Church Role</th>
		<th>Gender</th>
		<th>Age</th>
	</tr>
	<?php
	foreach ($dataResult as $key => $value) {
		?>
		<tr>
			<td><?= $value->attendance_name; ?></td>
			<td><?php if ($value->type_att == 1) { echo "In"; }else{ echo "Out"; } ?></td>
			<td><?= date("d M Y H:i:s",strtotime($value->created_date)); ?></td>
			<td><?= $value->region_name; ?></td>
			<td><?= $value->dpw_name; ?></td>
			<td><?= $value->satelit_church_name; ?></td>
			<td><?= $value->church_role_name; ?></td>
			<td><?= $value->gender; ?></td>
			<td><?= $value->age; ?></td>
		</tr>
		<?php 
	} 
	?>
</table>