<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Sales Report</h5>
					<h6 class="sub-heading">Report</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<!-- ==================================================WIZARD================================================== -->
			<div class="card">
				<div class="card-header main-head">Sales Report</div>
				<form method="get">
					<div class="card-body">
						<input type="hidden" name="generate" value="yes">
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Date</label>
							<div class="col-md-3">
								<input type="text" class="form-control daterange" placeholder="Date Range" readonly="" name="daterange" value="<?= isset($_GET['daterange'])?$_GET['daterange']:''; ?>">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Member</label>
							<div class="col-md-3">
								<select class="form-control selectpicker" data-live-search="true" name="member">
									<option value="">All Member</option>
									<?php foreach ($member as $mkey => $mvalue) { ?>
										<option value="<?= $mvalue->member_id ?>" <?= check_selected($mvalue->member_id,isset($_GET['member'])?$_GET['member']:"") ?>><?= $mvalue->member_name." (".$mvalue->member_email.")"; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Event</label>
							<div class="col-md-3">
								<select class="form-control selectpicker" data-live-search="true" name="event">
									<option value="">All Event</option>
									<?php foreach ($event as $ekey => $evalue) { ?>
										<option value="<?= $evalue->event_id ?>" <?= check_selected($evalue->event_id,isset($_GET['event'])?$_GET['event']:"") ?>><?= $evalue->event_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button class="btn btn-primary"><i class="fa fa-file-alt"></i> Generate Report	</button>
					</div>
				</form>
			</div>
			<!-- ==================================================WIZARD================================================== -->
			<?php if(!empty($_GET['generate'])){ ?>
				<!-- ==================================================GENERATE DATA================================================== -->
				<div class="card">
					<div class="card-header main-head">Sales Report : Result <a href="<?= $excelHref; ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-file-excel"></i> Download to excel</a></div>
					<div class="card-body">
						<center><b>Sales Report for : <?= $dateReport ?></b></center>
						<br><br>
						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Event</th>
										<th>Order Date</th>
										<th>Purchase Price</th>
										<th>Seat QTY</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no=1;
									$total_price = 0;
									$total_seat = 0;
									foreach ($reportData as $rkey => $rvalue) {
										$total_price +=$rvalue->order_price;
										$total_seat +=$rvalue->qty_seat;
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $rvalue->member_name; ?></td>
											<td><?= $rvalue->event_name; ?></td>
											<td><?= date("m/d/Y H:i", strtotime($rvalue->order_time)); ?></td>
											<td style="text-align: right;"><?= number_format($rvalue->order_price); ?></td>
											<td style="text-align: right;"><?= $rvalue->qty_seat; ?></td>
										</tr>
										<?php 
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="4">Total</th>
										<th style="text-align: right;"><?= number_format($total_price); ?></th>
										<th style="text-align: right;"><?= $total_seat; ?></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
				<!-- ==================================================GENERATE DATA================================================== -->
			<?php } ?>
		</div>
	</div>
</div>


