<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Select Attendance</h5>
					<h6 class="sub-heading">Report</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Select Attendance for Schedule : <span style="color: #2980b9;"><?= $schedule->schedule_name; ?></span></div>
				<div class="card-body">
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Attendance</label>
						<div class="col-md-3">
							<select class="form-control selectpicker" data-live-search="true" id="attendance">
								<option value="All">All Attendance</option>
								<?php foreach ($attendance as $key => $value) { ?>
									<option value="<?= $value->attendance_id; ?>"><?= $value->attendance_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-success" id="generate"><i class="fa fa-file-excel"></i> Generate Excel</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#generate").click(function(event) {
		var _sc  = '<?= $id; ?>';
		var _att = $("#attendance").val();
		window.open('<?= base_url('cms/report/event_report_inout_excel/'); ?>' + _sc + "/" + _att);
	});
</script>