<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Event Report Detail</h5>
					<h6 class="sub-heading">Report</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Event Report : Result - <span style="color: #2980b9;"><?= strtoupper($detailEvent->event_name); ?></span></div>
				<div class="card-body">
					<div class="form-group row gutters">
						<div class="col-md-12">
							<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							<table id="datatable" style="display: none;">
								<thead>
									<tr>
										<th></th>
										<th>Register</th>
										<th>Attendance</th>
										<?php foreach ($ttlPerSc as $tkey2 => $tvalue2) { ?>
											<th><?= $tvalue2['schedule'] ?></th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>People</th>
										<td data-link="<?= base_url($totalEventData['total_participant']); ?>"><?= $totalEventData['total_participant'] ?></td>
										<td><?= $totalEventData['total_attendance'] ?></td>
										<?php foreach ($ttlPerSc as $tkey3 => $tvalue3) { ?>
											<td><?= $tvalue3['total'] ?></td>
										<?php } ?>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<a href="<?= base_url('cms/report/event_report_participant_excel/'.$id); ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-file-excel"></i> Download Participant Data</a>
					<a href="<?= base_url('cms/report/event_report_attendance_excel/'.$id); ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-file-excel"></i> Download Attendance Data</a>
					<br>
					<br>
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Total Participant</th>
									<th>Total Sales</th>
									<th>Total Attendance</th>
									<th>Attendance %</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?= $totalEventData['total_participant'] ?></td>
									<td><?= number_format($totalEventData['total_sales']); ?></td>
									<td><?= $totalEventData['total_attendance'] ?></td>
									<td><?= round(($totalEventData['total_attendance']/$totalEventData['total_participant'])*100); ?> %</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-group row gutters">
						<div class="col-md-12">
							<div class="rep-title">Total Attendance By Schedule</div>
							<div class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Schedule</th>
											<th>Total</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($ttlPerSc as $t2key => $t2value) { ?>
											<tr>
												<td><?= $t2value['schedule']; ?></td>
												<td><?= $t2value['total']; ?></td>
												<td>
													<a href="<?= base_url('cms/report/event_report_schedule?param='.$id.'&param2='.encrypt_decrypt('encrypt',$t2value['id'])); ?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-excel"></i> Download data to excel</a>
													<a href="<?= base_url('cms/report/event_report_inout?param='.$id.'&param2='.encrypt_decrypt('encrypt',$t2value['id'])); ?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-excel"></i> Download Check In/Out to excel</a>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="form-group row gutters">
						<div class="col-md-12">
							<div class="rep-title">Check In/Out Schedule By Time</div>
							<div class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Schedule</th>
											<th>4 AM - 8 AM</th>
											<th>9 AM - 12 PM</th>
											<th>13 PM - 16 PM</th>
											<th>17 PM - 20 PM</th>
											<th>21 PM - 23 PM</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($ttlPerSc as $t1key => $t1value) { ?>
											<tr>
												<td><?= $t1value['schedule']; ?></td>
												<td>
													IN : <?= $this->M_report->countChinoutBytime($t1value['id'],'04','08',1); ?><br>
													OUT : <?= $this->M_report->countChinoutBytime($t1value['id'],'04','08',2); ?>
												</td>
												<td>
													IN : <?= $this->M_report->countChinoutBytime($t1value['id'],'09','12',1); ?><br>
													OUT : <?= $this->M_report->countChinoutBytime($t1value['id'],'09','12',2); ?>
												</td>
												<td>
													IN : <?= $this->M_report->countChinoutBytime($t1value['id'],13,16,1); ?><br>
													OUT : <?= $this->M_report->countChinoutBytime($t1value['id'],13,16,2); ?>
												</td>
												<td>
													IN : <?= $this->M_report->countChinoutBytime($t1value['id'],17,20,1); ?><br>
													OUT : <?= $this->M_report->countChinoutBytime($t1value['id'],17,20,2); ?>
												</td>
												<td>
													IN : <?= $this->M_report->countChinoutBytime($t1value['id'],21,23,1); ?><br>
													OUT : <?= $this->M_report->countChinoutBytime($t1value['id'],21,23,2); ?>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {

		/*Highcharts.chart('container', {
			data: {
				table: 'datatable'
			},
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			yAxis: {
				allowDecimals: false,
				title: {
					text: ''
				}
			},
			tooltip: {
				formatter: function () {
					return '<b>' + this.series.name + '</b><br/>' +
					this.point.y + ' ' + this.point.name.toLowerCase();
				}
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true,
						crop: true,
						overflow: 'none'
					}
				},
				series: {
		            cursor: 'pointer',
		            point: {
		                events: {
		                    click: function () {
		                        alert('value: ' + this.series);
		                        console.log(this.option);
		                    }
		                }
		            }
		        }
			},
		});*/

		Highcharts.chart('container', {

		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: ''
		    },

		    xAxis: {
		        type: 'category'
		    },
		    tooltip: {
				formatter: function () {
					return '<b>'+this.point.y + ' people for ' + this.point.name.toLowerCase() + '</b>';
				}
			},
		    plotOptions: {
		        series: {
		            cursor: 'pointer',
		            point: {
		                events: {
		                    click: function () {
		                    	window.open(this.options.key, '_blank');
		                    }
		                }
		            }
		        }
		    },

		    series: [{
		    	showInLegend: false,
		    	name: 'Result',
		        data: [<?= $dataChart; ?>]
		    }],
		});
	});
</script>