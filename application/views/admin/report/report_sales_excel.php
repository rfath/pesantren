<?php
$name = "Sales_report_excel".str_replace(array("/"," "), array("-","_"),$dateReport).".xls";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename='".$name."'");
header("Pragma: no-cache");
header("Expires: 0");
?>
<center><b>Sales Report for : <?= $dateReport ?></b></center>
<br><br>
<div class="table-responsive">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Name</th>
				<th>Event</th>
				<th>Order Date</th>
				<th>Purchase Price</th>
				<th>Seat QTY</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no=1;
			$total_price = 0;
			$total_seat = 0;
			foreach ($reportData as $rkey => $rvalue) {
				$total_price +=$rvalue->order_price;
				$total_seat +=$rvalue->qty_seat;
				?>
				<tr>
					<td><?= $no++; ?></td>
					<td><?= $rvalue->member_name; ?></td>
					<td><?= $rvalue->event_name; ?></td>
					<td><?= date("m/d/Y H:i", strtotime($rvalue->order_time)); ?></td>
					<td style="text-align: right;"><?= number_format($rvalue->order_price); ?></td>
					<td style="text-align: right;"><?= $rvalue->qty_seat; ?></td>
				</tr>
				<?php 
			}
			?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4">Total</th>
				<th style="text-align: right;"><?= number_format($total_price); ?></th>
				<th style="text-align: right;"><?= $total_seat; ?></th>
			</tr>
		</tfoot>
	</table>
</div>