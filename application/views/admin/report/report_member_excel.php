<?php
$name = "Member_report.xls";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename='".$name."'");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered">
	<thead>
		<tr>
			<th style="width: 10px;">No</th>
			<th>Event Name</th>
			<th>Date Event</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach ($dataGenerate as $key => $value) { ?>
			<tr>
				<td><?= $no++; ?></td>
				<td><?= $value->event_name ?></td>
				<td><?= date("d M Y",strtotime($value->event_start_date)); ?></td>
				<td></td>
			</tr>
			<tr class="chrow" style="display: none;" id="chdata_<?= $key; ?>">
				<?= $inoutArray[$value->event_id]; ?>
			</tr>
		<?php } ?>
	</tbody>
</table>