<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Event Report Detail</h5>
					<h6 class="sub-heading">Report</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Event Report : List <?= $headCard; ?></div>
				<div class="card-body">
					<div class="form-group">
						Filter : 
						<select class="form-control selectpicker col-md-2 oCh" id="typeFilter">
							<option value="all">All</option>
							<option value="name">Name</option>
							<option value="dpw">DPW</option>
							<option value="region">Region</option>
							<option value="satelit">Satelit Church</option>
							<option value="churchroll">Church Roll</option>
						</select>
						<input type="text" class="form-control col-md-3 iblock oInput" id="keyFilter" placeholder="Filter Key">
					</div>
					<table width="100%" id="datatable" class="table table-striped">
						<thead>
							<tr>
								<th>Name</th>
								<th>Date</th>
								<th>Region</th>
								<th>DPW</th>
								<th>Satelite Church</th>
								<th>Church Role</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<?php if ($dataType == 1) { //IF PARTICIPANT ?>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
			"bFilter": false, 
			"bInfo": true,
			"bLengthChange": false,
            "ajax": {	
                url: "<?php echo base_url('cms/report/get_list_report_event_attendance'); ?>", 
                type: "POST",
				data: function (d) {
					d.param = '<?= $param ?>';
					d.typeFilter = $("#typeFilter").val();
					d.keyFilter = $("#keyFilter").val();
                },
                error: function () {
                	
                }
            },
		});

		$(".oCh").on('change',function(event) {
			reload_table();
		});

		$(".oInput").on('input', function(event) {
			reload_table();
		});

		function reload_table(){
		  dataTable.ajax.reload(null,false); 
		}
	});
</script>
<?php } ?>
<?php if ($dataType == 2) { //IF REGISTRATION ?>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
			"bFilter": false, 
			"bInfo": true,
			"bLengthChange": false,
            "ajax": {	
                url: "<?php echo base_url('cms/report/get_list_report_event_register'); ?>", 
                type: "POST",
				data: function (d) {
					d.param = '<?= $param ?>';
					d.typeFilter = $("#typeFilter").val();
					d.keyFilter = $("#keyFilter").val();				
                },
                error: function () {
                	
                }
            },
		});

		$(".oCh").on('change',function(event) {
			reload_table();
		});

		$(".oInput").on('input', function(event) {
			reload_table();
		});

		function reload_table(){
		  dataTable.ajax.reload(null,false); 
		}
	});
</script>
<?php } ?>
<?php if ($dataType == 3) { //IF SCHEDULE ?>
	<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
			"bFilter": false, 
			"bInfo": true,
			"bLengthChange": false,
            "ajax": {	
                url: "<?php echo base_url('cms/report/get_list_report_event_schedule'); ?>", 
                type: "POST",
				data: function (d) {
					d.param = '<?= $param ?>';
					d.param2 = '<?= $param2 ?>';
					d.typeFilter = $("#typeFilter").val();
					d.keyFilter  = $("#keyFilter").val();				
                },
                error: function () {
                	
                }
            },
		});

		$(".oCh").on('change',function(event) {
			reload_table();
		});

		$(".oInput").on('input', function(event) {
			reload_table();
		});

		function reload_table(){
		  dataTable.ajax.reload(null,false); 
		}
	});
</script>
<?php } ?>