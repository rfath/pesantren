<?php
$name = str_replace(" ", "_", $scheduleData->schedule_name).".xls";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename='".$name."'");
header("Pragma: no-cache");
header("Expires: 0");
?>
<h3><?= $scheduleData->schedule_name; ?></h3>
<table width="100%" id="datatable" class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Date</th>
			<th>Region</th>
			<th>DPW</th>
			<th>Satelite Church</th>
			<th>Church Role</th>
			<th>Gender</th>
			<th>Age</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($listData as $key => $value) { ?>
			<tr>
				<td><?= $value->attendance_name ?></td>
				<td><?= $value->created_date ?></td>
				<td><?= $value->region_name ?></td>
				<td><?= $value->dpw_name ?></td>
				<td><?= $value->satelit_church_name; ?></td>
				<td><?= $value->church_role_name; ?></td>
				<td><?= $value->gender; ?></td>
				<td><?= $value->age; ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>