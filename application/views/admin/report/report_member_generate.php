<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Member Report</h5>
					<h6 class="sub-heading">Report</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<!-- ==================================================WIZARD================================================== -->
			<div class="card">
				<div class="card-header main-head">Member Report : Generate Report</div>
				<form method="get">
					<div class="card-body">
						<div class="form-group row gutters">
							<label class="col-md-1 col-form-label">Name</label>
							<div class="col-md-3">
								<input type="text" class="form-control" placeholder="Name" readonly="" value="<?= $dataMember->member_name; ?>">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-1 col-form-label">Year</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="year" placeholder="Year" value="<?= isset($_GET['year'])?$_GET['year']:date("Y"); ?>">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button class="btn btn-primary"><i class="fa fa-file-alt"></i> Generate Report	</button>
					</div>
				</form>
			</div>
			<!-- ==================================================WIZARD================================================== -->
			<div class="card">
				<div class="card-header main-head">Member Report : Result <?php if (!empty($dataGenerate)) { ?> <a href="<?= $excelHref; ?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-file-excel"></i> Download to excel</a> <?php } ?></div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Event Name</th>
									<th>Date Event</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($dataGenerate as $key => $value) { ?>
									<tr>
										<td><?= $no++; ?></td>
										<td><?= $value->event_name ?></td>
										<td><?= date("d M Y",strtotime($value->event_start_date)); ?></td>
										<td>
											<button class="btn btn-sm btn-success" onclick="chdata(<?= $key ?>,'<?= $member_id ?>','<?= encrypt_decrypt("encrypt",$value->event_id); ?>')">Track Check In/Out</button>
										</td>
									</tr>
									<tr class="chrow" style="display: none;" id="chdata_<?= $key; ?>">
										
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function chdata(keyData,param1,param2){
		$(".chrow").html('<td colspan="4">Loading tracking, please wait.</td>');
		$(".chrow").fadeOut('fast', function() {});
		$(".trow").remove();
		$("#chdata_"+keyData).fadeIn('fast', function() {
			$.ajax({
				url: '<?= base_url("cms/report/chinoutTrack/?param1="); ?>' + param1 + "&param2=" + param2,
				type: 'GET',
				dataType: 'HTML',
				async: true,
				processData: false,
				contentType: false
			})
			.done(function(e) {
				if (e != "") {
					$(".chrow").fadeOut('fast', function() {});
					$(e).insertAfter('#chdata_'+keyData);
				}else{
					$(".chrow").html(e);
				}
				
			})
			.fail(function() {
				$(".chrow").html('Error Something Went Wrong');
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});
	}
</script>