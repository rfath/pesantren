<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Report</h5>
					<h6 class="sub-heading">Select Report</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<!-- Row start -->
			<div class="row gutters">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class="card">
						<div class="card-header main-head">Select Report</div>
						<div class="card-body">
							<div class="reportlist">
								<a href="<?= base_url("cms/report/sales_report"); ?>" style="color: #007bff;"><i class="fa fa-file-alt"></i> Sales Report</a>
							</div>
							<div class="reportlist">
								<a href="<?= base_url('cms/report/member_report'); ?>" style="color: #007bff;"><i class="fa fa-file-alt"></i> Member Report</a>
							</div>
							<div class="reportlist">
								<a href="<?= base_url('cms/report/event_report'); ?>" style="color: #007bff;"><i class="fa fa-file-alt"></i> Event Report</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>