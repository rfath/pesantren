<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Keamanan</h5>
					<h6 class="sub-heading">List Santri Keluar</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Santri Keluar</div>
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label>Mulai Tanggal :</label>
								<input type="text" class="form-control datepicker" name="tgl_mulai" id="tgl_awal" value="<?= date('m/d/Y') ?>" required="">
							</div>
							<div class="col-md-3">
								<label>Hingga Tanggal :</label>
								<input type="text" class="form-control datepicker" name="tgl_akhir" id="tgl_akhir" value="<?= date('m/d/Y') ?>" required="">
							</div>
							<div class="col-md-3">
								<label>Filter :</label>
								<select class="form-control selectpicker oCh" id="status" data-live-search="true">
									<option value="">ALL</option>
									<option value="0">Belum Kembali</option>
									<option value="1">Sudah Kembali</option>
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-info range_tgl" style="margin-top: 30px;"><i class="far fa-calendar-check"></i> Submit</button>
								<a href="<?= base_url('cms/rekapkeamanan/'); ?>" target="_blank" class="btn btn-primary" id="printjurnal"  style="margin-top: 30px;"><i class="fas fa-print"></i> Print</a>
							</div>
						</div>
					</div>
					<?php echo return_custom_notif(); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Nama Santri</th>
									<th>Keperluan</th>
									<th>Jam Keluar</th>
									<th>Jam Harus Kembali</th>
									<th>Status Kembali</th>
									<th>Keterangan</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODAL 1 -->
<div class="modal fade" id="csantri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form id="delmodalForm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Santri Kembali Masuk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="dataresult">
					
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/rekapkeamanan/get_list_izin'); ?>", 
                type: "POST",
				data: function (d) {
					d.status = $("#status").val();
					d.tgl_awal 		= $("#tgl_awal").val();
					d.tgl_akhir 	= $("#tgl_akhir").val();

					// Ganti format tanggal
					var _dateAr 	= $("#tgl_awal").val().split('/');
					var _dateAr1 	= $("#tgl_akhir").val().split('/');
					// End

					var _tgl_awal 	= _dateAr[2] + '-' + _dateAr[0] + '-' + _dateAr[1];
					var _tgl_akhir	= _dateAr1[2] + '-' + _dateAr1[0] + '-' + _dateAr1[1];

					document.getElementById("printjurnal").href="<?= base_url() ?>cms/rekapkeamanan/printlaporanperizinan/3/"+_tgl_awal+"/"+_tgl_akhir;
                },
                error: function () {
                	
                }
            },
		});

		function reload_table(){
			dataTable.ajax.reload(null,false); 
		}

		$('.range_tgl').click(function(event) {
			reload_table();
			var status 		= $("#status").val();
			// Ganti format tanggal
			var dateAr 		= $("#tgl_awal").val().split('/');
			var dateAr1 	= $("#tgl_akhir").val().split('/');
			// End

			var tgl_awal 	= dateAr[2] + '-' + dateAr[0] + '-' + dateAr[1];
			var tgl_akhir	= dateAr1[2] + '-' + dateAr1[0] + '-' + dateAr1[1];
			// document.getElementById("printjurnal").href="<?= base_url() ?>cms/rekapkeamanan/printperizinan/"+tgl_awal+"/"+tgl_akhir;
			
			if (status == ""){
				document.getElementById("printjurnal").href="<?= base_url() ?>cms/rekapkeamanan/printlaporanperizinan/3/"+tgl_awal+"/"+tgl_akhir;
			}else{
				document.getElementById("printjurnal").href="<?= base_url() ?>cms/rekapkeamanan/printlaporanperizinan/"+status+"/"+tgl_awal+"/"+tgl_akhir;
			}
		});
	});
</script>