	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Edit Asatidz</h5>
					<h6 class="sub-heading">Asatidz</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/asatidz/doUpdate/'.$detailAsatidz->asatidz_id); ?>"  role="form" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Asatidz</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nomor Induk</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="no_induk" placeholder="Nomor Induk" value="<?= $detailAsatidz->asatidz_no_induk ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nomor NUPTK</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="nuptk" placeholder="Nomor NUPTK" value="<?= $detailAsatidz->asatidz_nuptk ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="nama" placeholder="Nama Astidz" value="<?= $detailAsatidz->asatidz_name ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tempat, Tanggal Lahir</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="tempat" placeholder="Tempat Lahir" value="<?= $detailAsatidz->tempat ?>" required="">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control datepicker" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?= date("m/d/Y", strtotime($detailAsatidz->tgl_lahir)) ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nomor HP</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="no_hp" placeholder="Nomor HP" value="<?= $detailAsatidz->asatidz_phone ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Alamat</label>
							<div class="col-md-10">
								<textarea class="form-control" name="alamat" placeholder="Alamat" required=""><?= $detailAsatidz->asatidz_address ?></textarea>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tanggal Mulai Tugas</label>
							<div class="col-md-10">
								<input type="text" class="form-control datepicker" name="tmt" placeholder="TMT" value="<?= date('m/d/Y', strtotime($detailAsatidz->asatidz_tmt)) ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Photo</label>
							<div class="col-md-10">
								<?php 
									// $photo = substr($detailAsatidz->photo, 15);`
								?>
								<!-- <input type="text" class="form-control" name="nama_file" value="<?= base_url($detailAsatidz->photo) ?>" readonly required=""> -->
								<input type="file" class="form-control" name="photo">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>