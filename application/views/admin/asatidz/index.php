<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Asatidz</h5>
					<h6 class="sub-heading">List Asatidz</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Asatidz</div>
				<div class="card-body">
					<div class="form-group">
						<a href="<?= base_url('cms/asatidz/add/'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a>
						<a href="<?= base_url('cms/asatidz/upload/'); ?>" class="btn btn-success btn-sm"><i class="fa fa-file-excel"></i> Upload Asatidz</a>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>No Induk</th>
									<th>No NUPTK</th>
									<th>Nama</th>
									<th>No Telepon</th>
									<th>Alamat</th>
									<th>Tanggal Mulai Tugas</th>
									<th>Lama Pengabdian (Tahun)</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$no = 0;
								foreach ($listData as $value) {
								$no++;
							?>
								<tr>
									<td><?= $no ?></td>
									<td><?= $value->asatidz_no_induk ?></td>
									<td><?= $value->asatidz_nuptk ?></td>
									<td><?= $value->asatidz_name ?></td>
									<td><?= $value->asatidz_phone ?></td>
									<td><?= $value->asatidz_address ?></td>
									<td><?= date("m/d/Y", strtotime($value->asatidz_tmt)) ?></td>
									<td><?= $value->asatidz_lama_pengambian ?></td>
									<td>
										<a href="<?= base_url('cms/asatidz/edit/'.$value->asatidz_id) ?>" class="btn btn-primary btn-sm" title="Edit / Detail" style="width: 30px;"><i class="fa fa-edit"></i></a>
										<button class="btn btn-danger btn-sm" onClick="is_delete('<?= base_url('cms/asatidz/doDelete/'.$value->asatidz_id) ?>')" title="Delete"><i class="fa fa-trash"></i></button>
									</td>
								</tr>
							<?php 
								}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			
		});
	});
</script>