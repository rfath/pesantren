<form action="<?php echo base_url('cms/perkeluar/doKembali/'.$idnya); ?>" method="POST">
	<div class="modal-body">
		<div class="form-group row gutters">
			<label class="col-md-2 col-form-label">Santri</label>
			<div class="col-md-10">
				<input type="text" class="form-control" name="santri" id="namasantri" placeholder="Select Santri"  required="" value="<?= $detailData->siswa_name; ?>" readonly="">
			</div>
		</div>
		<div class="form-group row gutters">
			<label class="col-md-2 col-form-label">Keperluan</label>
			<div class="col-md-10">
				<textarea rows="3" class="form-control" name="alasan" placeholder="Keperluan" required="" readonly=""><?= $detailData->keperluan ?></textarea>
			</div>
		</div>
		<div class="form-group row gutters">
			<label class="col-md-2 col-form-label">Tanggal Keluar</label>
			<div class="col-md-10">
				<input type="text" class="form-control" required="" readonly="" style="background-color: #fff;" value="<?= date("m/d/Y h:i A", strtotime($detailData->created_date)) ?>" readonly="">
			</div>
		</div>
		<div class="form-group row gutters">
			<label class="col-md-2 col-form-label">Harus kembali pada</label>
			<div class="col-md-10">
				<input type="text" class="form-control" required="" readonly="" style="background-color: #fff;" value="<?= date("m/d/Y h:i A", strtotime($detailData->time_limit)) ?>" readonly="">
			</div>
		</div>

		<div class="form-group row gutters">
			<label class="col-md-2 col-form-label">Tanggal Kembali</label>
			<div class="col-md-10">
				<input type="text" class="form-control datetimepicker" name="tanggalkembali" required="" readonly="" style="background-color: #fff;" value="<?= date("m/d/Y h:i A") ?>" readonly="">
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="submit" class="btn btn-primary">Update</button>
	</div>
</form>

<script type="text/javascript">
	$(function() {
		$('.datetimepicker').daterangepicker({
		    "singleDatePicker": true,
		    "showDropdowns": true,
		    "timePicker": true,
		    "autoApply": true,
		    "locale": {
		        "format": "MM/DD/YYYY hh:mm A",
		        "separator": " - ",
		        
		        "firstDay": 1
		    }
		}, function(start, end, label) {
		  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
		});
	});
</script>