<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Keamanan</h5>
					<h6 class="sub-heading">List Santri Keluar</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Santri Keluar</div>
				<div class="card-body">
					<div class="form-group">
						<a href="<?= base_url('cms/perkeluar/add'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a>
						Filter :
						<select class="form-control selectpicker col-md-2 oCh" id="status" data-live-search="true">
							<option value="0">Belum Kembali</option>
							<option value="1">Sudah Kembali</option>
						</select>
					</div>
					<?php echo return_custom_notif(); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Nama Santri</th>
									<th>Keperluan</th>
									<th>Jam Keluar</th>
									<th>Jam Harus Kembali</th>
									<th>Status Kembali</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODAL 1 -->
<div class="modal fade" id="csantri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form id="delmodalForm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Santri Kembali Masuk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="dataresult">
					
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/perkeluar/get_list_izin'); ?>", 
                type: "POST",
				data: function (d) {
					d.status_pulang = $("#status").val();
                },
                error: function () {
                	
                }
            },
		});

		function reload_table(){
			dataTable.ajax.reload(null,false); 
		}

		$('.oCh').change(function(event) {
			reload_table();
		});
	});

	function dokembali(param){
		$("#dataresult").html("Loading Content...");
		$("#csantri").modal("show");
		$.ajax({
			url: '<?php echo base_url('cms/perkeluar/getDetailIzin/?param='); ?>'+param,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			contentTypda: false,
			proccessData: false
		})
		.done(function(e) {
			$("#dataresult").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
</script>