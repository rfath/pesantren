
<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Edit Pelanggaran Siswa</h5>
					<h6 class="sub-heading">Pelanggaran Siswa</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
			<form method="post" action="<?= base_url('cms/pelanggaransantri/doUpdate/').$idnya."?siswa=".$_GET['siswa']; ?>">
				<div class="card">
					<div class="card-header main-head">Edit Pelanggaran Siswa</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pelanggaran</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="pelanggaran" required="">
									<?php
			                            foreach ($pelanggaran as $value) {
			                        ?>
				                        <option value="<?php echo $value['pelanggaran_id'] ?>"<?php if ($value['pelanggaran_id'] == @$detailUser->pelanggaran_id) {
			                            echo "selected";
			                            } ?> ><?php echo $value['pelanggaran_name']; ?>
			                            </option>
			                        <?php } ?>
								</select>
							</div>
						</div>

						<input type="hidden" name="siswa" class="form-control" value="<?= $detailUser->siswaId ?>">

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Hukuman</label>
							<div class="col-md-10">
								<div id="offbank">
									<input type="hidden" value="0" id="bankcount">
									<?php foreach ($hukumansantri->hukuman as $skey => $svalue) { ?>
									<div class="form-row form-bk" id="bnk10<?= $skey ?>">
										<input type="text" class="form-control form-bank col-md-5" name="additional[hukuman][]" placeholder="Hukuman" value="<?= $svalue ?>" requred>
										<label class=" col-form-label">Status</label>
										<select class="form-control col-md-5" data-live-search="true" name="additional[status][]" required="">
										<option value="masih berjalan" <?php if ($hukumansantri->status[$skey] == "masih berjalan") { echo "selected"; }?> >Masih Berjalan</option>
										<option value="selesai" <?php if ($hukumansantri->status[$skey] == "selesai") { echo "selected"; }?> >Selesai</option>
										
										</select>
										<button type="button" class="btn btn-danger btn-sm form-bank col-md-1" title="delete" onClick="deldiv('<?= '10'.$skey ?>')"><i class="fa fa-trash-alt"></i> Hapus</button>
									</div>
									<?php } ?>
								</div>
								<button type="button" class="btn btn-success btn-sm" id="addbank" style="margin-top: 5px;">
									<i class="fa fa-plus"></i> Add 
								</button>
							</div>
						</div>
					</div>

					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$("#addbank").click(function(event) {
		var _cnt = $("#bankcount").val();
		var _cnt = parseInt(_cnt)+1;
		$("#bankcount").val(_cnt);
		var _html = '<div class="form-row form-bk" id="bnk'+_cnt+'">\
				<input type="text" class="form-control form-bank col-md-5" name="additional[hukuman][]" placeholder="Hukuman" requred>\
				<label class=" col-form-label">Status</label>\
				<select class="form-control col-md-5" data-live-search="true" name="additional[status][]" required="">\
				<option value="masih berjalan">Masih Berjalan</option><option value=selesai>Selesai</option></select>\
				<button type="button" class="btn btn-danger btn-sm form-bank col-md-1" title="delete" onClick="deldiv('+_cnt+')"><i class="fa fa-trash-alt"></i> Hapus</button>\
			</div>';
		$("#offbank").append(_html);
	});
	
	function deldiv(_par){
		$("#bnk"+_par).remove();
	}
</script>