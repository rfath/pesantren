<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= base_url('cms/place'); ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Place</h5>
					<h6 class="sub-heading">Place</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/place/doAdd'); ?>">
				<div class="card">
					<div class="card-header main-head">Add Place</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Place name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Place Name" name="place_name" required="">
							</div>
						</div>

						<div class="col-md-12">
				            <div class="col-md-12">
				                <input type="hidden" class="form-control" id="latitude" name="lat" placeholder="latitude" value="-6.17511" readonly="">
				            </div>
				            <div class="col-md-2 col-sm-1">
				                <input type="hidden" class="form-control" id="longitude" name="lng" placeholder="longitude" value="106.86503949999997" readonly="">
				            </div>
				            <div class="input-group col-md-4" style="padding: 0px;">
								<input type="text" class="form-control" id="Postcode" name="Postcode" placeholder="Search your place or street...">
								<span class="input-group-btn">
									<button class="btn btn-primary" id="findbutton" type="button">Search</button>
								</span>
							</div>
				            <div class="row form-group" style="padding: 10px;">

				                <div id="map" style="height: 400px; widows: 100%;" class="col-md-12"></div>
				            </div>
				        </div>

					</div>
					<div class="card-footer">
						<a href="<?= base_url('cms/place'); ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        okmap();
    });

    function okmap(){
        var geocoder = new google.maps.Geocoder();
        var marker = null;
        var map = null;
        initialize();
        $('#findbutton').click(function (e) {
            var address = $("#Postcode").val();
            if(address == ""){
                address = "Jakarta";
            }
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $(latitude).val(marker.getPosition().lat());
                    $(longitude).val(marker.getPosition().lng());
                } else {
                    alert("There no such place.");
                }
            });
            e.preventDefault();
        });

        function initialize() {
            var $latitude = document.getElementById('latitude');
            var $longitude = document.getElementById('longitude');
            var latitude = -6.17511
            var longitude = 106.86503949999997;
            var zoom = 17;

            var LatLng = new google.maps.LatLng(latitude, longitude);

            var mapOptions = {
                zoom: zoom,
                center: LatLng,
                panControl: false,
                zoomControl: false,
                scaleControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            if (marker && marker.getMap) marker.setMap(map);
            marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                title: 'Drag Me!',
                draggable: true
            });

            google.maps.event.addListener(marker, 'dragend', function(marker) {
                var latLng = marker.latLng;
                $latitude.value = latLng.lat();
                $longitude.value = latLng.lng();
            });
        }
    }
</script>