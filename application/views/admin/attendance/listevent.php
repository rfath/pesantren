<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Attendance</h5>
					<h6 class="sub-heading">Select Event</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Event</div>
				<div class="card-body">
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Event Name</th>
									<th>Date Start</th>
									<th>Date End</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($lsitEvent as $key => $value) { ?>
									<tr>
										<td><?= $no++; ?></td>
										<td><?= $value->event_name ?></td>
										<td><?= date("d M Y h:i A",strtotime($value->event_start_date)); ?></td>
										<td><?= date("d M Y h:i A",strtotime($value->event_end_date)); ?></td>
										<td>
											<a href="<?= base_url('cms/attendance/aevent/'.encrypt_decrypt('encrypt',$value->event_id)); ?>" class="btn btn-success btn-sm">Attendance</a>
											<a href="<?= base_url('cms/attendance/chinout/'.encrypt_decrypt('encrypt',$value->event_id)); ?>" class="btn btn-primary btn-sm">Check IN/OUT</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		});
	});
</script>