<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Select Recurring Date</h5>
					<h6 class="sub-heading">Attendance</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<form method="POST" action="<?= base_url('cms/attendance/checkrecurring/'.$id); ?>">
					<div class="card-header main-head">Select recurring date for Event : <span style="color: #3498db;"><?= $eventDetail->event_name; ?></span></div>
					<div class="card-body">
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Recurring Date</label>
							<div class="col-md-3">
								<input type="text" class="form-control datepicker" name="date_start" id="date_start" required="" readonly="" style="background-color: #fff;">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>