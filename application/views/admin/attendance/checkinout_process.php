<table class="tabledetailch">
	<tr>
		<td>Event Name</td>
		<td> : </td>
		<td><?= $scheduleData->event_name ?></td>
	</tr>
	<tr>
		<td>Schedule</td>
		<td> : </td>
		<td><?= $scheduleData->schedule_name ?></td>
	</tr>
</table>
<div></div>
<button type="button" id="chinbtn" class="btn btn-sm btn-success">Check In</button>
<button type="button" id="choutbtn" class="btn btn-sm btn-danger">Check Out</button>
<div class="col-md-3" style="display: inline-block;">
	<select class="form-control form-contorl-sm och" id="chtype">
		<option value="">Sort by : All Type</option>
		<option value="1">Sort by : IN</option>
		<option value="2">Sort by : Out</option>
	</select>
</div>

<div class="table-responsive iblock w100">
	<table width="100%" class="table table-striped" id="datatable">
		<thead>
			<tr>
				<th style="width: 10px;">No</th>
				<th>Attendance Name</th>
				<th>Date Time</th>
				<th>Type</th>
			</tr>
		</thead>
	</table>
</div>

<!-- D MODAL -->
<div class="modal fade" id="chio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="chtitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="msg"></div>
				<input type="text" class="form-control" id="cardnumber" placeholder="Card Number">
				<input type="hidden" name="io" id="io">
				<input type="hidden" name="sci" id="sci" value="<?= $scheduleData->event_schedule_id; ?>">
				<input type="hidden" name="evi" id="evi" value="<?= $scheduleData->event_id; ?>">
				<input type="hidden" name="sesdata" id="sesdata" value="<?= $session; ?>">
			</div>
			<div class="modal-footer">
				<div id="prcbtn" style="display: block;">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="button" id="doProccess" class="btn btn-primary">Submit</button>
				</div>
				<div id="ldbtn" style="display: none;">
					<button type="button" class="btn">Loading Please Wait</button>
				</div>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var dataTable
	$(document).ready(function() {
		dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/attendance/list_attendance_schedule'); ?>", 
                type: "POST",
				data: function (d) {
					d.event    = '<?= encrypt_decrypt('encrypt',$scheduleData->event_id) ?>';
					d.schedule = '<?= encrypt_decrypt('encrypt',$scheduleData->event_schedule_id) ?>';
					d.typeIO   = $("#chtype").val();
					d.session = '<?= $session; ?>';
                },
                error: function () {
                	
                }
            },
		});

		$("#chinbtn").click(function(event) {
			chio(1, "Check In");
		});

		$("#choutbtn").click(function(event) {
			chio(2, "Check Out");
		});

		$('#chio').on('shown.bs.modal', function() {
		  $(this).find('[autofocus]').focus();
		  $("#msg").html("");
		});

		$("#cardnumber").keypress(function(event) {
			if ( event.which == 13 ) {
				$("#doProccess").trigger('click');
			}
		});
	});

	$("#doProccess").click(function(event) {
		var _cardnumber = $("#cardnumber").val();
		if (_cardnumber != "") {
			var _sci     = $("#sci").val();
			var _evi     = $("#evi").val();
			var _io      = $("#io").val();
			var _session = $("#sesdata").val();
			$("#cardnumber").prop('disabled', true);
			$("#prcbtn").fadeOut('fast', function() {
				$("#ldbtn").fadeIn('fast', function() {
					$.ajax({
						url: '<?= base_url('cms/attendance/dochinout/?cn='); ?>'+_cardnumber+'&evi='+_evi+'&sci='+_sci+"&io="+_io+"&ses="+_session,
						type: 'GET',
						dataType: 'json',
						async: true,
						processData: false,
						contentType: false
					})
					.done(function(e) {
						$("#ldbtn").fadeOut('fast', function() {
							$("#prcbtn").fadeIn('fast', function() {
								$("#cardnumber").focus();
							});
						});
						if (e.status == "ok") {
							$("#msg").html(alertCustom("success",e.msg));
							reload_table();
							$("#cardnumber").val("");
						}else{
							$("#msg").html(alertCustom("danger",e.msg));
							$("#cardnumber").val("");
						}
						$("#cardnumber").prop('disabled', false);
					})
					.fail(function() {
						$("#ldbtn").fadeOut('fast', function() {
							$("#prcbtn").fadeIn('fast', function() {
								$("#cardnumber").val("");
								$("#cardnumber").focus();
							});
						});
						$("#msg").html(alertCustom("danger","Something went wrong"));
						$("#cardnumber").prop('disabled', false);
					})
					.always(function() {
						console.log("complete");
					});
				});
			});
		}
	});

	function chio(type, param){
		$("#chtitle").html(param);
		$("#chio").modal("show");
		$("#cardnumber").attr('autofocus',"true");
		$("#io").val(type);
	}

	function reload_table(){
		dataTable.ajax.reload(null,false); 
	}

	$('.och').change(function(event) {
		reload_table();
	});
	
</script>