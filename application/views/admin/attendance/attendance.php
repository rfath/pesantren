<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Attendance Process</h5>
					<h6 class="sub-heading">Attendance</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Attendance</div>
				<div class="card-body">
					<center><h4 class="fw-bold">Event : <?= $eventDetail->event_name; ?></h4></center>
					<br>
					<div class="form-group">
						<button class="btn btn-success btn-sm" id="geta"><i class="fa fa-card"></i>Exchange Card</button>
						<button class="btn btn-success btn-sm" id="geta2" style="display: none;"><i class="fa fa-card"></i>Exchange Card</button>
						<button class="btn btn-primary btn-sm" id="serc"><i class="fa fa-search"></i> Search order</button>
						<a href="<?= base_url('event/manualregisterAttendance/'.$eventDetail->event_id); ?>" target="_blank" class="btn btn-primary btn-sm" id="att"><i class="fa fa-plus"></i> Add New Attendance</a>
						<a href="<?= base_url('cms/attendance/chinout/'.$id) ?>" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i> Check In/Out</a>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Card Number</th>
									<th>E-ticket Number</th>
									<th>Name</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Date Attendance</th>
									<th>Remarks</th>
									<th>Package</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($listAttendant as $key => $value) { ?>
									<tr>
										<td><?= $no++; ?></td>
										<td><?= $value->card_number; ?></td>
										<td><?= $value->eticket; ?></td>
										<td><?= $value->attendance_name; ?></td>
										<td><?= $value->attendance_number; ?></td>
										<td><?= $value->attendance_email; ?></td>
										<td><?= date("d M Y h:i:s A",strtotime($value->updated_date)); ?></td>
										<td><?= $value->payment_remarks; ?></td>
										<td><?= $value->package_name; ?></td>
										<td>
											<button type="button" class="btn btn-sm btn-primary tbl-btn" onClick="editAtt('<?= encrypt_decrypt('encrypt',$value->attendance_id); ?>')"><i class="fa fa-edit"></i></button>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- SEARCH ATTENDANCE BY BARCODE -->
<div class="modal fade" id="attendance_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form action="<?= base_url('cms/attendance/doProccess/'.$id); ?>" method="POST">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Exchange Card</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group row gutters">
						<input type="text" name="scan_order_number" id="onum" class="form-control" required="" placeholder="Scan / Type Order Number">
					</div>
					<div class="form-group row gutters">
						<span class="d1" style="color: green; display: none;"><i class="fa fa-check"></i> Order Number Valid, scan card below to continue.!</span>
						<span class="d2" style="color: red; display: none;"><i class="fa fa-check"></i> <span id="msgerr">Order Number Invalid!</span></span>
					</div>
					<div class="form-group row gutters" id="data-person">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary d1" style="display: none;">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- EDIT ATTENDANCE -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="Edit Modal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?= base_url('cms/attendance/doUpdate/'.$id); ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Edit Attendance</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group row gutters" id="dataedit">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary e1" style="display: none;">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- SEARCH ORDER BY NAME -->
<div class="modal fade" id="search_modal" tabindex="-1" role="dialog" aria-labelledby="Edit Modal" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			
			<div class="modal-header">
				<h5 class="modal-title">Search Order</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group row gutters" id="dtsearch">
					<div class="col-lg-12">
						<div class="input-group form-group">
							<input type="text" class="form-control" placeholder="Search Order Name & E-ticket" aria-label="Search Order Name" id="searchInput">
							<span class="input-group-btn">
								<button class="btn btn-primary" id="searchOrder" type="button">Search</button>
							</span>
						</div>
						<div id="searchResult">
							
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		});
	});

	$("#geta").click(function(event) {
		$("#data-person").html("");	
		$("#onum").val("");
		$(".d1").fadeOut('fast', function() {});
		$(".d2").fadeOut('fast', function() { });
		$("#attendance_modal").modal("show");
	});

	$("#onum").keypress(function(event) {
		if ( event.which == 13 ) {
			event.preventDefault();
      		return false;
		}
	});

	$("#onum").on('input',function(event) {
		var _val = $("#onum").val();
		$(".d1").fadeOut('fast', function() { });
		if (_val != "") {
			searchonum();
		}else{
			$(".d1").fadeOut('fast', function() {});
			$(".d2").fadeOut('fast', function() {});
		}	
	});

	function editAtt(param){
		$("#edit_modal").modal("show");
		$("#dataedit").html("Loading content, please wait.!");
		$(".e1").fadeOut('fast', function() {
			$.ajax({
				url: '<?= base_url("cms/attendance/eattendance/"); ?>'+param,
				type: 'GET',
				dataType: 'html',
				async: true,
				processData: false,
				contentType: false
			})
			.done(function(e) {
				$("#dataedit").html(e);
				$(".e1").fadeIn('fast', function() { });
			})
			.fail(function() {
				$("#dataedit").html("Failed to get content, something went wrong.");
			})
			.always(function() {
				console.log("complete");
			});
			
		});
	}

	$("#serc").click(function(event) {
		$("#searchInput").val("");
		$("#searchResult").html("");
		$("#search_modal").modal("show");
	});

	$("#searchOrder").click(function(event) {
		var _searchKey = $("#searchInput").val();
		$("#searchResult").html("Searching, please wait.");
		$.ajax({
			url: '<?= base_url('cms/attendance/searchOrder/?param='.$id); ?>'+'&searchKey='+_searchKey,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#searchResult").html(e);
		})
		.fail(function() {
			$("#searchResult").html("Error Something Went Wrong.");
		})
		.always(function() {
			console.log("complete");
		});
	});

	$("#searchInput").keypress(function(event) {
		if ( event.which == 13 ) {
			$("#searchOrder").trigger('click');
		}
	});

	function searchonum(){
		var _val = $("#onum").val();
		$("#data-person").html("Searching, Please wait");
		$.ajax({
			url: '<?= base_url('cms/attendance/checkCode/'.$id.'/'); ?>'+_val,
			type: 'GET',
			dataType: 'json',
			processData: false,
			contentType: false
		})
		.done(function(e) {
			if (e.is_valid == "OK") {
				$("#data-person").html("");
				/*LOOPING FOR DATA*/
				$.each(e.attd, function(index, val) {
					console.log(val);
					var _html = '<div class="attddatatxt">Attendance Data</div>\
						<input type="hidden" class="form-control col-md-4" value="'+val.param+'" name="datapart['+index+'][param]" required="">\
						<input type="text" class="form-control col-md-4 iatt" placeholder="E-ticket" value="E-ticket" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="E-ticket" value="'+val.eticket+'" name="datapart['+index+'][eticket]" required="">\
						\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Name" value="Name" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="Name" value="'+val.name+'" name="datapart['+index+'][name]" required="">\
						\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Email" value="Email" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="Email" value="'+val.email+'" name="datapart['+index+'][email]" required="">\
						\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Phone" value="Phone" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="Phone" value="'+val.phone+'" name="datapart['+index+'][phone]" required="">\
						\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Region" value="Region" readonly="">\
						<select class="form-control col-md-8 iatt" name="datapart['+index+'][region]">\
							<option value="">Select Region</option>\
							<?php foreach($listReligion AS $rval){ ?>
								<option value="<?= $rval->region_id ?>" '+selected_data('<?= $rval->region_id ?>',val.region)+' ><?= $rval->region_name; ?></optiom>\
							<?php } ?>
						</select>\
						<input type="text" class="form-control col-md-4 iatt" placeholder="DPW" value="DPW" readonly="">\
						<select class="form-control col-md-8 iatt" name="datapart['+index+'][dpw]">\
							<option value="">Select Dpw</option>\
							<?php foreach($listDPW AS $dval){ ?>
								<option value="<?= $dval->dpw_id ?>" '+selected_data('<?= $dval->dpw_id ?>',val.dpw)+' ><?= $dval->dpw_name; ?></option>\
							<?php } ?>
						</select>\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Satelit Church" value="Satelit Church" readonly="">\
						<select class="form-control col-md-8 iatt" name="datapart['+index+'][satelit_church]">\
							<option value="">Select Satelit Church</option>\
							<?php foreach($listSatelit AS $stval){ ?>
								<option value="<?= $stval->satelit_church_id ?>" '+selected_data('<?= $stval->satelit_church_id ?>',val.sateilite)+' ><?= $stval->satelit_church_name; ?></option>\
							<?php } ?>
						</select>\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Church Role" value="Church Role" readonly="">\
						<select class="form-control col-md-8 iatt" name="datapart['+index+'][church_role]">\
							<option value="">Select Church Role</option>\
							<?php foreach($listChurchR AS $crval){ ?>
								<option value="<?= $crval->church_role_id ?>" '+selected_data('<?= $crval->church_role_id ?>',val.role)+' ><?= $crval->church_role_name; ?></option>\
							<?php } ?>
						</select>\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Gender" value="Gender" readonly="">\
						<select class="form-control col-md-8 iatt" name="datapart['+index+'][gender]">\
							<option value="Male" '+selected_data(val.gender,"Male")+'>Male</option>\
							<option value="Female" '+selected_data(val.gender,"Female")+'>Female</option>\
						</select>\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Age" value="Age" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="Age" value="'+val.age+'" name="datapart['+index+'][age]">\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Remarks" value="Remarks" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="Remarks" value="'+val.remarks+'" name="remarks" readonly="">\
						\
						<input type="text" class="form-control col-md-4 iatt" placeholder="Card Number" value="Card Number" readonly="">\
						<input type="text" class="form-control col-md-8 iatt" placeholder="Scan Card Number" value="" name="datapart['+index+'][card]" required="">';
					$("#data-person").append(_html);
				});
				$(".d1").fadeIn('fast', function() {});
				$(".d2").fadeOut('fast', function() { });
			}else{
				$("#msgerr").html(e.msg);
				$("#data-person").html("");	
				$(".d1").fadeOut('fast', function() {});
				$(".d2").fadeIn('fast', function() { });
			}
		})
		.fail(function() {
			$("#data-person").html("");	
			$(".d1").fadeOut('fast', function() {});
			$(".d2").fadeIn('fast', function() { });
		})
		.always(function() {
			console.log("complete");
		});
	}
</script>
<script type="text/javascript">
	$("#geta2").click(function(event) {
		$("#attendance_modal").modal("show");
	});
	function selectSearch(param){
		$("#data-person").html("Searching, Please wait");
		$("#search_modal").modal("hide");
		$("#geta2").trigger('click');
		$("#onum").val(param);
		setTimeout(function(){ searchonum(); }, 900);
		setTimeout(function(){ $('body').addClass('modal-open');; }, 1300);		  
	}

	function selected_data(_par1, _par2){
		if (_par1 == _par2) {
			return "selected";
		}else{
			return false;
		}
	}
</script>