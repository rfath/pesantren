<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Order Number</th>
				<th>E-ticket Number</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dataOrder as $key => $value) { $orderNumber = orderNumberFormat($value->order_time, $value->order_id); ?>
				<tr>
					<td><?= $orderNumber ?></td>
					<td><?= $value->eticket; ?></td>
					<td><?= $value->attendance_name; ?></td>
					<td><?= $value->attendance_email; ?></td>
					<td><?= $value->attendance_number; ?></td>
					<td><button onClick="selectSearch('<?= $orderNumber ?>')" class="btn btn-primary btn-sm">Select</button></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>