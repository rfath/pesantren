<input type="hidden" class="form-control col-md-4" value="<?= $id; ?>" name="param" required="">
<input type="text" class="form-control col-md-4 iatt" placeholder="E-ticket" value="E-ticket" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="E-ticket" value="<?= $detailData->eticket; ?>" name="eticket" required="">

<input type="text" class="form-control col-md-4 iatt" placeholder="Name" value="Name" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="Name" value="<?= $detailData->attendance_name; ?>" name="att_name" required="">

<input type="text" class="form-control col-md-4 iatt" placeholder="Email" value="Email" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="Email" value="<?= $detailData->attendance_email; ?>" name="att_email" required="">

<input type="text" class="form-control col-md-4 iatt" placeholder="Phone" value="Phone" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="Phone" value="<?= $detailData->attendance_number; ?>" name="att_phone" required="">

<input type="text" class="form-control col-md-4 iatt" placeholder="Region" value="Region" readonly="">
<select class="form-control col-md-8 iatt" name="region">
	<option value="">Select Region</option>
	<?php foreach($listReligion AS $rval){ ?>
		<option value="<?= $rval->region_id ?>" <?= check_selected($rval->region_id,$detailData->region_id); ?> ><?= $rval->region_name; ?></optiom>
	<?php } ?>
</select>

<input type="text" class="form-control col-md-4 iatt" placeholder="DPW" value="DPW" readonly="">
<select class="form-control col-md-8 iatt" name="dpw">
	<option value="">Select Dpw</option>
	<?php foreach($listDPW AS $dval){ ?>
		<option value="<?= $dval->dpw_id ?>" <?= check_selected($dval->dpw_id,$detailData->dpw_id); ?> ><?= $dval->dpw_name; ?></option>
	<?php } ?>
</select>

<input type="text" class="form-control col-md-4 iatt" placeholder="Satelit Church" value="Satelit Church" readonly="">
<select class="form-control col-md-8 iatt" name="satelit_church">
	<option value="">Select Satelit Church</option>
	<?php foreach($listSatelit AS $stval){ ?>
		<option value="<?= $stval->satelit_church_id ?>" <?= check_selected($stval->satelit_church_id,$detailData->satelit_church_id); ?> ><?= $stval->satelit_church_name; ?></option>
	<?php } ?>
</select>

<input type="text" class="form-control col-md-4 iatt" placeholder="Church Role" value="Church Role" readonly="">
<select class="form-control col-md-8 iatt" name="church_role">
	<option value="">Select Church Role</option>
	<?php foreach($listChurchR AS $crval){ ?>
		<option value="<?= $crval->church_role_id ?>" <?= check_selected($crval->church_role_id,$detailData->church_role_id); ?> ><?= $crval->church_role_name; ?></option>
	<?php } ?>
</select>
<input type="text" class="form-control col-md-4 iatt" placeholder="Gender" value="Gender" readonly="">
<select class="form-control col-md-8 iatt" name="gender">
	<option value="Male" <?= check_selected($detailData->gender, "Male"); ?>>Male</option>
	<option value="Female" <?= check_selected($detailData->gender, "Female"); ?>>Female</option>
</select>

<input type="text" class="form-control col-md-4 iatt" placeholder="Age" value="Age" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="Age" value="<?= $detailData->age; ?>" name="age">

<input type="text" class="form-control col-md-4 iatt" placeholder="Remarks" value="Remarks" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="Remarks" value="<?= $detailData->payment_remarks; ?>" name="remarks" readonly="">

<input type="text" class="form-control col-md-4 iatt" placeholder="Package" value="Package" readonly="">
<select class="form-control col-md-8 iatt" name="package">
	<option value="">Change Package</option>
	<?php foreach($listPackage AS $lpack){ ?>
		<option value="<?= $lpack->event_package_id ?>" <?= check_selected($lpack->event_package_id,$detailData->package_id); ?> ><?= $lpack->package_name; ?></option>
	<?php } ?>
</select>

<input type="text" class="form-control col-md-4 iatt" placeholder="Card Number" value="Card Number" readonly="">
<input type="text" class="form-control col-md-8 iatt" placeholder="Scan Card Number" value="<?= $detailData->card_number; ?>" name="att_card" required="">