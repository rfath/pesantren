<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Check In/Out</h5>
					<h6 class="sub-heading">Attendance</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Select Package & Schedule : <span style="color: #3498db;"><?= $eventDetail->event_name; ?></span></div>
				<div class="card-body">
					<div class="form-group row gutters">
						<div class="col-md-4">
							<select class="form-control selectpicker" data-live-search="true" id="selectschedule" name="schedule" required="">
								<option value="">Select Schedule</option>
								<?php foreach ($scheduleData as $key => $value) { ?>
									<option value="<?= $value->event_schedule_id ?>"><?= $value->schedule_name; ?></option>
								<?php } ?>
							</select>
						</div>
						<input type="hidden" id="session" value="<?= $session; ?>">
						<div class="col-md-4">
							<a href="<?= base_url('cms/attendance/aevent/'.$id) ?>" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i> Attendance</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Check In/Out <?php if($session <> ""){ echo " : ".$sessionDetail->recurring_date;} ?></div>
				<div class="card-body" id="datacheck">
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#selectschedule").change(function(event) {
		var _schedule = $("#selectschedule").val();
		if (_schedule != "") {
			loadchin(_schedule);
		}		
	});

	function loadchin(schedule){
		$("#datacheck").html("Loading Content");
		var _session = $("#session").val();
		$.ajax({
			url: '<?= base_url("cms/attendance/chprocess/"); ?>'+ schedule + "?session="+_session,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#datacheck").html(e);
		})
		.fail(function() {
			$("#datacheck").html("No Content Data");
		})
		.always(function() {
			console.log("complete");
		});
	}
</script>
