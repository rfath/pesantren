	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Bendahara</h5>
					<h6 class="sub-heading">Pemasukan dan Pengeluaran Uang Kas</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/bendahara/doUpdate/'.$detailKas->kas_id); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Uang Kas</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama Kas</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="kas_name" placeholder="Nama Kas" value="<?= $detailKas->kas_name ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Jenis Kas</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="kas_type" id="kas_type" required="">
									<option value="">Pilih Jenis Kas</option>
									<option value="IN" <?php ($detailKas->kas_type == "IN") ? print_r("selected") : print_r("")?>>IN</option>
									<option value="OUT" <?php ($detailKas->kas_type == "OUT") ? print_r("selected") : print_r("")?>>OUT</option>
									<option value="IURAN" <?php ($detailKas->kas_type == "IURAN") ? print_r("selected") : print_r("")?>>OUT</option>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nominal</label>
							<div class="col-md-10">
								<input type="number" class="form-control" name="nominal" placeholder="Nominal" value="<?= $detailKas->kas_transaction ?>" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tanggal</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="tgl_trans" placeholder="Tanggal Transaksi" value="<?= date("Y-m-d H:i:s") ?>"  required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Bukti Pembayaran</label>
							<div class="col-md-10">
								<input type="file" class="form-control" name="photo">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>