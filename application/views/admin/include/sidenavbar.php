<?php 

$uri = $this->uri->segment(2);
if ($uri == "" || $uri == "dashboard") {
	$dashActive = "active selected";
}

if ($uri == "place") {
	$placeActive = "active selected";
}

?>
<!-- BEGIN .app-side -->
<aside class="app-side" id="app-side">
	<!-- BEGIN .side-content -->
	<div class="side-content ">
		<!-- BEGIN .user-profile -->
		<div class="user-profile">
			<img src="<?= base_url('assets_ticketing/back_end/') ?>img/pesantren_logo_med.png" class="profile-thumb" alt="User Thumb">
			<h6 class="profile-name"><?php echo $this->session->sessionData['name']; ?></h6>
			<ul class="profile-actions">
				
			</ul>
		</div>
		<!-- END .user-profile -->
		<!-- BEGIN .side-nav -->
		<nav class="side-nav">
			<!-- BEGIN: side-nav-content -->
			<ul class="unifyMenu" id="unifyMenu">

				<li class="menu-header">
					-- Dashboard
				</li>
				<li class="<?= isset($dashActive)?$dashActive:''; ?>">
					<a href="<?= base_url('cms/dashboard'); ?>">
						<span class="has-icon">
							<i class="icon-laptop_windows"></i>
						</span>
						<span class="nav-title">Dashboard</span>
					</a>
				</li>
				<!-- <?php 
				$listModule    = $this->session->sessionModule;
				$listSubmodule = $this->session->sessionSubmodule;
				foreach ($listModule as $vModule) {
					?>
					<li <?php ($uri == $vModule->module_url) ? print_r("class='active selected'") : print_r("") ?> >
						<a href="<?= base_url('cms/'.$vModule->module_url) ?>" <?php ($vModule->isParent <> 0) ? print_r("class='has-arrow'") : print_r("") ?> aria-expanded="false">
							<span class="has-icon">
								<i class="<?= $vModule->module_icon ?> fa-sm"></i>
							</span>
							<span class="nav-title"><?= $vModule->module_name ?> </span>
						</a>
						<?php 
						if ($vModule->isParent <> 0) {
							?>
							<ul aria-expanded="false">
								<?php 
								foreach ($listSubmodule as $vSubmodule) {
									if ($vSubmodule->module_id == $vModule->module_id) {
										?>
										<li>
											<a href='<?= base_url('cms/'.$vSubmodule->submodule_url) ?>' <?php if($uri == $vSubmodule->submodule_url){ echo "class='current-page'";} ?>><?= $vSubmodule->submodule_name ?></a>
										</li>
										<?php
									}
								}
								?>
							</ul>
						<?php } ?>
					</li>
					<?php 
				}
				?> -->

				<?php 
					$menu = $this->session->menusession; 
					//debugCode($menu);
					foreach ($menu as $mnkey => $mnvalue) {
						?>
						<?php if($mnvalue['isParent'] == 0){ ?>
							<li class="<?php if($uri == $mnvalue['module_url']){ echo 'active selected'; }?>">
								<a href="<?= base_url('cms/'.$mnvalue['module_url']); ?>">
									<span class="has-icon">
										<i class="<?= $mnvalue['module_icon']; ?>"></i>
									</span>
									<span class="nav-title"><?php echo $mnvalue['module_name']; ?></span>
								</a>
							</li>
						<?php }else{ ?>
							<?php 
							$active = "";
							foreach ($mnvalue['sub'] as $sbskey => $sbsvalue) {
								if ($sbsvalue['submodule_url'] == $uri) {
									$active = "active selected";
								}
							}
							?>
							<li class="<?= $active; ?>">
								<a href="#" class="has-arrow" aria-expanded="false">
									<span class="has-icon">
										<i class="<?= $mnvalue['module_icon']; ?>"></i>
									</span>
									<span class="nav-title"><?php echo $mnvalue['module_name']; ?></span>
								</a>
								<ul aria-expanded="false">
									<?php foreach ($mnvalue['sub'] as $sbkey => $sbvalue) { ?>
									<li>
										<a href="<?php echo base_url('cms/'.$sbvalue['submodule_url']); ?>" <?php if($sbvalue['submodule_url'] == $uri){ echo "class='current-page'"; } ?>><?php echo $sbvalue['submodule_name']; ?></a>
									</li>
									<?php } ?>
								</ul>
							</li>
						<?php } ?>
						<?php
					}
				?>
			</ul>
			<!-- END: side-nav-content -->
		</nav>
		<!-- END: .side-nav -->
	</div>
	<!-- END: .side-content -->
</aside>
<!-- END: .app-side -->