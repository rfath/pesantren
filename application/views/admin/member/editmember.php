	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Member</h5>
					<h6 class="sub-heading">Member</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/member/doUpdate'); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Add Member</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<!-- <li class="custom-separator">
							-- Event Detail
						</li> -->
						<input type="hidden" name="param" value="<?= $id; ?>">
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Member Name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="member_name" placeholder="Member Name" value="<?= $detailMember->member_name ?>"  required="">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Email</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="member_email" placeholder="Email" value="<?= $detailMember->member_email ?>"  required="">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Phone</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="member_phone" placeholder="Phone" value="<?= $detailMember->member_phone ?>"  required="">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Birth Date</label>
							<div class="col-md-10">
								<input type="text" class="form-control datepicker" name="member_birthdate" value="<?= date('m-d-Y', strtotime($detailMember->member_birthdate)) ?>" required="" readonly="" style="background-color: #fff;">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Address</label>
							<div class="col-md-10">
								<textarea rows="3" class="form-control" name="member_address" placeholder="Address"><?= $detailMember->member_address ?></textarea>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Province</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="province" id="province" required="">
									<option value="<?= $province_id->province_id ?>"><?= $province_id->province_name ?></option>
									<?php foreach ($provice as $key => $value) { ?>
										<?php if ($detailMember->province_id == $value->province_id): ?>
										<?php else: ?>
											<option value="<?= $value->province_id ?>"><?= $value->province_name ?></option>
										<?php endif ?>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">City</label>
							<div class="col-md-10">
								<select class="form-control" id="city" name="city">
									<option value="<?= $city_id->city_id ?>"><?= $city_id->city_name ?></option>
								</select>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Gender</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="gender" id="gender" required="">
									<option value="Male" <?= check_selected("Male",$detailMember->gender); ?>>Male</option>
									<option value="Female" <?= check_selected("Female",$detailMember->gender); ?>>Female</option>
								</select>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Password</label>
							<div class="col-md-10">
								<input type="password" class="form-control" name="member_password" placeholder="Password">
							</div>
						</div>

					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#province").change(function(event) {
		_myval = $("#province").val();
		$.ajax({
			url: '<?= base_url('cms/member/getCityByProvince/'); ?>'+_myval,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#city").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
</script>