<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="icon-laptop_windows"></i>
				</div>
				<div class="page-title">
					<h5>Dashboard</h5>
					<h6 class="sub-heading">Welcome</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<div class="main-content" style="display: none;">
	<hr style="border-color: #007ae1 !important;">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					Filter : 
					<input type="text" class="form-control col-md-3 iblock" id="datedashboard" value="<?= date("m/d/Y").' - '.date("m/d/Y") ?>">
					<button class="btn btn-sm btn-primary" onclick="filterDashboard()">Filter</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row gutters">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="stats-widget">
						<div class="stats-widget-header">
							<i class="icon-megaphone2"></i>
						</div>
						<div class="stats-widget-body">
							<!-- Row start -->
							<ul class="row no-gutters">
								<li class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col">
									<h6 class="title">Total Event</h6>
								</li>
								<li class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col">
									<h4 class="total" id="total-event">0</h4>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="stats-widget">
						<div class="stats-widget-header">
							<i class="icon-wallet"></i>
						</div>
						<div class="stats-widget-body">
							<!-- Row start -->
							<ul class="row no-gutters">
								<li class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col">
									<h6 class="title">Total payment</h6>
								</li>
								<li class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col">
									<h4 class="total" id="total-payment">0</h4>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="stats-widget">
						<div class="stats-widget-header">
							<i class="icon-basket"></i>
						</div>
						<div class="stats-widget-body">
							<!-- Row start -->
							<ul class="row no-gutters">
								<li class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col">
									<h6 class="title">Total Order</h6>
								</li>
								<li class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col">
									<h4 class="total" id="total-order">0</h4>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr style="border-color: #007ae1 !important;">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Active Event</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Event Name</th>
									<th></th>
								</tr>
							</thead>
							<!-- <tbody>
								<?php foreach ($activeEvent as $aekey => $aevalue) { ?>
									<tr>
										<td><?= $aevalue->event_name; ?></td>
										<td>
											<a href="#" target="_blank" class="btn btn-success btn-sm">Attendance</a>
											<a href="#" target="_blank" class="btn btn-primary btn-sm">Check In/Out</a>
										</td>
									</tr>
								<?php } ?>
							</tbody> -->
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr style="border-color: #007ae1 !important;">
	<div class="row gutters">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">10 Last Registration</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Date Registration</th>
								</tr>
							</thead>
							<tbody>
								<!-- <?php foreach ($last_register as $lrkey => $lrvalue) { ?> -->
									<tr>
										<td>Wanda Riswanda</td>
										<td><?= date("d M Y H:i"); ?></td>
									</tr>
								<!-- <?php } ?> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">10 Last Order</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Date Order</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<!-- <?php foreach ($last_order as $lokey => $lovalue) { ?> -->
									<tr>
										<td>Wanda Riswanda</td>
										<td>teuing</td>
										<td>Lunas</td>
									</tr>
								<!-- <?php } ?> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">10 Last Attendance</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Date Attendance</th>
								</tr>
							</thead>
							<tbody>
								<!-- <?php foreach ($last_attendance as $lakey => $lavalue) { ?> -->
									<tr>
										<td>Wayang</td>
										<td><?= date("d M Y H:i"); ?></td>
									</tr>
								<!-- <?php } ?> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">10 Last Check In/Out</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Type</th>
									<th>Date Check In/Out</th>
								</tr>
							</thead>
							<tbody>
								<!-- <?php foreach ($last_inout as $likey => $livalue) { ?> -->
									<tr>
										<td>Waa</td>
										<td></td>
										<td><?= date("d M Y H:i:s"); ?></td>
									</tr>
								<!-- <?php } ?> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#datedashboard").daterangepicker({showDropdowns:!0,autoApply:!0,ranges:{Today:[moment(),moment()],Yesterday:[moment().subtract(1,"days"),moment().subtract(1,"days")],"Last 7 Days":[moment().subtract(6,"days"),moment()],"Last 30 Days":[moment().subtract(29,"days"),moment()],"This Month":[moment().startOf("month"),moment().endOf("month")],"Last Month":[moment().subtract(1,"month").startOf("month"),moment().subtract(1,"month").endOf("month")]},alwaysShowCalendars:!0,startDate:moment().startOf("month"),endDate:moment().endOf("month")},function(t,a,e){console.log("New date range selected: "+t.format("YYYY-MM-DD")+" to "+a.format("YYYY-MM-DD")+" (predefined range: "+e+")")});
		filterDashboard();
	});
	function filterDashboard(){
		$("#total-event").html("Loading Data");
		$("#total-payment").html("Loading Data");
		$("#total-order").html("Loading Data");
		var _filterdate = $("#datedashboard").val();
		$.ajax({
			url: '<?= base_url('cms/dashboard/getDataTotal?daterange='); ?>'+_filterdate,
			type: 'GET',
			dataType: 'JSON',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#total-event").html($.number(e.total_event));
			$("#total-payment").html("Rp. "+$.number(e.total_payment));
			$("#total-order").html($.number(e.total_order));
		})
		.fail(function() {
			$(".total").html("Failed to retrive data");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
</script>