<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Bagi Kelas</h5>
					<h6 class="sub-heading">Pembagian Kelas Siswa</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/pembagiankelas/doUpdate/'.$detailKelas->siswa_id); ?>"  role="form" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Pembagian Kelas Siswa</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tahun Pelajaran</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="tp" id="tp" required="">
									<option value="">Pilih Tahun Pelajaran</option>
									<?php 
										foreach ($tp as $ltp) {
									?>
										<option value="<?= $ltp->tahun_pelajaran_id ?>" <?= check_selected($detailKelas->tahun_pelajaran_id, $ltp->tahun_pelajaran_id) ?>><?= $ltp->tahun_pelajaran_name ?></option>
									<?php 
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Kelas</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="kelas" id="kelas" required="">
									<option value="">Pilih Kelas</option>
									<?php 
										foreach ($kelas as $lKelas) {
									?>
										<option value="<?= $lKelas->kelas_id ?>" <?= check_selected($detailKelas->kelas_id, $lKelas->kelas_id) ?>><?= $lKelas->kelas_name ?></option>
									<?php 
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Rombel</label>
							<div class="col-md-10">
								<select class="form-control" data-live-search="true" name="rombel" id="rombel" required="">
								<?php 
									$rombel = $this->M_kelas->getRombel("kelas_id", $detailKelas->kelas_id);
									foreach ($rombel as $vRombel) {
								?>
									<option value="<?= $vRombel->kelas_rombel_id ?>" <?= check_selected($detailKelas->kelas_rombel_id, $vRombel->kelas_rombel_id) ?>><?= $vRombel->kelas_rombel_name ?></option>
								<?php 
									}
								?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Siswa</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="siswa_nama" id="siswa_nama" placeholder="Nama Siswa" value="<?= $detailKelas->siswa_name ?>" required="">
								<input type="hidden" class="form-control" name="siswa_id" id="siswa_id" placeholder="Nomor Induk" value="<?= encrypt_decrypt("encrypt", $detailKelas->siswa_id) ?>" required="">
								<button type="button" id="cardisantri" class="btn btn-success btn-sm" style="margin-top: 5px;">Cari Santri</button>
							</div>
						</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- D MODAL -->
<div class="modal fade" id="csantri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form id="delmodalForm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Pilih Santri</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-responsive" width="100%" id="datatable">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Santri</th>
									<th>NIK</th>
									<th>NISN</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	$("#kelas").change(function(event) {
		_myval = $("#kelas").val();
		$.ajax({
			url: '<?= base_url('cms/pembagiankelas/getRombelByKelas/'); ?>'+_myval,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#rombel").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/perkeluar/get_list_siswa'); ?>", 
                type: "POST",
				data: function (d) {
					
                },
                error: function () {
                	
                }
            },
		});
	});

	$("#cardisantri").click(function(event) {
		$("#csantri").modal("show");
	});

	function selectsantri(par1,par2){
		$("#siswa_nama").val(par2);
		$("#siswa_id").val(par1);
		$("#csantri").modal("toggle");
	}
</script>