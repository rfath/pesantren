	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Upload Pembagian Kelas</h5>
					<h6 class="sub-heading">Pembagian Kelas</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/pembagiankelas/uploadProses'); ?>"  role="form" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Pembagian Kelas</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); echo return_custom_notif();?>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Download Format</label>
							<div class="col-md-10">
								<a href="<?= base_url('upload/pembagian_kelas/format/pembagian_kelas.xlsx') ?>" class="btn btn-success btn-sm" title="Download Format"><i class="fa fa-file-excel"></i> Download Format</a>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tahun Pelajaran</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="tp" id="tp" required="">
									<option value="">Pilih Tahun Pelajaran</option>
									<?php 
										foreach ($tp as $ltp) {
									?>
										<option value="<?= $ltp->tahun_pelajaran_id ?>"><?= $ltp->tahun_pelajaran_name ?></option>
									<?php 
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Kelas</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="kelas" id="kelas" required="">
									<option value="">Pilih Kelas</option>
									<?php 
										foreach ($kelas as $lKelas) {
									?>
										<option value="<?= $lKelas->kelas_id ?>"><?= $lKelas->kelas_name ?></option>
									<?php 
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Rombel</label>
							<div class="col-md-10">
								<select class="form-control" data-live-search="true" name="rombel" id="rombel" required="">
									<option value="">Pilih Rombel</option>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Upload File</label>
							<div class="col-md-10">
								<input type="file" class="form-control" name="file">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	$("#kelas").change(function(event) {
		_myval = $("#kelas").val();
		$.ajax({
			url: '<?= base_url('cms/pembagiankelas/getRombelByKelas/'); ?>'+_myval,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#rombel").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
</script>