<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Buku Jurnal</h5>
					<h6 class="sub-heading">Detail Buku Jurnal</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Buku Jurnal</div>
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label>Mulai Tanggal :</label>
								<input type="text" class="form-control datepicker" name="tgl_mulai" id="tgl_awal" value="<?= date("m/d/Y") ?>" required="">
							</div>
							<div class="col-md-3">
								<label>Hingga Tanggal :</label>
								<input type="text" class="form-control datepicker" name="tgl_akhir" id="tgl_akhir" value="<?= date("m/d/Y") ?>" required="">
							</div>
							<div class="col-md-3">
								<label>Filter :</label>
								<select class="form-control selectpicker kas_type" data-live-search="true" name="kas_type" id="kas_type" required="">
									<option value="">ALL</option>
									<option value="IN">IN</option>
									<option value="OUT">OUT</option>
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-info range_tgl" style="margin-top: 30px;"><i class="far fa-calendar-check"></i> Submit</button>
								<a href="<?= base_url(); ?>" class="btn btn-primary" id="printjurnal"  style="margin-top: 30px;"><i class="fas fa-print"></i> Print</a>
							</div>
						</div>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Nama Kas</th>
									<th>Kas Tipe</th>
									<th>Nominal (Rp)</th>
									<th>Tanggal Transaksi</th>
									<th>Nominal Akhir</th>
									<!-- <th>Action</th> -->
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {

	var dataTable = $("#datatable").DataTable({
		"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"processing": true,
        "serverSide": true,
		"pagingType": "full_numbers",
        "ajax": {	
            url: "<?php echo base_url('cms/bukujurnal/get_list_kas'); ?>", 
            type: "POST",
			data: function (d) {
				d.kas_type 	= $("#kas_type").val();
				d.tgl_awal 	= $("#tgl_awal").val();
				d.tgl_akhir = $("#tgl_akhir").val();

				// Ganti format tanggal
				var _dateAr 	= $("#tgl_awal").val().split('/');
				var _dateAr1 	= $("#tgl_akhir").val().split('/');
				// End

				var _tgl_awal 	= _dateAr[2] + '-' + _dateAr[0] + '-' + _dateAr[1];
				var _tgl_akhir	= _dateAr1[2] + '-' + _dateAr1[0] + '-' + _dateAr1[1];
				// Display link awal laporan
				document.getElementById("printjurnal").href="<?= base_url() ?>cms/bukujurnal/printjurnal/ALL/"+_tgl_awal+"/"+_tgl_akhir;
            },
            error: function () {
            	
            }
        },
	});

	function reload_table(){
		dataTable.ajax.reload(null,false); 
	}

	// $('.kas_type').change(function(event) {
	// 	reload_table();
	// });

	$('.range_tgl').click(function(event) {
		reload_table();

		var kas_type 	= $("#kas_type").val();
		// Ganti format tanggal
		var dateAr 	= $("#tgl_awal").val().split('/');
		var dateAr1 	= $("#tgl_akhir").val().split('/');
		// End

		var tgl_awal 	= dateAr[2] + '-' + dateAr[0] + '-' + dateAr[1];
		var tgl_akhir	= dateAr1[2] + '-' + dateAr1[0] + '-' + dateAr1[1];

		if (kas_type == "" && tgl_awal != "" && tgl_akhir != ""){
			document.getElementById("printjurnal").href="<?= base_url() ?>cms/bukujurnal/printjurnal/ALL/"+tgl_awal+"/"+tgl_akhir;
		}else{
			document.getElementById("printjurnal").href="<?= base_url() ?>cms/bukujurnal/printjurnal/"+kas_type+"/"+tgl_awal+"/"+tgl_akhir;
		}
	});

});	
</script>