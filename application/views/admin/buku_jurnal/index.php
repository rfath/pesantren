<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Buku Jurnal</h5>
					<h6 class="sub-heading">Detail Buku Jurnal</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Buku Jurnal</div>
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-3">
								<label>Mulai Tanggal :</label>
								<input type="text" class="form-control datepicker" name="tgl_mulai" id="tgl_awal" value="<?= date("m/d/Y") ?>" required="">
							</div>
							<div class="col-md-3">
								<label>Hingga Tanggal :</label>
								<input type="text" class="form-control datepicker" name="tgl_akhir" id="tgl_akhir" value="<?= date("m/d/Y") ?>" required="">
							</div>
							<div class="col-md-3">
								<label>Filter :</label>
								<select class="form-control selectpicker kas_type" data-live-search="true" name="kas_type" id="kas_type" required="">
									<option value="">ALL</option>
									<option value="IN">IN</option>
									<option value="OUT">OUT</option>
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-info range_tgl" style="margin-top: 30px;" id="submitdata"><i class="far fa-calendar-check"></i> Submit</button>
								<a href="<?= base_url(); ?>" target="_blank" class="btn btn-primary" id="printjurnal"  style="margin-top: 30px; display: none;"><i class="fas fa-print"></i> Print</a>
							</div>
						</div>
					</div>
					<div id="result">
								
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#submitdata").click(function(event) {
		$("#result").html("Loading Please Wait...");
		$("#printjurnal").fadeOut('fast', function() {
			
		});
		var _start = $("#tgl_awal").val();
		var _end   = $("#tgl_akhir").val();
		var _type  = $("#kas_type").val();
		$.ajax({
			url: '<?php echo base_url('cms/bukujurnal/dodetail?type='); ?>'+_type+'&start='+_start+'&end='+_end,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			contentType: false,
			processData: false
		})
		.done(function(e) {
			$("#printjurnal").fadeIn('fast', function() {
				
			});
			// Ganti format tanggal
			var _dateAr 	= $("#tgl_awal").val().split('/');
			var _dateAr1 	= $("#tgl_akhir").val().split('/');
			// End

			var _tgl_awal 	= _dateAr[2] + '-' + _dateAr[0] + '-' + _dateAr[1];
			var _tgl_akhir	= _dateAr1[2] + '-' + _dateAr1[0] + '-' + _dateAr1[1];
			// Display link awal laporan
			var _kas_type = $("#kas_type").val();
			if (_kas_type.length == 0) {
				_kas_type = "ALL";
			}else{
				_kas_type = _kas_type;
			}
			document.getElementById("printjurnal").href="<?= base_url() ?>cms/bukujurnal/printjurnal/"+_kas_type+"/"+_tgl_awal+"/"+_tgl_akhir;

			$("#result").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
</script>