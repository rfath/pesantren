<div style="max-width: 720px;">
	<table width="100%">
		<tr>
            <td style="font-weight: bold;">
                <img src="<?php echo base_url("assets_ticketing/back_end/img/pesantren_logo_med.png") ?>" style="width: 110px;max-width: 150px;margin-top: -5px;">
            </td>
            <td>
                <h4>YAYASAN AL-ISLAM NGANJUK</h4>
                <h5 style="margin-top: -16px;">PONDOK MODERN "AL-ISLAM" NGANJUK</h5>
                <h5 style="margin-top: -16px;">NSPP: 510035180109</h5>
                <p style=";margin-top: -16px;">
                   Alamat: Lingkungan Jatirejo Jl. Raya Sukomoro - Pace Km.1 Kapas, Sukomoro, Nganjuk, Jawa timur, Indonesia<br>
                   Kode Pos: 64481 Telp: (0358) 325096 e-mail: sekretarispondok@gmail.com
               </p>
           </td>
       </tr>
   </table>
   <div style="border: 1px solid;margin-top: 10px;"></div>
   <div style="text-align: center;"><h3><u>Laporan Jurnal</u></h3></div>
<?php 
    if (!empty($this->uri->segment(4))) {
?>
    <div style="text-align: center;">Dari Tanggal : <?= date("m/d/Y", strtotime($this->uri->segment(5))) ?> | Sampai Tanggal : <?= date("m/d/Y", strtotime($this->uri->segment(6))) ?></div><br>
<?php 
    }
?>
<div style="border: 1px solid;"></div>
<div style="min-height: 400px;">
  <table width="100%" class="table table-bordered" id="diagnosCopy" border="1" style="border-collapse: collapse;">
    <tr>
        <th>No</th>
        <th>Nama Kas</th>
        <th>Tipe Kas</th>
        <th>Tanggal Transaksi</th>
        <th>Nominal (RP)</th>
    </tr>
    <?php 
    if (!empty($listJurnal)) {
        ?>
        <?php 
        $no = 0;
        $total = [];
        foreach ($listJurnal as $value) {
            $total[] = $value->kas_transaction;
            $no++;
            ?>
            <tr>
                <td align="center"><?= $no ?></td>
                <td><?= $value->kas_name ?></td>
                <td align="center"><?= $value->kas_type ?></td>
                <td align="center"><?= $value->created_date ?></td>
                <td align="right"><?= number_format($value->kas_transaction) ?></td>
            </tr>
            <?php
        }
        ?>
        <tfoot>
            <tr>
                <td colspan="4" align="right"><b>Total(Rp)</b></td>
                <td align="right"><b><?= number_format(array_sum($total)) ?></b></td>
            </tr>
        </tfoot>
    <?php }else{ ?>
        <tr>
         <td colspan="5" align="center">Tidak ada data.</td>
     </tr>
 <?php } ?>
</table>
</div>
</div>