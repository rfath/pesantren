<div class="table-responsive">
	<table width="100%" id="datatable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th style="width: 10px;">No</th>
				<th>Nama Kas</th>
				<th>Kas Tipe</th>
				<th>Tanggal Transaksi</th>
				<th>Nominal (Rp)</th>
				<th>Nominal Akhir</th>
			</tr>
		</thead>
		<tbody>
			<?php $total = 0; $no=1; foreach ($dataJurnal as $key => $value) { $total+=$value->kas_transaction; ?>
				<tr>
					<td><?= $no++ ?></td>
					<td><?= $value->kas_name ?></td>
					<td><?= $value->kas_type ?></td>
					<td><?= $value->created_date ?></td>
					<td align="right">Rp. <?= number_format($value->kas_transaction) ?></td>
					<td align="right">Rp. <?php echo number_format($value->amount_after) ?></td>
				</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr style="font-weight: bold;">
				<td colspan="4">TOTAL</td>
				<td align="right">Rp. <?= number_format($total); ?></td>
				<td></td>
			</tr>
		</tfoot>
	</table>
</div>

<script type="text/javascript">
	var dataTable = $("#datatable").DataTable({
		"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
	});
</script>