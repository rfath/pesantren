<?php
$name = "Example.xls";
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename='".$name."'");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
	<tr>
		<td>Name</td>
		<td>Email</td>
		<td>Phone</td>
		<td>Gender (F/M)</td>
		<td>Order Date(m/d/Y)</td>
		<td>Payment (CASH/TRANSFER)</td>
		<td>Bank Name</td>
		<td>Acc Number</td>
		<td>Acc Name</td>
		<td>Remarks</td>
	</tr>
	<tr>
		<td>Johnny Deep</td>
		<td>jhony@gmail.com</td>
		<td>+6877887462321</td>
		<td>M</td>
		<td>05/20/2018</td>
		<td>TRANSFER</td>
		<td>BCA</td>
		<td>9847773287232</td>
		<td>IFGF</td>
		<td></td>
	</tr>
	<tr>
		<td>Johnny Deep (2)</td>
		<td>jhony2@gmail.com</td>
		<td>+6877887462321</td>
		<td>M</td>
		<td>05/20/2018</td>
		<td>CASH</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>