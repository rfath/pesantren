	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Order</h5>
					<h6 class="sub-heading">Order</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Select Member</label>
						<div class="col-md-8">
							<select class="form-control selectpicker" data-live-search="true" id="member">
								<option value="">Select Member</option>
								<?php foreach ($dataMember as $key => $value) { ?>
									<option value="<?= $value->member_id ?>"><?= $value->member_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Select Event</label>
						<div class="col-md-8">
							<select class="form-control selectpicker" data-live-search="true" id="event">
								<option value="">Select Event</option>
								<?php foreach ($dataEvent as $ekey => $evalue) { ?>
									<option value="<?= $evalue->event_id ?>"><?= $evalue->event_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-primary" id="gogo">Go to Order Page</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#gogo").click(function(event) {
		var _member = $("#member").val();
		var _event = $("#event").val();
		if (_event != "" & _member != "") {
			window.open('<?= base_url('event/manualregister/') ?>'+_event+'/'+_member, '_blank');
		}else{
			alert("Please select member or event");
		}
	});
</script>