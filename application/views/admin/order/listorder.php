<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Order</h5>
					<h6 class="sub-heading">List Order</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Order</div>
				<div class="card-body">
					<div class="form-group">
						<a href="<?= base_url('cms/order/add'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Manual Order</a>
						<button type="button" class="btn btn-success btn-sm" id="importOffline"><i class="fa fa-file-excel"></i> Import Offline Order</button>
						Filter :
						<select class="form-control selectpicker col-md-2 oCh" id="event" data-live-search="true">
							<option value="">All Event</option>
							<?php foreach ($listEvent as $key => $value) { ?>
								<option value="<?= $value->event_id ?>"><?= $value->event_name; ?></option>
							<?php } ?>
						</select>
						<select class="form-control selectpicker col-md-2 oCh" id="status" data-live-search="true">
							<option value="">All Status</option>
							<option value="0">Waiting for Payment</option>
							<option value="1">Waiting Admin Approve</option>
							<option value="2">Accepted</option>
							<option value="3">Rejected</option>
						</select>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); echo return_custom_notif(); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Eticket</th>
									<th>Order Number</th>
									<th>Name Participant</th>
									<th>Event</th>
									<th>Package</th>
									<th>Payment Status</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- D MODAL -->
<div class="modal fade" id="importmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?= base_url('cms/order/doImportData'); ?>" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Import Offline Order</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="dwexample">
						<p>Download example file format! <a href="#" onClick="downloadSample()" class="href-style">Download Here</a></p>
					</div>
					<br>
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Select Event</label>
						<div class="col-md-8">
							<select class="form-control selectpicker" name="eventData" data-live-search="true" id="eveDataModal" required="">
								<option value="">Select Event</option>
								<?php foreach ($listEvent as $zkey => $zvalue) { ?>
									<option value="<?= encrypt_decrypt('encrypt',$zvalue->event_id) ?>"><?= $zvalue->event_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Select Package</label>
						<div class="col-md-8">
							<select class="form-control selectpicker" name="packageData" data-live-search="true" id="packageDataModal" required="">
								<option value="">Select Package</option>
							</select>
						</div>
					</div>
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Import Data</label>
						<div class="col-md-8">
							<input type="file" class="form-control" name="data_file" required="">
							<small class="note-1">Make sure you change Name/Email/Eticket in example, and dont leave Name or Email blank</small>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div id="btn-proccess ds-none">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary">Import</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
	        "serverSide": true,
			"pagingType": "full_numbers",
	        "ajax": {	
	            url: "<?php echo base_url('cms/order/get_list_order'); ?>", 
	            type: "POST",
				data: function (d) {
					d.event = $("#event").val();
					d.status = $("#status").val();
	            },
	            error: function () {
	            	
	            }
	        },
		});
		$(".oCh").on('change',function(event) {
			reload_table();
		});

		function reload_table(){
		  dataTable.ajax.reload(null,false); 
		}
	});

	$("#importOffline").click(function(event) {
		$("#importmodal").modal("show");
	});

	function downloadSample(){
		var _evData = $("#eveDataModal").val();
		window.open('<?= base_url('tests/Example.xls'); ?>', '_blank');
	}

	$("#eveDataModal").change(function(event) {
		var _evData = $("#eveDataModal").val();
		$.ajax({
			url: '<?= base_url('cms/order/packageData/'); ?>'+_evData,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#packageDataModal").html(e);
			$(".selectpicker").selectpicker('refresh');
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
</script>