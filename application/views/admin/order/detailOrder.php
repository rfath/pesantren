	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Detail Order</h5>
					<h6 class="sub-heading">Order</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->
<?php 
$readonly = "";
if($detailOrder->payment_status == 2){
	$readonly = "readonly";
}
?>
<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/order/doProccess'); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Detail Order</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<!-- <li class="custom-separator">
							-- Event Detail
						</li> -->
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Order Number</label>
							<div class="col-md-10">
								<input type="hidden" name="param" value="<?= $id; ?>">
								<input type="text" class="form-control" placeholder="Order Number"  required="" readonly="" value="<?= $order_number; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">E-ticket Number</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="E-ticket Number" name="eticket" value="<?= $detailOrder->eticket; ?>" <?= $readonly; ?> >
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Status</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Status"  required="" readonly="" value="<?= $status_payment; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Event Name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Event Name"  required="" readonly="" value="<?= $detailOrder->event_name ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Participant Name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Participant Name" name="part_name"  required="" readonly="" value="<?= $detailOrder->member_name ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Participant Email</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Participant Name" name="part_mail"  required="" readonly="" value="<?= $detailOrder->member_email ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Participant Phone</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Participant Name" name="part_phone"  required="" readonly="" value="<?= $detailOrder->member_phone ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Gender</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Gender" name="gender"  required="" readonly="" value="<?= $detailOrder->gender ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Age</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Age" name="age"  required="" readonly="" value="<?= $detailOrder->age ?>">
							</div>
						</div>
						<hr>
						<?php foreach ($additional as $key => $value) { ?>
							<h5>Additional Participant</h5>
							<br>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Additional Participant Name" name="additional[<?= $key ?>][name]"  required="" readonly="" value="<?= $value->name; ?>">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">E-ticket</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Additional Participant Eticket" name="additional[<?= $key ?>][eticket]" value="<?= $value->eticket; ?>" <?= $readonly; ?>>
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Email</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Additional Participant Email" name="additional[<?= $key ?>][email]"  required="" readonly="" value="<?= $value->email; ?>">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Phone Number</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Additional Participant Phone" name="additional[<?= $key ?>][phpone]"  required="" readonly="" value="<?= $value->phone; ?>">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Gender</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Additional Participant Gender" name="additional[<?= $key ?>][gender]" required="" readonly="" value="<?= $value->gender; ?>">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Age</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Age Participant Gender" name="additional[<?= $key ?>][age]" required="" readonly="" value="<?= $value->age; ?>">
								</div>
							</div>
							<hr>
						<?php } ?>
						

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Seat</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Seat"  required="" readonly="" value="<?= $seat; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Package</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Package"  required="" readonly="" value="<?= $detailOrder->package_name; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Total Price</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Total Price"  required="" readonly="" value="Rp. <?= number_format($detailOrder->order_price); ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Payment</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Payment"  required="" readonly="" value="<?= $payment ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Remarks</label>
							<div class="col-md-10">
								<input type="text" class="form-control" placeholder="Remarks"  required="" readonly="" value="<?= $detailOrder->payment_remarks ?>">
							</div>
						</div>
						<?php if($payData->pay_type == 2 AND $detailOrder->payment_file <> ""){ ?>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Confirmation File</label>
								<div class="col-md-10">
									<a class="btn btn-primary btn-sm" href="<?= base_url($detailOrder->payment_file); ?>" target="_blank"><i class="fa fa-download"></i> Download / View File</a>
								</div>
							</div>
						<?php } ?>
						<!-- <?php if($detailOrder->payment_status == 0 OR $detailOrder->payment_status == 1){ ?>
							<hr>
							<i>Please use "-" on set card number if you want to reject</i> <br><br>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Set Card Number</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Set Card Number" name="card_number"  required="" value="">
								</div>
							</div>
							<?php foreach ($additional as $akey => $avalue) { ?>
								<div class="form-group row gutters">
									<label class="col-md-2 col-form-label">Set Card Number <?= $avalue->name; ?></label>
									<div class="col-md-10">
										<input type="text" class="form-control" name="additional[<?= $akey ?>][card_number]" placeholder="Set Card Number" name="card_number"  required="" value="">
									</div>
								</div>
							<?php } ?>
						<?php } ?> -->
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<?php if($detailOrder->payment_status == 0 OR $detailOrder->payment_status == 1){ ?>
							<button type="submit" name="accept" class="btn btn-success"><i class="fa fa-check"></i> Accept</button>
							<button type="submit" name="reject" class="btn btn-danger"><i class="fa fa-times"></i> Reject</button>
						<?php } ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>