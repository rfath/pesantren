<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Edit Prestasi</h5>
					<h6 class="sub-heading">Prestasi</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
			<form method="post" action="<?= base_url('cms/prestasisiswa/doUpdate/').$idnya."?siswa=".$_GET['siswa'];; ?>">
				<div class="card">
					<div class="card-header main-head">Edit prestasi</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pelanggaran</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="prestasi" required="">
									<?php
			                            foreach ($prestasi as $value) {
			                        ?>
				                        <option value="<?php echo $value['prestasi_id'] ?>"<?php if ($value['prestasi_id'] == @$detailUser->prestasi_id) {
			                            echo "selected";
			                            } ?> ><?php echo $value['prestasi_name']; ?>
			                            </option>
			                        <?php } ?>
								</select>

							</div>
						</div>
						<input type="hidden" name="siswa" class="form-control" value="<?= $detailUser->siswaId ?>">

					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>