<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Add Prestasi Siswa</h5>
					<h6 class="sub-heading">Prestasi Siswa</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
			<form method="post" action="<?= base_url('cms/prestasisiswa/doAdd')."?siswa=".$_GET['siswa'];; ?>">
				<div class="card">
					<div class="card-header main-head">
					Nama Siswa:
						<?php 
							foreach ($prestasi_siswa as $value) {
								if ($value['siswa_id']==$siswa) {
									echo $value['siswa_name'];
								}
							}
						 ?>
					</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Prestasi</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="prestasi" required="">
									<?php
			                            foreach ($prestasi as $value) {
			                        ?>
				                        <option value="<?php echo $value['prestasi_id'] ?>" ><?php echo $value['prestasi_name']; ?>
			                            </option>
			                        <?php } ?>
								</select>
							</div>
						</div>
						
	                        <input type="hidden" name="siswa" class="form-control" value="<?= $siswa ?>">
						<!-- <input type="hidden" name="siswa" class="form-control" value="<?= $detailUser->siswaId ?>"> -->
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Prestasi DatE Achive</label>
							<div class="col-md-10">
								<input type="text" class="form-control datepicker" name="dateachive" required="" readonly="" style="background-color: #fff;">
							</div>
						</div>

					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('.datepicker').daterangepicker({
		    "singleDatePicker": true,
		    "showDropdowns": true,
		    "timePicker": false,
		    "autoApply": true,
		    "locale": {
		        "format": "MM/DD/YYYY",
		        "separator": " - ",
		        
		        "firstDay": 1
		    }
		}, function(start, end, label) {
		  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
		});
	});

	// $("#date_start").change(function(event) {
	// 	var _startDate = new Date($("#date_start").val());
	// 	var _endDate = new Date($("#date_end").val());

	// 	if (_endDate < _startDate) {
	// 		$("#date_end").val($("#date_start").val());
	// 	}
	// });
</script>