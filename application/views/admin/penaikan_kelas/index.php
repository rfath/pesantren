<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<i class="fa fa-angle-down"></i>
				</div>
				<div class="page-title">
					<h5>Posting Naik Kelas</h5>
					<h6 class="sub-heading">Naik Kelas / Lulus </h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">Data Siswa Kelas</div>
				<div class="card-body">
					<!-- <div class="form-group">
						<a href="<?= base_url('cms/pembagiankelas/add/'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a>
						<a href="<?= base_url('cms/pembagiankelas/upload/'); ?>" class="btn btn-success btn-sm"><i class="fa fa-file-excel"></i> Upload Pembagian Kelas</a>
					</div> -->
					<div class="form-group row gutters">
						<label class="col-md-2 col-form-label">Tahun Pelajaran</label>
						<div class="col-md-10">
							<select class="form-control selectpicker kelas" data-live-search="true" name="tp" id="tp" required="">
								<option value="">Pilih Tahun Pelajaran</option>
								<?php 
								foreach ($tp as $ltp) {
									?>
									<option value="<?= $ltp->tahun_pelajaran_id ?>"><?= $ltp->tahun_pelajaran_name ?></option>
									<?php 
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group row gutters" id="group_kelas">
						<label class="col-md-2 col-form-label">Kelas</label>
						<div class="col-md-10">
							<select class="form-control selectpicker kelas" data-live-search="true" name="kelas" id="kelas" required="">
								<option value="">Pilih Kelas</option>
								<?php 
								foreach ($kelas as $lKelas) {
									?>
									<option value="<?= $lKelas->kelas_id ?>"><?= $lKelas->kelas_name ?></option>
									<?php 
								}
								?>
							</select>
						</div>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Tahun Pelajar</th>
									<th>Kelas</th>
									<th>Rombel</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- D MODAL -->
<div class="modal fade" id="csantri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">		
		<form method="post" action="<?= base_url('cms/penaikankelas/doAdd'); ?>"  role="form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">List Santri</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="filter">
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tipe</label>
							<div class="col-md-10">
								<select class="form-control selectpicker kelas" data-live-search="true" name="type" id="tipe">
									<option value="1">Penaikan Kelas</option>
									<option value="2">Kelulusan</option>
								</select>
							</div>
						</div>
						
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Ke Tahun Pelajaran</label>
							<div class="col-md-10">
								<select class="form-control selectpicker kelas" data-live-search="true" name="tp_siswa" required="">
									<option value="">Pilih Tahun Pelajaran</option>
									<?php 
									foreach ($tp as $ltp) {
										?>
										<option value="<?= $ltp->tahun_pelajaran_id ?>"><?= $ltp->tahun_pelajaran_name ?></option>
										<?php 
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters" id="kelasnya">
							<label class="col-md-2 col-form-label">Naik Ke Kelas</label>
							<div class="col-md-10">
								<select class="form-control selectpicker kelas" data-live-search="true" name="kelas_siswa" id="kelas_siswa" required="">
									<option value="">Naik Ke kelas</option>
									<?php 
									foreach ($kelas as $lKelas) {
										?>
										<option value="<?= $lKelas->kelas_id ?>"><?= $lKelas->kelas_name ?></option>
										<?php 
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-responsive" width="100%" id="datatable1">
							<thead>
								<tr>
									<th>No</th>
									<th>Nomor Induk</th>
									<th>Nama Santri</th>
									<th>Kelas</th>
									<th>Rombel</th>
									<th>Tahun Pelajaran</th>
									<th>Naik/Lulus</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
			"serverSide": true,
			"pagingType": "full_numbers",
			"ajax": {	
				url: "<?php echo base_url('cms/penaikankelas/get_list_siswa_rombel'); ?>", 
				type: "POST",
				data: function (d) {
					d.tahun_pelajaran = $("#tp").val();
					d.kelas = $("#kelas").val();
				},
				error: function () {

				}
			},
		});

		function reload_table(){
			dataTable.ajax.reload(null,false); 
		}

		$('.kelas').change(function(event) {
			reload_table();
		});
	});

// $("#cardisantri").click(function(event) {
// 	$("#csantri").modal("show");
// });

function cardisantri(par1,par2, par3){
	$(document).ready(function() {
		var dataTable1 = $("#datatable1").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
			"serverSide": true,
			"pagingType": "full_numbers",
			"bDestroy": true,
			"ajax": {	
				url: "<?php echo base_url('cms/penaikankelas/get_siswa_kelas_detail'); ?>", 
				type: "POST",
				data: function (d) {
					d.tahun_pelajaran = par1;
					d.rombel = par2;
					d.kelas_id = par3;
				},
				error: function () {

				}
			},
		});

		dataTable.ajax.reload(null,false);
	});

	$("#csantri").modal("show");
	// $("#namasantri").val(par2);
	// $("#idsantri").val(par1);
	// $("#csantri").modal("toggle");

	
}
$("#tipe").change(function(event) {
	var _val = $("#tipe").val();
	if (_val == 2) {
		$("#kelasnya").fadeOut('fast', function() { });
		$("#kelas_siswa").removeAttr('required');
	}else{
		$("#kelasnya").fadeIn('fast', function() { });
		$("#kelas_siswa").attr('required','');
	}
});
</script>