<div style="max-width: 720px;">
	<table width="100%">
		<tr>
            <td style="font-weight: bold;">
                <img src="<?php echo base_url("assets_ticketing/back_end/img/pesantren_logo_med.png") ?>" style="width: 110px;max-width: 150px;margin-top: -5px;">
            </td>
			<td>
                <h4>YAYASAN AL-ISLAM NGANJUK</h4>
                <h5 style="margin-top: -16px;">PONDOK MODERN "AL-ISLAM" NGANJUK</h5>
                <h5 style="margin-top: -16px;">NSPP: 510035180109</h5>
				<p style=";margin-top: -16px;">
					Alamat: Lingkungan Jatirejo Jl. Raya Sukomoro - Pace Km.1 Kapas, Sukomoro, Nganjuk, Jawa timur, Indonesia<br>
                    Kode Pos: 64481 Telp: (0358) 325096 e-mail: sekretarispondok@gmail.com
				</p>
			</td>
		</tr>
	</table>
	<div style="border: 1px solid;margin-top: 10px;"></div>
    <div style="text-align: center;"><u>Bukti Pembayaran</u></div>
    <div style="text-align: center;">Nomor Kwitansi : <?= $detailSiswa->siswa_nomor_induk."/".date("ymdhis", strtotime($dataKas->created_date))."/".rand(10, 1000)  ?></div><br>
	<table>
		<tr>
			<td>Nomor Induk</td>
            <td>:</td>
			<td><?= $detailSiswa->siswa_nomor_induk ?></td>
		</tr>
		<tr>
			<td>Nama Santri</td>
            <td>:</td>
			<td><?= $detailSiswa->siswa_name ?></td>
		</tr>
		<tr>
			<td>Kelas</td>
            <td>:</td>
			<td><?= $detailSiswa->kelas_name ?></td>
		</tr>
        <tr>
            <td>Rombel</td>
            <td>:</td>
            <td><?= $detailSiswa->kelas_rombel_name ?></td>
        </tr>
		<tr>
			<td>Tanggal Bayar</td>
            <td>:</td>
			<td><?= $dataKas->created_date ?></td>
		</tr>
        <tr>
            <td>Title Pembayaran</td>
            <td>:</td>
            <td><b><?= $pembayaran->pembayaran_title ?></b></td>
        </tr>
	</table>
	<div style="border: 1px solid;"></div>
	<div style="min-height: 400px;">
		<table width="100%" class="table table-bordered" id="diagnosCopy" border="1" style="border-collapse: collapse;">
                            <tr>
                                <th>No</th>
                                <th>Detial Pembayaran</th>
                                <th>Nominal</th>
                            </tr>
                        <?php 
                            $no = 0;
                            $total = [];
                            foreach ($installItem as $vInstallItem) {
                            $total[] = $vInstallItem->price;
                            $no++;
                        ?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td><?= $vInstallItem->title ?></td>
                                <td align="right"><?= number_format($vInstallItem->price) ?></td>
                            </tr>
                        <?php
                            }
                        ?>
                            <tfoot>
                                <tr>
                                    <td colspan="2" align="right"><b>Total Bayar (Rp)</b></td>
                                    <td align="right"><b><?= number_format(array_sum($total)) ?></b></td>
                                </tr>
                            </tfoot>
                        </table>
	</div>
	
    <div style="border: 1px solid;"></div>
    <table width="100%">
    	<tr>
    		<td></td>
    		<td style="text-align: right;"><?= date("d/m/Y") ?></td>
    	</tr>
        <tr>
            <td></td>
            <td style="text-align: right;">Petugas</td>
        </tr>
        <br><br><br><br><br>
        <tr>
            <td></td>
            <td style="text-align: right;"><?= $this->sessionData['name'] ?></td>
        </tr>
    </table>
</div>
<script type="text/javascript">
	// window.print();
</script>