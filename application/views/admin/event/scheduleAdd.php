<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Schedule</h5>
					<h6 class="sub-heading">Schedule</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/event/doAddSchedule/'.$id); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Add Schedule</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Schedule Name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="schedule_name" placeholder="Schedule Name"  required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Alias Name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="alias_name" placeholder="Alias Name"  required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Description</label>
							<div class="col-md-10">
								<textarea class="form-control" name="description" placeholder="Description"></textarea>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Time Start</label>
							
							<div class="col-md-3">
								<input type="text" class="form-control datetimepicker" name="time_start" required="" readonly="" style="background-color: #fff;">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Time End</label>
							
							<div class="col-md-3">
								<input type="text" class="form-control datetimepicker" name="time_end" required="" readonly="" style="background-color: #fff;">
							</div>
							
						</div>
						<!-- <div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Time End</label>
							<div class="col-md-3">
								<div class="input-group form-group input-append timepicker1">
									<input type="text" name="time_end" data-format="HH:mm PP" class="form-control" placeholder="Time End" aria-label="Search for..." value="<?= date("h:i A") ?>">
									<span class="input-group-btn add-on">
										<button class="btn btn-primary" type="button"><i class="icon-calendar"></i></button>
									</span>
								</div>
							</div>
						</div> -->
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Room</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="room" required="">
									<?php foreach ($room as $rkey => $rvalue) { ?>
										<option value="<?= $rvalue->room_id ?>"><?= $rvalue->room_name ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Show in front</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="show_in_front" required="">
									<option value="1">Yes</option>
									<option value="2">No</option>
								</select>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Package</label>
							<div class="col-md-10" style="padding-top: 7px;">
								<!-- <select class="form-control selectpicker" data-live-search="true" name="package" required="">
									<?php foreach ($package as $pkey => $pvalue) { ?>
										<option value="<?= $pvalue->event_package_id ?>"><?= $pvalue->package_name ?></option>
									<?php } ?>
								</select> -->
								<?php foreach ($package as $pkey => $pvalue) { ?>
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="package[]" value="<?= $pvalue->event_package_id ?>">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description"><?= $pvalue->package_name; ?></span>
									</label>
								<?php } ?>
							</div>

						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>