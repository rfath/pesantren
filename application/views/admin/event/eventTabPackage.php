<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Event</h5>
					<h6 class="sub-heading">Event</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/event/doAddEventPackage'); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Add Event</div>
					<div class="card-body">
						<div class="tab">
							<button class="tablinks disabled">Event Data</button>
							<button class="tablinks active">Package</button>
							<button class="tablinks">Schedule</button>
						</div>
						<div class="tabcontent">
							<?php echo flashdata_notif("is_success","Yes"); ?>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Event Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Event Name" readonly="" value="<?= $dataEvent->event_name ?>" required="">
									<input type="hidden" class="form-control" name="param" placeholder="Event Name" readonly="" value="<?= $dataEvent->event_id ?>" required="">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Package Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="package_name" placeholder="Package Name"  required="">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Price</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="package_price" placeholder="Package Price"  required="">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Description</label>
								<div class="col-md-10">
									<textarea rows="3" class="form-control" name="package_description" placeholder="Description"></textarea>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Seat</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="package_seat" placeholder="Seat"  required="">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Remaining Seat</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="package_r_seat" placeholder="Remaining Seat"  required="">
								</div>
							</div>

							<h5><strong>List Added Package</strong></h5>
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th>Package Name</th>
											<th>Price</th>
											<th>Seat</th>
											<th>Remaining Seat</th>
											<th>Description</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $ang=0; $no=1; foreach ($listPackage as $pckey => $pcvalue) { $ang+=1; $encID = encrypt_decrypt("encrypt",$pcvalue->event_package_id); ?>
											<tr>
												<td><?= $no++; ?></td>
												<td><?= $pcvalue->package_name; ?></td>
												<td><?= $pcvalue->package_price; ?></td>
												<td><?= $pcvalue->package_seat; ?></td>
												<td><?= $pcvalue->package_remaining_seat; ?></td>
												<td><?= $pcvalue->package_description; ?></td>
												<th><button type="button" class="btn btn-danger btn-sm" onclick="is_delete('<?= base_url("/cms/event/deleteEventPackage/".$id."/".$encID); ?>')" title="Delete"><i class="fa fa-trash"></i></button></th>
											</tr>
										<?php } ?>
									</tbody>
								</table>
								<input type="hidden" value="<?= $ang; ?>" readonly="" id="ang">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back to list event</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Add Package</button>
						<?php if($ang > 0){ ?>
							<button type="button" id="nextschedule" class="btn btn-success">Add Schedule <i class="fa fa-arrow-circle-right"></i></button>
						<?php } ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#addbank").click(function(event) {
		var _cnt = $("#bankcount").val();
		var _cnt = parseInt(_cnt)+1;
		$("#bankcount").val(_cnt);
		var _html = '<div class="form-row form-bk" id="bnk'+_cnt+'">\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][name]" placeholder="Bank Name" requred>\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][number]" placeholder="Number" requred>\
				<input type="text" class="form-control form-bank col-md-4" name="offbank['+(_cnt-1)+'][accname]" placeholder="Account Name" requred>\
				<button class="btn btn-danger btn-sm form-bank" title="delete" onClick="deldiv('+_cnt+')"><i class="fa fa-trash-alt"></i></button>\
			</div>';
		$("#offbank").append(_html);
	});

	$("#nextschedule").click(function(event) {
		var _ang = $("#ang").val();
		if (_ang < 1) {
			alert("Plase Add package first");
		}else{
			window.location.href= '<?= base_url('cms/event/addScheduleEvent/'.$id); ?>';
		}
	});

</script>
