<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Event</h5>
					<h6 class="sub-heading">Event</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/event/doAdd'); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Add Event</div>
					<div class="card-body">
						<!-- START VERTICAL TAB -->
						<div class="tab">
							<button type="button" class="tablinks active" id="evdata">Event Data</button>
							<button type="button" class="tablinks">Package</button>
							<button type="button" class="tablinks">Schedule</button>
						</div>
						<!-- END VERTICAL TAB -->

						<!-- START TAB CONTENT -->
						<div class="tabcontent">
							<?php echo flashdata_notif("is_success","Yes"); ?>
							<li class="custom-separator">
								-- Event Detail
							</li>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Event name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="event_name" placeholder="Event Name"  required="">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Description</label>
								<div class="col-md-10">
									<textarea rows="3" class="form-control" name="event_description" placeholder="Description"></textarea>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Highlight Description</label>
								<div class="col-md-10">
									<textarea rows="3" class="form-control" name="event_h_description" placeholder="Highlight Description"></textarea>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Featured Image</label>
								<div class="col-md-10">
									<label class="custom-file">
										<input type="file" name="feature_image" class="form-control">
									</label>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Logo Image</label>
								<div class="col-md-10">
									<label class="custom-file">
										<input type="file" name="logo_image" class="form-control">
									</label>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Date Start</label>
								<div class="col-md-3">
									<input type="text" class="form-control dateStart" name="date_start" id="date_start" required="" readonly="" style="background-color: #fff;">
								</div>

								<label class="col-md-2 col-form-label">Date End</label>
								<div class="col-md-3">
									<input type="text" class="form-control datepicker" name="date_end" id="date_end" required="" readonly="" style="background-color: #fff;">
								</div>
							</div>
							<!--############################# START RECURRING DATA HERE #############################-->
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label"></label>
								<div class="col-md-8">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="recurring" id="recurring" value="1">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">This is recurring event pattern</span>
									</label>
								</div>
							</div>
							<div id="recurr_event" class="ds-none">
								<div class="form-group row gutters">
									<label class="col-md-2 col-form-label">Repeat</label>
									<div class="col-md-3">
										<select class="form-control selectpicker" data-live-search="true" name="repeat" id="repeat">
											<option value="1">Daily</option>
											<option value="2">Weekly</option>
											<option value="3">Montly</option>
											<option value="4">Yearly</option>
										</select>
									</div>
									<label class="col-md-2 col-form-label">Repeat Every</label>
									<div class="col-md-2">
										<select class="form-control selectpicker" data-live-search="true" name="repeat_every">
											<?php for ($s=1; $s <31 ; $s++) { ?>
												<option value="<?= $s ?>"><?php echo $s; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							
							<!-- 
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Select Days</label>
								<div class="col-md-3">
									<a href="javascript:void(0)" class="href-style" id="checkalldays">Check All</a>
									|
									<a href="javascript:void(0)" class="href-style" id="uncheckalldays">Uncheck All</a>
									<?php for ($i=1; $i <= 7 ; $i++) { ?>
									<label class="custom-control custom-checkbox" style="display: block;">
										<input type="checkbox" class="custom-control-input days-check" name="days" value="<?= $i; ?>">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description"><?= daysByNumber($i); ?></span>
									</label>
									<?php } ?>
								</div>
							</div>
							<br> -->
							<!--############################# END RECURRING DATA HERE #############################-->
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Open Registration Date</label>
								<div class="col-md-3">
									<input type="text" class="form-control datepicker" name="open_reg_date" required="" readonly="" style="background-color: #fff;">
								</div>
								<label class="col-md-2 col-form-label">Close Registration Date</label>
								<div class="col-md-3">
									<input type="text" class="form-control datepicker" name="close_reg_date" required="" readonly="" style="background-color: #fff;">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Total Seat</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="total_seat" placeholder="Total Seat">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Available Seat</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="available_seat" placeholder="Available Seat">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Place</label>
								<div class="col-md-10">
									<select class="form-control selectpicker" data-live-search="true" name="event_place" required="">
										<?php foreach ($place as $key => $value) { ?>
											<option value="<?= $value->place_id ?>"><?= $value->place_name ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<li class="custom-separator">
								-- Contact
							</li>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Contact Email</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="contact_email" placeholder="Contact Email">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Contact Phone</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="contact_phone" placeholder="Contact Phone">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Contact Whatsapp</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="contact_whatsapp" placeholder="Contact Whatsapp">
								</div>
							</div>
							<li class="custom-separator">
								-- Social Media
							</li>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Instagram</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="instagram" placeholder="Instagram">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Twitter</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="twitter" placeholder="Twitter">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Facebook</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="facebook" placeholder="Facebook">
								</div>
							</div>
							<li class="custom-separator">
								-- Other
							</li>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Offline Bank Transfer</label>
								<div class="col-md-10">
									<div id="offbank">
										<input type="hidden" value="0" id="bankcount">
									</div>
									<button type="button" class="btn btn-success btn-sm" id="addbank" style="margin-top: 5px;">
										<i class="fa fa-plus"></i> Add bank
									</button>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Organizer Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="event_organizer" placeholder="Organizer Name">
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Terms and Condition</label>
								<div class="col-md-10">
									<textarea rows="5" class="form-control" name="tac" placeholder="Terms and Condition"></textarea>
								</div>
							</div>
						</div>
						<!-- END TAB CONTENT -->
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#addbank").click(function(event) {
		var _cnt = $("#bankcount").val();
		var _cnt = parseInt(_cnt)+1;
		$("#bankcount").val(_cnt);
		var _html = '<div class="form-row form-bk" id="bnk'+_cnt+'">\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][name]" placeholder="Bank Name" requred>\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][number]" placeholder="Number" requred>\
				<input type="text" class="form-control form-bank col-md-4" name="offbank['+(_cnt-1)+'][accname]" placeholder="Account Name" requred>\
				<button type="button" class="btn btn-danger btn-sm form-bank" title="delete" onClick="deldiv('+_cnt+')"><i class="fa fa-trash-alt"></i></button>\
			</div>';
		$("#offbank").append(_html);
	});
	
	function deldiv(_par){
		$("#bnk"+_par).remove();
	}

	$("#checkalldays").click(function(event) {
		$(".days-check").prop("checked", true);
	});

	$("#uncheckalldays").click(function(event) {
		$(".days-check").prop("checked", false);
	});

	$(function() {
		$('.dateStart').daterangepicker({
		    "singleDatePicker": true,
		    "showDropdowns": true,
		    "timePicker": false,
		    "autoApply": true,
		    "locale": {
		        "format": "MM/DD/YYYY",
		        "separator": " - ",
		        
		        "firstDay": 1
		    }
		}, function(start, end, label) {
		  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
		});
	});

	$("#date_start").change(function(event) {
		var _startDate = new Date($("#date_start").val());
		var _endDate = new Date($("#date_end").val());

		if (_endDate < _startDate) {
			$("#date_end").val($("#date_start").val());
		}
	});

	$("#date_end").change(function(event) {
		var _startDate = new Date($("#date_start").val());
		var _endDate = new Date($("#date_end").val());

		if (_endDate < _startDate) {
			$("#date_start").val($("#date_end").val());
		}
	});

	$("#recurring").click(function(event) {
        if ($('#recurring').is(':checked')) {
            $("#recurr_event").fadeIn('fast', function() {});
        }else{
            $("#recurr_event").fadeOut('fast', function() {});
        }
    });
</script>
