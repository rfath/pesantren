<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>List Schedule</h5>
					<h6 class="sub-heading">Schedule</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header main-head">List Schedule</div>
				<div class="card-body">
					<center><h4 class="fw-bold">Event : <?= $event->event_name; ?></h4></center>
					<div class="form-group">
						<a href="<?= base_url('cms/event/addSchedule/'.$id); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add</a>
						Filter :
						<select class="form-control selectpicker col-md-2 oCh" id="package" data-live-search="true">
							<option value="">All Package</option>
							<?php foreach ($package as $pkey => $pvalue) { ?>
								<option value="<?= $pvalue->event_package_id ?>"><?= $pvalue->package_name ?></option>
							<?php } ?>
						</select>
					</div>
					<?php echo flashdata_notif("is_success","Yes"); ?>
					<div class="table-responsive">
						<table width="100%" id="datatable" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 10px;">No</th>
									<th>Schedule Name</th>
									<th>Package</th>
									<th>Room</th>
									<th>Start Time</th>
									<th>End Time</th>
									<th>Description</th>
									<th>Show in Front</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"processing": true,
            "serverSide": true,
			"pagingType": "full_numbers",
            "ajax": {	
                url: "<?php echo base_url('cms/event/get_list_schedule'); ?>", 
                type: "POST",
				data: function (d) {
					d.event = '<?= $id; ?>';
					d.package = $("#package").val();
                },
                error: function () {
                	
                }
            },
		});
		$(".oCh").on('change',function(event) {
			reload_table();
		});

		function reload_table(){
		  dataTable.ajax.reload(null,false); 
		}
	});

	
</script>