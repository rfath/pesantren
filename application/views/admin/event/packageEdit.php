<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Edit Package</h5>
					<h6 class="sub-heading">Package</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/event/doUpdatePackage/'.$id_ev); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Edit Package</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Package Name</label>
							<div class="col-md-10">
								<input type="hidden" name="param" value="<?= $id; ?>" required="">
								<input type="text" class="form-control" name="package_name" placeholder="Package Name"  required="" value="<?= $detailPackage->package_name ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Price</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="package_price" placeholder="Package package_price"  required="" value="<?= $detailPackage->package_price ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Description</label>
							<div class="col-md-10">
								<textarea rows="3" class="form-control" name="package_description" placeholder="Description"><?= $detailPackage->package_description; ?></textarea>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Seat</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="package_seat" placeholder="Seat"  required="" value="<?= $detailPackage->package_seat; ?>">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Remaining Seat</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="package_r_seat" placeholder="Remaining Seat"  required="" value="<?= $detailPackage->package_remaining_seat; ?>">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>