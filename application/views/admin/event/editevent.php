<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Edit Event</h5>
					<h6 class="sub-heading">Event</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/event/doUpdate'); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Edit Event</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); ?>
						<li class="custom-separator">
							-- Event Detail
						</li>
						<input type="hidden" name="param" value="<?= $id; ?>">
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Event name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="event_name" placeholder="Event Name" value="<?= $detailEvent->event_name ?>"  required="">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Description</label>
							<div class="col-md-10">
								<textarea rows="3" class="form-control" name="event_description" placeholder="Description"><?= $detailEvent->event_description ?></textarea>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Highlight Description</label>
							<div class="col-md-10">
								<textarea rows="3" class="form-control" name="event_h_description" placeholder="Highlight Description"><?= $detailEvent->event_name ?></textarea>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Featured Image</label>
							<div class="col-md-10">

								<label class="custom-file">
									<?php if($detailEvent->event_featured_image <> ""){ ?>
										<a href="<?= base_url($detailEvent->event_featured_image) ?>" target="_blank" style="color: #007bff;">View Current Image</a>
									<?php } ?>
									<input type="file" name="feature_image" class="form-control">
								</label>
								*) Select to change image
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Logo Image</label>
							<div class="col-md-10">
								<label class="custom-file">
									<?php if($detailEvent->event_logo <> ""){ ?>
										<a href="<?= base_url($detailEvent->event_logo) ?>" target="_blank" style="color: #007bff;">View Current Image</a>
									<?php } ?>
									<input type="file" name="logo_image" class="form-control">
								</label>
								*) Select to change image
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Date Start</label>
							<div class="col-md-3">
								<input type="text" class="form-control datepicker" name="date_start" value="<?= $date_start ?>" required="" readonly="" style="background-color: #fff;">
							</div>

							<label class="col-md-2 col-form-label">Date End</label>
							<div class="col-md-3">
								<input type="text" class="form-control datepicker" name="date_end" value="<?= $date_end ?>" required="" readonly="" style="background-color: #fff;">
							</div>
						</div>

						<!--############################# START RECURRING DATA HERE #############################-->
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label"></label>
							<div class="col-md-8">
								<label class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="recurring" id="recurring" value="1" <?= is_checked(1,$detailEvent->is_recurring); ?> >
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">This is recurring event pattern</span>
								</label>
							</div>
						</div>
						<div id="recurr_event" class="<?php if($detailEvent->is_recurring <> 1){ echo 'ds-none';} ?>">
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Repeat</label>
								<div class="col-md-3">
									<select class="form-control selectpicker" data-live-search="true" name="repeat" id="repeat">
										<option value="1" <?= check_selected(1,$detailEvent->recurring_repeat); ?> >Daily</option>
										<option value="2" <?= check_selected(2,$detailEvent->recurring_repeat); ?> >Weekly</option>
										<option value="3" <?= check_selected(3,$detailEvent->recurring_repeat); ?> >Montly</option>
										<option value="4" <?= check_selected(4,$detailEvent->recurring_repeat); ?> >Yearly</option>
									</select>
								</div>
								<label class="col-md-2 col-form-label">Repeat Every</label>
								<div class="col-md-2">
									<select class="form-control selectpicker" data-live-search="true" name="repeat_every">
										<?php for ($s=1; $s <31 ; $s++) { ?>
											<option value="<?= $s ?>" <?= check_selected($s,$detailEvent->recurring_repeat); ?> ><?php echo $s; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Open Registration Date</label>
							<div class="col-md-3">
								<input type="text" class="form-control datepicker" name="open_reg_date" value="<?= $open_registration_date ?>" required="" readonly="" style="background-color: #fff;">
							</div>
							<label class="col-md-2 col-form-label">Close Registration Date</label>
							<div class="col-md-3">
								<input type="text" class="form-control datepicker" name="close_reg_date" value="<?= $close_registration_date ?>" required="" readonly="" style="background-color: #fff;">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Total Seat</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="total_seat" value="<?= $detailEvent->event_total_seat ?>" placeholder="Total Seat">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Available Seat</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="available_seat" value="<?= $detailEvent->event_available_seat ?>" placeholder="Available Seat">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Place</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" data-live-search="true" name="event_place" required="">
									<?php foreach ($place as $key => $value) { ?>
										<option value="<?= $value->place_id ?>" <?= check_selected($value->place_id,$detailEvent->event_place_id) ?> ><?= $value->place_name ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<li class="custom-separator">
							-- Contact
						</li>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Contact Email</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="contact_email" value="<?= $detailEvent->event_contact_email ?>" placeholder="Contact Email">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Contact Phone</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="contact_phone" value="<?= $detailEvent->event_contact_telephone ?>" placeholder="Contact Phone">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Contact Whatsapp</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="contact_whatsapp" value="<?= $detailEvent->event_contact_whatsapp ?>" placeholder="Contact Whatsapp">
							</div>
						</div>
						<li class="custom-separator">
							-- Social Media
						</li>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Instagram</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="instagram" value="<?= $detailEvent->event_social_ig ?>" placeholder="Instagram">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Twitter</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="twitter" value="<?= $detailEvent->event_social_twitter ?>" placeholder="Twitter">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Facebook</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="facebook" value="<?= $detailEvent->event_social_fb ?>" placeholder="Facebook">
							</div>
						</div>
						<li class="custom-separator">
							-- Other
						</li>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Offline Bank Transfer</label>
							<div class="col-md-10">
								<div id="offbank">
									<input type="hidden" value="0" id="bankcount">
									<?php foreach ($offBank as $bkey => $bvalue) { ?>
										<div class="form-row form-bk" id="bnk10<?= $bkey ?>">
											<input type="text" class="form-control form-bank col-md-3" name="offbank[<?= '10'.$bkey; ?>][name]" placeholder="Bank Name" value="<?= $bvalue->name ?>" required=""  >
											<input type="text" class="form-control form-bank col-md-3" name="offbank[<?= '10'.$bkey; ?>][number]" placeholder="Number" value="<?= $bvalue->number ?>" required="">
											<input type="text" class="form-control form-bank col-md-4" name="offbank[<?= '10'.$bkey; ?>][accname]" placeholder="Account Name" value="<?= $bvalue->accname ?>" required="">
											<button class="btn btn-danger btn-sm form-bank" title="delete" onClick="deldiv(<?= '10'.$bkey ?>)"><i class="fa fa-trash-alt"></i></button>
										</div>
									<?php } ?>
								</div>
								<button type="button" class="btn btn-success btn-sm" id="addbank" style="margin-top: 5px;">
									<i class="fa fa-plus"></i> Add bank
								</button>
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Organizer Name</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="event_organizer" value="<?= $detailEvent->organizer_name ?>" placeholder="Organizer Name">
							</div>
						</div>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Terms and Condition</label>
							<div class="col-md-10">
								<textarea rows="5" class="form-control" name="tac" placeholder="Terms and Condition"><?= $detailEvent->tac; ?></textarea>
							</div>
						</div>

					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#addbank").click(function(event) {
		var _cnt = $("#bankcount").val();
		var _cnt = parseInt(_cnt)+1;
		$("#bankcount").val(_cnt);
		var _html = '<div class="form-row form-bk" id="bnk'+_cnt+'">\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][name]" placeholder="Bank Name" required="">\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][number]" placeholder="Number" required="">\
				<input type="text" class="form-control form-bank col-md-4" name="offbank['+(_cnt-1)+'][accname]" placeholder="Account Name" required="">\
				<button class="btn btn-danger btn-sm form-bank" title="delete" onClick="deldiv('+_cnt+')"><i class="fa fa-trash-alt"></i></button>\
			</div>';
		$("#offbank").append(_html);
	});

	function deldiv(_par){
		$("#bnk"+_par).remove();
	}

	$("#recurring").click(function(event) {
        if ($('#recurring').is(':checked')) {
            $("#recurr_event").fadeIn('fast', function() {});
        }else{
            $("#recurr_event").fadeOut('fast', function() {});
        }
    });
</script>
