<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Event</h5>
					<h6 class="sub-heading">Event</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/event/doAddEventSchedule'); ?>" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Add Event</div>
					<div class="card-body">
						<div class="tab">
							<button class="tablinks disabled">Event Data</button>
							<button class="tablinks disabled">Package</button>
							<button class="tablinks active">Schedule</button>
						</div>
						<div class="tabcontent">
							<?php echo flashdata_notif("is_success","Yes"); ?>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Event Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" placeholder="Event Name" readonly="" value="<?= $dataEvent->event_name ?>" required="">
									<input type="hidden" class="form-control" name="param" placeholder="Event Name" readonly="" value="<?= $dataEvent->event_id ?>" required="">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Schedule Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="schedule_name" placeholder="Schedule Name"  required="">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Alias Name</label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="alias_name" placeholder="Alias Name"  required="">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Description</label>
								<div class="col-md-10">
									<textarea class="form-control" name="description" placeholder="Description"></textarea>
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Time Start</label>
								
								<div class="col-md-3">
									<input type="text" class="form-control datetimepicker" name="time_start" required="" readonly="" style="background-color: #fff;">
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Time End</label>
								
								<div class="col-md-3">
									<input type="text" class="form-control datetimepicker" name="time_end" required="" readonly="" style="background-color: #fff;">
								</div>
								
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Room</label>
								<div class="col-md-10">
									<select class="form-control selectpicker" data-live-search="true" name="room" required="">
										<?php foreach ($room as $rkey => $rvalue) { ?>
											<option value="<?= $rvalue->room_id ?>"><?= $rvalue->room_name ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Show in front</label>
								<div class="col-md-10">
									<select class="form-control selectpicker" data-live-search="true" name="show_in_front" required="">
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>

							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Package</label>
								<div class="col-md-10" style="padding-top: 8px;">
									<?php foreach ($package as $pkey => $pvalue) { ?>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="package[]" value="<?= $pvalue->event_package_id ?>">
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description"><?= $pvalue->package_name; ?></span>
										</label>
									<?php } ?>
								</div>
							</div>

							<h5><strong>List Added Schedule</strong></h5>
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th style="width: 10px;">No</th>
											<th>Schedule Name</th>
											<th>Package</th>
											<th>Room</th>
											<th>Start Time</th>
											<th>End Time</th>
											<th>Description</th>
											<th>Show In Front</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $ang=0; $no=1; foreach ($schedule as $sckey => $scvalue) { $scvalue = (object) $scvalue; $ang+=1; $encID = encrypt_decrypt("encrypt",$scvalue->event_schedule_id); ?>
											<tr>
												<td><?= $no++; ?></td>
												<td><?= $scvalue->schedule_name; ?></td>
												<td><?= implode(" ", $scvalue->package_info); ?></td>
												<td><?= $scvalue->room_name; ?></td>
												<td><span class="badge badge-bdr badge-success"><?= date("d M Y, H:i",strtotime($scvalue->schedule_start_time)); ?></span></td>
												<td><span class="badge badge-bdr badge-danger"><?= date("d M Y, H:i",strtotime($scvalue->schedule_end_time)); ?></span></td>
												<td><?= $scvalue->schedule_description; ?></td>
												<th><?php if($scvalue->is_show_front == 1){ echo "Y"; }else{ echo "N"; } ?></th>
												<td><button type="button" class="btn btn-danger btn-sm" onclick="is_delete('<?= base_url("/cms/event/deleteEventschedule/".$id."/".$encID); ?>')" title="Delete"><i class="fa fa-trash"></i></button></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
								<input type="hidden" value="<?= $ang; ?>" readonly="" id="ang">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back to list event</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Add Schedule</button>
						<?php if($ang > 0){ ?>
							<button type="button" id="finish" class="btn btn-success"><i class="fa fa-check"></i> Finish</button>
						<?php } ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#addbank").click(function(event) {
		var _cnt = $("#bankcount").val();
		var _cnt = parseInt(_cnt)+1;
		$("#bankcount").val(_cnt);
		var _html = '<div class="form-row form-bk" id="bnk'+_cnt+'">\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][name]" placeholder="Bank Name" requred>\
				<input type="text" class="form-control form-bank col-md-3" name="offbank['+(_cnt-1)+'][number]" placeholder="Number" requred>\
				<input type="text" class="form-control form-bank col-md-4" name="offbank['+(_cnt-1)+'][accname]" placeholder="Account Name" requred>\
				<button class="btn btn-danger btn-sm form-bank" title="delete" onClick="deldiv('+_cnt+')"><i class="fa fa-trash-alt"></i></button>\
			</div>';
		$("#offbank").append(_html);
	});

	$("#finish").click(function(event) {
		var _ang = $("#ang").val();
		if (_ang < 1) {
			alert("Plase Add Schedule first");
		}else{
			window.location.href= '<?= base_url('cms/event/'); ?>';
		}
	});

</script>
