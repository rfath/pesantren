	<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Add Santri</h5>
					<h6 class="sub-heading">Santri</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/santri/doAdd'); ?>"  role="form" enctype="multipart/form-data">
				<div class="card">
					<div class="card-header main-head">Data Santri</div>
					<div class="card-body">
						<?php echo flashdata_notif("is_success","Yes"); echo return_custom_notif();?>

						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nomor Induk</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="no_induk" placeholder="Nomor Induk" value="" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">NISN</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="nisn" placeholder="NISN" value="" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="nama" placeholder="Nama Santri" value="" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tempat, Tanggal Lahir</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="tempat" placeholder="Tempat Lahir" value="" required="">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control datepicker" name="tgl_lahir" placeholder="Tanggal Lahir" value="" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nomor HP</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="no_hp" placeholder="Nomor HP" value="" required="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Kewarganegaraan</label>
							<div class="col-md-10">
								<select class="form-control selectpicker" name="kewarganegaraan" id="status" data-live-search="true">
									<option value="WNI">WNI</option>
									<option value="WNA">WNA</option>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Alamat</label>
							<div class="col-md-10">
								<textarea class="form-control" name="alamat" placeholder="Alamat" required=""></textarea>
							</div>
						</div>
						<div id="detail_alamat">
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Provinsi</label>
								<div class="col-md-10">
									<select class="form-control selectpicker" data-live-search="true" name="provinsi" id="province">
										<option value="">Pilih Provinsi</option>
										<?php 
											foreach ($provinsi as $lProvinsi) {
										?>
											<option value="<?= $lProvinsi->province_id ?>"><?= $lProvinsi->province_name ?></option>
										<?php 
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Kota</label>
								<div class="col-md-10">
									<select class="form-control" data-live-search="true" name="kota" id="city">
										<option value="">Pilih Kota</option>
									</select>
								</div>
							</div>
							<div class="form-group row gutters">
								<label class="col-md-2 col-form-label">Kecamatan & Kelurahan</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" id="kecamatan" value="">
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control" name="kelurahan" placeholder="Desa / Kelurahan" id="kel_kab" value="">
								</div>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Photo</label>
							<div class="col-md-10">
								<input type="file" class="form-control" name="photo">
							</div>
						</div>
					</div>
					<!-- Data Orang tua dan Wali -->
					<div class="card-header main-head">Data Orang Tua & Wali</div>
					<div class="card-body">
						<div class="card-header main-head">Data Orang Tua</div><br>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama Ayah</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Nama Ayah" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pekerjaan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Pekerjaan" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pendidikan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Pendidikan" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama Ibu</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Nama Ibu" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pekerjaan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Pekerjaan" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Pendidikan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Pendidikan" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Kontak Orang Tua</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Kontak" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Alamat Orang Tua</label>
							<div class="col-md-10">
								<textarea class="form-control" name="additional[]" placeholder="Alamat Orang Tua" required=""></textarea>
							</div>
						</div>
						<div class="card-header main-head">Data Wali</div><br>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Nama Wali</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="Nama Wali" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">No HP</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="additional[]" placeholder="No Hp" value="">
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Alamat Wali</label>
							<div class="col-md-10">
								<textarea class="form-control" name="additional[]" placeholder="Alamat Wali" required=""></textarea>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	$("#province").change(function(event) {
		_myval = $("#province").val();
		$.ajax({
			url: '<?= base_url('cms/santri/getCityByProvince/'); ?>'+_myval,
			type: 'GET',
			dataType: 'HTML',
			async: true,
			processData: false,
			contentType: false
		})
		.done(function(e) {
			$("#city").html(e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$("#status").on("change", function(){
		var val = $("#status").val();

		if (val == "WNA") {
			$("#detail_alamat").toggle("hide");
			$("#province").val("");
			$("#city").val("");
			$("#kecamatan").val("");
			$("#kel_kab").val("");
		}else{
			$("#detail_alamat").toggle("show");
		}
	});
</script>