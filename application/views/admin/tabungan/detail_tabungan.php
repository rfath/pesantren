<!-- BEGIN .main-heading -->
<header class="main-heading">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
				<div class="page-icon">
					<a href="<?= $back_link ?>" title="Back"><i class="fa fa-angle-left"></i></a>
				</div>
				<div class="page-title">
					<h5>Detail Tabungan</h5>
					<h6 class="sub-heading">Tabungan</h6>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END: .main-heading -->

<!-- BEGIN .main-content -->
<div class="main-content">
	<!-- Row start -->
	<div class="row gutters">
		<div class="col-md-12">
			<form method="post" action="<?= base_url('cms/tabungan/doAddTabungan'); ?>">
				<div class="card">
					<div class="card-header main-head">Transaksi Tabungan</div>
					<div class="card-body">
						<?php echo return_custom_notif(); ?>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Tipe</label>
							<div class="col-md-10">
								<select class="form-control" name="tipe">
									<option value="IN">IN</option>
									<option value="OUT">OUT</option>
								</select>
							</div>
						</div>
						<div class="form-group row gutters">
							<label class="col-md-2 col-form-label">Jumlah</label>
							<div class="col-md-10">
								<input type="hidden" name="param" value="<?= $id ?>">
								<input type="text" class="form-control number" placeholder="Jumlah" name="jumlah_store" required="" value="<?= $detailData->satelit_church_name; ?>">
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= $back_link; ?>" class="btn btn-light"><i class="fa fa-arrow-circle-left"></i> Back</a>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
					</div>
				</div>
			</form>

			<div class="card">
				<div class="card-header main-head">Detail Transaksi Tabungan</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama Siswa</th>
									<th>NIK</th>
									<th>NISN</th>
									<th>Jumlah Tabungan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $dataSiswa->siswa_name; ?></td>
									<td><?php echo $dataSiswa->siswa_nomor_induk; ?></td>
									<td><?php echo $dataSiswa->siswa_nisn; ?></td>
									<td>Rp. <?php echo number_format($dataSiswa->amount_tabungan); ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					<center><h3>RIWAYAT TRANSAKSI</h3></center>
					<div class="table-responsive">
						<table width="100%" class="table table-bordered table-striped" id="datatable">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal Transaksi</th>
									<th>Tipe</th>
									<th>Nominal</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($riwayat as $key => $value) { ?>
									<tr>
										<td><?= $no++; ?></td>
										<td><?php echo date("d M Y H:i",strtotime($value->created_date)); ?></td>
										<td><?php echo $value->tabungan_type; ?></td>
										<td><?php echo number_format($value->amount); ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $("#datatable").DataTable({
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		});
	});
</script>