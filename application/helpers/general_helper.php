<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('debugCode')){
	function debugCode($r=array(),$f=TRUE){
	  echo "<pre>";
	  print_r($r);
	  echo "</pre>";

	  if($f==TRUE) 
	     die;
	}
}

if( ! function_exists('getRandomWord')){
	function getRandomWord($len = 10) {
	    $word = array_merge(range('a', 'z'), range('A', 'Z'));
	    shuffle($word);
	    return substr(implode($word), 0, $len);
	}
}
if( ! function_exists('getRandomWord2')){
	function getRandomWord2($len = 6) {
	    $word = array_merge(range('A', 'Z'), range(10, 99));
	    shuffle($word);
	    return substr(implode($word), 0, $len);
	}
}

if( ! function_exists('sKey')){
	function sKey($par = "") {
		if ($par <> "") {
			$keyword = str_replace(" ", "%20", $par);
		}else{
			$keyword = "";
		}
		return $keyword;
	}
}

if ( ! function_exists('okHeader')) {
	function okHeader($dataArray){
		foreach ($dataArray as $key => $value) {
			$hresponse[$key] = $value;
		}
		return $hresponse;
	}
}

if ( ! function_exists('okHeader')) {
	function decodeData($paging){
		$result = json_decode($paging);
		return $result;
	}

}

if ( ! function_exists('status_return')) {
	function status_return($status,$msg){
		return json_encode(array("status" => $status , "msg" => $msg));		
	}
}


if ( ! function_exists('checkSideBar')) {
	function checkSideBar($session,$name = "",$second = ""){
		$side = "";
        $CI =& get_instance();    
		$side = $CI->session->$session;
		
		if (empty($second)) {
			return $side[$name];
		}else{
			return $side[$name][$second];
		}
	}
}

if(!function_exists('add_date')) {
   function add_date($date) {
		$date = date_create($date);
		$date = date_format($date,"Y-m-d");
		return $date;
   }
}

if(!function_exists('dateFormat')) {
   function dateFormat($date) {
		$date = date_create($date);
		$date = date_format($date,"Y/m/d");
		return $date;
   }
}

if(!function_exists('status')) {
   function status($data) {
		if($data == '1'){
			$status = '<label style="color:red;">Inactive</label>';
		}else{
			$status = '<label style="color:green;">Active</label>';
		}
		return $status;
   }
}

if(!function_exists('status_word')) {
   function status_word($data) {
		if($data == 'Active'){
			$status = '<label style="color:green;">Active</label>';
		}else{
			$status = '<label style="color:red;">Inactive</label>';
		}
		return $status;
   }
}

if( ! function_exists('sNominal')){
	function sNominal($par = "") {
		if ($par <> "") {
			$keyword = str_replace(",", "", $par);
		}else{
			$keyword = "";
		}
		return $keyword;
	}
}


if( ! function_exists('chCheck')){
	function chCheck($par1, $par2, $par3, $par4 = "") {
		if ($par1 == $par2) {
			$return = $par3;
		}else{
			if ($par4 <> "") {
				$return = $par4;
			}else{
				$return = "";
			}
		}
		return $return;
	}
}

if( ! function_exists('sNilai')){
	function sNilai($par = "") {
		if ($par <> "") {
			$keyword = str_replace(".", "", $par);
		}else{
			$keyword = "";
		}
		return $keyword;
	}
}

if ( ! function_exists('getLoginData')) {
	function getLoginData($param=null){
		$side = "";
        $CI =& get_instance();    
		$side = $CI->session->sessionData;
		// var_dump($side);
		if($side){
			//debugCode($side);
		if($param==null){
			return $side;
		}else{
			return $side[$param];
		}
		}else{

		return false;
		}
	}
}

if ( ! function_exists('flashdata_check')) {
	function flashdata_check($param=""){
		$side = "";
        $CI =& get_instance();
        $result = $CI->session->flashdata($param);
        return $result;
	}
}

if ( ! function_exists('flashdata_notif')) {
	function flashdata_notif($param = "", $trueparam = "", $costum_message = ""){
		$side = "";
        $CI =& get_instance();
        $result = $CI->session->flashdata($param);
        $html = "";
        if ($result == $trueparam) {
        	if ($costum_message) { // Costum Message
        		$html = '<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="icon-tick"></i><strong>Congratulations!</strong> '.$costum_message.'
					</div>';
        	}else{
        		$html = '<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="icon-tick"></i><strong>Congratulations!</strong> Action Success.
					</div>';
        	}
        	$CI->session->flashdata($param);
        }elseif($result == "No" AND $result <> ""){
			$html = '<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="icon-warning2"></i><strong>Oh snap!</strong> Action Failed.
					</div>';
        }elseif($result <> "No" AND $result <> ""){
        	$html = '<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="icon-warning2"></i><strong>Oh snap!</strong> '.$result.'
					</div>';
        }

        return $html;
	}
}

function custom_notif($type = "success",$name="", $message = ""){
	//HOW TO USE BELOW
	//custom_notif("success","Nave of Session Success","Hello");
	//custom_notif("failed","Nave of Session Failed","Hello");

	$CI =& get_instance();
	$html = "";
	if ($type == "success") {
		if ($message <> "") {
			$html = array(
				"type"	=> "Notif",
				"html"	=> '<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<i class="icon-tick"></i><strong>Congratulations!</strong> '.$message.'
				</div>'
			);
		}else{
			$html = array(
				"type"	=> "Notif",
				"html"	=> '<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<i class="icon-tick"></i><strong>Congratulations!</strong> Action Success.
				</div>'
			);
		}
		$CI->session->set_flashdata($name, $html);
	}elseif($type == "failed"){
		if ($message <> "") {
			$html = array(
				"type" => "Notif",
				"html" => '<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<i class="icon-warning2"></i><strong>Oh snap!</strong> '.$message.'
				</div>'
			);
		}else{
			$html = array(
				"tyoe" => "Notif",
				"html"	=> '<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<i class="icon-warning2"></i><strong>Oh snap!</strong> Action Failed.
				</div>'
			);
		}
		$CI->session->set_flashdata($name, $html);
	}
}

function return_custom_notif(){
	$CI =& get_instance();
	$flashData = $CI->session->flashdata();
	$html = "";
	foreach ($flashData as $key => $value) {
		if ($value['type'] == "Notif") {
			$html.=$value['html'];
		}
	}
	return $html;
}

if(!function_exists('encrypt_decrypt')){
	function encrypt_decrypt($action, $string) {
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$secret_key = 'd9b9f2e4a4abdbeb5987f697c15f9671';
		$secret_iv = 'ae2f0e96fd0f6cb1bda97bb00316b3b0';
		// hash
		$key = hash('sha256', $secret_key);
		
		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);
		if ( $action == 'encrypt' ) {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		} else if( $action == 'decrypt' ) {
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}
		return $output;
	}
}

function check_selected($par1, $par2){
	if ($par1 == $par2) {
		$return = "selected";
	}else{
		$return = "";
	}
	return $return;
}

function is_checked($par1, $par2){
	if ($par1 == $par2) {
		$return = "checked";
	}else{
		$return = "";
	}
	return $return;	
}

function defaultFormEmail(){
	$email = "event@ticketing.com";
	return $email;
}

function defaultFormName(){
	$email = "Ticketing";
	return $email;
}

function checkMemberLogin(){
	$CI =& get_instance();
	
	if (empty($CI->session->userdata['sessionMember'])) {
		redirect();
	}
}

function orderNumberFormat($date, $idOrder){
	$order = "OD-".date("Ymd",strtotime($date))."-".$idOrder;
	return $order;
}

function stats_order_html($payment_status){
	$status_payment = "";
	if ($payment_status == 0) {
		$status_payment = '<span class="badge badge-bdr badge-warning">Waiting for Payment</span>';
	}elseif($payment_status == 1){
		$status_payment = '<span class="badge badge-bdr badge-info">Waiting Admin to Approve</span>';
	}elseif($payment_status == 2){
		$status_payment = '<span class="badge badge-bdr badge-success">Accepted</span>';
	}else{
		$status_payment = '<span class="badge badge-bdr badge-danger">Rejected</span>';
	}
	return $status_payment;
}

function type_inout_html($type){
	if ($type == 1) {
		$html = '<span class="badge badge-bdr badge-success">IN</span>';
	}else{
		$html = '<span class="badge badge-bdr badge-danger">Out</span>';
	}
	return $html;
}

function generateEticketNumber(){	 
	$rand = getRandomWord2();
	$enum = "E-".rand(100,999).$rand;
	return $enum;

}

function daysByNumber($number){
	if ($number == 1) {
		$days = "Monday";
	}

	if ($number == 2) {
		$days = "Tuesday";
	}

	if ($number == 3) {
		$days = "Wednesday";
	}

	if ($number == 4) {
		$days = "Thursday";
	}

	if ($number == 5) {
		$days = "Friday";
	}

	if ($number == 6) {
		$days = "Saturday";
	}

	if ($number == 7) {
		$days = "Sunday";
	}

	return $days;
}

function izin_status($param = ""){
	if ($param == "") {
		$status = "Belum Kembali";
	}else{
		$status = "Sudah Kembali Kembali";
	}
	return $status;
}

function izin_status_html($param = ""){
	if ($param == "") {
		$status = "<span style='color:#e74c3c;'><b>Belum Kembali</b></span>";
	}else{
		$status = "<span style='color:#27ae60;'><b>Sudah Kembali</b></span>";
	}
	return $status;
}



function generate_card($detsantri = array()){
	$pathinfo = pathinfo($detsantri->photo, PATHINFO_EXTENSION);
	//debugCode($detsantri);
	header('Content-type: image/jpeg');
  	// Create Image From Existing File
	//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/bckg_dpn_.png');
	$number    = $detsantri->siswa_nomor_induk; //NiK or number card
	$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/nologo.png');
	$qr_raw    = 'http://chart.apis.google.com/chart?cht=qr&chs=300x300&chld=L|1&chl='.$number;
	$qr_image  = imagecreatefrompng($qr_raw);
	$logo      = imagecreatefrompng(FCPATH.'assets_ticketing/back_end/img/pesantren_logo_med.png');
	$logo_back = imagecreatefrompng(FCPATH.'images/card/image_ok/trans_bg.png');

  	// Allocate A Color For The Text
	$black = imagecolorallocate($jpg_image, 0, 0, 0);
	$white = imagecolorallocate($jpg_image, 255, 255, 255);


  	// Set Path to Font File
	$font_path  = FCPATH.'images/card/OpenSans-Regular.ttf';
	$font_path2 = FCPATH.'images/card/VeraSerif-Bold.ttf';
	$font_path3 = FCPATH.'images/card/arbutusslab-regular.ttf';

  	// Set Text to Be Printed On Image
	$text       = $number;
	$text2      = strtoupper($detsantri->siswa_name);
	$text3 	    = "KARTU IDENTITAS SANTRI";
	$text4      = 'PONDOK MODERN "AL-ISLAM" NGANJUK';
	$text5      = 'Jl. Raya Sukomoro - Pace Km 1 Kel. Kapas Kec. Sukomoro Kab. Nganjuk';
	$text6      = 'YAYASAN "AL-ISLAM" NGANJUK';
	$text7      = 'Telp (0358) 325 096';
	$text8      = 'Berlaku selama menjadi santri Pondok Modern "AL-ISLAM" Nganjuk';

	$alamat     = wordwrap($detsantri->siswa_alamat,35,"\n");
	
	imagealphablending($jpg_image, true); // setting alpha blending on
	imagesavealpha($jpg_image, true); // save alphablending setting (important)

	/*==== START RESIZE PHOTO ====*/
	if ($detsantri->photo <> "") {
		$photo_raw = FCPATH.$detsantri->photo;
		$extension = pathinfo($photo_raw, PATHINFO_EXTENSION);
		if ($extension == "PNG" || $extension == "png") {
			$photo 	= imagecreatefrompng($photo_raw);
		}else{
			$photo 	= imagecreatefromjpeg($photo_raw);
		}
	}else{
		$photo_raw = FCPATH.'images/card/image_ok/photobg.png';
		$photo     = imagecreatefrompng($photo_raw);
	}
	
	$photo_none_rw = FCPATH.'images/card/image_ok/photobg.png';
	$photo_none    = imagecreatefrompng(FCPATH.'images/card/image_ok/photobg.png');
	$resiize       = resize_image($photo_none_rw,imagesx($photo),imagesy($photo)); //get resize_image from helper
	$photo_real    = imagecreatetruecolor($resiize[0], $resiize[1]);
	imagecopyresampled($photo_real, $photo, 0, 0, 0, 0, $resiize[0], $resiize[1], imagesx($photo), imagesy($photo));
	/*==== END RESIZE PHOTO ====*/
	
	/*==== START RESIZE BARCODE ====*/
	$white_bg  = imagecreatefrompng(FCPATH.'images/card/image_ok/whitbg.png');
	$qr_resize = resize_image($qr_raw,imagesx($white_bg),imagesy($white_bg)); //get resize_image from helper
	$qr_real   = imagecreatetruecolor($qr_resize[0], $qr_resize[1]);
	imagecopyresampled($qr_real, $qr_image, 0, 0, 0, 0, $qr_resize[0], $qr_resize[1], imagesx($qr_image), imagesy($qr_image));
	/*==== END RESIZE BARCODE ====*/
  	// Print Text On Image
  	imagettftext($jpg_image, 20, 0, 158, 51, $black, $font_path3, $text6);
	imagettftext($jpg_image, 22, 0, 158, 85, $black, $font_path2, $text4);
	imagettftext($jpg_image, 16, 0, 158, 116, $black, $font_path3, $text5);
	imagettftext($jpg_image, 16, 0, 158, 143, $black, $font_path3, $text7);

	imagettftext($jpg_image, 27, 0, 320, 232, $black, $font_path2, $text3);
	imagettftext($jpg_image, 23, 0, 320, 278, $black, $font_path, $text);
	imagettftext($jpg_image, 23, 0, 320, 330, $black, $font_path, $text2);

	imagettftext($jpg_image, 20, 0, 520, 430, $white, $font_path, $alamat);
	imagettftext($jpg_image, 15, 0, 75, 610, $white, $font_path3, $text8);

	
	imagecopy($jpg_image, $qr_real, 307, 382, 0, 0, imagesx($white_bg), imagesy($white_bg));
	imagecopy($jpg_image, $photo_real, 59, 240, 0, 0, imagesx($photo_none), imagesy($photo_none));
	imagecopyresampled($jpg_image, $logo, 15, 15, 0, 0, imagesx($logo_back),imagesx($logo_back),imagesx($logo),imagesy($logo));
  	// Send Image to Browser
  	
  	imagepng($jpg_image);
  	$pngQuality = ($quality - 100) / 11.111111;
	$pngQuality = round(abs($pngQuality));
  	if ($extension == "PNG" || $extension == "png") {
  		$img_edit = imagepng($jpg_image,FCPATH.'images/card/'.$number.".".$extension);
  	}else{
  		$img_edit = imagejpeg($jpg_image,FCPATH.'images/card/'.$number.".".$extension);
  	}
	
	$merger   = imagecopy ( $jpg_image , $img_edit , (imagesx($jpg_image)/2)-(imagesx($img_edit)/2), (imagesy($jpg_image)/2)-(imagesy($img_edit)/2 ));
	if ($extension == "PNG" || $extension == "png") {
		imagepng(merger);
	}else{
		imagejpeg(merger);
	}
	// imagejpeg($new);
  	// Clear Memory
	imagedestroy($jpg_image);
	imagedestroy($photo);
	imagedestroy($photo_none);
	imagedestroy($qr_real);
	imagedestroy($white_bg);
	imagedestroy($qr_image);
	imagedestroy($logo_back);
	imagedestroy($merger);
}

function resize_image($img,$maxwidth,$maxheight) {
    //This function will return the specified dimension(width,height)
    //dimension[0] - width
    //dimension[1] - height
    $dimension = array();
    $imginfo = getimagesize($img);
    
    $imgwidth = $imginfo[0];
    $imgheight = $imginfo[1];
    if($imgwidth > $maxwidth){
        $ratio = $maxwidth/$imgwidth;
        $newwidth = round($imgwidth*$ratio);
        $newheight = round($imgheight*$ratio);
        if($newheight > $maxheight){
            $ratio = $maxheight/$newheight;
            $dimension[] = round($newwidth*$ratio);
            $dimension[] = round($newheight*$ratio);
            return $dimension;
        }else{
            $dimension[] = $newwidth;
            $dimension[] = $newheight;
            return $dimension;
        }
    }elseif($imgheight > $maxheight){
        $ratio = $maxheight/$imgheight;
        $newwidth = round($imgwidth*$ratio);
        $newheight = round($imgheight*$ratio);
        if($newwidth > $maxwidth){
            $ratio = $maxwidth/$newwidth;
            $dimension[] = round($newwidth*$ratio);
            $dimension[] = round($newheight*$ratio);
            return $dimension;
        }else{
            $dimension[] = $newwidth;
            $dimension[] = $newheight;
            return $dimension;
        }
    }else{
        $dimension[] = $imgwidth;
        $dimension[] = $imgheight;
        return $dimension;
    }
}

function generate_card_new($detsantri = array()){
	$pathinfo = pathinfo($detsantri->photo, PATHINFO_EXTENSION);
	//debugCode($detsantri);
	header('Content-type: image/jpeg');
  	// Create Image From Existing File
	//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/bckg_dpn_.png');
	$number    = $detsantri->siswa_nomor_induk; //NiK or number card
	//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/depan_new.jpeg');
	$jpg_image = imagecreatefromjpeg(FCPATH.'images/card/image_ok/depan_new.jpg');
	$qr_raw    = 'http://chart.apis.google.com/chart?cht=qr&chs=300x300&chld=L|1&chl='.$number;
	$qr_image  = imagecreatefrompng($qr_raw);
	//$logo      = imagecreatefrompng(FCPATH.'assets_ticketing/back_end/img/pesantren_logo_med.png');
	$logo_back = imagecreatefrompng(FCPATH.'images/card/image_ok/trans_bg.png');

  	// Allocate A Color For The Text
	$black  = imagecolorallocate($jpg_image, 0, 0, 0);
	$white  = imagecolorallocate($jpg_image, 255, 255, 255);
	$yellow = imagecolorallocate($jpg_image, 252, 255, 0);


  	// Set Path to Font File
	//$font_path  = FCPATH.'images/card/OpenSans-Regular.ttf';
	$font_path  = FCPATH.'images/card/arial-boldmt.ttf';
	$font_path2 = FCPATH.'images/card/VeraSerif-Bold.ttf';
	$font_path3 = FCPATH.'images/card/arbutusslab-regular.ttf';

  	// Set Text to Be Printed On Image
	$text       = $number;
	$text2      = $detsantri->siswa_name;
	$text3 	    = "KARTU IDENTITAS SANTRI";
	$text4      = 'PONDOK MODERN "AL-ISLAM" NGANJUK';
	$text5      = 'Jl. Raya Sukomoro - Pace Km 1 Kel. Kapas Kec. Sukomoro Kab. Nganjuk';
	$text6      = 'YAYASAN "AL-ISLAM" NGANJUK';
	$text7      = 'Telp (0358) 325 096';
	$text8      = 'Berlaku selama menjadi santri Pondok Modern "AL-ISLAM" Nganjuk';
	$alamat     = wordwrap($detsantri->siswa_alamat,35,"\n");
	
	

	/*==== START RESIZE PHOTO ====*/
	if ($detsantri->photo <> "") {
		$photo_raw = FCPATH.$detsantri->photo;
		//$photo_raw = FCPATH."images/santri/SNT-18290804203875.jpg";
		
		$extension = pathinfo($photo_raw, PATHINFO_EXTENSION);
		if ($extension == "PNG" || $extension == "png") {
			$photo 	= imagecreatefrompng($photo_raw);
		}else{
			$photo 	= imagecreatefromjpeg($photo_raw);
		}
	}else{
		$photo_raw = FCPATH.'images/card/image_ok/photo_white.png';
		$photo     = imagecreatefrompng($photo_raw);
	}
	
	$photo_none_rw = FCPATH.'images/card/image_ok/photo_white.png';
	$photo_none    = imagecreatefrompng(FCPATH.'images/card/image_ok/photo_white.png');
	$resiize       = resize_image($photo_none_rw,imagesx($photo),imagesy($photo)); //get resize_image from helper
	$photo_real    = imagecreatetruecolor($resiize[0], $resiize[1]);
	imagecopyresampled($photo_real, $photo, 0, 0, 0, 0, $resiize[0], $resiize[1], imagesx($photo), imagesy($photo));
	/*==== END RESIZE PHOTO ====*/
	
	/*==== START RESIZE BARCODE ====*/
	$white_bg  = imagecreatefrompng(FCPATH.'images/card/image_ok/qr_white.png');
	$qr_resize = resize_image($qr_raw,imagesx($white_bg),imagesy($white_bg)); //get resize_image from helper
	$qr_real   = imagecreatetruecolor($qr_resize[0], $qr_resize[1]);
	imagecopyresampled($qr_real, $qr_image, 0, 0, 0, 0, $qr_resize[0], $qr_resize[1], imagesx($qr_image), imagesy($qr_image));
	/*==== END RESIZE BARCODE ====*/
  	// Print Text On Image

	imagettftext($jpg_image, 24, 0, 200, 355, $yellow, $font_path, $text3);
	imagettftext($jpg_image, 23, 0, 200, 445, $black, $font_path, $text);
	imagettftext($jpg_image, 23, 0, 200, 490, $black, $font_path, $text2);

	imagettftext($jpg_image, 14, 0, 200, 534, $black, $font_path, $alamat);
	imagecopy($jpg_image, $qr_real, 870, 352, 0, 0, imagesx($white_bg), imagesy($white_bg));
	imagecopy($jpg_image, $photo_real, 29, 410, 0, 0, imagesx($photo_none), imagesy($photo_none));
	//imagecopyresampled($jpg_image, $logo, 15, 15, 0, 0, imagesx($logo_back),imagesx($logo_back),imagesx($logo),imagesy($logo));
  	// Send Image to Browser
  	imagealphablending($jpg_image, true); // setting alpha blending on
	imagesavealpha($jpg_image, true); // save alphablending setting (important)
	//imagejpeg($jpg_image);
  	imagepng($jpg_image,NULL,1);
  	$pngQuality = ($quality - 100) / 11.111111;
	$pngQuality = round(abs($pngQuality));
  	if ($extension == "PNG" || $extension == "png") {
  		$img_edit = imagepng($jpg_image,FCPATH.'images/card/'.$number.".".$extension);
  	}else{
  		$img_edit = imagejpeg($jpg_image,FCPATH.'images/card/'.$number.".".$extension);
  	}
	
	$merger   = imagecopy ( $jpg_image , $img_edit , (imagesx($jpg_image)/2)-(imagesx($img_edit)/2), (imagesy($jpg_image)/2)-(imagesy($img_edit)/2 ));
	if ($extension == "PNG" || $extension == "png") {
		imagepng(merger);
	}else{
		imagejpeg(merger);
	}
	// imagejpeg($new);
  	// Clear Memory
	imagedestroy($jpg_image);
	imagedestroy($photo);
	imagedestroy($photo_none);
	imagedestroy($qr_real);
	imagedestroy($white_bg);
	imagedestroy($qr_image);
	imagedestroy($logo_back);
	imagedestroy($merger);
}

function generate_card_save($detsantri){
	$pathinfo = pathinfo($detsantri->photo, PATHINFO_EXTENSION);
	//debugCode($detsantri);
	//header('Content-type: image/jpeg');
  	// Create Image From Existing File
	//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/bckg_dpn_.png');
	$number    = $detsantri->siswa_nomor_induk; //NiK or number card
	//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/depan_new.png');
	$jpg_image = imagecreatefromjpeg(FCPATH.'images/card/image_ok/depan_new.jpg');
	$qr_raw    = 'http://chart.apis.google.com/chart?cht=qr&chs=300x300&chld=L|1&chl='.$number;
	$qr_image  = imagecreatefrompng($qr_raw);
	$logo_back = imagecreatefrompng(FCPATH.'images/card/image_ok/trans_bg.png');

  	// Allocate A Color For The Text
	$black  = imagecolorallocate($jpg_image, 0, 0, 0);
	$white  = imagecolorallocate($jpg_image, 255, 255, 255);
	$yellow = imagecolorallocate($jpg_image, 252, 255, 0);


  	// Set Path to Font File
	//$font_path  = FCPATH.'images/card/OpenSans-Regular.ttf';
	$font_path  = FCPATH.'images/card/arial-boldmt.ttf';
	$font_path2 = FCPATH.'images/card/VeraSerif-Bold.ttf';
	$font_path3 = FCPATH.'images/card/arbutusslab-regular.ttf';

  	// Set Text to Be Printed On Image
	$text       = $number;
	$text2      = $detsantri->siswa_name;
	$text3 	    = "KARTU IDENTITAS SANTRI";
	$text4      = 'PONDOK MODERN "AL-ISLAM" NGANJUK';
	$text5      = 'Jl. Raya Sukomoro - Pace Km 1 Kel. Kapas Kec. Sukomoro Kab. Nganjuk';
	$text6      = 'YAYASAN "AL-ISLAM" NGANJUK';
	$text7      = 'Telp (0358) 325 096';
	$text8      = 'Berlaku selama menjadi santri Pondok Modern "AL-ISLAM" Nganjuk';
	$alamat     = wordwrap($detsantri->siswa_alamat,35,"\n");

	/*==== START RESIZE PHOTO ====*/
	if ($detsantri->photo <> "") {
		$photo_raw = FCPATH.$detsantri->photo;
		$extension = pathinfo($photo_raw, PATHINFO_EXTENSION);
		if ($extension == "PNG" || $extension == "png") {
			$photo 	= imagecreatefrompng($photo_raw);
		}else{
			$photo 	= imagecreatefromjpeg($photo_raw);
		}
	}else{
		$photo_raw = FCPATH.'images/card/image_ok/photo_white.png';
		$photo     = imagecreatefrompng($photo_raw);
	}
	
	$photo_none_rw = FCPATH.'images/card/image_ok/photo_white.png';
	$photo_none    = imagecreatefrompng(FCPATH.'images/card/image_ok/photo_white.png');
	$resiize       = resize_image($photo_none_rw,imagesx($photo),imagesy($photo)); //get resize_image from helper
	$photo_real    = imagecreatetruecolor($resiize[0], $resiize[1]);
	imagecopyresampled($photo_real, $photo, 0, 0, 0, 0, $resiize[0], $resiize[1], imagesx($photo), imagesy($photo));
	/*==== END RESIZE PHOTO ====*/
	
	/*==== START RESIZE BARCODE ====*/
	$white_bg  = imagecreatefrompng(FCPATH.'images/card/image_ok/whitbg.png');
	$qr_resize = resize_image($qr_raw,imagesx($white_bg),imagesy($white_bg)); //get resize_image from helper
	$qr_real   = imagecreatetruecolor($qr_resize[0], $qr_resize[1]);
	imagecopyresampled($qr_real, $qr_image, 0, 0, 0, 0, $qr_resize[0], $qr_resize[1], imagesx($qr_image), imagesy($qr_image));
	/*==== END RESIZE BARCODE ====*/
  	// Print Text On Image
  	imagettftext($jpg_image, 24, 0, 200, 355, $yellow, $font_path, $text3);
	imagettftext($jpg_image, 23, 0, 200, 445, $black, $font_path, $text);
	imagettftext($jpg_image, 23, 0, 200, 490, $black, $font_path, $text2);

	imagettftext($jpg_image, 14, 0, 200, 534, $black, $font_path, $alamat);
	
	imagecopy($jpg_image, $qr_real, 870, 352, 0, 0, imagesx($white_bg), imagesy($white_bg));
	imagecopy($jpg_image, $photo_real, 29, 410, 0, 0, imagesx($photo_none), imagesy($photo_none));
	imagecopyresampled($jpg_image, $logo, 15, 15, 0, 0, imagesx($logo_back),imagesx($logo_back),imagesx($logo),imagesy($logo));
  	// Send Image to Browser

  	imagealphablending($jpg_image, true); // setting alpha blending on
	imagesavealpha($jpg_image, true); // save alphablending setting (important)
  	// imagepng($jpg_image);
  	$pngQuality = ($quality - 100) / 11.111111;
	$pngQuality = round(abs($pngQuality));
  	if ($extension == "PNG" || $extension == "png") {
  		$img_edit = imagepng($jpg_image,FCPATH.'images/card/generate/'.$number.".".$extension);
  	}else{
  		$img_edit = imagejpeg($jpg_image,FCPATH.'images/card/generate/'.$number.".".$extension);
  	}
	
	$merger   = imagecopy ( $jpg_image , $img_edit , (imagesx($jpg_image)/2)-(imagesx($img_edit)/2), (imagesy($jpg_image)/2)-(imagesy($img_edit)/2 ));
	if ($extension == "PNG" || $extension == "png") {
		imagepng(merger);
	}else{
		imagejpeg(merger);
	}
	// imagejpeg($new);
  	// Clear Memory
	imagedestroy($jpg_image);
	imagedestroy($photo);
	imagedestroy($photo_none);
	imagedestroy($qr_real);
	imagedestroy($white_bg);
	imagedestroy($qr_image);
	imagedestroy($logo_back);
	imagedestroy($merger);
}

function generate_card_save_old($detsantri = array()){
	$pathinfo = pathinfo($detsantri->photo, PATHINFO_EXTENSION);
	//debugCode($detsantri);
	//header('Content-type: image/jpeg');
  	// Create Image From Existing File
	//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/bckg_dpn_.png');
	$number    = $detsantri->siswa_nomor_induk; //NiK or number card
	$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/nologo.png');
	$qr_raw    = 'http://chart.apis.google.com/chart?cht=qr&chs=300x300&chld=L|1&chl='.$number;
	$qr_image  = imagecreatefrompng($qr_raw);
	$logo      = imagecreatefrompng(FCPATH.'assets_ticketing/back_end/img/pesantren_logo_med.png');
	$logo_back = imagecreatefrompng(FCPATH.'images/card/image_ok/trans_bg.png');

  	// Allocate A Color For The Text
	$black = imagecolorallocate($jpg_image, 0, 0, 0);
	$white = imagecolorallocate($jpg_image, 255, 255, 255);


  	// Set Path to Font File
	$font_path  = FCPATH.'images/card/OpenSans-Regular.ttf';
	$font_path2 = FCPATH.'images/card/VeraSerif-Bold.ttf';
	$font_path3 = FCPATH.'images/card/arbutusslab-regular.ttf';

  	// Set Text to Be Printed On Image
	$text       = $number;
	$text2      = strtoupper($detsantri->siswa_name);
	$text3 	    = "KARTU IDENTITAS SANTRI";
	$text4      = 'PONDOK MODERN "AL-ISLAM" NGANJUK';
	$text5      = 'Jl. Raya Sukomoro - Pace Km 1 Kel. Kapas Kec. Sukomoro Kab. Nganjuk';
	$text6      = 'YAYASAN "AL-ISLAM" NGANJUK';
	$text7      = 'Telp (0358) 325 096';
	$text8      = 'Berlaku selama menjadi santri Pondok Modern "AL-ISLAM" Nganjuk';

	$alamat     = wordwrap($detsantri->siswa_alamat,35,"\n");
	
	imagealphablending($jpg_image, true); // setting alpha blending on
	imagesavealpha($jpg_image, true); // save alphablending setting (important)

	/*==== START RESIZE PHOTO ====*/
	if ($detsantri->photo <> "") {
		$photo_raw = FCPATH.$detsantri->photo;
		$extension = pathinfo($photo_raw, PATHINFO_EXTENSION);
		if ($extension == "PNG" || $extension == "png") {
			$photo 	= imagecreatefrompng($photo_raw);
		}else{
			$photo 	= imagecreatefromjpeg($photo_raw);
		}
	}else{
		$photo_raw = FCPATH.'images/card/image_ok/photobg.png';
		$photo     = imagecreatefrompng($photo_raw);
	}
	
	$photo_none_rw = FCPATH.'images/card/image_ok/photobg.png';
	$photo_none    = imagecreatefrompng(FCPATH.'images/card/image_ok/photobg.png');
	$resiize       = resize_image($photo_none_rw,imagesx($photo),imagesy($photo)); //get resize_image from helper
	$photo_real    = imagecreatetruecolor($resiize[0], $resiize[1]);
	imagecopyresampled($photo_real, $photo, 0, 0, 0, 0, $resiize[0], $resiize[1], imagesx($photo), imagesy($photo));
	/*==== END RESIZE PHOTO ====*/
	
	/*==== START RESIZE BARCODE ====*/
	$white_bg  = imagecreatefrompng(FCPATH.'images/card/image_ok/whitbg.png');
	$qr_resize = resize_image($qr_raw,imagesx($white_bg),imagesy($white_bg)); //get resize_image from helper
	$qr_real   = imagecreatetruecolor($qr_resize[0], $qr_resize[1]);
	imagecopyresampled($qr_real, $qr_image, 0, 0, 0, 0, $qr_resize[0], $qr_resize[1], imagesx($qr_image), imagesy($qr_image));
	/*==== END RESIZE BARCODE ====*/
  	// Print Text On Image
  	imagettftext($jpg_image, 20, 0, 158, 51, $black, $font_path3, $text6);
	imagettftext($jpg_image, 22, 0, 158, 85, $black, $font_path2, $text4);
	imagettftext($jpg_image, 16, 0, 158, 116, $black, $font_path3, $text5);
	imagettftext($jpg_image, 16, 0, 158, 143, $black, $font_path3, $text7);

	imagettftext($jpg_image, 27, 0, 320, 232, $black, $font_path2, $text3);
	imagettftext($jpg_image, 23, 0, 320, 278, $black, $font_path, $text);
	imagettftext($jpg_image, 23, 0, 320, 330, $black, $font_path, $text2);

	imagettftext($jpg_image, 20, 0, 520, 430, $white, $font_path, $alamat);
	imagettftext($jpg_image, 15, 0, 75, 610, $white, $font_path3, $text8);

	
	imagecopy($jpg_image, $qr_real, 307, 382, 0, 0, imagesx($white_bg), imagesy($white_bg));
	imagecopy($jpg_image, $photo_real, 59, 240, 0, 0, imagesx($photo_none), imagesy($photo_none));
	imagecopyresampled($jpg_image, $logo, 15, 15, 0, 0, imagesx($logo_back),imagesx($logo_back),imagesx($logo),imagesy($logo));
  	// Send Image to Browser
  	
  	// imagepng($jpg_image);
  	$pngQuality = ($quality - 100) / 11.111111;
	$pngQuality = round(abs($pngQuality));
  	if ($extension == "PNG" || $extension == "png") {
  		$img_edit = imagepng($jpg_image,FCPATH.'images/card/generate/'.$number.".".$extension);
  	}else{
  		$img_edit = imagejpeg($jpg_image,FCPATH.'images/card/generate/'.$number.".".$extension);
  	}
	
	$merger   = imagecopy ( $jpg_image , $img_edit , (imagesx($jpg_image)/2)-(imagesx($img_edit)/2), (imagesy($jpg_image)/2)-(imagesy($img_edit)/2 ));
	if ($extension == "PNG" || $extension == "png") {
		imagepng(merger);
	}else{
		imagejpeg(merger);
	}
	// imagejpeg($new);
  	// Clear Memory
	imagedestroy($jpg_image);
	imagedestroy($photo);
	imagedestroy($photo_none);
	imagedestroy($qr_real);
	imagedestroy($white_bg);
	imagedestroy($qr_image);
	imagedestroy($logo_back);
	imagedestroy($merger);
}