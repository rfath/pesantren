<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_asatidz extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getAsatidz(){
    	$this->db->select("*");
    	$this->db->from("asatidz");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getAsatidzDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("asatidz");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }
}