<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pelanggaran extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getDetailPelanggaran($id){
    	$this->db->select("*");
    	$this->db->from("pelanggaran");
    	$this->db->where("pelanggaran_id",$id);
    	$this->db->where("isDeleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getDetailPelanggaranSiswa($id){
        $this->db->select("*");
        $this->db->from("pelanggaran_siswa");
        $this->db->where("pelanggaran_siswa_id",$id);
        //$this->db->where("isDeleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_list_pelanggaran($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'pelanggaran_name',
		);
		
		$sql = "SELECT * FROM pelanggaran";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE isDeleted <> '1'";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (pelanggaran_name like '%".$param['search']['value']."%' ";
			//$where.= " or user_email like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY pelanggaran_name DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

    function get_list_pelanggaran_siswa($param = array(),$id, $method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'pelanggaran_name',
            2 => 'siswa_name',
            3 => "hukuman",
        );
        
        $sql = "SELECT * FROM pelanggaran_siswa INNER JOIN pelanggaran ON pelanggaran.pelanggaran_id = pelanggaran_siswa.pelanggaran_id INNER JOIN siswa ON siswa.siswa_id = pelanggaran_siswa.siswaId ";
        $where = "";
        $orderby = " ";
        
        $where.=" WHERE siswaId = '$id'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (pelanggaran_name like '%".$param['search']['value']."%' ";
            $where.= " or siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or hukuman like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY pelanggaran_name DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }

    function getPelanggaran(){
        $this->db->select('*');
        $this->db->from('pelanggaran');
        $this->db->where('isDeleted',0);
        $que = $this->db->get()->result_array();
        return $que;
    }

    function getSiswa(){
        $this->db->select('*');
        $this->db->from('siswa');
        //$this->db->where('siswa_id', $id);
        $this->db->where('isDeleted',0);
        $que = $this->db->get()->result_array();
        return $que;
    }


    function get_list_siswa($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'siswa_name',
            2 => 'siswa_nomor_induk',
            3 => 'siswa_nisn',
   
        );
        
        $sql = "SELECT * FROM siswa";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE isDeleted = '0'";
        $where.=" AND is_graduate = '0'";
        $where.=" AND status = '1'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY siswa_name DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }

}