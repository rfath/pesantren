<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_master extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getRegion(){
    	$this->db->select("*");
    	$this->db->from("region");
    	$this->db->where('is_active',1);
    	$this->db->where("is_deleted",0);
    	$this->db->order_by("created_date","DESC");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getRegionDetail($region_id){
    	$this->db->select("*");
    	$this->db->from("region");
    	$this->db->where("is_deleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getDPW(){
        $this->db->select("*");
        $this->db->from("dpw");
        $this->db->where('is_active',1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("created_date","DESC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getDetailDPW($id){
        $this->db->select("*");
        $this->db->from("dpw");    
        $this->db->where("is_deleted",0);
        $this->db->where("dpw_id",$id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getSatelitChurch(){
        $this->db->select("*");
        $this->db->from("satelit_church");
        $this->db->where('is_active',1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("created_date","DESC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getDetailListChurch($id){
        $this->db->select("*");
        $this->db->from("satelit_church");
        $this->db->where("is_deleted",0);
        $this->db->where("satelit_church_id",$id);
        $this->db->order_by("created_date","DESC");
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getChurchRole(){
        $this->db->select("*");
        $this->db->from("church_role");
        $this->db->where('is_active',1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("created_date","DESC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getDetailChurchRole($id){
        $this->db->select("*");
        $this->db->from("church_role");
        $this->db->where("is_deleted",0);
        $this->db->where("church_role_id",$id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getAllSlider(){
        $this->db->select("*");
        $this->db->from("slider");
        $this->db->where("is_deleted",0);
        $this->db->order_by('seq',"ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getSliderById($id){
        $this->db->select("*");
        $this->db->from("slider");
        $this->db->where("is_deleted",0);
        $this->db->where("slider_id",$id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }
}