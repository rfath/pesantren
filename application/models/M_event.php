<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_event extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getEventDetail($id){
    	$this->db->select("*");
    	$this->db->from("event");
    	$this->db->where("event_id",$id);
    	$this->db->where("is_deleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getPackageDetail($id){
    	$this->db->select("*");
    	$this->db->from("event_package");
    	$this->db->where("event_package_id",$id);
    	$this->db->where("is_deleted",0);
		$query  = $this->db->get();
		$result = $query->row();
    	return $result;
    }

    function getScheduleDetail($id){
    	$this->db->select("*");
    	$this->db->from("event_schedule");
    	$this->db->where("event_schedule_id",$id);
    	$this->db->where("is_deleted",0);
		$query  = $this->db->get();
		$result = $query->row();
    	return $result;
    }

    function getEventPackage($event_id){
    	$this->db->select("*");
    	$this->db->from("event_package");
    	$this->db->where("event_id",$event_id);
    	$this->db->where("is_deleted",0);
    	$this->db->where("is_active",1);
    	$this->db->where("package_remaining_seat > 0");
    	$this->db->order_by("package_name","ASC");
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getScheduleByPackage($package_id){
    	$this->db->select("*");
    	$this->db->from("event_schedule");
    	$this->db->where("package_id like'".'%"'.$package_id.'"%'."'");
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$this->db->where("is_show_front",1);
    	$this->db->order_by("schedule_start_time","ASC");
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getShortEventDetail($event_id){
    	$this->db->select("event_id, event_name");
    	$this->db->from("event");
    	$this->db->where("event_id",$event_id);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function listPackageByEvent($event_id){
    	$this->db->select("*");
    	$this->db->from("event_package");
    	$this->db->where("event_id",$event_id);
    	$this->db->where("is_deleted",0);
    	$this->db->where("is_active",1);
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function listScheduleByEvent($event_id){
    	$sql = 'SELECT sc.*, rm.room_name FROM event_schedule AS sc
		INNER JOIN room AS rm ON rm.room_id = sc.room_id
		WHERE sc.is_deleted = 0
		AND sc.is_active = 1
		AND sc.event_id = '.$event_id;
		$query 	= $this->db->query($sql);
		$result = $query->result();
		return $result;
    }

    function getShordSchedule($schedule_id){
    	$this->db->select("sc.event_schedule_id, sc.schedule_name, ev.event_name, ev.event_id");
    	$this->db->from("event_schedule as sc");    	
    	$this->db->join("event AS ev","ev.event_id = sc.event_id");
    	$this->db->where("sc.event_schedule_id",$schedule_id);
    	$this->db->where("sc.is_deleted",0);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkAttendanceIO($event_id, $card_number){
    	$this->db->select("att.attendance_id, ord.package_id");
    	$this->db->from("attendance AS att");
    	$this->db->join("order AS ord","ord.order_id = att.order_id");
    	$this->db->where("ord.event_id",$event_id);
    	$this->db->where("att.is_active",1);
    	$this->db->where("att.is_deleted",0);
    	$this->db->where("att.card_number",$card_number);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getShortPackageByID($id){
    	$this->db->select("package_name, event_package_id");
    	$this->db->from("event_package");
    	$this->db->where("event_package_id",$id);
    	$this->db->where("is_deleted",0);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkScheduleINOUT($package_id,$event_schedule_id){
        $this->db->select("event_schedule_id, schedule_name");
        $this->db->from("event_schedule");
        $this->db->where("package_id like'".'%"'.$package_id.'"%'."'");
        $this->db->where("event_schedule_id",$event_schedule_id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("schedule_start_time","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function checkrecurring($date,$event_id){
    	$this->db->select("*");
    	$this->db->from("recurring_session");
    	$this->db->where("event_id",$event_id);
    	$this->db->where("recurring_date",$date);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getRecurringSessionById($id){
    	$this->db->select("*");
    	$this->db->from("recurring_session");
    	$this->db->where("recurring_session_id",$id);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }


    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_list_event($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'ev.event_name',
			2 => 'pl.place_name',
			3 => 'ev.event_start_date',
			4 => 'ev.event_open_registration_date'
		);
		
		$sql = "SELECT ev.*, pl.place_name FROM event AS ev
		INNER JOIN place AS pl on pl.place_id = ev.event_place_id";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE ev.is_deleted <> '1' AND ev.is_active = 1";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (ev.event_name like '%".$param['search']['value']."%' ";
			$where.= " or pl.place_name like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY ev.created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_package($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'package_name',
			2 => 'package_price',
			3 => 'package_seat',
			4 => 'package_remaining_seat',
			5 => 'package_description'
		);
		
		$sql = "SELECT * FROM event_package";

		$where    = "";
		$orderby  = " ";
		$event_id = encrypt_decrypt("decrypt",$param['event']);

		$where.=" WHERE is_deleted <> '1' AND is_active = 1 AND event_id = '".$event_id."'";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (package_name like '%".$param['search']['value']."%' ";
			$where.= " or package_price like '%".$param['search']['value']."%' ";
			$where.= " or package_seat like '%".$param['search']['value']."%' ";
			$where.= " or package_remaining_seat like '%".$param['search']['value']."%' ";
			$where.= " or package_description like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_schedule($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'sc.schedule_name',
			2 => 'sc.package_id',
			3 => 'rm.room_name',
			4 => 'sc.schedule_start_time',
			5 => 'sc.schedule_end_time',
			6 => 'sc.schedule_description'
		);
		
		$sql = "SELECT sc.*, rm.room_name FROM event_schedule AS sc
		INNER JOIN room AS rm ON rm.room_id = sc.room_id";

		$where    = "";
		$orderby  = " ";
		$event_id = encrypt_decrypt("decrypt",$param['event']);

		$where.=" WHERE sc.is_deleted <> '1' AND sc.is_active = 1 AND sc.event_id = '".$event_id."'";
		if ($param['package'] <> "") {
			$where.=" AND sc.package_id like '".'%"'.$param['package'].'"%'."'";
		}
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (sc.schedule_name like '%".$param['search']['value']."%' ";
			$where.= " or rm.room_name like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;         
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_attendance_schedule($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'att.attendance_name',
			2 => 'atc.created_date',
			3 => 'atc.type_att'
		);
		
		$sql = "SELECT atc.*, att.attendance_name From attendance_schedule AS atc
		INNER JOIN attendance AS att ON att.attendance_id = atc.attendance_id
		INNER JOIN `order` AS ord ON ord.order_id = att.order_id";

		$where    = "";
		$orderby  = " ";
		$event_id = encrypt_decrypt("decrypt",$param['event']);
		$schedule_id = encrypt_decrypt("decrypt",$param['schedule']);

		$where.=" WHERE att.is_deleted <> '1' AND att.is_active = 1 AND ord.event_id = '".$event_id."' AND atc.schedule_id = '".$schedule_id."'";
		if ($param['typeIO'] <> "") {
			$where.=" AND atc.type_att = ".$param['typeIO'];
		}
		/*---- if data recurring ----*/
		if ($param['session'] <> "") {
			$where.=" AND atc.recurring_session_id = ".$param['session'];
		}

		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (att.attendance_name like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY atc.created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;        
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_event_report($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'ev.event_name',
			2 => 'pl.place_name',
			3 => 'ev.event_start_date'
		);
		
		$sql = "SELECT ev.*, pl.place_name FROM event AS ev
		INNER JOIN place AS pl on pl.place_id = ev.event_place_id";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE ev.is_deleted <> '1' AND ev.is_active = 1";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (ev.event_name like '%".$param['search']['value']."%' ";
			$where.= " or pl.place_name like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY ev.created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

}