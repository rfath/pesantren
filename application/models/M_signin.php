<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_signin extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->model("M_groupmodule");
    }

    function checkLogin($email,$password){
    	if ($email == "superadmin@zmail.com" AND $password == "123123") {
    		$dataArr = array(
				'user_id'        => 1,
				'name'           => "superadmin",
				'email'          => $email,
				'is_super_admin' => 1
				);
			$this->session->set_userdata('sessionData',$dataArr);
    	}else{
    		$this->db->select('*');
	        $this->db->from('app_user');
			$this->db->where('user_email', $email);
	        $this->db->where('user_password', md5($password));
			$this->db->where('isDeleted',0);
	        $query = $this->db->get();
			
	        if($query->num_rows()>0){
				$querycheck = $query->row();
				
				$dataArr = array(
					'user_id'        	=> $querycheck->user_id,
					'name'           	=> $querycheck->user_full_name,
					'username'       	=> $querycheck->user_username,
					'email'          	=> $querycheck->user_email,
					'access_group_id' 	=> $querycheck->access_group_id,
					);
				$this->session->set_userdata('sessionData',$dataArr);

				// Array Module
				$listModule = $this->M_groupmodule->getModuleDetail("access_group_id", $querycheck->access_group_id);
				$this->session->set_userdata('sessionModule', $listModule);

				// Array Sub Module
				$listSubmodule = $this->M_groupmodule->getSubmodule("access_group_id", $querycheck->access_group_id);
				$this->session->set_userdata('sessionSubmodule', $listSubmodule);

				$dataModule = $this->getmoduleByAccesGroup($querycheck->access_group_id);
				foreach ($dataModule as $key => $value) {
					$dataSub = $this->getsubmoduleByAccessGroup($querycheck->access_group_id, $value->module_id);
					$menu[] = array(
						"module_name" => $value->module_name,
						"module_url"  => $value->module_url,
						"module_icon" => $value->module_icon,
						"isParent"    => $value->isParent,
						"sub"         => (array)$dataSub
					);
				}
				$this->session->set_userdata('menusession',$menu);
				

				$arrayLog = array(
					"last_login_time"	=> date("Y-m-d H:i:s"),
				);
				$this->db->update("app_user",$arrayLog, array("user_id" => $querycheck->user_id));
				return true;
	        }else{
				$this->session->set_flashdata('failedtologin', 'Yes');    
	            return false;
	        }  
    	}
    }

	function checkLoginReset($email){
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email',$email);
        $que = $this->db->get();
		if($que->num_rows() > 0){
			$data = $que->result_array();
			return $data[0];
		}else{	
			return array();
		}
    }
	
	function newPassword($param){
        $insert = array('password' => md5($param['Password']));
		if($this->kppdb->update('admin',$insert,array('id_admin' => $param['id_admin']))){
			return true;
		}else{
			return false;
		}
    }

    function checkLoginMember($email,$password){
    	$this->db->select("*");
    	$this->db->from("member");
    	$this->db->where("member_email",$email);
    	$this->db->where("member_password", md5($password));
    	$this->db->where("is_deleted",0);
    	$this->db->where("is_active",1);
    	$query = $this->db->get();
    	if($query->num_rows()>0){
			$querycheck = $query->row();
			
			$dataArr = array(
				'member_id'   => $querycheck->member_id,
				'member_name' => $querycheck->member_name
				);
			$this->session->set_userdata('sessionMember',$dataArr);
			$arrayLog = array(
				"last_login_time"	=> date("Y-m-d H:i:s"),
			);
			$this->db->update("member",$arrayLog,array("member_id" => $querycheck->member_id));
			return true;
        }else{
			$this->session->set_flashdata('failedtologin', 'Yes');    
            return false;
        }  
    }

    function checklostpassword($member_id,$expired_date){
    	$this->db->select("*");
    	$this->db->from("reset_password_request");
    	$this->db->where("member_id",$member_id);
    	$this->db->where("status",0);
    	$this->db->where("expired_date >=",date("Y-m-d H:i:s"));
    	$this->db->where("expired_date", $expired_date);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getmoduleByAccesGroup($acc_id){
    	$this->db->select("am.module_id, am.module_name, am.module_url, am.isParent, am.module_icon");
    	$this->db->from("app_access_group_modulDetail AS agm");
    	$this->db->join("app_module AS am","am.module_id = agm.access_module_id");
    	$this->db->where("access_group_id",$acc_id);
    	$this->db->where("am.module_status",1);
    	$this->db->where("am.isDeleted",0);
    	$this->db->order_by("module_order","ASC");
		$query  = $this->db->get();
		$result = $query->result();
    	return $result;
    }

    function getsubmoduleByAccessGroup($acc_id, $module_id){
    	$this->db->select("asm.submodule_id, asm.submodule_name, asm.submodule_url");
    	$this->db->from("app_access_group_submodule AS ags");
    	$this->db->join("app_submodule AS asm","asm.submodule_id = ags.access_submodule_id");
    	$this->db->join("app_module AS am","am.module_id = asm.module_id");
    	$this->db->where("ags.access_group_id",$acc_id);
    	$this->db->where("asm.module_id",$module_id);
    	$this->db->where("asm.submodule_status",1);
    	$this->db->where("asm.isDeleted",0);
    	$this->db->order_by("asm.submodule_order","ASC");
		$query  = $this->db->get();
		$result = $query->result_array();
    	return $result;
    }
}
?>