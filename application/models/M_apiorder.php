<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_apiorder extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function checkTransaction($order_id){
    	$this->db->select("*");
    	$this->db->from("order");
    	$this->db->where("order_id",$order_id);
    	$this->db->where("payment_status",0);
		$query  = $this->db->get();
		$result = $query->row();
    	return $result;
    }

    function getDoneLog($id){
    	$this->db->select("*");
    	$this->db->from("doku_log");
    	$this->db->where("type","DONE");
    	$this->db->where("id",$id);
		$query  = $this->db->get();
		$result = $query->row();
    	return $result;
    }
}