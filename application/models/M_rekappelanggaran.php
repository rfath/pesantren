<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_rekappelanggaran extends CI_Model{
    function __construct(){
        parent::__construct();
    }


    function listSantriMelanggar($condition, $val){
        $this->db->select("ps.*, st.siswa_name, pl.pelanggaran_name");
        $this->db->from("pelanggaran_siswa ps");
        $this->db->join("siswa st","ps.siswaId = st.siswa_id");
        $this->db->join("pelanggaran pl","ps.pelanggaran_id = pl.pelanggaran_id");
        $this->db->where("ps.".$condition, $val);
        // $this->db->where("status", 1);
        // $this->db->where("isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getListPelanggaran($siswaId){
        $this->db->select("ps.*, st.siswa_name, pl.pelanggaran_name");
        $this->db->from("pelanggaran_siswa ps");
        $this->db->join("siswa st","ps.siswaId = st.siswa_id");
        $this->db->join("pelanggaran pl","ps.pelanggaran_id = pl.pelanggaran_id");
        $this->db->where("siswaId", $siswaId);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getListPelanggaranRange($siswaId, $tgl_awal="", $tgl_akhir=""){
        $this->db->select("ps.*, st.siswa_name, pl.pelanggaran_name");
        $this->db->from("pelanggaran_siswa ps");
        $this->db->join("siswa st","ps.siswaId = st.siswa_id");
        $this->db->join("pelanggaran pl","ps.pelanggaran_id = pl.pelanggaran_id");
        $this->db->where("ps.created_date >=", $tgl_awal." 00:00:00");
        $this->db->where("ps.created_date <=", $tgl_akhir." 23:59:59");
        $this->db->where("siswaId", $siswaId);
        // $this->db->where("isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    // function getKelas(){
    // 	$this->db->select("*");
    // 	$this->db->from("kelas");
    //     $this->db->where("status", 1);
    // 	$this->db->where("isDeleted", 0);
    // 	$query	= $this->db->get();
    // 	$result = $query->result();
    // 	return $result;
    // }

    // function getKelasDetail($condition, $val){
    // 	$this->db->select("*");
    // 	$this->db->from("kelas");
    // 	$this->db->where($condition, $val);
    // 	$query	= $this->db->get();
    // 	$result = $query->row();
    // 	return $result;
    // }

    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_pelanggan_siswa($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'st.siswa_nomor_induk',
            2 => 'st.siswa_nisn',
            3 => 'st.siswa_name',
            4 => 'st.siswa_no_hp',
        );

        $sql = "SELECT ps.*, st.siswa_nomor_induk, st.siswa_name FROM pelanggaran_siswa AS ps
                INNER JOIN siswa AS st on ps.siswaId = st.siswa_id";

        // $sql = "SELECT * FROM siswa AS st";

        $where = "";
        $orderby = " ";

        // if (!empty($param['status'])) {
        //     $where.= " AND sk.tahun_pelajaran_id='".$param['status']."'";
        //     // $where.= " AND sk.tahun_pelajaran_id='1' AND sk.kelas_rombel_id='1'";
        // }else{
        //     $where.= " AND sk.tahun_pelajaran_id != ''";
        // }
        
        // $where.=" WHERE sk.isDeleted <> '1' AND sk.status = '1'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (st.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" GROUP BY ps.siswaId ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" GROUP BY ps.siswaId ORDER BY st.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        $query = $this->db->query($sql);
        return $query;
    }

    /*===================================== Detail Pelanggaran =====================================*/

    function get_list_pelanggaran_siswa($param = array(),$id, $method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'pelanggaran_name',
            2 => 'siswa_name',
            3 => "hukuman",
        );
        
        $sql = "SELECT pelanggaran_siswa.*, pelanggaran.pelanggaran_name, siswa.siswa_name FROM pelanggaran_siswa 
                INNER JOIN pelanggaran ON pelanggaran.pelanggaran_id = pelanggaran_siswa.pelanggaran_id 
                INNER JOIN siswa ON siswa.siswa_id = pelanggaran_siswa.siswaId ";
        $where = "";
        $orderby = " ";
        
        $where.=" WHERE siswaId = '$id'";

        if (!empty($param['tgl_awal'] && $param['tgl_akhir'])) {
            $where.= " AND (pelanggaran_siswa.created_date BETWEEN '".date("Y-m-d", strtotime($param['tgl_awal']))." 00:00:00' AND '".date("Y-m-d", strtotime($param['tgl_akhir']))." 23:59:59')";
            // $where.= " AND sk.tahun_pelajaran_id='1' AND sk.kelas_rombel_id='1'";
        }else{
            $where.= " AND pelanggaran_siswa.created_date != ''";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (pelanggaran_name like '%".$param['search']['value']."%' ";
            $where.= " or siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or hukuman like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY pelanggaran_name DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }
}