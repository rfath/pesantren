<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pembagiankelas extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getPKelas(){
        $this->db->select("sk.*, kr.kelas_rombel_name, kl.kelas_name, st.siswa_name");
        $this->db->from("siswa_kelas sk");
        $this->db->join("siswa st","sk.siswa_id = st.siswa_id");
        $this->db->join("kelas_rombel kr","sk.kelas_rombel_id = kr.kelas_rombel_id");
        $this->db->join("kelas kl","kr.kelas_id = kl.kelas_id");
        $this->db->where("sk.status", 1);
        $this->db->where("sk.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getPKelasDetail($condition, $val){
        $this->db->select("sk.*, kr.kelas_rombel_name, kl.kelas_name, st.siswa_name");
        $this->db->from("siswa_kelas sk");
        $this->db->join("siswa st","sk.siswa_id = st.siswa_id");
        $this->db->join("kelas_rombel kr","sk.kelas_rombel_id = kr.kelas_rombel_id");
        $this->db->join("kelas kl","kr.kelas_id = kl.kelas_id");
        $this->db->where("sk.".$condition, $val);
        $this->db->where("sk.status", 1);
        $this->db->where("sk.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getInfoSiswaKelas($tp, $kelas_id, $rombel_id, $siswa_id){
        $this->db->select("*");
        $this->db->from("siswa_kelas");
        $this->db->where("tahun_pelajaran_id", $tp);
        $this->db->where("kelas_id", $kelas_id);
        $this->db->where("kelas_rombel_id", $rombel_id);
        $this->db->where("siswa_id", $siswa_id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

     /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_list_kelas_detial($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'st.siswa_nomor_induk',
            2 => 'st.siswa_nisn',
            3 => 'st.siswa_name',
            4 => 'kr.kelas_rombel_name',
            5 => 'kl.kelas_name',
            6 => 'sk.tahun_pelajaran_id',
            7 => 1
        );
        
        $sql = "SELECT sk.*, 
            st.siswa_nomor_induk, 
            st.siswa_name, 
            kr.kelas_rombel_name, 
            kl.kelas_name,
            st.siswa_nisn
        FROM siswa_kelas AS sk
        INNER JOIN siswa AS st on sk.siswa_id = st.siswa_id
        INNER JOIN kelas AS kl on sk.kelas_id = kl.kelas_id
        INNER JOIN kelas_rombel AS kr on sk.kelas_rombel_id = kr.kelas_rombel_id";

        // $sql = "SELECT * FROM siswa AS st";

        $where = "";
        $orderby = " ";

        if (!empty($param['status'])) {
            $where.= " AND sk.tahun_pelajaran_id='".$param['status']."'";
            // $where.= " AND sk.tahun_pelajaran_id='1' AND sk.kelas_rombel_id='1'";
        }else{
            $where.= " AND sk.tahun_pelajaran_id != ''";
        }
        
        $where.=" WHERE sk.isDeleted <> '1' AND sk.status = '1'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (st.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY st.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }
}