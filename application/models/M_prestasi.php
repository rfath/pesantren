<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_prestasi extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getDetailPrestasi($id){
    	$this->db->select("*");
    	$this->db->from("prestasi");
        //$this->db->join("app_access_group", 'app_access_group.access_group_id = app_user.access_group_id');
    	$this->db->where("prestasi_id",$id);
    	$this->db->where("isDeleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getDetailPrestasiSiswa($id){
        $this->db->select("*");
        $this->db->from("prestasi_siswa");
        //$this->db->join("app_access_group", 'app_access_group.access_group_id = app_user.access_group_id');
        $this->db->where("prestasi_siswa_id",$id);
        $this->db->where("isDeleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_list_prestasi($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'prestasi_name',
			2 => 'created_date',
            3 => "created_by",
            4 => "update_date",
            5 => "status",
            6 => "1"
		);
		
		$sql = "SELECT * FROM prestasi";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE isDeleted <> '1'";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (prestasi_name like '%".$param['search']['value']."%' ";
			//$where.= " or user_email like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY prestasi_name DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

    function get_list_siswa($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'siswa_name',
            2 => 'siswa_nomor_induk',
            3 => 'siswa_nisn',
   
        );
        
        $sql = "SELECT * FROM siswa";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE isDeleted = '0'";
        $where.=" AND is_graduate = '0'";
        $where.=" AND status = '1'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY siswa_name DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }

    function get_list_prestasi_siswa($param = array(),$id, $method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'prestasi_name',
            2 => 'siswa_name',
            
        );
        
        $sql = "SELECT * FROM prestasi_siswa INNER JOIN prestasi ON prestasi.prestasi_id = prestasi_siswa.prestasi_id INNER JOIN siswa ON siswa.siswa_id = prestasi_siswa.siswaId";

        $where = "";
        $orderby = " ";
        
        //$where.=" WHERE isDeleted <> '1'";
        $where.=" AND siswaId = '$id'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (prestasi_name like '%".$param['search']['value']."%' ";
            //$where.= " or user_email like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY prestasi_name ASC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }

    function getListPrestasi(){
        $this->db->select('*');
        $this->db->from('prestasi');
        $this->db->where('isDeleted',0);
        $que = $this->db->get()->result_array();
        return $que;
    }

    function getSiswa(){
        $this->db->select('*');
        $this->db->from('siswa');
        //$this->db->where('siswa_id',$id);
        $this->db->where('isDeleted',0);
        $que = $this->db->get()->result_array();
        return $que;
    }
}