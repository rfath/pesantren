<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_rekapkeamanan extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    // function listPerizinan($condition, $val){
    //     $this->db->select("ik.*, sw.siswa_name, sw.siswa_nomor_induk, sw.siswa_nisn");
    //     $this->db->from("izin_keluar ik");
    //     $this->db->join("siswa sw","ik.siswa_id = sw.siswa_id");
    //     $this->db->where("ps.".$condition, $val);
    //     // $this->db->where("status", 1);
    //     // $this->db->where("isDeleted", 0);
    //     $query  = $this->db->get();
    //     $result = $query->result();
    //     return $result;
    // }

    function getListPerizinan($tgl_awal="", $tgl_akhir=""){
        $this->db->select("ik.*, sw.siswa_name, sw.siswa_nomor_induk, sw.siswa_nisn");
        $this->db->from("izin_keluar ik");
        $this->db->join("siswa sw","ik.siswa_id = sw.siswa_id");
        // $this->db->where($type, $type_val);
        $this->db->where("ik.created_date >=", $tgl_awal." 00:00:00");
        $this->db->where("ik.created_date <=", $tgl_akhir." 23:59:59");
        $this->db->where("ik.status", 1);
        $this->db->where("ik.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getListtPerizinanRange($type, $type_val, $tgl_awal="", $tgl_akhir=""){
        $this->db->select("ik.*, sw.siswa_name, sw.siswa_nomor_induk, sw.siswa_nisn");
        $this->db->from("izin_keluar ik");
        $this->db->join("siswa sw","ik.siswa_id = sw.siswa_id");
        $this->db->where("ik.".$type, $type_val);
        $this->db->where("ik.created_date >=", $tgl_awal." 00:00:00");
        $this->db->where("ik.created_date <=", $tgl_akhir." 23:59:59");
        $this->db->where("ik.status", 1);
        $this->db->where("ik.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function get_list_izinkeluar($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'sw.siswa_name',
            2 => 'ik.keperluan',
            3 => 'ik.created_date',
            4 => 'ik.time_limit',
            5 => 1
        );

        $sql = "SELECT ik.*, sw.siswa_id, sw.siswa_name, sw.siswa_nomor_induk, sw.siswa_nisn FROM izin_keluar AS ik
                INNER JOIN siswa AS sw ON sw.siswa_id = ik.siswa_id";

        $where   = "";
        $orderby = " ";
        
        $where.=" WHERE ik.status = 1 AND ik.isDeleted = 0 AND ik.izin_type = 1";
        
        if ($param['status'] == "") {
            $where.= " AND (ik.created_date BETWEEN '".date("Y-m-d", strtotime($param['tgl_awal']))." 00:00:00' AND '".date("Y-m-d", strtotime($param['tgl_akhir']))." 23:59:59')";
        }elseif($param['status'] == "0"){
            // $where.= " AND (return_date = '' OR return_date IS NULL)";
            $where.= " AND (ik.created_date BETWEEN '".date("Y-m-d", strtotime($param['tgl_awal']))." 00:00:00' AND '".date("Y-m-d", strtotime($param['tgl_akhir']))." 23:59:59') AND (return_date = '' OR return_date IS NULL)";
        }else{
            // $where.= " AND return_date <> ''";
            $where.= " AND (ik.created_date BETWEEN '".date("Y-m-d", strtotime($param['tgl_awal']))." 00:00:00' AND '".date("Y-m-d", strtotime($param['tgl_akhir']))." 23:59:59') AND return_date <> ''";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (sw.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or ik.keperluan like '%".$param['search']['value']."%' ";
            $where.= " or sw.siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " or sw.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY izin_keluar_id DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        
        $query = $this->db->query($sql);
        return $query;
    }
}