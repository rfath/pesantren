<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_alumni extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function listAlumni($condition, $val){
        $this->db->select("al.*, st.siswa_nomor_induk, st.siswa_nisn, st.siswa_name, st.siswa_no_hp, st.siswa_alamat, tp.tahun_pelajaran_name");
        $this->db->from("alumni al");
        $this->db->join("siswa st","al.siswa_id = st.siswa_id");
        $this->db->join("tahun_pelajaran tp","al.tahun_pelajaran_id = tp.tahun_pelajaran_id");
        $this->db->where("al.".$condition, $val);
        // $this->db->where("status", 1);
        // $this->db->where("isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_list_alumni($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'st.siswa_nomor_induk',
            2 => 'st.siswa_nisn',
            3 => 'st.siswa_name',
            4 => 'tp.tahun_pelajaran_name',
            5 => 1
        );

        $sql = "SELECT al.*, st.siswa_nomor_induk, st.siswa_nisn, st.siswa_name, st.siswa_no_hp, st.siswa_alamat, tp.tahun_pelajaran_name FROM alumni AS al
                INNER JOIN siswa AS st ON al.siswa_id = st.siswa_id
                INNER JOIN tahun_pelajaran as tp ON al.tahun_pelajaran_id = tp.tahun_pelajaran_id";

        $where   = "";
        $orderby = " ";
        
        // $where.=" WHERE al.status = 1 AND al.isDeleted = 0";
        
        if (!empty($param['tp'])) {
            $where.= " AND al.tahun_pelajaran_id = '".$param['tp']."'";
        }else{
            $where.= " AND al.tahun_pelajaran_id != ''";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (sw.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY tp.tahun_pelajaran_name DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        $query = $this->db->query($sql);
        return $query;
    }
}