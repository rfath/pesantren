<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_room extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getRoomDetail($id){
    	$this->db->select("*");
    	$this->db->from("room");
    	$this->db->where("room_id",$id);
    	$this->db->where("is_deleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function get_list_room($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'rm.room_name',
			2 => 'pl.place_name',
			3 => 'rm.room_description',
		);
		
		$sql = "SELECT rm.*, pl.place_name FROM room AS rm
		INNER JOIN place AS pl ON pl.place_id = rm.place_id";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE rm.is_deleted <> '1' AND rm.is_active = 1";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (rm.room_name like '%".$param['search']['value']."%' ";
			$where.= " or rm.room_description like '%".$param['search']['value']."%' ";
			$where.= " or pl.place_name like '%".$param['search']['value']."%' ";
			$where.= " ) "; 
			//$where.= "AND vendor_status = '1'";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY rm.created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}
}