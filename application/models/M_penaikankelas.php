<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_penaikankelas extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getKelas(){
    	$this->db->select("*");
    	$this->db->from("kelas");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getSiswaKelasDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("siswa_kelas");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkHistory($siswa_id, $kelas_id, $tahun_pelajaran_id){
        $this->db->select("*");
        $this->db->from("siswa_kelas_history AS skh");
        $this->db->where("siswa_id",$siswa_id);
        $this->db->where("kelas_id",$kelas_id);
        $this->db->where("tahun_pelajaran_id",$tahun_pelajaran_id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    /*===================================== FUNCTION FOR DATATABLE Rombel =====================================*/
    function get_siswa_kelas_rombel($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'st.siswa_nomor_induk',
            2 => 'st.siswa_nisn',
            3 => 'st.siswa_name',
            4 => 'st.siswa_no_hp',
        );

        $sql = "SELECT sk.*, st.siswa_nomor_induk, st.siswa_name, kr.kelas_rombel_name, kl.kelas_name FROM siswa_kelas AS sk
                INNER JOIN siswa AS st on sk.siswa_id = st.siswa_id
                INNER JOIN kelas_rombel AS kr on sk.kelas_rombel_id = kr.kelas_rombel_id
                INNER JOIN kelas AS kl on sk.kelas_id = kl.kelas_id";
                // INNER JOIN kelas AS kl on kr.kelas_id = kl.kelas_id";

        // $sql = "SELECT * FROM siswa AS st";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE sk.isDeleted <> '1' AND sk.status = '1'";
        // $where.=" WHERE sk.isDeleted <> '1' AND sk.status = '1'";
        if (!empty($param['tahun_pelajaran'] && $param['kelas'])) {
            $where.= " AND sk.tahun_pelajaran_id='".$param['tahun_pelajaran']."' AND sk.kelas_id='".$param['kelas']."' GROUP BY sk.kelas_rombel_id, sk.kelas_id";
        }else{
            $where.= " AND sk.tahun_pelajaran_id= '' AND sk.kelas_id= '' GROUP BY sk.kelas_rombel_id, sk.kelas_id";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }

            // $where.= " GROUP BY sk.kelas_rombel_id";
            $where.= " (st.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY sk.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        $query = $this->db->query($sql);
        return $query;
    }

    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_siswa_kelas_detail($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'st.siswa_nomor_induk',
            2 => 'st.siswa_nisn',
            3 => 'st.siswa_name',
            4 => 'st.siswa_no_hp',
        );

        $sql = "SELECT sk.*, st.siswa_nomor_induk, st.siswa_name, kr.kelas_rombel_name, kl.kelas_name, st.is_graduate FROM siswa_kelas AS sk
                INNER JOIN siswa AS st on sk.siswa_id = st.siswa_id
                INNER JOIN kelas_rombel AS kr on sk.kelas_rombel_id = kr.kelas_rombel_id
                INNER JOIN kelas AS kl on sk.kelas_id = kl.kelas_id";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE sk.isDeleted <> '1' AND sk.status = '1'";
        if (!empty($param['tahun_pelajaran'] && $param['rombel'])) {
            $where.= " AND sk.tahun_pelajaran_id='".$param['tahun_pelajaran']."' AND sk.kelas_rombel_id='".$param['rombel']."' AND sk.kelas_id='".$param['kelas_id']."'";
        }else{
            $where.= " AND sk.tahun_pelajaran_id= '' AND sk.kelas_rombel_id= ''  AND sk.kelas_rombel_id=''";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }

            // $where.= " GROUP BY sk.kelas_rombel_id";
            $where.= " (st.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY sk.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        $query = $this->db->query($sql);
        return $query;
    }
}