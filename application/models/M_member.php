<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_member extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getMemberDetail($where, $id){
    	$this->db->select("*");
    	$this->db->from("member");
    	$this->db->where($where, $id);
    	$this->db->where("is_deleted", 0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

  //   function getPackageDetail($id){
  //   	$this->db->select("*");
  //   	$this->db->from("event_package");
  //   	$this->db->where("event_package_id",$id);
  //   	$this->db->where("is_deleted",0);
		// $query  = $this->db->get();
		// $result = $query->row();
  //   	return $result;
  //   }

    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_list_member($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'mb.member_name',
			2 => 'mb.member_phone',
			3 => 'mb.member_email',
			4 => 'mb.member_address',
		);

		$sql = "SELECT mb.*, ap.province_name FROM member AS mb
				LEFT JOIN app_province AS ap on mb.province_id = ap.province_id
				LEFT JOIN app_city AS ac on mb.city_id = ac.city_id";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE mb.is_deleted <> '1' AND mb.is_active = 1";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (mb.member_name like '%".$param['search']['value']."%' ";
			$where.= " or mb.member_email like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY mb.created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_member_report($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'mb.member_name',
			2 => 'mb.member_phone',
			3 => 'mb.member_email',
		);
		
		$sql = "SELECT mb.*, ap.province_name FROM member AS mb
				LEFT JOIN app_province AS ap on mb.province_id = ap.province_id
				LEFT JOIN app_city AS ac on mb.city_id = ac.city_id";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE mb.is_deleted <> '1' AND mb.is_active = 1";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (mb.member_name like '%".$param['search']['value']."%' ";
			$where.= " or mb.member_email like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY mb.member_name ASC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}
}