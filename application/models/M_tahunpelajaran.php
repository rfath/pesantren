<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tahunpelajaran extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getTahunPelajaran(){
    	$this->db->select("*");
    	$this->db->from("tahun_pelajaran");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
        $this->db->order_by("tahun_pelajaran_name","DESC");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getTahunPelajaraneDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("tahun_pelajaran");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }
}