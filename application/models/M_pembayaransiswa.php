<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pembayaransiswa extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getPembayaranSiswa(){
    	$this->db->select("pis.*, pb.pembayaran_title, st.siswa_name, st.siswa_nomor_induk");
    	$this->db->from("pembayaran_install_siswa pis");
        $this->db->join("pembayaran pb","pis.pembayaran_id = pb.pembayaran_id");
        $this->db->join("siswa st","pis.siswa_id = st.siswa_id");
        $this->db->where("pis.status", 1);
    	$this->db->where("pis.isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getPembayaranSiswaDetail($condition, $val){
    	$this->db->select("pis.*, pb.pembayaran_title, st.siswa_name, st.siswa_nomor_induk");
        $this->db->from("pembayaran_install_siswa pis");
        $this->db->join("pembayaran pb","pis.pembayaran_id = pb.pembayaran_id");
        $this->db->join("siswa st","pis.siswa_id = st.siswa_id");
    	$this->db->where("pis.".$condition, $val);
        $this->db->where("pis.status", 1);
        $this->db->where("pis.isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function pembayaranDetail(){
        
    }
}