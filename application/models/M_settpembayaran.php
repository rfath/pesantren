<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_settpembayaran extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getPembayaranItem(){
    	$this->db->select("*");
    	$this->db->from("pembayaran_item");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
        $this->db->order_by("title","ASC");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getPembayaranItemDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("pembayaran_item");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function get_list_pembayaran_item($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'pi.title',
            2 => 'pi.price',
            // 3 => 'st.siswa_name',
            // 4 => 'st.siswa_no_hp',
        );

        $sql = "SELECT * FROM pembayaran_item AS pi";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE pi.isDeleted <> '1' AND pi.status = '1'";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (pi.title like '%".$param['search']['value']."%' ";
            $where.= " or pi.price like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY pi.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }
}