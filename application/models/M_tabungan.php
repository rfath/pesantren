<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tabungan extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getDetailSiswa($siswa_id){
        $this->db->select("*");
        $this->db->from("siswa");
        $this->db->where("siswa_id",$siswa_id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getRiwayatTabungan($siswa_id){
        $this->db->select("*");
        $this->db->from("tabungan_transfer");
        $this->db->where("siswa_id",$siswa_id);
        $this->db->order_by("created_date","DESC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }
}