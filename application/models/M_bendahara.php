<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bendahara extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getKas(){
    	$this->db->select("*");
    	$this->db->from("uang_kas");
        $this->db->where("kas_type !=", "");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getKasDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("uang_kas");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_list_kas($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'ak.kas_name',
            2 => 'ak.kas_type',
            3 => 'ak.kas_transaction',
            4 => 'ak.created_date',
        );

        $sql = "SELECT * FROM uang_kas AS ak";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE ak.isDeleted <> '1' AND ak.status = '1' AND ak.is_from_kas = 1";

        if (!empty($param['kas_type'])) {
            $where.= " AND ak.kas_type='".$param['kas_type']."'";
        }else{
            $where.= " AND ak.kas_type != ''";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
        
            $where.= " (ak.kas_name like '%".$param['search']['value']."%' ";
            $where.= " or ak.kas_type like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY ak.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        $query = $this->db->query($sql);
        return $query;
    }
}