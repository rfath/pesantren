<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_order extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function orderDetail($id){
    	$this->db->select("ord.*,ev.event_name, pc.package_name, mb.member_name, mb.member_email, mb.member_phone");
    	$this->db->from("order AS ord");
    	$this->db->join("event AS ev","ev.event_id = ord.event_id");
    	$this->db->join("event_package AS pc","pc.event_package_id = ord.package_id");
    	$this->db->join("member AS mb","mb.member_id = ord.member_id");
    	$this->db->where("ord.order_id",$id);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkOrderCode($event_id,$order_id){
    	$this->db->select("atd.*, od.payment_remarks");
    	$this->db->from("attendance AS atd");
    	$this->db->join("order AS od","od.order_id = atd.order_id");
    	$this->db->where("atd.order_id",$order_id);
    	$this->db->where("od.event_id",$event_id);
    	//$this->db->where("(atd.card_number = '' or atd.card_number IS NULL)");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getAttendance($event_id){
    	$this->db->select("atd.*, pck.package_name, od.payment_remarks");
    	$this->db->from("attendance AS atd");
    	$this->db->join("order AS od","od.order_id = atd.order_id");
        $this->db->join("event_package AS pck", "pck.event_package_id = od.package_id");
    	$this->db->where("od.event_id",$event_id);
    	$this->db->where("atd.card_number <> ''");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function checkCardNumber($event_id,$card_number, $attendance_id = ""){
    	$this->db->select("att.card_number");
    	$this->db->from("attendance AS att");
    	$this->db->join("order AS ord","ord.order_id = att.order_id");
    	$this->db->where("ord.event_id",$event_id);
    	$this->db->where("att.card_number", $card_number);
    	if ($attendance_id <> "") {
    		$this->db->where("attendance_id <> ",$attendance_id);
    	}
    	$query = $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getDetailAttendance($attendance_id){
    	$this->db->select("att.*, ord.package_id, ord.event_id, ord.payment_remarks");
    	$this->db->from("attendance AS att");
        $this->db->join("`order` AS ord", "att.order_id = ord.order_id");
    	$this->db->where("att.attendance_id",$attendance_id);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function searchOrder($event_id, $searchKey = ""){
    	$sql = "SELECT att.attendance_name, att.attendance_email, att.attendance_number, ord.order_id, ord.order_time, att.eticket
    	FROM attendance AS att
    	INNER JOIN `order` AS ord ON ord.order_id = att.order_id
    	WHERE ord.event_id = ".$event_id." ".
    	" AND (att.attendance_name LIKE '%".$searchKey."%' OR att.eticket LIKE '%".$searchKey."%' )
    	AND (card_number = '' OR card_number IS NULL)
    	AND ord.payment_status = 2";
    	$query 	= $this->db->query($sql);
    	$result = $query->result();
    	return $result;
    }

    function getMemberActive(){
    	$this->db->select("member_name, member_id");
    	$this->db->from("member");
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$this->db->order_by("member_name");
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function checkMemberByEmail($email){
    	$this->db->select("*");
    	$this->db->from("member");
    	$this->db->where("is_deleted",0);
    	$this->db->where("member_email",$email);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkRegion($region_name){
    	$this->db->select("*");
    	$this->db->from("region");
    	$this->db->where("region_name",$region_name);
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkDPW($dpw_name){
    	$this->db->select("*");
    	$this->db->from("dpw");
    	$this->db->where("dpw_name",$dpw_name);
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkSatelitChurch($name){
    	$this->db->select("*");
    	$this->db->from("satelit_church");
    	$this->db->where("satelit_church_name",$name);
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$query 	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkChurchRole($name){
        $this->db->select("*");
        $this->db->from("church_role");
        $this->db->where("church_role_name",$name);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function checkAvailableEticket($event_id, $eticket = ""){
        $this->db->select("eticket");
        $this->db->from("order");
        $this->db->where("event_id",$event_id);
        $this->db->where("eticket",$eticket);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/
    function get_list_order($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'ord.eticket',
            2 => 'ord.order_id',
			3 => 'mbr.member_name',
			4 => 'ev.event_name',
			5 => 'pck.package_name',
			6 => 'ord.payment_status'
		);
		

		$sql = "SELECT ord.*, pck.package_name, mbr.member_name, ev.event_name FROM `order` AS ord
		INNER JOIN event AS ev ON ev.event_id = ord.event_id
		INNER JOIN event_package AS pck ON pck.event_package_id = ord.package_id
		INNER JOIN member AS mbr ON mbr.member_id = ord.member_id";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE ord.order_id <> 0";
		if ($param['event'] <> "") {
			$where.=" AND ord.event_id = ".$param['event'];
		}

		if ($param['status'] <> "") {
			$where.=" AND ord.payment_status = ".$param['status'];
		}
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (mbr.member_name like '%".$param['search']['value']."%' ";
			$where.= " or ev.event_name like '%".$param['search']['value']."%' ";
			$where.= " or pck.package_name like '%".$param['search']['value']."%' ";
            $where.= " or ord.eticket like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY ord.order_id DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        }

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_order_by_member($param = array(),$method="default",$addtional="", $member_id=""){
		$start	= $param['start'];
		$length = $param['length'];
		$member_id = $this->session->sessionMember['member_id'];
		$columns	= array(
			1 => 'ord.order_id',
			2 => 'mbr.member_name',
			3 => 'ev.event_name',
			4 => 'pck.package_name',
			5 => 'ord.payment_status'
		);
		

		$sql = "SELECT ord.*, pck.package_name, mbr.member_name, ev.event_name FROM `order` AS ord
				INNER JOIN event AS ev ON ev.event_id = ord.event_id
				INNER JOIN event_package AS pck ON pck.event_package_id = ord.package_id
				INNER JOIN member AS mbr ON mbr.member_id = ord.member_id
				WHERE ord.member_id = ".$member_id."";

		$where = "";
		$orderby = " ";
		
		$where.=" AND ord.order_id <> 0";
		if ($param['event'] <> "") {
			$where.=" AND ord.event_id = ".$param['event'];
		}

		if ($param['status'] <> "") {
			$where.=" AND ord.payment_status = ".$param['status'];
		}
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (mbr.member_name like '%".$param['search']['value']."%' ";
			$where.= " or ev.event_name like '%".$param['search']['value']."%' ";
			$where.= " or pck.package_name like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY ord.order_time DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}
}