<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pembayaran extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getPembayaran(){
    	$this->db->select("*");
    	$this->db->from("pembayaran");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getPembayaranDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("pembayaran");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function check_pembayaran(){
        $this->db->select("*");
        $this->db->from("pembayaran_install_siswa");
        $this->db->where("siswa_id",$siswa_id);
        $this->db->where("pembayaran_id",$pembayaran_id);
        $this->db->where("status",1);
        $this->db->where("isDeleted",1);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }    

    function getSiswaPerKelas($kelas_id){
        $this->db->select("sw.siswa_id, sw.siswa_name, sk.kelas_id, sk.kelas_rombel_id");
        $this->db->from("siswa AS sw");
        $this->db->join("siswa_kelas AS sk","sk.siswa_id = sw.siswa_id");
        $this->db->where("sw.status",1);
        $this->db->where("sw.isDeleted",0);
        $this->db->where("sw.is_graduate",0);
        $this->db->where("sk.kelas_id", $kelas_id);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getSiswaPerKelas2($siswa_id){
        $this->db->select("sw.siswa_id, sw.siswa_name, sk.kelas_id, sk.kelas_rombel_id");
        $this->db->from("siswa AS sw");
        $this->db->join("siswa_kelas AS sk","sk.siswa_id = sw.siswa_id");
        $this->db->where("sw.status",1);
        $this->db->where("sw.isDeleted",0);
        $this->db->where("sw.is_graduate",0);
        $this->db->where("sw.siswa_id", $siswa_id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function checkPembayaranExist($pembayaran_id, $siswa_id){
        $this->db->select("*");
        $this->db->from("pembayaran_install_siswa");
        $this->db->where("isDeleted",0);
        $this->db->where("pembayaran_id",$pembayaran_id);
        $this->db->where("siswa_id",$siswa_id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getDetailPembayaran($id){
        $this->db->select("
            pis.pembayaran_install_siswa_id,
            pb.pembayaran_id,
            pb.pembayaran_title,
            sw.siswa_nomor_induk,
            sw.siswa_name,
            sw.siswa_id,
            kl.kelas_name,
            rl.kelas_rombel_name,
            pis.no_kwitansi,
            pis.date_pay
            ");
        $this->db->from("pembayaran_install_siswa AS pis");
        $this->db->join("siswa AS sw","sw.siswa_id = pis.siswa_id");
        $this->db->join("kelas AS kl","kl.kelas_id = pis.kelas_id");
        $this->db->join("kelas_rombel AS rl","pis.rombel_id = rl.kelas_rombel_id");
        $this->db->join("pembayaran AS pb","pb.pembayaran_id = pis.pembayaran_id");
        $this->db->where("pis.isDeleted",0);
        $this->db->where("pis.pembayaran_install_siswa_id",$id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    // ============================================== Pembayaran Install Item =============================================//
    function getPembayaranInstallItem($condition, $val){
        $this->db->select("pii.*, pi.title, pi.price");
        $this->db->from("pembayaran_install_item pii");
        $this->db->join("pembayaran_item pi","pii.pembayaran_item_id = pi.pembayaran_item_id");
        $this->db->where($condition, $val);
        $this->db->where("pii.status", 1);
        $this->db->where("pii.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getPembayaranInstallItemDetail($condition, $val, $pembayaran_id){
        $this->db->select("pii.*, pi.title");
        $this->db->from("pembayaran_install_item pii");
        $this->db->join("pembayaran_item pi","pii.pembayaran_item_id = pi.pembayaran_item_id");
        $this->db->where("pii.pembayaran_id", $pembayaran_id);
        $this->db->where("pii.".$condition, $val);
        // $this->db->where("pii.status", 1);
        // $this->db->where("pii.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getInstallItemDetail($condition, $val, $pembayaran_id){
        $this->db->select("pii.*, pi.title");
        $this->db->from("pembayaran_install_item pii");
        $this->db->join("pembayaran_item pi","pii.pembayaran_item_id = pi.pembayaran_item_id");
        $this->db->where("pii.pembayaran_id", $pembayaran_id);
        $this->db->where("pii.".$condition, $val);
        $this->db->where("pii.status", 1);
        $this->db->where("pii.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_list_pembayaran($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'sw.siswa_nomor_induk',
            2 => 'sw.siswa_name',
            3 => 'pm.pembayaran_title',
            4 => 'pis.created_date',
            5 => 'pis.status',
            6 => 1
        );

        $sql = "SELECT 
            pis.pembayaran_install_siswa_id,
            sw.siswa_id, 
            sw.siswa_name,
            sw.siswa_nomor_induk,
            sw.siswa_nisn,
            pm.pembayaran_title,
            pis.created_date,
            pis.status
        FROM pembayaran_install_siswa AS pis
        INNER JOIN siswa AS sw ON sw.siswa_id = pis.siswa_id
        INNER JOIN pembayaran AS pm ON pm.pembayaran_id = pis.pembayaran_id";

        $where   = "";
        $orderby = " ";
        
        $where.=" WHERE pis.isDeleted = 0";

        if(!empty($param['search']['value'])){
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (sw.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or sw.siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or sw.siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " or pm.pembayaran_title like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY pis.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        
        $query = $this->db->query($sql);
        return $query;
    }
}