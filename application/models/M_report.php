<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_report extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getMemberActive(){
    	$this->db->select("member_id, member_name, member_email");
    	$this->db->from("member");
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$this->db->order_by("member_name","ASC");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getEventActive(){
    	$this->db->select("event_id, event_name");
    	$this->db->from("event");
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$this->db->order_by("event_name","ASC");
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }


   	/*=========================================== START SALES REPORT ===========================================*/
   	function generate_sales_report($date_start, $date_end, $member = "", $event = ""){
		$this->db->select("ord.order_id, ev.event_name, ord.order_price, ord.qty_seat, mb.member_name, ord.payment_status, ord.order_time");
		$this->db->from("`order` AS ord");
		$this->db->join("event AS ev","ev.event_id = ord.event_id");
		$this->db->join("member AS mb","mb.member_id = ord.member_id");
		$this->db->where("DATE(ord.order_time)>=",$date_start);
		$this->db->where("DATE(ord.order_time)<=",$date_end);
		$this->db->where("ord.payment_status",2);
		if ($member <> "") {
			$this->db->where("mb.member_id",$member);
		}
		if ($event <> "") {
			$this->db->where("ord.event_id",$event);
		}
		$this->db->order_by("ord.order_time","ASC");
		$query	= $this->db->get();
		$result = $query->result();
		return $result;
	}
	/*=========================================== END SALES REPORT ===========================================*/

	/*=========================================== START MEMBER REPORT ===========================================*/
	function generate_member_report($year, $member_id){
		$this->db->select("ord.order_id, ev.event_name, ord.order_price, ord.qty_seat, mb.member_name, ord.payment_status, ord.order_time, ev.event_start_date, ev.event_id");
		$this->db->from("`order` AS ord");
		$this->db->join("event AS ev","ev.event_id = ord.event_id");
		$this->db->join("member AS mb","mb.member_id = ord.member_id");
		$this->db->where("ord.payment_status",2);
		$this->db->where("YEAR(ev.event_start_date)",$year);
		$this->db->where("mb.member_id",$member_id);
		$query	= $this->db->get();
		$result = $query->result();
		return $result;
	}

	function get_check_inoutData($event_id, $member_id){
		$this->db->select("ats.type_att, evs.schedule_name, ats.created_date");
		$this->db->from("attendance_schedule AS ats");
		$this->db->join("attendance AS att","ats.attendance_id = att.attendance_id");
		$this->db->join("order AS ord","ord.order_id = att.order_id");
		$this->db->join("event_schedule AS evs","evs.event_schedule_id = ats.schedule_id");
		$this->db->where("ord.event_id",$event_id);
		$this->db->where("ord.member_id",$member_id);
		$query	= $this->db->get();
		$result = $query->result();
		return $result;
	}
	/*=========================================== END MEMBER REPORT ===========================================*/

	/*=========================================== START EVENT REPORT ===========================================*/
	function totalEventReport($event_id){
		/*===================== START TOTAL PARTICIPANT =====================*/
		$sql1 = 'SELECT COUNT(*) AS total_participant FROM attendance AS att
		INNER JOIN `order` as ord ON ord.order_id = att.order_id
		WHERE ord.event_id = '.$event_id.'
		AND ord.payment_status = 2';
		$query1	= $this->db->query($sql1);
		$data['total_participant'] = $query1->row()->total_participant;
		/*===================== END TOTAL PARTICIPANT =====================*/

		/*===================== START TOTAL SALES =====================*/
		$sql2   = 'SELECT SUM(order_price) AS total_sales FROM `order` WHERE event_id ='.$event_id;
		$query2 = $this->db->query($sql2);
		$data['total_sales'] = $query2->row()->total_sales;
		/*===================== START TOTAL SALES =====================*/

		/*===================== START TOTAL ATTENDANCE =====================*/
		$sql3 = 'SELECT COUNT(*) AS total_attendance FROM attendance AS att
		INNER JOIN `order` as ord ON ord.order_id = att.order_id
		WHERE ord.event_id = '.$event_id.'
		AND ord.payment_status = 2
		AND att.is_active = 1
		AND (att.card_number <> "" OR att.card_number IS NOT NULL)';
		$query3 = $this->db->query($sql3);
		$data['total_attendance'] = $query3->row()->total_attendance;
		/*===================== END TOTAL ATTENDANCE =====================*/

		return $data;
	}

	function er_listPP($event_id){
		$sql1 = 'SELECT att.attendance_name FROM attendance AS att
		INNER JOIN `order` as ord ON ord.order_id = att.order_id
		WHERE ord.event_id = '.$event_id.'
		AND ord.payment_status = 2';
		$query1	= $this->db->query($sql1);
		$data['list_participant'] = $query1->result();

		$sql3 = 'SELECT att.attendance_name FROM attendance AS att
		INNER JOIN `order` as ord ON ord.order_id = att.order_id
		WHERE ord.event_id = '.$event_id.'
		AND ord.payment_status = 2
		AND att.is_active = 1
		AND (att.card_number <> "" OR att.card_number IS NOT NULL)';
		$query3 = $this->db->query($sql3);
		$data['list_attendance'] = $query3->result();

		return $data;
	}

	function er_totalParticipantBySchedule($schedule_id){
		$sql = 'SELECT COUNT(*) AS total_participant FROM (SELECT schedule_id AS total_participant
		FROM `attendance_schedule`
		WHERE `schedule_id` = '.$schedule_id.'
		GROUP BY `schedule_id`, `attendance_id`) q1';
		$query  = $this->db->query($sql);
		$result = $query->row()->total_participant;
		return $result;
	}

	function er_getLIstAttendanceBySchedule($schedule_id){
		$this->db->select("att.attendance_name");
		$this->db->from("attendance_schedule AS ats");
		$this->db->join("attendance AS att","att.attendance_id = ats.attendance_id");
		$this->db->where("ats.schedule_id",$schedule_id);
		$this->db->where("att.is_active",1);
		$this->db->group_by("ats.attendance_id");
		$query 	= $this->db->get();
		$result = $query->result();
		return $result;
	}

	function getListDataAttendance($event_id){
		$sql = 'SELECT att.attendance_name, att.updated_date, regi.region_name, dp.dpw_name, sc.satelit_church_name, cr.church_role_name FROM attendance AS att
			INNER JOIN `order` as ord ON ord.order_id = att.order_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id
			WHERE ord.event_id = '.$event_id.'
			AND ord.payment_status = 2
			AND att.is_active = 1
			AND (att.card_number <> "" OR att.card_number IS NOT NULL)';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	function get_list_report_event_attendance($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'att.attendance_name',
			2 => 'att.updated_date',
			3 => 'regi.region_name',
			4 => 'dp.dpw_name',
			5 => 'sc.satelit_church_name',
			6 => 'cr.church_role_name'
		);
		
		$sql = 'SELECT att.attendance_name, att.updated_date, regi.region_name, dp.dpw_name, sc.satelit_church_name, cr.church_role_name FROM attendance AS att
			INNER JOIN `order` as ord ON ord.order_id = att.order_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id';
		$where   = "";
		$orderby = " ";
		
		$where.=' WHERE ord.event_id = '.$param['event_id'].' 
			AND ord.payment_status = 2
			AND att.is_active = 1
			AND (att.card_number <> "" OR att.card_number IS NOT NULL)';

		if($param['keyFilter'] <> ""){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			if ($param['typeFilter'] == "all") {
				$where.= " (att.attendance_name like '%".$param['keyFilter']."%' ";
				$where.= " or regi.region_name like '%".$param['keyFilter']."%' ";
				$where.= " or dp.dpw_name like '%".$param['keyFilter']."%' ";
				$where.= " or sc.satelit_church_name like '%".$param['keyFilter']."%' ";
				$where.= " or cr.church_role_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "name"){
				$where.= " (att.attendance_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "dpw"){
				$where.= " (dp.dpw_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "region"){
				$where.= " (regi.region_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "churchroll"){
				$where.= " (cr.church_role_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "satelit"){
				$where.= " (sc.satelit_church_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}
			
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY att.attendance_id ASC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;
        
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_report_event_register($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'att.attendance_name',
			2 => 'ord.order_time',
			3 => 'regi.region_name',
			4 => 'dp.dpw_name',
			5 => 'sc.satelit_church_name',
			6 => 'cr.church_role_name'
		);
		
		$sql = 'SELECT att.attendance_name, att.updated_date, regi.region_name, dp.dpw_name, sc.satelit_church_name, cr.church_role_name, ord.order_time FROM attendance AS att
			INNER JOIN `order` as ord ON ord.order_id = att.order_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id';
		$where   = "";
		$orderby = " ";
		
		$where.=' WHERE ord.event_id = '.$param['event_id'].' 
			AND ord.payment_status = 2';

		if($param['keyFilter'] <> ""){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			if ($param['typeFilter'] == "all") {
				$where.= " (att.attendance_name like '%".$param['keyFilter']."%' ";
				$where.= " or regi.region_name like '%".$param['keyFilter']."%' ";
				$where.= " or dp.dpw_name like '%".$param['keyFilter']."%' ";
				$where.= " or sc.satelit_church_name like '%".$param['keyFilter']."%' ";
				$where.= " or cr.church_role_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "name"){
				$where.= " (att.attendance_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "dpw"){
				$where.= " (dp.dpw_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "region"){
				$where.= " (regi.region_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "churchroll"){
				$where.= " (cr.church_role_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "satelit"){
				$where.= " (sc.satelit_church_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY att.attendance_id ASC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;
        
        $query = $this->db->query($sql);
        return $query;
	}

	function get_list_report_event_schedule($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'att.attendance_name',
			2 => 'ats.created_date',
			3 => 'regi.region_name',
			4 => 'dp.dpw_name',
			5 => 'sc.satelit_church_name',
			6 => 'cr.church_role_name'
		);
		
		$sql = 'SELECT att.attendance_name, att.updated_date, regi.region_name, dp.dpw_name, sc.satelit_church_name, cr.church_role_name, ats.created_date 
			FROM attendance AS att
			INNER JOIN `attendance_schedule` as ats ON ats.attendance_id = att.attendance_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id';
		$where   = "";
		$orderby = " ";
		
		$where.=' WHERE ats.schedule_id = '.$param['schedule_id'];

		if($param['keyFilter'] <> ""){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			if ($param['typeFilter'] == "all") {
				$where.= " (att.attendance_name like '%".$param['keyFilter']."%' ";
				$where.= " or regi.region_name like '%".$param['keyFilter']."%' ";
				$where.= " or dp.dpw_name like '%".$param['keyFilter']."%' ";
				$where.= " or sc.satelit_church_name like '%".$param['keyFilter']."%' ";
				$where.= " or cr.church_role_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "name"){
				$where.= " (att.attendance_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "dpw"){
				$where.= " (dp.dpw_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "region"){
				$where.= " (regi.region_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "churchroll"){
				$where.= " (cr.church_role_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}elseif($param['typeFilter'] == "satelit"){
				$where.= " (sc.satelit_church_name like '%".$param['keyFilter']."%' ";
				$where.= " ) ";
			}
		}

		$group_by = " GROUP BY att.attendance_id";

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY ats.attendance_schedule DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$group_by.$orderby;
        
        $query = $this->db->query($sql);
        return $query;
	}

	function getAttendanceDataBYSchedule($schedule_id){
		$sql = 'SELECT 
				att.attendance_name, 
				att.updated_date, 
				regi.region_name, 
				dp.dpw_name, 
				sc.satelit_church_name, 
				cr.church_role_name, 
				ats.created_date,
				att.gender,
				att.age
			FROM attendance AS att
			INNER JOIN `attendance_schedule` as ats ON ats.attendance_id = att.attendance_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id
			WHERE ats.schedule_id ='.$schedule_id.' GROUP BY att.attendance_id ORDER BY ats.attendance_schedule DESC';
		$query 	= $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	function getAllListParticipantBYEvent($event_id){
		$sql = 'SELECT 
				att.attendance_name, 
				att.updated_date, 
				regi.region_name, 
				dp.dpw_name, 
				sc.satelit_church_name, 
				cr.church_role_name,
				att.gender,
				att.age
			FROM attendance AS att
			INNER JOIN `order` as ord ON ord.order_id = att.order_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id WHERE ord.event_id = '.$event_id.' 
			AND ord.payment_status = 2';
		$query 	= $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	function getAllListAttendanceBYEvent($event_id){
		$sql = 'SELECT 
				att.attendance_id, 
				att.attendance_name, 
				att.updated_date, 
				regi.region_name, 
				dp.dpw_name, 
				sc.satelit_church_name, 
				cr.church_role_name, 
				evp.package_name, 
				att.card_number, 
				att.attendance_email, 
				att.attendance_number, 
				ord.payment_type,
				att.gender,
				att.age
			FROM attendance AS att
			INNER JOIN `order` as ord ON ord.order_id = att.order_id
			INNER JOIN event_package AS evp ON evp.event_package_id = ord.package_id
			LEFT JOIN region AS regi ON regi.region_id = att.region_id
			LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
			LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
			LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id  WHERE ord.event_id = '.$event_id.' 
			AND ord.payment_status = 2
			AND att.is_active = 1
			AND (att.card_number <> "" OR att.card_number IS NOT NULL)';
		$query 	= $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	function countChinoutBytime($schedule, $time1, $time2, $type){
		$time1 = $time1.":00:00";
		$time2 = $time2.":59:59";
		$sql = 'SELECT COUNT(*) as total FROM attendance_schedule WHERE TIME(created_date) >= "'.$time1.'" and TIME(created_date) <= "'.$time2.'" AND schedule_id = '.$schedule." AND type_att = ".$type;
		$query = $this->db->query($sql);
		$result = $query->row()->total;
		return $result;
	}

	function getListInoutExcel($schedule_id,$attendance){
		if ($attendance == "All") {
			$where = "";
		}else{
			$where = " AND ats.attendance_id = ".$attendance;
		}
		$sql = "SELECT 
			att.attendance_name,
			ats.*,
			regi.region_name,
			dp.dpw_name,
			sc.satelit_church_name,
			cr.church_role_name,
			att.gender,
			att.age
		FROM attendance_schedule AS ats
		INNER JOIN attendance AS att ON att.attendance_id = ats.attendance_id
		LEFT JOIN region AS regi ON regi.region_id = att.region_id
		LEFT JOIN dpw AS dp ON dp.dpw_id = att.dpw_id
		LEFT JOIN satelit_church AS sc ON sc.satelit_church_id = att.satelit_church_id
		LEFT JOIN church_role AS cr ON cr.church_role_id = att.church_role_id  
		WHERE att.is_active = 1
		AND ats.schedule_id = ".$schedule_id.$where;

		$query 	= $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	/*=========================================== END EVENT REPORT ===========================================*/
}