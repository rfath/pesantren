<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dashboard extends CI_Model{
    function __construct(){
        parent::__construct();
    }


    function getTotalData($dateArray = array()){
    	$sqlTotalEvent = "SELECT 
    		COUNT(*) AS total_event FROM event 
    		WHERE DATE(event_start_date) >= '".$dateArray['dateStart']."' 
    		AND DATE(event_start_date) <= '".$dateArray['dateEnd']."'";
    	$queryTotalEvent = $this->db->query($sqlTotalEvent);
    	$data['total_event'] = $queryTotalEvent->row()->total_event;

    	$sqlTotalPayment = "SELECT SUM(order_price) AS total_payment FROM `order`
    		WHERE DATE(payment_time) >= '".$dateArray['dateStart']."'
    		AND DATE(payment_time) <= '".$dateArray['dateEnd']."'
    		AND payment_status = 2";
    	$queryTotalPayment = $this->db->query($sqlTotalPayment);
    	$data['total_payment'] = $queryTotalPayment->row()->total_payment;

    	$sqlTotalOrder = "SELECT COUNT(*) AS total_order FROM `order`
    		WHERE DATE(payment_time) >= '".$dateArray['dateStart']."'
    		AND DATE(payment_time) <= '".$dateArray['dateEnd']."'
    		AND payment_status = 2";
    	$queryTotalOrder = $this->db->query($sqlTotalOrder);
    	$data['total_order'] = $queryTotalOrder->row()->total_order;
    	return $data;
    }

    function get10LastRegister(){
    	$this->db->select("member_name, created_date");
    	$this->db->from("member");
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$this->db->order_by("created_date","DESC");
    	$this->db->limit(10);
    	$query = $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function get10LastOrder(){
    	$this->db->select("ord.order_id, ord.order_time, ord.payment_status, member_name, ord.order_participant_name");
    	$this->db->from("order AS ord");
    	$this->db->join("member AS mb","mb.member_id = ord.member_id");
    	$this->db->order_by("order_time","DESC");
    	$this->db->limit(10);
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function get10LastAttendance(){
    	$this->db->select("attendance_name, attendance_id, updated_date");
    	$this->db->from('attendance');
    	$this->db->where("is_active",1);
    	$this->db->order_by("updated_date","DESC");
    	$this->db->limit(10);
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function get10CheckInOut(){
    	$this->db->select("ats.attendance_schedule, ats.type_att, ats.created_date, att.attendance_name");
    	$this->db->from("attendance_schedule AS ats");
    	$this->db->join("attendance AS att", "att.attendance_id = ats.attendance_id");
    	$this->db->order_by("ats.created_date","DESC");
    	$this->db->limit(10);
    	$query 	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }
}