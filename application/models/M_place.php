<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_place extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getDetailPlace($id){
    	$this->db->select("*");
    	$this->db->from("place");
    	$this->db->where("place_id",$id);
    	$this->db->where("is_deleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function get_list_place($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'place_name',
			2 => '1',
		);
		
		$sql = "SELECT * FROM place";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE is_deleted <> '1' AND is_active = 1";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (place_name like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY created_date DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}
}