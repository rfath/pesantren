<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bukujurnal extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    // function getKas(){
    // 	$this->db->select("*");
    // 	$this->db->from("uang_kas");
    //     $this->db->where("kas_type !=", "");
    //     $this->db->where("status", 1);
    // 	$this->db->where("isDeleted", 0);
    // 	$query	= $this->db->get();
    // 	$result = $query->result();
    // 	return $result;
    // }

    function getKasDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("uang_kas");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    /*===================================== Laporan Buku Jurnal =====================================*/

    function getListJurnal($type, $type_val, $tgl_awal="", $tgl_akhir=""){
        $this->db->select("*");
        $this->db->from("uang_kas");
        $this->db->where($type, $type_val);
        $this->db->where("created_date >=", $tgl_awal." 00:00:00");
        $this->db->where("created_date <=", $tgl_akhir." 23:59:59");
        $this->db->where("status", 1);
        $this->db->where("isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_list_kas($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'ak.kas_name',
            2 => 'ak.kas_type',
            3 => 'ak.kas_transaction',
            4 => 'ak.created_date',
        );

        $sql = "SELECT * FROM uang_kas AS ak";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE ak.isDeleted <> '1' AND ak.status = '1'";
        
        if (!empty($param['kas_type'])) {
            $where.= " AND ak.kas_type ='".$param['kas_type']."' AND (ak.created_date BETWEEN '".date("Y-m-d", strtotime($param['tgl_awal']))." 00:00:00' AND '".date("Y-m-d", strtotime($param['tgl_akhir']))." 23:59:59') ";
        }else{
            $where.= " AND ak.kas_type !='' AND (ak.created_date BETWEEN '".date("Y-m-d", strtotime($param['tgl_awal']))." 00:00:00' AND '".date("Y-m-d", strtotime($param['tgl_akhir']))." 23:59:59') ";
        }

        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }

            // $where.= " GROUP BY sk.kelas_rombel_id";
            $where.= " (ak.kas_name like '%".$param['search']['value']."%' ";
            $where.= " or ak.kas_type like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY ak.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;
        $query = $this->db->query($sql);
        // debugCode($sql);
        return $query;
    }
}