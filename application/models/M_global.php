<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_global extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getListPlace(){
    	$this->db->select("*");
    	$this->db->from("place");
    	$this->db->where("is_active",1);
    	$this->db->where("is_deleted",0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getListRoom(){
        $this->db->select("*");
        $this->db->from("room");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getPackageByEvent($event_id){
        $this->db->select("*");
        $this->db->from("event_package");
        $this->db->where("event_id",$event_id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getProvince(){
        $this->db->select("*");
        $this->db->from("app_province");
        $this->db->order_by("province_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getProvinceById($id){
        $this->db->select("*");
        $this->db->from("app_province");
        $this->db->where("province_id", $id);
        $this->db->order_by("province_name","ASC");
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getCityById($id){
        $this->db->select("*");
        $this->db->from("app_city");
        $this->db->where("city_id", $id);
        $this->db->order_by("city_name","ASC");
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getCityByProvince($id){
        $this->db->select("*");
        $this->db->from("app_city");
        $this->db->where("province_id",$id);
        $this->db->order_by('city_name','ASC');
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function checkEmailMember($email){
        $email = str_replace(" ", "", $email);
        $this->db->select("COUNT(*) AS count");
        $this->db->from("member");
        $this->db->where("member_email",$email);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row()->count;
        return $result;
    }

    function getDetailMember($id){
        $this->db->select("*");
        $this->db->from("member");
        $this->db->where("member_id",$id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getShortDetailMember($id){
        $this->db->select("member_id, member_name");
        $this->db->from("member");
        $this->db->where("member_id",$id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getMemberByEmail($email){
        $this->db->select("*");
        $this->db->from("member");
        $this->db->where("member_email",$email);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getListUpcomingEvent(){
        $this->db->select("*");
        $this->db->from("event");
        $this->db->where("is_deleted",0);
        $this->db->where("is_active",1);
        /*$this->db->where("event_start_date >=",date("Y-m-d H:i:s"));*/
        $this->db->order_by("event_start_date","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function send_email_fake($fromEmail, $fromName, $to, $cc, $subject, $template, $attach = array()){
        return true;
    }

    function send_email($fromEmail, $fromName, $to, $cc, $subject, $template, $attach = array()){
        $config = Array (
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'adit@juraganskripsi.com',
            'smtp_pass' => 'Adhita1988',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
        $this->load->library ( 'email', $config );
        $this->email->set_newline ( "\r\n" );
        /*$this->email->initialize ($config);*/
        $this->email->from ( $fromEmail, $fromName );
        $this->email->to ( $to );
        $this->email->cc ( $cc );
        $this->email->subject ( $subject );
        $this->email->message ( $template );
        $this->email->reply_to("transaction@kendiair.com");
        if (count ( $attach ) > 0) {
            for($i = 0; $i < count ( $attach ); $i ++) {
                $this->email->attach ( $attach [$i] );
            }
        }

        if ($this->email->send ()) {
            $this->email->clear ( TRUE );
            return true;
        } else {
            /*echo $this->email->print_debugger();die;
            return false;*/
            return true; 
        }
    }

    function getEventDetail($id){
        $this->db->select("*");
        $this->db->from("event");
        $this->db->where("event_id",$id);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getShortEventDetail($event_id){
        $this->db->select("event_id, event_name,is_recurring");
        $this->db->from("event");
        $this->db->where("event_id",$event_id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getAllScheduleByEvent($event){
        $this->db->select("*");
        $this->db->from("event_schedule");
        $this->db->where("event_id",$event);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("schedule_start_time","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getScheduleByPackage($package_id){
        $this->db->select("*");
        $this->db->from("event_schedule");
        $this->db->where("package_id like'".'%"'.$package_id.'"%'."'");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->where("is_show_front",1);
        $this->db->order_by("schedule_start_time","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getShortAllScheduleByPackage($package_id){
        $this->db->select("event_schedule_id, schedule_name");
        $this->db->from("event_schedule");
        $this->db->where("package_id like'".'%"'.$package_id.'"%'."'");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("schedule_start_time","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getScheduleIsShowFrontByEvent($event_id){
        $this->db->select("*");
        $this->db->from("event_schedule");
        $this->db->where("event_id",$event_id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->where("is_show_front",1);
        $this->db->order_by("schedule_start_time","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getAllScheduleByEvent_short($event){
        $this->db->select("event_schedule_id, schedule_name");
        $this->db->from("event_schedule");
        $this->db->where("event_id",$event);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("schedule_start_time","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getListRegion(){
        $this->db->select("*");
        $this->db->from("region");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("region_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getListDPW(){
        $this->db->select("*");
        $this->db->from("dpw");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("dpw_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getSatelitChurch(){
        $this->db->select("*");
        $this->db->from("satelit_church");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("satelit_church_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getChurchRole(){
        $this->db->select("*");
        $this->db->from("church_role");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("church_role_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getActiveEvent(){
        $this->db->select("*");
        $this->db->from("event");
        $this->db->where("DATE(event_start_date) <=", date("Y-m-d"));
        $this->db->where('DATE(event_end_date) >=',date("Y-m-d"));
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("event_start_date","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getActiveEventAndUpcomming(){
        $this->db->select("*");
        $this->db->from("event");
        $this->db->where("(DATE(event_start_date) <= '".date("Y-m-d")."' OR DATE(event_start_date) >= '".date("Y-m-d")."')");
        $this->db->where('DATE(event_end_date) >=',date("Y-m-d",strtotime(date("Y-m-d"). ' -1 day')));
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("event_start_date","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        //debugCode($this->db->last_query());
        return $result;
    }

    function getAllEventShort(){
        $this->db->select("event_name, event_id");
        $this->db->from("event");
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $this->db->order_by("event_id","DESC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getPackageDetail($id){
        $this->db->select("*");
        $this->db->from("event_package");
        $this->db->where("event_package_id",$id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getScheduleDetail($id){
        $this->db->select("*");
        $this->db->from("event_schedule");
        $this->db->where("event_schedule_id",$id);
        $this->db->where("is_active",1);
        $this->db->where("is_deleted",0);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_doku_config(){
        $this->db->select("*");
        $this->db->from("doku_config");
        $this->db->where("status",1);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_active_slider(){
        $this->db->select("*");
        $this->db->from("slider");
        $this->db->where("is_deleted",0);
        $this->db->order_by("seq","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getOrderIDbyAttendance($attendance_id){
        $this->db->select("order_id");
        $this->db->from("attendance");
        $this->db->where("attendance_id",$attendance_id);
        $query  = $this->db->get();
        $result = $query->row()->order_id;
        return $result;
    }

    function get_list_siswa($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'siswa_name',
            2 => 'siswa_nomor_induk',
            3 => 'siswa_nisn',
            4 => 1
        );

        $sql = "SELECT siswa_id, siswa_name, siswa_nomor_induk, siswa_nisn FROM siswa";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE status = 1 AND isDeleted = 0 AND is_graduate = 0";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (siswa_name like '%".$param['search']['value']."%' ";
            $where.= " or siswa_nomor_induk like '%".$param['search']['value']."%' ";
            $where.= " or siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY siswa_name ASC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }

    function getKelas(){
        $this->db->select("*");
        $this->db->from("kelas");
        $this->db->where("status",1);
        $this->db->where("isDeleted",0);
        $this->db->order_by("kelas_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getTahunPembelajaran(){
        $this->db->select("*");
        $this->db->from("tahun_pelajaran");
        $this->db->where("status",1);
        $this->db->where("isDeleted",0);
        $this->db->order_by("tahun_pelajaran_name","DESC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getCurrentMoney(){
        $this->db->select("*");
        $this->db->from("money");
        $this->db->where("id",1);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }
}