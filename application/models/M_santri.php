<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_santri extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getSantri(){
    	$this->db->select("*");
    	$this->db->from("siswa");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getSantriById($condition, $val){
        $this->db->select("st.*");
        $this->db->from("siswa st");
        $this->db->where("st.".$condition, $val);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    // ======================= Get siswa with kelas & rombel ==================================================//

    function getSantriDetail($condition, $val){
    	$this->db->select("st.*, kl.kelas_name, kr.kelas_rombel_name");
    	$this->db->from("siswa st");
        $this->db->join("siswa_kelas sk","st.siswa_id = sk.siswa_id");
        $this->db->join("kelas kl","sk.kelas_id = kl.kelas_id");
        $this->db->join("kelas_rombel kr","sk.kelas_rombel_id = kr.kelas_rombel_id");
    	$this->db->where("st.".$condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getSantriByID2($id){
        $this->db->select("*");
        $this->db->from("siswa");
        $this->db->where("siswa_id",$id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    /*===================================== FUNCTION FOR DATATABLE QUERY =====================================*/

    function get_list_santri($param = array(),$method="default",$addtional=""){
        $start  = $param['start'];
        $length = $param['length'];
        
        $columns    = array(
            1 => 'st.siswa_nomor_induk',
            2 => 'st.siswa_nisn',
            3 => 'st.siswa_name',
            4 => 'st.siswa_no_hp',
        );

        // $sql = "SELECT mb.*, ap.province_name FROM member AS mb
        //      LEFT JOIN app_province AS ap on mb.province_id = ap.province_id
        //      LEFT JOIN app_city AS ac on mb.city_id = ac.city_id";

        $sql = "SELECT * FROM siswa AS st";

        $where = "";
        $orderby = " ";
        
        $where.=" WHERE st.isDeleted <> '1' AND status = '1' AND is_graduate = 0";
        if(!empty($param['search']['value'])){ 
            if($where != ""){
                $where.= " AND ";
            }else{
                $where.= " WHERE ";
            }
            
            $where.= " (st.siswa_nisn like '%".$param['search']['value']."%' ";
            $where.= " or st.siswa_name like '%".$param['search']['value']."%' ";
            $where.= " ) ";
        }

        if(!empty($param['order'][0]['column'])){
            $orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
        }else{
            $orderby.=" ORDER BY st.created_date DESC";
        }

        if($addtional == ""){
            if($param['length'] == '-1'){
                $orderby.="";
            }else{
                $orderby.="  LIMIT ".$start." ,".$length." ";
            }
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
    }
}