<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getDetailUser($id){
    	$this->db->select("*");
    	$this->db->from("app_user");
        $this->db->join("app_access_group", 'app_access_group.access_group_id = app_user.access_group_id');
    	$this->db->where("user_id",$id);
    	$this->db->where("isDeleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function getAccessGroup(){
        $this->db->select('*');
        $this->db->from('app_access_group');
        $que = $this->db->get()->result_array();
        return $que;
    }

    function checkEmailExist($email){
    	$this->db->select("email");
    	$this->db->from("user");
    	$this->db->where("email",$email);
    	$this->db->where("isDeleted",0);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function checkEmailByUser($user_id, $email){
    	$this->db->select("email");
    	$this->db->from("user");
    	$this->db->where("email",$email);
    	$this->db->where("isDeleted",0);
    	$this->db->where("user_id <>",$user_id);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function get_list_user($param = array(),$method="default",$addtional=""){
		$start	= $param['start'];
		$length = $param['length'];
		
		$columns	= array(
			1 => 'user_full_name',
			2 => 'user_username',
            3 => "user_email",
            4 => "access_group_id",
            5 => "status",
            6 => "1"
		);
		
		$sql = "SELECT * FROM app_user INNER join app_access_group ON app_access_group.access_group_id = app_user.access_group_id ";

		$where = "";
		$orderby = " ";
		
		$where.=" WHERE isDeleted <> '1'";
		if(!empty($param['search']['value'])){ 
			if($where != ""){
				$where.= " AND ";
			}else{
				$where.= " WHERE ";
			}
			
			$where.= " (user_full_name like '%".$param['search']['value']."%' ";
			$where.= " or user_email like '%".$param['search']['value']."%' ";
			$where.= " ) ";
		}

		if(!empty($param['order'][0]['column'])){
			$orderby.=" ORDER BY ".$columns[$param['order'][0]['column']]." ".$param['order'][0]['dir']." ";        
		}else{
			$orderby.=" ORDER BY last_login_time DESC";
		}

        if($addtional == ""){
			if($param['length'] == '-1'){
				$orderby.="";
			}else{
				$orderby.="  LIMIT ".$start." ,".$length." ";
			}
        } 

        $sql.=$where.$orderby;  
        $query = $this->db->query($sql);
        return $query;
	}
}