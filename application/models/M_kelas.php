<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kelas extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getKelas(){
    	$this->db->select("*");
    	$this->db->from("kelas");
        $this->db->where("status", 1);
    	$this->db->where("isDeleted", 0);
    	$query	= $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    function getKelasDetail($condition, $val){
    	$this->db->select("*");
    	$this->db->from("kelas");
    	$this->db->where($condition, $val);
    	$query	= $this->db->get();
    	$result = $query->row();
    	return $result;
    }

    function rfGetKelas(){
        $this->db->select("kelas_id, kelas_name");
        $this->db->from("kelas");
        $this->db->where("status",1);
        $this->db->where("isDeleted",0);
        $this->db->order_by("kelas_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }


    // ==================================================== Rombel =======================================//
    function getRombel($condition, $val){
        $this->db->select("kr.*, kl.kelas_name");
        $this->db->from("kelas_rombel kr");
        $this->db->join("kelas kl","kr.kelas_id = kl.kelas_id");
        $this->db->where("kr.".$condition, $val);
        $this->db->where("kr.status", 1);
        $this->db->where("kr.isDeleted", 0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getRombelDetail($condition, $val){
        $this->db->select("kr.*");
        $this->db->from("kelas_rombel kr");
        $this->db->join("kelas kl","kr.kelas_id = kl.kelas_id");
        $this->db->where("kr.".$condition, $val);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function rfGetRombelByIdKelasId($kelas_id){
        $this->db->select("kelas_rombel_id,kelas_rombel_name");
        $this->db->from("kelas_rombel");
        $this->db->where("kelas_id",$kelas_id);
        $this->db->where("status",1);
        $this->db->where("isDeleted",0);
        $this->db->order_by("kelas_rombel_name","ASC");
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function rfGetSiswaByKelasRombel($kelas_id,$rombel_id){
        $this->db->select("sw.siswa_id,sw.siswa_name, sw.siswa_nomor_induk, sw.photo");
        $this->db->from("siswa_kelas AS sk");
        $this->db->join("siswa AS sw","sw.siswa_id = sk.siswa_id");
        $this->db->join("kelas AS kl","kl.kelas_id = sk.kelas_id");
        $this->db->join("kelas_rombel AS kr","kr.kelas_rombel_id = sk.kelas_rombel_id");
        $this->db->where("sk.kelas_id",$kelas_id);
        $this->db->where("sk.kelas_rombel_id",$rombel_id);
        $this->db->where("sw.status",1);
        $this->db->where('sw.isDeleted',0);
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }
}