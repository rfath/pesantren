<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myprofile extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->sessionMember = $this->session->sessionMember;
		$this->load->model("M_event");
		$this->load->model("M_order");
	}

	function user($id){
		$dataUser = $this->global->getDetailMember($this->sessionMember['member_id']);
		$province = $this->global->getProvince();
		// debugCode($provice);
		$data['province']     = $province;
		$data['dataUser']    = $dataUser;
		$data['front_slider'] = array();
		$data['image_post']   = array(); // Image post in layout.php
		$data['content']      = "front/front_page/myProfile";
		$this->load->view('front/layout',$data);
	}

	function editprofile($id){
		$dataUser = $this->global->getDetailMember($this->sessionMember['member_id']);
		$province = $this->global->getProvince();
		$city     = $this->global->getCityByProvince($dataUser->province_id);

		$data['city']     = $city;
		$data['province'] = $province;
		$data['dataUser'] = $dataUser;
		$data['province'] = $province;
		$this->load->view('front/front_page/editProfile',$data);
	}

	function updateprofile($id){
		$post     = $this->input->post();
		// $event_id = encrypt_decrypt("decrypt", $post['param']);

		if (!empty($post['member_password'])) {
			$updateArray = array(
				"member_name"      => $post['member_name'],
				"member_email"     => $post['member_email'],
				"member_phone"     => $post['member_phone'],
				"member_birthdate" => date('y-m-d', strtotime($post['member_birthdate'])),
				"member_address"   => $post['member_address'],
				"member_password"  => md5($post['member_password']),
				"province_id"      => $post['province'],
				"city_id"          => $post['city'],
				"updated_date"     => date('Y-m-d H:i:s'),
				"updated_by"       => $this->sessionData['user_id'],
				"gender"           => $post['gender']

			);
		}else{
			$updateArray = array(
				"member_name"      => $post['member_name'],
				"member_email"     => $post['member_email'],
				"member_phone"     => $post['member_phone'],
				"member_birthdate" => date('y-m-d', strtotime($post['member_birthdate'])),
				"member_address"   => $post['member_address'],
				"province_id"      => $post['province'],
				"city_id"          => $post['city'],
				"updated_date"     => date('Y-m-d H:i:s'),
				"updated_by"       => $this->sessionData['user_id'],
				"gender"           => $post['gender']
			);
		}

		$update = $this->db->update("member", $updateArray, array("member_id" => $id));

		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("myprofile/user/".$id);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("myprofile/editprofile/".$id);
		}
	}

	/*==================================================== Get City ====================================================*/
	function getCityByProvince($id){
		$city = $this->global->getCityByProvince($id);
		$html = '<option value="">Select City</option>';
		foreach ($city as $key => $value) {
			$html.='<option value="'.$value->city_id.'">'.$value->city_name.'</option>';
		}
		die($html);
	}
	/*==================================================== End Get City ====================================================*/
}