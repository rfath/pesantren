<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('M_signin');
		$this->sessionMember = $this->session->sessionMember;
	}

	function index(){
		if (!empty($this->sessionMember)) {
			redirect();
		}
		$this->load->view('front/front_page/signin');
	}

	function signup(){
		if (!empty($this->sessionMember)) {
			redirect();
		}
		$province = $this->global->getProvince();
		$data['province']	= $province;
		$this->load->view('front/front_page/signup',$data);
	}

	function getCityByProvince($id){
		$city = $this->global->getCityByProvince($id);
		$html = '<option value="">Select City</option>';
		foreach ($city as $key => $value) {
			$html.='<option value="'.$value->city_id.'">'.$value->city_name.'</option>';
		}
		die($html);
	}

	function doSignup(){
		$post = $this->input->post();
		$insertArray = array(
			'member_name'      => $post['signup_name'],
			'member_email'     => $post['email'],
			'member_phone'     => $post['signup_phone'],
			'member_birthdate' => date("Y-m-d",strtotime($post['birthday'])),
			'member_address'   => $post['signup_address'],
			'member_password'  => md5($post['password']),
			'province_id'      => $post['province'],
			'city_id'          => $post['city'],
			'created_date'     => date('Y-m-d H:i:s'),
			"gender"           => $post['gender']
		);
		$checkEmail = $this->global->checkEmailMember($post['email']);
		if ($checkEmail == 0) {
			$insert = $this->db->insert("member",$insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("signin/signup");
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("signin/signup");
			}
		}else{
			$this->session->set_flashdata('is_success', 'Email Already Registered');
			redirect("signin/signup");
		}
	}

	function doLogin(){
		$post = $this->input->post();
		$checkemail = $this->M_signin->checkLoginMember($post['email'],$post['password']);
		if ($checkemail == 1) {
			redirect();
		}else{
			redirect("signin");
		}
	}

	function signout(){
		session_destroy();
		redirect();
	}

	function resetpassword(){
		$this->load->view('front/front_page/reset_password');
	}

	function doReset(){
		$post  = $this->input->post();
		$check = $this->global->getMemberByEmail($post['email']);
		
		if (!empty($check)) {
			$dateExpired = date("Y-m-d H:i:s",strtotime("+1 Hour"));
			$parameter   = encrypt_decrypt('encrypt',$check->member_id."||".$dateExpired);
			$newpassword = rand(100000,999999);
			$insertResetArray = array(
				"member_id"    => $check->member_id,
				"token"        => $parameter,
				"expired_date" => $dateExpired,
				"status"       => 0,
				"newpassword"  => $newpassword
			);
			$insert = $this->db->insert("reset_password_request", $insertResetArray);
			if ($insert) {
				$template    = '
				<div style="width:100%; padding:20px; background-color:#ecf0f1;">
					<div style="width: 50%;background-color: #fff;padding: 10px; margin-left:24%; margin-right:24%;">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<span style="float:left;font-size:43px;color:#2d3436;font-family:arial;font-weight:bold">Password Reset</span>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td>
									<span style="font-size: 20px;color: #2d3436;font-family: arial;">We have received a request to reset your password. Please <a href="'.base_url('signin/confirmlost/'.$parameter).'" style="color:#3498db;">confirm</a> to reset your password. Otherwise you can ignore this email.<br> Your Password will be : <b>'.$newpassword.'<b>.</span>
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td>
									<span style="font-size: 20px;color: #2d3436;font-family: arial;">Thank You</span>
								</td>
							</tr>
						</table>
					<div>
				</div>';
				$generatelink = "";
				$attachments = array();
				$subject     = "RESET EVENT IFGF PASSOWRD";
				$this->global->send_email ( defaultFormEmail(), defaultFormName(), $post['email'], "", $subject, $template,$attachments );
				custom_notif("success","success","Request has been sent to your email. Please check your email.");
				redirect('signin/resetpassword');
			}else{
				custom_notif("failed","failed","Failed to insert request");
				redirect('signin/resetpassword');
			}
		}else{
			custom_notif("failed","failed","There is no member with email ".$post['email']);
			redirect('signin/resetpassword');
		}
		debugCode($check);
	}

	function confirmlost($param){
		$decode        = encrypt_decrypt("decrypt",$param);
		$explode       = explode("||", $decode);
		$checkPassword = $this->M_signin->checklostpassword($explode[0],$explode[1]);
		if (!empty($checkPassword)) {
			$updatePassword = $this->db->update("member",array("member_password" => md5($checkPassword->newpassword)), array("member_id" => $explode[0]));
			$updateRequest = $this->db->update("reset_password_request",array("status" => 1), array("status" => 0, "member_id" => $explode[0]));
			custom_notif("success","success","Reset Password success, please login with your new password : <b>".$checkPassword->newpassword."</b>");
			redirect('signin/resetpassword');
		}else{
			custom_notif("failed","failed","Token Failed".$post['email']);
			redirect('signin/resetpassword');
		}
	}
}