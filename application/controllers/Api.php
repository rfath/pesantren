<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->sessionMember = $this->session->sessionMember;
		$this->load->model("M_apiorder");
	}

	function verify(){
		$_SERVER['REMOTE_ADDR'];
		$post = $_POST;
		//Test Data Below
		//$post = json_decode('{"STOREID":"10276524","AMOUNT":"500000.00","TRANSIDMERCHANT":"49","WORDS":"0324a23fe02a25d872de36094d0d68be23cfbf4d"}',true);	

		$transidmerchant = $post["TRANSIDMERCHANT"];
		$totalamount     = $post["AMOUNT"];
		$storeid         = $post["STOREID"];

		/*====== START ARRAY LOG ======*/
		$logArray = array(
			"payment_id" => $transidmerchant,
			"datetime"   => date("Y-m-d H:i:s"),
			"amount"     => $totalamount,
			"log"        => json_encode($post),
			"type"       => "Verify",
			"server"     => $_SERVER['REMOTE_ADDR']
		);
		/*====== END ARRAY LOG ======*/

		if ($_SERVER['REMOTE_ADDR'] == '103.10.128.14') { 
			// Check If Transaction Valid
			$checkTransaction = $this->M_apiorder->checkTransaction($transidmerchant);
			if (!empty($checkTransaction)) {
				$logArray['reason'] = "CONTINUE";
				echo "CONTINUE";
			}else{
				$logArray['reason'] = "Data not found in database";
				echo "STOP";
			}
		}else{
			$logArray['reason'] = "STOP REMOTE_ADDR INVALID WITH:".$_SERVER['REMOTE_ADDR'];
			echo "STOP";
		}
		$this->db->insert("doku_log",$logArray);
	}

	function notify(){
		$_SERVER['REMOTE_ADDR'];
		$post = $_POST;

		$transidmerchant = $post["TRANSIDMERCHANT"];
		$totalamount     = $post["AMOUNT"];
		$result          = strtoupper($post["RESULT"]); // Result can be (Success or Fail)

		/*====== START ARRAY LOG ======*/
		$logArray = array(
			"payment_id" => $transidmerchant,
			"datetime"   => date("Y-m-d H:i:s"),
			"amount"     => $totalamount,
			"log"        => json_encode($post),
			"type"       => "Notify",
			"server"     => $_SERVER['REMOTE_ADDR']
		);
		/*====== END ARRAY LOG ======*/

		if ($_SERVER['REMOTE_ADDR'] == '103.10.128.14') {
			// Check If Transaction Valid
			$checkTransaction = $this->M_apiorder->checkTransaction($transidmerchant);
			if (!empty($checkTransaction)) {
				//if result success from doku
				if ($result == 'SUCCESS') {
					$updateArray = array(
						"payment_status" => 1,
						"payment_time"   => date("Y-m-d H:i:s")
					);
					$update = $this->db->update("order",$updateArray, array("order_id" => $transidmerchant));
					if ($update) {
						echo "CONTINUE";
					}else{
						$logArray['reason'] = "Update Transaction Failed, Transaction Success";
						echo "STOP";
					}
				}else{
					$logArray['reason'] = "Transaction Failed";
					echo "STOP";
				}
			}else{
				$logArray['reason'] = "Data not found in database";
				echo "STOP";
			}
		}else{
			$logArray['reason'] = "STOP REMOTE_ADDR INVALID WITH:".$_SERVER['REMOTE_ADDR'];
			echo "STOP";
		}
		$this->db->insert("doku_log",$logArray);
	}

	function done_transaction(){
		$_SERVER['REMOTE_ADDR'];
		$post = $_POST;
		$transidmerchant = $post["TRANSIDMERCHANT"];
		/*====== START ARRAY LOG ======*/
		$logArray = array(
			"payment_id" => $transidmerchant,
			"datetime"   => date("Y-m-d H:i:s"),
			"amount"     => "",
			"log"        => json_encode($post),
			"type"       => "DONE",
			"server"     => $_SERVER['REMOTE_ADDR']
		);
		/*====== END ARRAY LOG ======*/
		$this->db->insert("doku_log",$logArray);
		$insert_id = $this->db->insert_id();
		redirect("order/onlineresult/".$insert_id);
	}
}