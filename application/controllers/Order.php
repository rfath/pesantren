<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->sessionMember = $this->session->sessionMember;
		$this->load->model("M_event");
		$this->load->model("M_order");
		$this->load->model("M_apiorder");
	}


	function historyorder(){
		//$listEvent = $this->global->getListUpcomingEvent();
		$member_id = $this->sessionMember['member_id'];
		$listEvent = $this->frontpage->getEventByMember($member_id);

		$data['listEvent'] = $listEvent;
		$data['content']   = "front/front_page/historyOrder";
		$this->load->view('front/layout',$data);
	}

	function onlineresult($id){
		$check = $this->M_apiorder->getDoneLog($id);
		if (!empty($check)) {
			$resultLog = json_decode($check->log,true);
			
			$result = strtoupper($resultLog['RESULT']);
			if ($resultLog['RESULT'] == "SUCCESS") {
				$data['content']   = "front/front_page/orderSuccess";
			}else{
				$data['content']   = "front/front_page/orderFailed";
			}
			
			$this->load->view('front/layout',$data);
		}else{
			redirect();
		}
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_order_by_member(){
		$requestParam 			= $_REQUEST;
		// debugCode($requestParam);
		$getData 				= $this->M_order->get_list_order_by_member ( $requestParam, 'nofilter', 1 );
		$totalAllData 			= $this->M_order->get_list_order_by_member ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_order->get_list_order_by_member ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_order->get_list_order_by_member ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			// $encrypt_id = encrypt_decrypt('encrypt',$value->order_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = '
			<a href="'.base_url("event/detailOrder/".$value->order_id).'" class="btn btn-info btn-sm ed" title="Detail"><i class="fa fa-info-circle"></i></a>
			';
			$button_proccess= '
				<button class="btn btn-success btn-sm ed" title="Approve"><i class="fa fa-check"></i></button>
				<button class="btn btn-danger btn-sm ed" onClick="#" title="Reject"><i class="fa fa-times"></i></button>
				';
			/*========================================= END BUTTON STUFF =========================================*/
			/*========================================= START STATUS PAYMENT =========================================*/
			//0: Waiting Payment, 1: Waiting Admin Approve , 2: Accept, 3: Reject
			$status_payment = "";
			if ($value->payment_status == 0) {
				$status_payment = '<span class="badge badge-bdr badge-warning">Waiting for Payment</span>';
			}elseif($value->payment_status == 1){
				$status_payment = '<span class="badge badge-bdr badge-info">Waiting Admin to Approve</span>';
			}elseif($value->payment_status == 2){
				$status_payment = '<span class="badge badge-bdr badge-success">Accepted</span>';
			}else{
				$status_payment = '<span class="badge badge-bdr badge-danger">Rejected</span>';
			}
			/*========================================= END STATUS PAYMENT =========================================*/

			/*========================================= START STATUS ORDER =========================================*/
			// 0: Order Place , 1: On Progress, 2: Valid, 3: Invalid

			/*========================================= END STATUS ORDER =========================================*/


			$rowData[] = $no++;
			$rowData[] = "OD-".date("Ymd",strtotime($value->order_time))."-".$value->order_id;
			$rowData[] = $value->member_name;
			$rowData[] = $value->event_name;
			$rowData[] = $value->package_name;
			$rowData[] = $status_payment;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}