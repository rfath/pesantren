<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_alumni");
		$this->load->model("M_tahunpelajaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataTP = $this->M_tahunpelajaran->getTahunPelajaran();
		
		$data['tp']			= $dataTP;
		$data['web_title'] 	= "List Alumni";
		$data['content']   	= "admin/alumni/list_alumni";
		$this->load->view('admin/layout',$data);
	}

	public function laporanalumni($tp=""){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);

		if ($tp == "") {
			$field_condition 	=  "tahun_pelajaran_id != ";
			$dataTP				=  "ALL";
		}else{
			$field_condition 	=  "tahun_pelajaran_id";
			$dataTP 			= $this->M_tahunpelajaran->getTahunPelajaraneDetail("tahun_pelajaran_id", $tp)->tahun_pelajaran_name;
		}

		$dataAlumni 		= $this->M_alumni->listAlumni($field_condition, $tp);
		$data['tp']			= $dataTP;
		$data['listData']	= $dataAlumni;

		$filename = date("ymdHis")."_laporan_alumni.pdf";

		$view = $this->load->view('admin/alumni/laporan_alumni', $data, true);
		$mpdf->WriteHTML($view);
		// $mpdf->Output($filename, "D");
		$mpdf->Output(FCPATH.'/upload/laporan/alumni/'.$filename, "F");
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	function get_list_alumni(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_alumni->get_list_alumni ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_alumni->get_list_alumni ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_alumni->get_list_alumni ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_alumni->get_list_alumni ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData    = array();			

			$rowData[]  = $no++;
			$rowData[]  = $value->siswa_nisn;
			$rowData[]  = $value->siswa_nomor_induk;
			$rowData[]  = $value->siswa_name;
			$rowData[]  = $value->siswa_no_hp;
			$rowData[]  = $value->siswa_alamat;
			$rowData[]	= $value->tahun_pelajaran_name;
			// $rowData[]  = $status;
			// $rowData[]  = $button;
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}
}