<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_pembayaran");
		$this->load->model("M_settpembayaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataPembayaran 	= $this->M_pembayaran->getPembayaran();

		$data['listData'] 	= $dataPembayaran;
		$data['web_title'] 	= "Pembayaran";
		$data['content']   	= "admin/pembayaran/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$dataPembayaranItem = $this->M_settpembayaran->getPembayaranItem();

		$data['listData'] 	= $dataPembayaranItem;
		$data['back_link'] = base_url('cms/pembayaran');
		$data['web_title'] = "Add Pembayaran";
		$data['content']   = "admin/pembayaran/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		// Insert to table pembayaran
		$pembayaranArray = array(
			"pembayaran_title"  => $post['title'],
			"status" 			=> "1",
			"isDeleted" 		=> "0",
			"created_date"  	=> date('Y-m-d H:i:s'),
			"created_by"    	=> $this->sessionData['user_id']
		);

		$insertPembayaran	= $this->db->insert("pembayaran", $pembayaranArray);
		$pembayaran_id 		= $this->db->insert_id();

		// Insert to table pembayaran_install_item
		for ($i=0; $i < count($post['pembayaran_item']) ; $i++) { 
			$pembayaranItemArray = array(
				"pembayaran_id"  		=> $pembayaran_id,
				"pembayaran_item_id" 	=> $post['pembayaran_item'][$i],
				"status" 				=> "1",
				"isDeleted" 			=> "0",
				"created_date"  		=> date('Y-m-d H:i:s'),
				"created_by"    		=> $this->sessionData['user_id']
			);

			$insert	= $this->db->insert("pembayaran_install_item", $pembayaranItemArray);
		}

		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaran");
		}
	}

	function edit($id){
		$dataPembayaranItem 		= $this->M_settpembayaran->getPembayaranItem();
		$detailPembayaran 			= $this->M_pembayaran->getPembayaranDetail("pembayaran_id", $id);
		// debugCode($detailPembayaran);

		$data['detailPembayaran'] = $detailPembayaran;
		$data['listData']         = $dataPembayaranItem;
		$data['back_link']        = base_url('cms/pembayaran');
		$data['web_title']        = "Edit Bentuk Pembayaran";
		$data['content']          = "admin/pembayaran/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		// debugCode($post);
		$arrayPembayaran = array(
			"pembayaran_title"  => $post['title'],
			"updated_date"  	=> date('Y-m-d H:i:s'),
			"updated_by"    	=> $this->sessionData['user_id']
		);
		$updatePembayaran = $this->db->update("pembayaran", $arrayPembayaran, array("pembayaran_id" => $id));


		$this->db->update("pembayaran_install_item", ["status" => "0", "isDeleted" => "1"], array("pembayaran_id" => $id));
		for ($i=0; $i < count($post['pembayaran_item']); $i++) { 

			$detailInstallItem = $this->M_pembayaran->getPembayaranInstallItemDetail("pembayaran_item_id", $post['pembayaran_item'][$i], $id);

			if (!empty($detailInstallItem)) {
				// Jika ID sudah terdaftar
				$arrayInstallPembayaran = array(
					"pembayaran_item_id"  	=> $post['pembayaran_item'][$i],
					"status"  				=> "1",
					"isDeleted"    			=> "0"
				);	

				$proses = $this->db->update("pembayaran_install_item", $arrayInstallPembayaran, array("pembayaran_id" => $id, "pembayaran_item_id" => $post['pembayaran_item'][$i]));
			}else{
				// jika ID belum terdaftar
				$arrayInstallPembayaran = array(
					"pembayaran_id"  		=> $id,
					"pembayaran_item_id"  	=> $post['pembayaran_item'][$i],
					"status"				=> "1",
					"isDeleted"				=> "0",
					"created_date"  		=> date('Y-m-d H:i:s'),
					"created_by"    		=> $this->sessionData['user_id']
				);	

				$proses	= $this->db->insert("pembayaran_install_item", $arrayInstallPembayaran);
			}
		}


		if ($proses) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaran/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("pembayaran", $updateArray, array("pembayaran_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaran");
		}
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_siswa_pembayaran_item(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_settpembayaran->get_list_pembayaran_item ( $requestParam, 'nofilter');
		$totalAllData 			= $this->M_settpembayaran->get_list_pembayaran_item ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_settpembayaran->get_list_pembayaran_item ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_settpembayaran->get_list_pembayaran_item ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			// debugCode($value);
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$check = "";
			$check .='
				<div class="form-check form-check-inline">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" name="pembayaran_item[]" id="pembayaran_item" value="'.$value->pembayaran_item_id.'">
					</label>
				</div>
			';
			/*========================================= END BUTTON STUFF =========================================*/
			
			$rowData[] = $no++;
			$rowData[] = $value->title;
			$rowData[] = number_format($value->price);
			$rowData[] = $check;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/
}