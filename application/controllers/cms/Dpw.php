<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dpw extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_master");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$getDPW  = $this->M_master->getDPW();
		$data['dpwData']   = $getDPW;
		$data['web_title'] = "DPW";
		$data['content']   = "admin/dpw/listdpw";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add DPW";
		$data['back_link'] = base_url('cms/dpw');
		$data['content']   = "admin/dpw/adddpw";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"dpw_name"     => $post['DPW_name'],
			"created_date" => date("Y-m-d H:i:s"),
			"created_by"   => $this->sessionData['user_id']
		);
		$insert = $this->db->insert("dpw",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/dpw");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/dpw/add");
		}
	}

	function edit($param){
		$dpw_id    = encrypt_decrypt("decrypt",$param);
		$dpwDetail = $this->M_master->getDetailDPW($dpw_id);
		$data['id']	= $param;
		$data['detailDpw'] = $dpwDetail;
		$data['web_title'] = "Edit DPW";
		$data['back_link'] = base_url('cms/dpw');
		$data['content']   = "admin/dpw/editdpw";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post   = $this->input->post();
		$dpw_id = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"dpw_name"     => $post['DPW_name'],
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("dpw",$updateArray,array("dpw_id" => $dpw_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/dpw");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/dpw/edit/".$post['param']);
		}
	}

	function doDelete($param){
		$post   = $this->input->post();
		$dpw_id = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("dpw",$updateArray,array("dpw_id" => $dpw_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/dpw/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/dpw/");
		}
	}
}