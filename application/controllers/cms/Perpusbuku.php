<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perpusbuku extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_perizinan");
		$this->sessionData = $this->session->sessionData;
		$sessionData       = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "List Buku";
		$data['content']   = "admin/perpustakaan/listbuku";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Tambah Buku";
		$data['back_link'] = base_url('cms/perpusbuku');
		$data['content']   = "admin/perpustakaan/addbuku";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		debugCode($post);
	}
}