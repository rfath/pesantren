<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_member");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Member";
		$data['content']   = "admin/member/listmember";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		// $provice = $this->global->getProvince();

		$data['provice']     = $provice;
		$data['back_link'] = base_url('cms/member');
		$data['web_title'] = "Add Member";
		$data['content']   = "admin/member/addmember";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		// debugCode($post['member_email']);
		$detailMember = $this->M_member->getMemberDetail("member_email", $post['member_email']);
		$insertArray = array(
			"member_name"      => $post['member_name'],
			"member_email"     => $post['member_email'],
			"member_phone"     => $post['member_phone'],
			"member_birthdate" => date('y-m-d', strtotime($post['member_birthdate'])),
			"member_address"   => $post['member_address'],
			"member_password"  => md5($post['member_password']),
			"province_id"      => $post['province'],
			"city_id"          => $post['city'],
			"created_date"     => date('Y-m-d H:i:s'),
			"created_by"       => $this->sessionData['user_id'],
			"is_active"        => "1",
			"is_deleted"       => "0",
			"gender"           => $post['gender']

		);
		// debugCode($detailMember);

		if (!empty($detailMember)) {
			$this->session->set_flashdata('is_success', 'Email already exist');
			redirect("cms/member/add/");
		}else{
			$insert = $this->db->insert("member", $insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/member/");
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/member/add/");
			}
		}
	}

	function edit($param){
		$member_id    = encrypt_decrypt("decrypt", $param);
		$detailMember = $this->M_member->getMemberDetail("member_id", $member_id);
		$provice      = $this->global->getProvince();
		$province_id  = $this->global->getProvinceById($detailMember->province_id);
		$city_id      = $this->global->getCityById($detailMember->city_id);

		$data['id']           = $param;
		$data['provice']      = $provice;
		$data['province_id']  = $province_id;
		$data['city_id']      = $city_id;
		$data['detailMember'] = $detailMember;
		$data['back_link']    = base_url('cms/member');
		$data['web_title']    = "Edit Member";
		$data['content']      = "admin/member/editmember";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post     = $this->input->post();
		$event_id = encrypt_decrypt("decrypt", $post['param']);

		$updateArray = array(
			"member_name"      => $post['member_name'],
			"member_email"     => $post['member_email'],
			"member_phone"     => $post['member_phone'],
			"member_birthdate" => date('y-m-d', strtotime($post['member_birthdate'])),
			"member_address"   => $post['member_address'],
			"member_password"  => md5($post['member_password']),
			"province_id"      => $post['province'],
			"city_id"          => $post['city'],
			"updated_date"     => date('Y-m-d H:i:s'),
			"updated_by"       => $this->sessionData['user_id'],
			"gender"           => $post['gender']

		);

		$update = $this->db->update("member", $updateArray, array("member_id" => $event_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/member");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/member/edit/".$post['param']);
		}
	}

	function doDelete($param){
		$member_id    = encrypt_decrypt("decrypt", $param);

		$updateArray = array(
			"is_deleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("member", $updateArray, array("member_id" => $member_id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/member");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/member");
		}
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_member(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_member->get_list_member ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_member->get_list_member ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_member->get_list_member ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_member->get_list_member ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->member_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/member/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				<a href="'.base_url('cms/member/edit/'.$encrypt_id).'" class="btn btn-primary btn-sm" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				';
			/*========================================= END BUTTON STUFF =========================================*/

			// /*========================================= BEGIN DATE STUFF =========================================*/
			// $date_start = date("m/d/Y h:i A", strtotime($value->event_start_date));
			// $date_end   = date("m/d/Y h:i A", strtotime($value->event_end_date));

			// $open_date_start = date("m/d/Y h:i A", strtotime($value->event_open_registration_date));
			// $close_date_end  = date("m/d/Y h:i A", strtotime($value->event_close_registration_date));
			/*========================================= END DATE STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->member_name;
			$rowData[] = $value->member_phone;
			$rowData[] = $value->member_email;
			$rowData[] = $value->member_address;
			// $rowData[] = '<span class="badge badge-bdr badge-success">'.$date_start.'</span><br><br>'.'<span class="badge badge-bdr badge-danger">'.$date_end.'</span>';
			// $rowData[] = '<span class="badge badge-bdr badge-success">'.$open_date_start.'</span><br><br>'.'<span class="badge badge-bdr badge-danger">'.$close_date_end.'</span>';
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/

	/*==================================================== Get City ====================================================*/
	function getCityByProvince($id){
		$city = $this->global->getCityByProvince($id);
		$html = '<option value="">Select City</option>';
		foreach ($city as $key => $value) {
			$html.='<option value="'.$value->city_id.'">'.$value->city_name.'</option>';
		}
		die($html);
	}
	/*==================================================== End Get City ====================================================*/
}