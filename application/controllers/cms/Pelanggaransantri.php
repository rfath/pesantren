<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggaransantri extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_pelanggaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Pelanggaran santri";
		$data['content']   = "admin/pelanggaran/listpelanggaransantri";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$get = $this->input->get();
		$user_id = encrypt_decrypt("decrypt",$get['siswa']);

		$data['back_link']      = base_url('cms/pelanggaransantri/pelanggaransantri?siswa='.$get['siswa']);
		$data['web_title'] = "Add Pelanggaran santri";
		$data['pelanggaran'] = $this->M_pelanggaran->getPelanggaran();
		$data['siswa'] = $user_id;
		$data['pelanggaran_siswa'] = $this->M_pelanggaran->getSiswa();
		$data['content']   = "admin/pelanggaran/addpelanggaransiswa";
		$this->load->view('admin/layout',$data);
		//debugcode($data['siswa']);
	}

	function doAdd(){
		$get 	 = $this->input->get();
		//$user_id = encrypt_decrypt("decrypt",$id);
		$post = $this->input->post();
		
		$hukuman = json_encode($post['additional']);
		$insertArray = array(
			"pelanggaran_id"    	=> $post['pelanggaran'],
			"siswaId"    	=> $post['siswa'],
			"hukuman"    	=> $hukuman,
			"created_date"     	=> date('Y-m-d H:i:s'),
			"created_by"       	=> $this->sessionData['user_id'],
		);
			
		$insert = $this->db->insert("pelanggaran_siswa",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/Pelanggaransantri/pelanggaransantri/?siswa=".$get['siswa']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/Pelanggaransantri/add");
		}
		
		
	}

	function edit($param){
		$user_id = encrypt_decrypt("decrypt",$param);
		$get = $this->input->get();
		$detailUser = $this->M_pelanggaran->getDetailPelanggaranSiswa($user_id);
		
		$data['id']	= "";
		if (!empty($detailUser)) {
			$data['id']	= $param;
		}

		$hukumansantri = json_decode($detailUser->hukuman);
		
		$data['back_link']      = base_url('cms/pelanggaransantri/pelanggaransantri?siswa='.$get['siswa']);
		$data['hukumansantri']	= $hukumansantri;
		$data['pelanggaran'] 	= $this->M_pelanggaran->getPelanggaran();
		$data['siswa'] 			= $this->M_pelanggaran->getSiswa($user_id);
		$data["idnya"]			= $param;
		$data['detailUser'] 	= $detailUser;
		$data['web_title']  	= "Edit Pelanggaran santri";
		$data['content']    	= "admin/pelanggaran/editpelanggaransiswa";
		$this->load->view('admin/layout',$data);
		//debugcode($data['detailUser']);
	}

	function doUpdate($id){
		$get 	 = $this->input->get();
		$post    = $this->input->post();
		$user_id = encrypt_decrypt("decrypt", $id);

		$hukuman = json_encode($post['additional']);
		$updateArray = array(
			"pelanggaran_id"    => $post['pelanggaran'],
			"siswaId"    		=> $post['siswa'],
			"hukuman"    		=> $hukuman,
			"updated_date"     	=> date('Y-m-d H:i:s'),
			"updated_by"       	=> $this->sessionData['user_id'],
		);

		$update = $this->db->update("pelanggaran_siswa", $updateArray, array("pelanggaran_siswa_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pelanggaransantri/pelanggaransantri/?siswa=".$get['siswa']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pelanggaransantri/edit/".$id, 'refresh');
		}
	}

	function doDelete($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"isDeleted" => 1
		);
		$update = $this->db->update("pelanggaran",$updateArray,array("pelanggaran_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pelanggaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pelanggaran");
		}
	}

	public function get_list_pelanggaran($id){
		$requestParam 			= $_REQUEST;
		$user_id = encrypt_decrypt("decrypt", $id);

		$getData 				= $this->M_pelanggaran->get_list_pelanggaran_siswa ( $requestParam, $user_id, 'nofilter' );
		$totalAllData 			= $this->M_pelanggaran->get_list_pelanggaran_siswa ( $requestParam, $user_id, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_pelanggaran->get_list_pelanggaran_siswa ( $requestParam, $user_id, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_pelanggaran->get_list_pelanggaran_siswa ( $requestParam, $user_id);
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->pelanggaran_siswa_id);
			$siswa_encrypt = encrypt_decrypt('encrypt', $value->siswa_id);
			$button = "";
			// $button .= '
			// <button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pelanggaransantri/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/pelanggaransantri/edit/'.$encrypt_id."?siswa=".$siswa_encrypt).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}

			$hukuman = array();
			$hukuman = json_decode($value->hukuman,true);

			$detailhukuman = "";

			foreach ($hukuman['hukuman'] as $skey => $svalue) {
				$detailhukuman.="<b>Hukuman</b> : ".$svalue." (".$hukuman['status'][$skey].")<br/>";
			}

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->pelanggaran_name;
			$rowData[] = $detailhukuman;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_pelanggaran->get_list_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_pelanggaran->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_pelanggaran->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_pelanggaran->get_list_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->siswa_id);

			$button = "";
			// $button .= '
			// <button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pelanggaransantri/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/pelanggaransantri/pelanggaransantri'.'?siswa='.$encrypt_id).'" class="btn btn-primary btn-sm" title="pilihan"><i class="fa fa-edit"></i> Lihat Pelanggaran</a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	function pelanggaransantri(){
		$data['back_link'] 	= base_url('cms/pelanggaransantri');
		$data['web_title'] 	= "Pelanggaran santri";
		$data['content']  	= "admin/pelanggaran/listpelanggaransiswa";
		$this->load->view('admin/layout',$data);
	}

}