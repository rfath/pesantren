<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_kelas");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataKelas = $this->M_kelas->getKelas();

		$data['listData'] 	= $dataKelas;
		$data['web_title'] 	= "Kelas";
		$data['content']   	= "admin/kelas/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['back_link'] = base_url('cms/kelas');
		$data['web_title'] = "Add Kelas";
		$data['content']   = "admin/kelas/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"kelas_name"  	=> $post['kelas_name'],
			"status" 		=> "1",
			"isDeleted" 	=> "0",
			"created_date"  => date('Y-m-d H:i:s'),
			"created_by"    => $this->sessionData['user_id']

		);

		$insert	= $this->db->insert("kelas", $insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/kelas/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/kelas/add/");
		}
	}

	function edit($id){
		$detailKelas 			= $this->M_kelas->getKelasDetail("kelas_id", $id);
		// debugCode($detailKelas);
		$data['detailKelas'] 	= $detailKelas;
		$data['back_link']   	= base_url('cms/kelas');
		$data['web_title']   	= "Edit Kelas";
		$data['content']     	= "admin/kelas/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		// debugCode($post);
		$updateArray = array(
			"kelas_name"  	=> $post['kelas_name'],
			"updated_date"  => date('Y-m-d H:i:s'),
			"updated_by"    => $this->sessionData['user_id']
		);

		$update = $this->db->update("kelas", $updateArray, array("kelas_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/kelas");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/kelas/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("kelas", $updateArray, array("kelas_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/kelas");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/kelas");
		}
	}


	function listrombel($id){
		$dataRombel = $this->M_kelas->getRombel("kelas_id", $id);
		// debugCode($dataRombel);
		$data['listData'] 	= $dataRombel;
		$data['web_title'] 	= "List Rombel";
		$data['content']   	= "admin/kelas/kelas_rombel/index";
		$this->load->view('admin/layout',$data);
	}


	function addrombel($id){
		$data['back_link'] = base_url('cms/kelas/listrombel/'.$id);
		$data['web_title'] = "Add Rombel";
		$data['content']   = "admin/kelas/kelas_rombel/rombel";
		$this->load->view('admin/layout',$data);
	}

	function doAddRombel(){
		$post = $this->input->post();
		
		$insertArray = array(
			"kelas_id"  		=> $post['kelas_id'],
			"kelas_rombel_name" => $post['rombel'],
			"status" 		=> "1",
			"isDeleted" 	=> "0",
			"created_date"  => date('Y-m-d H:i:s'),
			"created_by"    => $this->sessionData['user_id']

		);

		$insert	= $this->db->insert("kelas_rombel", $insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/kelas/listrombel/".$post['kelas_id']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/kelas/listrombel/add/".$post['kelas_id']);
		}
	}

	function editrombel($id){
		$detailRombel 			= $this->M_kelas->getRombelDetail("kelas_rombel_id", $id);
		// debugCode($detailRombel);
		$data['detailRombel'] 	= $detailRombel;
		$data['back_link']   	= base_url('cms/kelas');
		$data['web_title']   	= "Edit Kelas";
		$data['content']     	= "admin/kelas/kelas_rombel/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdateRombel($id){
		$post     = $this->input->post();
		// debugCode($post);
		$updateArray = array(
			"kelas_rombel_name" => $post['rombel'],
			"updated_date"  	=> date('Y-m-d H:i:s'),
			"updated_by"    	=> $this->sessionData['user_id']
		);

		$update = $this->db->update("kelas_rombel", $updateArray, array("kelas_rombel_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/kelas/listrombel/".$post['kelas_id']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/kelas/listrombel/add/".$post['kelas_id']);
		}
	}

	function doDeleteRombel($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("kelas_rombel", $updateArray, array("kelas_rombel_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}


}