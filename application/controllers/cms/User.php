<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_user");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "User";
		$data['content']   = "admin/user/listuser";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['access_group'] = $this->M_user->getAccessGroup();
		$data['web_title'] = "Add User";
		$data['content']   = "admin/user/adduser";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		$cekemail = $this->db->query("SELECT * FROM app_user WHERE user_email = '$post[user_email]'")->num_rows();
		if ($cekemail <=1){
			$cekusername = $this->db->query("SELECT * FROM app_user WHERE user_username = '$post[user_name]'")->num_rows();
			if($cekusername <= 1) {
				$insertArray = array(
					"user_full_name"           => $post['user_full_name'],
					"user_username"           => $post['user_name'],
					"user_email"          => $post['user_email'],
					"user_password"       => md5($post['user_password']),
					"access_group_id" => $post['access_group'],
					"status"   => 1,
					"isDeleted"   => 0,
					"last_login_time"   => date('Y-m-d H:i:s'),
				);
				$insert = $this->db->insert("app_user",$insertArray);
				if ($insert) {
					$this->session->set_flashdata('is_success', 'Yes');
					redirect("cms/user");
				}else{
					$this->session->set_flashdata('is_success', 'No');
					redirect("cms/user/add");
				}
			} else {
				$this->session->set_flashdata('is_success', 'Mohon Maaf Username sudah Terdaftar...!!!');
				redirect('cms/user/add','refresh');
			}
		} else {
			$this->session->set_flashdata('is_success', 'Mohon Maaf Email sudah Terdaftar...!!!');
			redirect('cms/user/add','refresh');
		}
		
		
	}

	function edit($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$detailUser = $this->M_user->getDetailUser($user_id);
		$data['access_group'] = $this->M_user->getAccessGroup();
		$data['id']	= "";
		if (!empty($detailUser)) {
			$data['id']	= $param;
		}

		$data["idnya"]	= $param;
		$data['detailUser'] = $detailUser;
		$data['web_title']  = "Edit User";
		$data['content']    = "admin/user/edituser";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post    = $this->input->post();
		$user_id = encrypt_decrypt("decrypt", $id);

		$cekemail = $this->db->query("SELECT * FROM app_user WHERE user_email = '$post[user_email]'")->num_rows();
		if ($cekemail <=1){
			$cekusername = $this->db->query("SELECT * FROM app_user WHERE user_username = '$post[user_username]'")->num_rows();
			if($cekusername <= 1) {
				$updateArray = array(
				"user_full_name"  => $post['user_full_name'],
				"user_username"   => $post['user_username'],
				"user_email"      => $post['user_email'],
				"access_group_id" => $post['access_group'],
				);

				if ($post['user_password'] <> "") {
					$updateArray['user_password'] = md5($post['user_password']);
				}

				$update = $this->db->update("app_user", $updateArray, array("user_id" => $user_id));
				if ($update) {
					$this->session->set_flashdata('is_success', 'Yes');
					redirect("cms/user");
				}else{
					$this->session->set_flashdata('is_success', 'No');
					redirect("cms/user/edit/".$id, 'refresh');
				}
			} else {
				$this->session->set_flashdata('is_success', 'Mohon Maaf Username sudah Terdaftar...!!!');
				redirect("cms/user/edit/".$id,'refresh');
			}
		} else {
			$this->session->set_flashdata('is_success', 'Mohon Maaf Email sudah Terdaftar...!!!');
			redirect("cms/user/edit/".$id,'refresh');
		}
	}

	function doDelete($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"isDeleted" => 1
		);
		$update = $this->db->update("app_user",$updateArray,array("user_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/user");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/user");
		}
	}

	public function get_list_user(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_user->get_list_user ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_user->get_list_user ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_user->get_list_user ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_user->get_list_user ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->user_id);

			$button = "";
			$button .= '
			<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/user/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/user/edit/'.$encrypt_id).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}
			$rowData[] = $no++;
			$rowData[] = $value->user_full_name;
			$rowData[] = $value->user_username;
			$rowData[] = $value->user_email;
			$rowData[] = $value->access_group_name;
			$rowData[] = $status;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}