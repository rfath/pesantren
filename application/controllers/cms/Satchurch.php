<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satchurch extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_master");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$listData  = $this->M_master->getSatelitChurch();
		$data['listData']  = $listData;
		$data['web_title'] = "Satelit Church";
		$data['content']   = "admin/satelit_church/listchurch";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add Satelit Church";
		$data['back_link'] = base_url('cms/satchurch');
		$data['content']   = "admin/satelit_church/addchurch";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"satelit_church_name"     => $post['sat_church_name'],
			"created_date" => date("Y-m-d H:i:s"),
			"created_by"   => $this->sessionData['user_id']
		);
		$insert = $this->db->insert("satelit_church",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/satchurch");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/satchurch/add");
		}
	}

	function edit($param){
		$id    = encrypt_decrypt("decrypt",$param);
		$detailData = $this->M_master->getDetailListChurch($id);
		$data['id']	= $param;
		$data['detailData'] = $detailData;
		$data['web_title'] = "Edit Satelit Church";
		$data['back_link'] = base_url('cms/satchurch');
		$data['content']   = "admin/satelit_church/editchurch";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post = $this->input->post();
		$id   = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"satelit_church_name" => $post['sat_church_name'],
			"updated_date"        => date("Y-m-d H:i:s"),
			"updated_by"          => $this->sessionData['user_id']
		);
		$update = $this->db->update("satelit_church",$updateArray,array("satelit_church_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/satchurch");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/satchurch/edit/".$post['param']);
		}
	}

	function doDelete($param){
		$post = $this->input->post();
		$id   = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("satelit_church",$updateArray,array("satelit_church_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/satchurch/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/satchurch/");
		}
	}
}