<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_event");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Event";
		$data['content']   = "admin/event/listevent";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$place = $this->global->getListPlace();

		$data['place']     = $place;
		$data['back_link'] = base_url('cms/event');
		$data['web_title'] = "Add Event";
		$data['content']   = "admin/event/addevent";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		if (!empty($post['offbank'])) {
			$offbank = json_encode(array_values($post['offbank']));
		}
		
		$upload_path = './assets_ticketing/event_image/';
		/*====================================== BEGIN UPLOADING FEATEURE IMAGE ======================================*/
		$feature_image = "";
		if ($_FILES['feature_image']['name'] <> "") {
			$ext           = pathinfo($_FILES['feature_image']['name'], PATHINFO_EXTENSION);
			$feature_image = "FI".date("dmYHis").rand(100,999).".".$ext;

			$config['upload_path']   = $upload_path;
			$config['allowed_types'] = 'PNG|png|JPG|jpg|JPEG|jpeg';
			$config['file_name']     = $feature_image;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('feature_image')){
				$error = 'error: '. $this->upload->display_errors();
				echo $error;
				die();
			}else{
				$feature_image = "/assets_ticketing/event_image/".$feature_image;
			}
		}
		/*====================================== END UPLOADING FEATEURE IMAGE ======================================*/

		/*====================================== BEGIN UPLOADING LOGO IMAGE ======================================*/
		$logo_image = "";
		if ($_FILES['logo_image']['name'] <> "") {
			$ext        = pathinfo($_FILES['logo_image']['name'], PATHINFO_EXTENSION);
			$logo_image = "LI".date("dmYHis").rand(100,999).".".$ext;

			$config['upload_path']   = $upload_path;
			$config['allowed_types'] = 'PNG|png|JPG|jpg|JPEG|jpeg';
			$config['file_name']     = $logo_image;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('logo_image')){
				$error = 'error: '. $this->upload->display_errors();
				echo $error;
				die();
			}else{
				$logo_image = "/assets_ticketing/event_image/".$logo_image;
			}
		}
		/*====================================== END UPLOADING LOGO IMAGE ======================================*/

		$start_date         = date("Y-m-d H:i:s",strtotime($post['date_start']));
		$end_date           = date("Y-m-d H:i:s",strtotime($post['date_end']));
		$open_registration  = date("Y-m-d H:i:s",strtotime($post['open_reg_date']));
		$close_registration = date("Y-m-d H:i:s",strtotime($post['close_reg_date']));

		if (isset($post['recurring'])) {
			$is_recurring           = 1;
			$recurring_repeat       = $post['repeat'];
			$recurring_repeat_every = $post['repeat_every'];
		}else{
			$is_recurring           = 0;
			$recurring_repeat       = "";
			$recurring_repeat_every = "";
		}

		$insertArray = array(
			"event_name"                        => $post['event_name'],
			"event_description"                 => $post['event_description'],
			"event_highlight_description"       => $post['event_h_description'],
			"event_featured_image"              => $feature_image,
			"event_logo"                        => $logo_image,
			"event_start_date"                  => $start_date,
			"event_end_date"                    => $end_date,
			"event_open_registration_date"      => $open_registration,
			"event_close_registration_date"     => $close_registration,
			"event_total_seat"                  => $post['total_seat'],
			"event_available_seat"              => $post['available_seat'],
			"event_place_id"                    => $post['event_place'],
			"event_contact_email"               => $post['contact_email'],
			"event_contact_telephone"           => $post['contact_phone'],
			"event_contact_whatsapp"            => $post['contact_whatsapp'],
			"event_social_ig"                   => $post['instagram'],
			"event_social_twitter"              => $post['twitter'],
			"event_social_fb"                   => $post['facebook'],
			"is_active"                         => 1,
			"is_deleted"                        => 0,
			"created_date"                      => date("Y-m-d H:i:s"),
			"created_by"                        => $this->sessionData['user_id'],
			"organizer_name"                    => $post['event_organizer'],
			"tac"                               => $post['tac'],
			"offline_bank_transfer_destination" => $offbank,
			"is_recurring"                      => $is_recurring,
			"recurring_repeat"                  => $recurring_repeat,
			"recurring_repeat_every"            => $recurring_repeat_every
		);

		$insert = $this->db->insert("event",$insertArray);
		$insert_id = $this->db->insert_id();
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/addPackageEvent/".encrypt_decrypt("encrypt",$insert_id));
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/add");
		}
	}

	function edit($param){
		$event_id    = encrypt_decrypt("decrypt",$param);
		$place       = $this->global->getListPlace();
		$detailEvent = $this->M_event->getEventDetail($event_id);
		
		$data['id']	= "";
		$offBank = array();
		if (!empty($detailEvent)) {
			$data['id']	= $param;

			$date_start = date("m/d/Y", strtotime($detailEvent->event_start_date));
			$date_end   = date("m/d/Y", strtotime($detailEvent->event_end_date));

			$open_date_start = date("m/d/Y", strtotime($detailEvent->event_open_registration_date));
			$close_date_end  = date("m/d/Y", strtotime($detailEvent->event_close_registration_date));

			if ($detailEvent->offline_bank_transfer_destination <> "") {
				$offBank = json_decode($detailEvent->offline_bank_transfer_destination);
			}
		}
		//debugCode($detailEvent);
		$data['offBank']                 = $offBank;
		$data['date_start']              = $date_start;
		$data['date_end']                = $date_end;
		$data['open_registration_date']  = $open_date_start;
		$data['close_registration_date'] = $close_date_end;

		$data['detailEvent'] = $detailEvent;
		$data['place']       = $place;
		$data['back_link']   = base_url('cms/event');
		$data['web_title']   = "Edit Event";
		$data['content']     = "admin/event/editevent";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post     = $this->input->post();
		$event_id = encrypt_decrypt("decrypt",$post['param']);

		/*======================== BEGIN IF THERE'S OFFLINE BANK IN POST =====================*/
		if (!empty($post['offbank'])) {
			$offbank = json_encode(array_values($post['offbank']));
		}
		/*======================== END IF THERE'S OFFLINE BANK IN POST =====================*/

		$start_date         = date("Y-m-d H:i:s",strtotime($post['date_start']));
		$end_date           = date("Y-m-d H:i:s",strtotime($post['date_end']));
		$open_registration  = date("Y-m-d H:i:s",strtotime($post['open_reg_date']));
		$close_registration = date("Y-m-d H:i:s",strtotime($post['close_reg_date']));

		/*======================== BEGIN IF IF RECURRING =====================*/
		if (isset($post['recurring'])) {
			$is_recurring           = 1;
			$recurring_repeat       = $post['repeat'];
			$recurring_repeat_every = $post['repeat_every'];
		}else{
			$is_recurring           = 0;
			$recurring_repeat       = "";
			$recurring_repeat_every = "";
		}
		/*======================== END IF IF RECURRING =====================*/
		
		$updateArray = array(
			"event_name"                        => $post['event_name'],
			"event_description"                 => $post['event_description'],
			"event_highlight_description"       => $post['event_h_description'],
			"event_start_date"                  => $start_date,
			"event_end_date"                    => $end_date,
			"event_open_registration_date"      => $open_registration,
			"event_close_registration_date"     => $close_registration,
			"event_total_seat"                  => $post['total_seat'],
			"event_available_seat"              => $post['available_seat'],
			"event_place_id"                    => $post['event_place'],
			"event_contact_email"               => $post['contact_email'],
			"event_contact_telephone"           => $post['contact_phone'],
			"event_contact_whatsapp"            => $post['contact_whatsapp'],
			"event_social_ig"                   => $post['instagram'],
			"event_social_twitter"              => $post['twitter'],
			"event_social_fb"                   => $post['facebook'],
			"is_active"                         => 1,
			"is_deleted"                        => 0,
			"updated_date"                      => date("Y-m-d H:i:s"),
			"updated_by"                        => $this->sessionData['user_id'],
			"organizer_name"                    => $post['event_organizer'],
			"tac"                               => $post['tac'],
			"offline_bank_transfer_destination" => $offbank
		);

		$upload_path = './assets_ticketing/event_image/';
		/*====================================== BEGIN UPLOADING FEATEURE IMAGE ======================================*/
		$feature_image = "";
		if ($_FILES['feature_image']['name'] <> "") {
			$ext           = pathinfo($_FILES['feature_image']['name'], PATHINFO_EXTENSION);
			$feature_image = "FI".date("dmYHis").rand(100,999).".".$ext;

			$config['upload_path']   = $upload_path;
			$config['allowed_types'] = 'PNG|png|JPG|jpg|JPEG|jpeg';
			$config['file_name']     = $feature_image;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('feature_image')){
				$error = 'error: '. $this->upload->display_errors();
				echo $error;
				die();
			}else{
				$feature_image = "/assets_ticketing/event_image/".$feature_image;
				$updateArray['event_featured_image']	= $feature_image; // Create variable for insert new name feature image
			}
		}
		/*====================================== END UPLOADING FEATEURE IMAGE ======================================*/

		/*====================================== BEGIN UPLOADING LOGO IMAGE ======================================*/
		$logo_image = "";
		if ($_FILES['logo_image']['name'] <> "") {
			$ext        = pathinfo($_FILES['logo_image']['name'], PATHINFO_EXTENSION);
			$logo_image = "LI".date("dmYHis").rand(100,999).".".$ext;

			$config['upload_path']   = $upload_path;
			$config['allowed_types'] = 'PNG|png|JPG|jpg|JPEG|jpeg';
			$config['file_name']     = $logo_image;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('logo_image')){
				$error = 'error: '. $this->upload->display_errors();
				echo $error;
				die();
			}else{
				$logo_image = "/assets_ticketing/event_image/".$logo_image;
				$updateArray['event_logo'] = $logo_image;
			}
		}
		/*====================================== END UPLOADING LOGO IMAGE ======================================*/

		$update = $this->db->update("event",$updateArray, array("event_id" => $event_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/edit/".$post['param']);
		}
	}

	function doDelete($param){
		$event_id    = encrypt_decrypt("decrypt",$param);
		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);
		$update = $this->db->update("event",$updateArray,array("event_id" => $event_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event");
		}
	}

	function addPackageEvent($param){
		$event_id    = encrypt_decrypt("decrypt",$param);
		$dataEvent   = $this->M_event->getShortEventDetail($event_id);
		$listPackage = $this->M_event->listPackageByEvent($event_id);

		$data['id']          = $param;
		$data['listPackage'] = $listPackage;
		$data['dataEvent']   = $dataEvent;
		$data['back_link']   = base_url('cms/event');
		$data['web_title']   = "Add Event Package";
		$data['content']     = "admin/event/eventTabPackage";
		$this->load->view('admin/layout',$data);
	}

	function doAddEventPackage(){
		$post     = $this->input->post();
		$event_id = $post['param'];
		$encrypt = encrypt_decrypt("encrypt",$event_id);
		$insertArray = array(
			"event_id"               => $event_id,
			"package_name"           => $post['package_name'],
			"package_price"          => $post['package_price'],
			"package_description"    => $post['package_description'],
			"package_seat"           => $post['package_seat'],
			"package_remaining_seat" => $post['package_r_seat'],
			"created_date"           => date("Y-m-d H:is:s"),
			"created_by"             => $this->sessionData['user_id']
		);

		$insert = $this->db->insert("event_package",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/addPackageEvent/".$encrypt);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addPackageEvent/".$encrypt);
		}
	}

	function deleteEventPackage($param1, $param2){
		$event_id   = encrypt_decrypt("decrypt",$param1);
		$package_id = encrypt_decrypt("decrypt",$param2);

		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:is:s"),
			"updated_by"   =>  $this->sessionData['user_id']
		);
		$update = $this->db->update("event_package",$updateArray,array("event_package_id" => $package_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/addPackageEvent/".$param1);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addPackageEvent/".$param1);
		}
	}

	function addScheduleEvent($param){
		$event_id    = encrypt_decrypt("decrypt",$param);
		$dataEvent   = $this->M_event->getShortEventDetail($event_id);
		$listPackage = $this->M_event->listPackageByEvent($event_id);
		$listRoom     = $this->global->getListRoom();


		$listSchedule = $this->M_event->listScheduleByEvent($event_id);
		

		$scheduleData = array();
		foreach ($listSchedule as $key => $value) {
			$value = (array)$value;

			$packageID = json_decode($value['package_id'], true);
			
			foreach ($packageID as $zkey => $zvalue) {
				$getDataPackage = $this->M_event->getShortPackageByID($zvalue);
				$value['package_info'][] = '<span class="badge badge-bdr badge-primary">'.$getDataPackage->package_name.'</span>';
			}
			$scheduleData[] = $value;	
		}
		$scheduleData = (object) $scheduleData;

		$data['room']      = $listRoom;
		$data['id']        = $param;
		$data['package']   = $listPackage;
		$data['dataEvent'] = $dataEvent;
		$data['schedule']  = $scheduleData;

		$data['back_link']   = base_url('cms/event');
		$data['web_title']   = "Add Event Schedule";
		$data['content']     = "admin/event/eventTabSchedule";
		$this->load->view('admin/layout',$data);
	}

	function doAddEventSchedule(){
		$post  = $this->input->post();
		$param = encrypt_decrypt('encrypt',$post['param']);

		$package = json_encode($post['package']);

		$timeStart = date("Y-m-d H:i:s",strtotime($post['time_start']));
		$timeEnd   = date("Y-m-d H:i:s",strtotime($post['time_end']));

		$insertArray = array(
			"event_id"             => $post['param'],
			"schedule_name"        => $post['schedule_name'],
			"schedule_alias"       => $post['alias_name'],
			"schedule_description" => $post['description'],
			"schedule_start_time"  => $timeStart,
			"schedule_end_time"    => $timeEnd,
			"room_id"              => $post['room'],
			"package_id"           => $package,
			"created_date"         => date("Y-m-d H:i:s"),
			"created_by"           => $this->sessionData['user_id'],
			"is_show_front"        => $post['show_in_front']
		);
		$insert = $this->db->insert("event_schedule",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/addScheduleEvent/".$param);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addScheduleEvent/".$param);
		}
	}

	function deleteEventschedule($param1, $param2){
		$event_id   = encrypt_decrypt("decrypt",$param1);
		$schedule_id = encrypt_decrypt("decrypt",$param2);
		
		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("event_schedule",$updateArray,array("event_schedule_id" => $schedule_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/addScheduleEvent/".$param1);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addScheduleEvent/".$param1);
		}
	}

	/*==================================================== START SCHEDULE DATA ====================================================*/
	function schedule($param){
		$event_id     = encrypt_decrypt("decrypt",$param);
		$event_detail = $this->M_event->getEventDetail($event_id);
		$listPackage  = $this->global->getPackageByEvent($event_id);

		$data['event']     = $event_detail;
		$data['package']   = $listPackage;
		$data['id']        = $param;
		$data['back_link'] = base_url('cms/event');
		$data['web_title'] = "Schedule";
		$data['content']   = "admin/event/schedule";
		$this->load->view('admin/layout',$data);
	}

	function addSchedule($param){
		$event_id     = encrypt_decrypt("decrypt",$param);
		$event_detail = $this->M_event->getEventDetail($event_id);
		$listRoom     = $this->global->getListRoom();
		$listPackage  = $this->global->getPackageByEvent($event_id);

		$data['package']   = $listPackage;
		$data['room']      = $listRoom;
		$data['event']     = $event_detail;
		$data['id']        = $param;
		$data['back_link'] = base_url('cms/event/schedule/'.$param);
		$data['web_title'] = "Add Schedule";
		$data['content']   = "admin/event/scheduleAdd";
		$this->load->view('admin/layout',$data);
	}

	function doAddSchedule($param){
		$post     = $this->input->post();
		$event_id = encrypt_decrypt("decrypt",$param);
		
		$package = json_encode($post['package']);

		$timeStart = date("Y-m-d H:i:s",strtotime($post['time_start']));
		$timeEnd   = date("Y-m-d H:i:s",strtotime($post['time_end']));
		
		if ($event_id <> "") {
			$insertArray = array(
				"event_id"             => $event_id,
				"schedule_name"        => $post['schedule_name'],
				"schedule_alias"       => $post['alias_name'],
				"schedule_description" => $post['description'],
				"schedule_start_time"  => $timeStart,
				"schedule_end_time"    => $timeEnd,
				"room_id"              => $post['room'],
				"package_id"           => $package,
				"created_date"         => date("Y-m-d H:i:s"),
				"created_by"           => $this->sessionData['user_id'],
				"is_show_front"        => $post['show_in_front']
			);
			$insert = $this->db->insert("event_schedule",$insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/event/schedule/".$param);
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/event/addSchedule/".$param);
			}
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addSchedule/".$param);
		}
	}

	function editSchedule($param1,$param2){
		$event_id    = encrypt_decrypt("decrypt",$param1);
		$schedule_id = encrypt_decrypt("decrypt",$param2);

		$event_detail   = $this->M_event->getEventDetail($event_id);
		$listRoom       = $this->global->getListRoom();
		$listPackage    = $this->global->getPackageByEvent($event_id);
		
		$detailSchedule = $this->M_event->getScheduleDetail($schedule_id);
		
		$time_start = date("m/d/Y H:i A",strtotime($detailSchedule->schedule_start_time));
		$time_end = date("m/d/Y H:i A",strtotime($detailSchedule->schedule_end_time));

		/*==== CREATE PACKAGE ARRAY FOR CHECK ====*/
		$packageDecode = json_decode($detailSchedule->package_id);
		foreach ($packageDecode as $pdkey => $pdvalue) {
			$jsonPackage[$pdvalue] = $pdvalue;
		}

		$data['packageDecode']	= $jsonPackage;
		$data['detailSchedule'] = $detailSchedule;
		$data['time_start']     = $time_start;
		$data['time_end']       = $time_end;
		$data['package']        = $listPackage;
		$data['room']           = $listRoom;
		$data['event']          = $event_detail;
		$data['id']             = $param2;
		$data['id_ev']          = $param1;
		$data['back_link']      = base_url('cms/event/schedule/'.$param1);
		$data['web_title']      = "Edit Schedule";
		$data['content']        = "admin/event/scheduleEdit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdateSchedule($param){
		$post = $this->input->post();

		$event_id    = encrypt_decrypt("decrypt",$param);
		$schedule_id = encrypt_decrypt("decrypt",$post['param']);

		$package = json_encode($post['package']);

		$timeStart = date("Y-m-d H:i:s",strtotime($post['time_start']));
		$timeEnd   = date("Y-m-d H:i:s",strtotime($post['time_end']));

		$updateArray = array(
			"schedule_name"        => $post['schedule_name'],
			"schedule_alias"       => $post['alias_name'],
			"schedule_description" => $post['description'],
			"schedule_start_time"  => $timeStart,
			"schedule_end_time"    => $timeEnd,
			"room_id"              => $post['room'],
			"package_id"           => $package,
			"updated_date"         => date("Y-m-d H:i:s"),
			"updated_by"           => $this->sessionData['user_id'],
			"is_show_front"        => $post['show_in_front']
		);
		$update = $this->db->update("event_schedule",$updateArray,array("event_schedule_id" => $schedule_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/schedule/".$param);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addSchedule/".$param."/".$post['param']);
		}
	}

	function doDeleteSchedule($param1, $param2){
		$event_id    = encrypt_decrypt("decrypt",$param1);
		$schedule_id = encrypt_decrypt("decrypt",$param2);

		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("event_schedule",$updateArray,array("event_schedule_id" => $schedule_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/schedule/".$param1);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/schedule/".$param1);
		}
	}
	/*==================================================== END SCHEDULE DATA ====================================================*/

	/*==================================================== START PACKAGE DATA ====================================================*/
	function package($param){
		$event_id = encrypt_decrypt("decrypt",$param);
		$event_detail = $this->M_event->getEventDetail($event_id);
		$place        = $this->global->getListPlace();

		$data['event']     = $event_detail;
		$data['place']     = $place;
		$data['id']        = $param;
		$data['back_link'] = base_url('cms/event');
		$data['web_title'] = "Package";
		$data['content']   = "admin/event/package";
		$this->load->view('admin/layout',$data);
	}

	function addPackage($param){
		$event_id     = encrypt_decrypt("decrypt",$param);
		$event_detail = $this->M_event->getEventDetail($event_id);
		$place        = $this->global->getListPlace();		

		$data['event']     = $event_detail;
		$data['place']     = $place;
		$data['id']        = $param;
		$data['back_link'] = base_url('cms/event/package/'.$param);
		$data['web_title'] = "Add Package";
		$data['content']   = "admin/event/packageAdd";
		$this->load->view('admin/layout',$data);
	}

	function doAddPackage($param){
		$post     = $this->input->post();
		$event_id = encrypt_decrypt("decrypt",$param);
		if ($event_id <> "") {
			$insertArray = array(
				"event_id"               => $event_id,
				"package_name"           => $post['package_name'],
				"package_price"          => $post['package_price'],
				"package_description"    => $post['package_description'],
				"package_seat"           => $post['package_seat'],
				"package_remaining_seat" => $post['package_r_seat'],
				"created_date"           => date("Y-m-d H:is:s"),
				"created_by"             =>  $this->sessionData['user_id']
			);

			$insert = $this->db->insert("event_package",$insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/event/package/".$param);
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/event/addPackage/".$param);
			}
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/addPackage/".$param);
		}
	}

	function editPackage($param1,$param2){
		$event_id   = encrypt_decrypt("decrypt",$param1);
		$package_id = encrypt_decrypt("decrypt",$param2);

		$event_detail  = $this->M_event->getEventDetail($event_id);
		$place         = $this->global->getListPlace();
		$packageDetail = $this->M_event->getPackageDetail($package_id);


		$data['detailPackage'] = $packageDetail;
		$data['event']         = $event_detail;
		$data['place']         = $place;
		$data['id']            = $param2;
		$data['id_ev']         = $param1;
		$data['back_link']     = base_url('cms/event/package/'.$param1);
		$data['web_title']     = "Edit Package";
		$data['content']       = "admin/event/packageEdit";
		$this->load->view('admin/layout',$data);	
	}

	function doUpdatePackage($param){
		$post       = $this->input->post();
		$event_id   = encrypt_decrypt("decrypt",$param);
		$package_id = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"event_id"               => $event_id,
			"package_name"           => $post['package_name'],
			"package_price"          => $post['package_price'],
			"package_description"    => $post['package_description'],
			"package_seat"           => $post['package_seat'],
			"package_remaining_seat" => $post['package_r_seat'],
			"updated_date"           => date("Y-m-d H:is:s"),
			"updated_by"             =>  $this->sessionData['user_id']
		);

		$update = $this->db->update("event_package",$updateArray,array("event_package_id" => $package_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/package/".$param);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/editPackage/".$param."/".$post['param']);
		}
	}

	function doDeletePackage($param1, $param2){
		$event_id   = encrypt_decrypt("decrypt",$param1);
		$package_id = encrypt_decrypt("decrypt",$param2);

		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:is:s"),
			"updated_by"   =>  $this->sessionData['user_id']
		);
		$update = $this->db->update("event_package",$updateArray,array("event_package_id" => $package_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/event/package/".$param1);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/event/package/".$param1);
		}
	}
	/*==================================================== END PACKAGE DATA ====================================================*/

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_event(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_event->get_list_event ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_event->get_list_event ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_event->get_list_event ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_event->get_list_event ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->event_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<a href="'.base_url('cms/event/package/'.$encrypt_id).'" class="btn btn-dark btn-sm tbl-btn" title="Package"><i class="fa fa-box"></i></a>
				<a href="'.base_url('cms/event/schedule/'.$encrypt_id).'" class="btn btn-success btn-sm tbl-btn" title="Schedule"><i class="fa fa-clipboard-list"></i></a>
				<button class="btn btn-danger btn-sm tbl-btn" onClick="is_delete(\''.base_url('cms/event/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				<a href="'.base_url('cms/event/edit/'.$encrypt_id).'" class="btn btn-primary btn-sm tbl-btn" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				<a href="'.base_url('cms/report/event_report_detail/'.$encrypt_id).'" target="_blank" class="btn btn-warning btn-sm tbl-btn" title="Report"><i class="fa fa-chart-line"></i></a>
				';
			/*========================================= END BUTTON STUFF =========================================*/

			/*========================================= BEGIN DATE STUFF =========================================*/
			$date_start = date("m/d/Y h:i A", strtotime($value->event_start_date));
			$date_end   = date("m/d/Y h:i A", strtotime($value->event_end_date));

			$open_date_start = date("m/d/Y h:i A", strtotime($value->event_open_registration_date));
			$close_date_end  = date("m/d/Y h:i A", strtotime($value->event_close_registration_date));
			/*========================================= END DATE STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->event_name;
			$rowData[] = $value->place_name;
			$rowData[] = '<span class="badge badge-bdr badge-success">'.$date_start.'</span><br><br>'.'<span class="badge badge-bdr badge-danger">'.$date_end.'</span>';
			$rowData[] = '<span class="badge badge-bdr badge-success">'.$open_date_start.'</span><br><br>'.'<span class="badge badge-bdr badge-danger">'.$close_date_end.'</span>';
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_package(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_event->get_list_package ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_event->get_list_package ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_event->get_list_package ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_event->get_list_package ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->event_package_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/event/doDeletePackage/'.$requestParam['event']."/".$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				<a href="'.base_url('cms/event/editPackage/'.$requestParam['event']."/".$encrypt_id).'" class="btn btn-primary btn-sm" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				';
			/*========================================= END BUTTON STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->package_name;
			$rowData[] = $value->package_price;
			$rowData[] = $value->package_seat;
			$rowData[] = $value->package_remaining_seat;
			$rowData[] = $value->package_description;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_schedule(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_event->get_list_schedule ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_event->get_list_schedule ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_event->get_list_schedule ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_event->get_list_schedule ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->event_schedule_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<button type="button" class="btn btn-danger btn-sm tbl-btn" onClick="is_delete(\''.base_url('cms/event/doDeleteSchedule/'.$requestParam['event']."/".$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				<a href="'.base_url('cms/event/editSchedule/'.$requestParam['event']."/".$encrypt_id).'" class="btn btn-primary btn-sm tbl-btn" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				';
			/*========================================= END BUTTON STUFF =========================================*/

			/*========================================= BEGIN DATE STUFF =========================================*/
			$date_start = date("m/d/Y h:i A", strtotime($value->schedule_start_time));
			$date_end   = date("m/d/Y h:i A", strtotime($value->schedule_end_time));
			/*========================================= END DATE STUFF =========================================*/

			/*========================================= START GET DATA PACKAGE =========================================*/
			$decodePackage = json_decode($value->package_id);
			$packageList = array();
			foreach ($decodePackage as $dkey => $dvalue) {
				$dataPackage = $this->M_event->getShortPackageByID($dvalue);
				$packageList[] = '<span class="badge badge-bdr badge-primary">'.$dataPackage->package_name.'</span>';
			}
			/*========================================= END GET DATA PACKAGE =========================================*/

			$show = "";
			if ($value->is_show_front == 1) {
				$show = "Yes";
			}else{
				$show = "No";
			}

			$rowData[] = $no++;
			$rowData[] = $value->schedule_name;
			$rowData[] = implode(" ", $packageList);
			$rowData[] = $value->room_name;
			$rowData[] = '<span class="badge badge-bdr badge-success">'.$date_start.'</span>';
			$rowData[] = '<span class="badge badge-bdr badge-danger">'.$date_end.'</span>';
			$rowData[] = $value->schedule_description;
			$rowData[] = $show;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}