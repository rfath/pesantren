<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_dashboard");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		// $listEventActive = $this->global->getActiveEvent();

		// $data['last_order']      = $this->M_dashboard->get10LastOrder();
		// $data['last_register']   = $this->M_dashboard->get10LastRegister();
		// $data['last_attendance'] = $this->M_dashboard->get10LastAttendance();
		// $data['last_inout']      = $this->M_dashboard->get10CheckInOut();
		
		// $data['activeEvent']     = $listEventActive;

		$data['web_title']     = "Dashboard";
		$data['content']       = "admin/dashboard/dashboard";
		$this->load->view('admin/layout',$data);
	}

	function getDataTotal(){
		$get = $this->input->get();
		$explodeDate = explode(" - ", $get['daterange']);
		$dataDate['dateStart'] = date("Y-m-d",strtotime($explodeDate[0]));
		$dataDate['dateEnd']   = date("Y-m-d",strtotime($explodeDate[1]));

		$searchProcess = $this->M_dashboard->getTotalData($dataDate);
		echo json_encode($searchProcess);
	}

	function generate_image(){
		header('Content-type: image/jpeg');
      	// Create Image From Existing File
		//$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/bckg_dpn_.png');
		$number = "100"; //NiK or number card
		$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/nologo.png');
		$qr_raw    = 'http://chart.apis.google.com/chart?cht=qr&chs=300x300&chld=L|1&chl='.$number;
		$qr_image  = imagecreatefrompng($qr_raw);
		$logo      = imagecreatefrompng(FCPATH.'assets_ticketing/back_end/img/pesantren_logo_med.png');
		$logo_back = imagecreatefrompng(FCPATH.'images/card/image_ok/trans_bg.png');

      	// Allocate A Color For The Text
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$white = imagecolorallocate($jpg_image, 255, 255, 255);


      	// Set Path to Font File
		$font_path  = FCPATH.'images/card/OpenSans-Regular.ttf';
		$font_path2 = FCPATH.'images/card/VeraSerif-Bold.ttf';
		$font_path3 = FCPATH.'images/card/arbutusslab-regular.ttf';

      	// Set Text to Be Printed On Image
		$text       = "8872 2881 2019 2991";
		$text2      = "SUTARNO SUTARNO DAN SUTARNO";
		$text3 	    = "KARTU IDENTITAS SANTRI";
		$text4      = 'PONDOK MODERN "AL-ISLAM" NGANJUK';
		$text5      = 'Jl. Raya Sukomoro - Pace Km 1 Kel. Kapas Kec. Sukomoro Kab. Nganjuk';
		$text6      = 'YAYASAN "AL-ISLAM" NGANJUK';
		$text7      = 'Telp (0358) 325 096';
		$text8      = 'Berlaku selama menjadi santri Pondok Modern "AL-ISLAM" Nganjuk';

		$alamat     = wordwrap("Jl. Baru no 12 perumaha ABC raya kalimalang",40,"\n");
		
		/*==== START RESIZE PHOTO ====*/
		$photo         = imagecreatefromjpeg(FCPATH.'images/santri/SNT-18290804203875.jpg');
		$photo_none    = imagecreatefrompng(FCPATH.'images/card/image_ok/photobg.png');
		$photo_raw     = FCPATH.'images/santri/SNT-18290804203875.jpg';
		$photo_none_rw = FCPATH.'images/card/image_ok/photobg.png';
		$resiize       = resize_image($photo_none_rw,imagesx($photo),imagesy($photo)); //get resize_image from helper
		$photo_real    = imagecreatetruecolor($resiize[0], $resiize[1]);
		imagecopyresampled($photo_real, $photo, 0, 0, 0, 0, $resiize[0], $resiize[1], imagesx($photo), imagesy($photo));
		/*==== END RESIZE PHOTO ====*/
		
		/*==== START RESIZE BARCODE ====*/
		$white_bg  = imagecreatefrompng(FCPATH.'images/card/image_ok/whitbg.png');
		$qr_resize = resize_image($qr_raw,imagesx($white_bg),imagesy($white_bg)); //get resize_image from helper
		$qr_real   = imagecreatetruecolor($qr_resize[0], $qr_resize[1]);
		imagecopyresampled($qr_real, $qr_image, 0, 0, 0, 0, $qr_resize[0], $qr_resize[1], imagesx($qr_image), imagesy($qr_image));
		/*==== END RESIZE BARCODE ====*/
      	// Print Text On Image
      	imagettftext($jpg_image, 20, 0, 158, 51, $black, $font_path3, $text6);
		imagettftext($jpg_image, 22, 0, 158, 85, $black, $font_path2, $text4);
		imagettftext($jpg_image, 16, 0, 158, 116, $black, $font_path3, $text5);
		imagettftext($jpg_image, 16, 0, 158, 143, $black, $font_path3, $text7);

		imagettftext($jpg_image, 27, 0, 320, 232, $black, $font_path2, $text3);
		imagettftext($jpg_image, 23, 0, 320, 278, $black, $font_path, $text);
		imagettftext($jpg_image, 23, 0, 320, 330, $black, $font_path, $text2);

		imagettftext($jpg_image, 20, 0, 520, 430, $white, $font_path, $alamat);
		imagettftext($jpg_image, 15, 0, 75, 610, $white, $font_path3, $text8);

		
		imagecopy($jpg_image, $qr_real, 307, 382, 0, 0, imagesx($white_bg), imagesy($white_bg));
		imagecopy($jpg_image, $photo_real, 59, 239, 0, 0, imagesx($photo_none), imagesy($photo_none));
		imagecopyresampled($jpg_image, $logo, 15, 15, 0, 0, imagesx($logo_back),imagesx($logo_back),imagesx($logo),imagesy($logo));
      	// Send Image to Browser
      	imagealphablending($jpg_image, true); // setting alpha blending on
 		imagesavealpha($jpg_image, true); // save alphablending setting (important)
      	imagepng($jpg_image);
		$img_edit = imagepng($jpg_image);
		$merger   = imagecopy ( $jpg_image , $img_edit , (imagesx($jpg_image)/2)-(imagesx($img_edit)/2), (imagesy($jpg_image)/2)-(imagesy($img_edit)/2 ));
		imagepng(merger);
		// imagejpeg($new);
      	// Clear Memory
		imagedestroy($jpg_image);
		imagedestroy($photo);
		imagedestroy($photo_none);
		imagedestroy($qr_real);
		imagedestroy($white_bg);
		imagedestroy($qr_image);
		imagedestroy($logo_back);
		imagedestroy($merger);
	}

	function generate_image2(){
		header('Content-type: image/png');
		$jpg_image = imagecreatefrompng(FCPATH.'images/card/image_ok/bckg_blkg.png');
		//imagecopy($jpg_image,$jpg_image, imagesx($jpg_image),imagesy($jpg_image), imagesx($jpg_image),imagesy($jpg_image));
		//imagecreatetruecolor(100, 100);
		imagealphablending($jpg_image, true); // setting alpha blending on
 		imagesavealpha($jpg_image, true); // save alphablending setting (important)
		imagepng($jpg_image);
		imagedestroy($jpg_image);
	}

	function barcode($str_e){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://chart.apis.google.com/chart?cht=qr&chs=220x220&chld=L|1&chl=".$str_e); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // good edit, thanks!
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); // also, this seems wise considering output is image.
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
		/*$im2 = imagecreatefromstring($data);
		return $im2;*/
	}

	function testpdf(){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);
		$mpdf->WriteHTML('<h1>Hello world!</h1>');
		$mpdf->Output();
	}

	function execute_database(){
		$namesql  = date("Y_m_d_H_i_s");
		$database = $this->db->database;
		$user     = $this->db->username;
		$pass     = $this->db->password;
		$host     = $this->db->hostname;
		$dir      = FCPATH.'database/Backup/'.$namesql.'.sql';
		echo "<h3>Backing up database to `<code>{$dir}</code>`</h3>";
		exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$dir} 2>&1", $output);
		redirect('database/Backup/'.$namesql.".sql");
	}
}