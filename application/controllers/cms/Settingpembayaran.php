<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SettingPembayaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_settpembayaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataPembayaranItem = $this->M_settpembayaran->getPembayaranItem();

		$data['listData'] 	= $dataPembayaranItem;
		$data['web_title'] 	= "Setting Pembayaran";
		$data['content']   	= "admin/setting_pembayaran/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['back_link'] = base_url('cms/settingpembayaran');
		$data['web_title'] = "Add Bentuk Pembayaran";
		$data['content']   = "admin/setting_pembayaran/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"title"  		=> $post['title'],
			"price"  		=> $post['nominal'],
			"status" 		=> "1",
			"isDeleted" 	=> "0",
			"created_date"  => date('Y-m-d H:i:s'),
			"created_by"    => $this->sessionData['user_id']
		);

		$insert	= $this->db->insert("pembayaran_item", $insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/settingpembayaran/add");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/settingpembayaran/add/");
		}
	}

	function edit($id){
		$detailItem 			= $this->M_settpembayaran->getPembayaranItemDetail("pembayaran_item_id", $id);
		// debugCode($detailItem);
		$data['detailItem'] 	= $detailItem;
		$data['back_link']   	= base_url('cms/settingpembayaran');
		$data['web_title']   	= "Edit Bentuk Pembayaran";
		$data['content']     	= "admin/setting_pembayaran/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		
		$updateArray = array(
			"title"  		=> $post['title'],
			"price"  		=> $post['nominal'],
			"updated_date"  => date('Y-m-d H:i:s'),
			"updated_by"    => $this->sessionData['user_id']
		);

		$update = $this->db->update("pembayaran_item", $updateArray, array("pembayaran_item_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/settingpembayaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/settingpembayaran/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("pembayaran_item", $updateArray, array("pembayaran_item_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/settingpembayaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/settingpembayaran");
		}
	}


}