<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestasisiswa extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_prestasi");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Prestasi";
		$data['content']   = "admin/prestasi/listprestasisiswa";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		
		$get = $this->input->get();
		$user_id = encrypt_decrypt("decrypt",$get['siswa']);

		$data['back_link']      = base_url('cms/prestasisiswa/prestasisiswa?siswa='.$get['siswa']);
		$data['prestasi'] = $this->M_prestasi->getListPrestasi($user_id);
		$data['siswa'] = $user_id;
		$data['prestasi_siswa'] = $this->M_prestasi->getSiswa();
		$data['web_title'] = "Add Prestasi Siswa";
		$data['content']   = "admin/prestasi/addprestasisiswa";

		$this->load->view('admin/layout',$data);
		//debugcode($data['siswa']);
	}

	function doAdd(){
		$get 	 = $this->input->get();
		//$user_id = encrypt_decrypt("decrypt",$id);
		$post = $this->input->post();
		
		$insertArray = array(
			"siswaId"    				=> $post['siswa'],
			"prestasi_id"    			=> $post['prestasi'],
			"prestasi_date_achive"    	=> date('Y-m-d H:i:s'),
			"created_date"     			=> date('Y-m-d H:i:s'),
			"created_by"       			=> $this->sessionData['user_id'],
			"status"   					=> 1,
			"isDeleted"   				=> 0,
		);
		$insert = $this->db->insert("prestasi_siswa",$insertArray);
		//debugcode($insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/prestasisiswa/prestasisiswa/?siswa=".$get['siswa']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/prestasisiswa/add");
		}

		
		
	}

	function edit($param){
		$user_id = encrypt_decrypt("decrypt",$param);
		$get = $this->input->get();
		$detailUser = $this->M_prestasi->getDetailPrestasiSiswa($user_id);
		//$data['access_group'] = $this->M_user->getAccessGroup();
		$data['id']	= "";
		if (!empty($detailUser)) {
			$data['id']	= $param;
		}

		$data['back_link']      = base_url('cms/prestasisiswa/prestasisiswa?siswa='.$get['siswa']);
		$data['prestasi'] = $this->M_prestasi->getListPrestasi();
		$data['siswa'] = $this->M_prestasi->getSiswa();
		$data["idnya"]	= $param;
		$data['detailUser'] = $detailUser;
		$data['web_title']  = "Edit Prestasi";
		$data['content']    = "admin/prestasi/editprestasisiswa";
		$this->load->view('admin/layout',$data);
		//debugcode($data['detailUser']);
	}

	function doUpdate($id){
		$get 	 = $this->input->get();
		$post    = $this->input->post();
		$user_id = encrypt_decrypt("decrypt", $id);

		$updateArray = array(
		"prestasi_id"    	=> $post['prestasi'],
		//"siswaId"			=> $post['siswa'],
		"updated_date"     	=> date('Y-m-d H:i:s'),
		"updated_by"       	=> $this->sessionData['user_id'],
		);

		$update = $this->db->update("prestasi_siswa", $updateArray, array("prestasi_siswa_id" => $user_id));
		
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/prestasisiswa/prestasisiswa/?siswa=".$get['siswa']);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/prestasisiswa/edit/".$id, 'refresh');
		}
	}

	function doDelete($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"isDeleted" => 1
		);
		$update = $this->db->update("prestasi",$updateArray,array("prestasi_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/prestasi");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/prestasi");
		}
	}

	public function get_list_prestasi($id){
		$requestParam 			= $_REQUEST;
		$user_id = encrypt_decrypt("decrypt", $id);

		$getData 				= $this->M_prestasi->get_list_prestasi_siswa ( $requestParam, $user_id, 'nofilter' );
		$totalAllData 			= $this->M_prestasi->get_list_prestasi_siswa ( $requestParam, $user_id, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_prestasi->get_list_prestasi_siswa ( $requestParam, $user_id, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_prestasi->get_list_prestasi_siswa ( $requestParam, $user_id );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			//debugcode($value);
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->prestasi_siswa_id);
			$siswa_encrypt = encrypt_decrypt('encrypt', $value->siswa_id);
			$button = "";
			// $button .= '
			// <button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pelanggaransantri/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/prestasisiswa/edit/'.$encrypt_id."?siswa=".$siswa_encrypt).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			//.$encrypt_id."?siswa=".$siswa_encrypt
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}
			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->prestasi_name;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_prestasi->get_list_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_prestasi->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_prestasi->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_prestasi->get_list_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->siswa_id);

			$button = "";
			// $button .= '
			// <button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pelanggaransantri/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/prestasisiswa/prestasisiswa/'.'?siswa='.$encrypt_id).'" class="btn btn-primary btn-sm" title="pilihan"><i class="fa fa-edit"></i> Lihat Prestasi</a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	function prestasisiswa(){
		$data['web_title'] = "Prestasi";
		$data['content']   = "admin/prestasi/listprestasisantri";
		$this->load->view('admin/layout',$data);
	}
}