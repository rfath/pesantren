<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exportcard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_kelas");
		$this->load->model("M_santri");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataKelas = $this->M_kelas->rfGetKelas();

		$data['kelas']     = $dataKelas;
		$data['web_title'] = "Export Card";
		$data['content']   = "admin/card/exportwizard";
		$this->load->view('admin/layout',$data);
	}

	function getRombel($id){
		$getrombel = $this->M_kelas->rfGetRombelByIdKelasId($id);
		$html = "<option value=''>Pilih Rombel</option>";
		foreach ($getrombel as $key => $value) {
			$html.="<option value='".$value->kelas_rombel_id."'>".$value->kelas_rombel_name."</option>";
		}
		die($html);
	}

	function topdf($kelas_id, $rombel_id){
		$listSiswa = $this->M_kelas->rfGetSiswaByKelasRombel($kelas_id,$rombel_id);
		/*GENERATE IMAGE*/
		foreach ($listSiswa as $key => $value) {
			$detsantri = $this->M_santri->getSantriByID("siswa_id",$value->siswa_id);
			generate_card_save($detsantri);
		}
		/*END GENERATE IMAGE*/
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);
		$data['siswa'] = $listSiswa;
		$html = $this->load->view('admin/card/pdfexport',$data,true);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}