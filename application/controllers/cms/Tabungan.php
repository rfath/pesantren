<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabungan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_tabungan");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Tabungan";
		$data['content']   = "admin/tabungan/tabungan_listSiswa";
		$this->load->view('admin/layout',$data);
	}

	public function get_list_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->global->get_list_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->global->get_list_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->siswa_id);

			$button = "";
			// $button .= '
			// <button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pelanggaransantri/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/tabungan/datatabungan'.'?siswa='.$encrypt_id).'" class="btn btn-primary btn-sm" title="pilihan"><i class="fa fa-edit"></i> Lihat Tabungan</a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	function datatabungan(){
		$get = $this->input->get();
		
		$siswa_id     = encrypt_decrypt("decrypt",$get['siswa']);
		$detail_siswa = $this->M_tabungan->getDetailSiswa($siswa_id);
		$riwayat      = $this->M_tabungan->getRiwayatTabungan($siswa_id);

		$data['id']        = $get['siswa'];
		$data['riwayat']   = $riwayat;
		$data['dataSiswa'] = $detail_siswa;
		$data['back_link'] = base_url('cms/tabungan');
		$data['web_title'] = "Detail Tabungan";
		$data['content']   = "admin/tabungan/detail_tabungan";
		$this->load->view('admin/layout',$data);
	}

	function doAddTabungan(){
		$post     = $this->input->post();
		$siswa_id = encrypt_decrypt("decrypt",$post['param']);
		$nominal  = sNominal($post['jumlah_store']);

		$insertArray = array(
			"tabungan_type" => $post['tipe'],
			"amount"        => $nominal,
			"siswa_id"      => $siswa_id,
			"created_date"  => date("Y-m-d H:i:s"),
			"created_by"    => $this->sessionData['user_id'],
			"status"        => 1,
			"isDeleted"     => 0
		);
		$insert = $this->db->insert("tabungan_transfer",$insertArray);
		if ($insert) {
			/*UPDATE AMOUNT FOR SISWA*/
			if ($post['tipe'] == "IN") {
				$sql     = "UPDATE siswa SET amount_tabungan = amount_tabungan + ".$nominal." WHERE siswa_id = ".$siswa_id;
				$doQuery = $this->db->query($sql);
			}else{
				$sql     = "UPDATE siswa SET amount_tabungan = amount_tabungan - ".$nominal." WHERE siswa_id = ".$siswa_id;
				$doQuery = $this->db->query($sql);
			}
			
			custom_notif("success");
		}else{
			custom_notif("failed","101","Something went wrong inserting data");
		}
		redirect("cms/tabungan/datatabungan?siswa=".$post['param']);
	}
}