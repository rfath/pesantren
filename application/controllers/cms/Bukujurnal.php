<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BukuJurnal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_bukujurnal");
		$this->load->model("M_bendahara");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] 	= "Buku Jurnal";
		$data['content']   	= "admin/buku_jurnal/index";
		$this->load->view('admin/layout',$data);
	}

	public function printjurnal($kas_type, $tgl_awal, $tgl_akhir){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);
		if ($kas_type != "ALL") {
			$kas_type 	= $kas_type;
			$field_condition 	=  "kas_type";
		}else{
			$kas_type 	= "";
			$field_condition 	=  "kas_type !=";
		}
		
		$dataJurnal 		= $this->M_bukujurnal->getListJurnal($field_condition, $kas_type, $tgl_awal, $tgl_akhir);
		$data['listJurnal']	= $dataJurnal;
		$view = $this->load->view('admin/buku_jurnal/laporan_jurnal', $data,true);
		$mpdf->WriteHTML($view);
		$mpdf->Output();
	}

	function dodetail(){
		$get = $this->input->get();
		
		if ($get['type'] <> "") {
			$kas_type 	= $get['type'];
			$field_condition 	=  "kas_type";
		}else{
			$kas_type 	= "";
			$field_condition 	=  "kas_type !=";
		}

		$tgl_awal   = date("Y-m-d",strtotime($get['start']));
		$tgl_akhir  = date("Y-m-d",strtotime($get['end']));
		$dataJurnal = $this->M_bukujurnal->getListJurnal($field_condition, $kas_type, $tgl_awal, $tgl_akhir);
		//debugCode($dataJurnal);
		$data['dataJurnal'] = $dataJurnal;
		$this->load->view("admin/buku_jurnal/detailjurnal",$data);
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_kas(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_bukujurnal->get_list_kas ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_bukujurnal->get_list_kas ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_bukujurnal->get_list_kas ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_bukujurnal->get_list_kas ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			
			$rowData[] = $no++;
			$rowData[] = $value->kas_name;
			$rowData[] = $value->kas_type;
			$rowData[] = number_format($value->kas_transaction);
			$rowData[] = $value->created_date;
			$rowData[] = number_format($value->amount_after);		
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/
}