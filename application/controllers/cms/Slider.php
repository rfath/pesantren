<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_master");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$slider = $this->M_master->getAllSlider();
		$data['sliderData']   = $slider;
		$data['web_title'] = "Slider";
		$data['content']   = "admin/slider/listSlider";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add Slider";
		$data['back_link']	= base_url('cms/slider');
		$data['content']   = "admin/slider/addslider";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();

		$dir = './assets_ticketing/slider/';
		if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}

		/*==== START UPLOADING FILE ====*/
		$ext       = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
		$file_name = "slide".date("dmYHis").rand(100,999).".".$ext;
		
		$config['upload_path']   = $dir;
		$config['allowed_types'] = '*';
		$config['file_name']     = $file_name;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image')){
			$error = 'error: '. $this->upload->display_errors();
			echo $error;
			die();
		}else{
			$file_name = $dir.$file_name;
			$file_name = substr($file_name, 1); //remove "."
		}
		/*==== END UPLOADING FILE ====*/


		/*==== START INSERT SLIDER ====*/
		$insertArray = array(
			"img"	=> $file_name,
			"seq"	=> $post['sequence'],
			"url"	=> $post['url']
		);
		$insert = $this->db->insert("slider",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect('cms/slider/');
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect('cms/slider/');
		}
		/*==== END INSERT SLIDER ====*/
	}

	function doDelete($param){
		$slider_id = encrypt_decrypt("decrypt",$param);
		$updateArray = array(
			"is_deleted"	=> 1
		);
		$update = $this->db->update("slider",$updateArray, array("slider_id" => $slider_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect('cms/slider/');
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect('cms/slider/');
		}
	}

	function edit($param){
		$slider_id = encrypt_decrypt("decrypt",$param);

		$getDataSlider = $this->M_master->getSliderById($slider_id);

		$data['sliderData'] = $getDataSlider;
		$data['id']         = $param;
		$data['web_title']  = "Edit Slider";
		$data['back_link']  = base_url('cms/slider');
		$data['content']    = "admin/slider/editslider";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post = $this->input->post();

		$slider_id = encrypt_decrypt("decrypt",$post['param']);
		/*==== START INSERT SLIDER ====*/
		$updateArray = array(			
			"seq"	=> $post['sequence'],
			"url"	=> $post['url']
		);


		if ($_FILES['image']['name'] <> "") {
			/*==== START UPLOADING FILE ====*/
			$ext       = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
			$file_name = "slide".date("dmYHis").rand(100,999).".".$ext;
			
			$config['upload_path']   = $dir;
			$config['allowed_types'] = '*';
			$config['file_name']     = $file_name;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image')){
				$error = 'error: '. $this->upload->display_errors();
				echo $error;
				die();
			}else{
				$file_name = $dir.$file_name;
				$file_name = substr($file_name, 1); //remove "."
				$updateArray['img'] = $file_name;
			}
			/*==== END UPLOADING FILE ====*/
		}
		$update = $this->db->update("slider",$updateArray, array("slider_id" => $slider_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect('cms/slider/');
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect('cms/slider/');
		}
	}
}