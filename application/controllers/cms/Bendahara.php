<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bendahara extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_bendahara");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] 	= "Bendahara";
		$data['content']   	= "admin/bendahara/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['back_link'] = base_url('cms/bendahara');
		$data['web_title'] = "Add Kas";
		$data['content']   = "admin/bendahara/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		$currentMoney = $this->global->getCurrentMoney();
		$ext          = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
		$nama_file    = "BKT-".date("dmYHis").rand(100,999).".".$ext;
		
		$config['upload_path']   = './images/bukti_pembayaran/';
		$config['allowed_types'] = 'gif|jpg|png|pdf|PNG|JPG|JPEG|PDF';
		$config['file_name']     = $nama_file;

		$this->load->library('upload', $config);
 		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('photo')){
			$error = array('error' => $this->upload->display_errors());
			debugCode($error);
		}else{
			$data = array('upload_data' => $this->upload->data());
		}
		
		if ($post['kas_type'] == "IN") {
			$amount_after = $currentMoney->amount + $post['nominal'];
		}else{
			$amount_after = $currentMoney->amount - $post['nominal'];
		}
		$insertArray  = array(
			"kas_name"             => $post['kas_name'],
			"kas_type"             => $post['kas_type'],
			"kas_transaction"      => $post['nominal'],
			"kas_bukti_pembayaran" => "images/bukti_pembayaran/".$nama_file,
			"status"               => 1,
			"isDeleted"            => 0,
			"amount_after"         => $amount_after,
			"created_date"         => $post['tgl_trans'],
			"created_by"           => $this->sessionData['user_id'],
			"is_from_kas"          => 1,
			"desc"                 => $post['deskripsi']
		);

		$insert	= $this->db->insert("uang_kas", $insertArray);
		if ($insert) {
			/*====== START UPDATE AMOUNT IN MONEY =====*/
			if ($post['kas_type'] == "IN") {
				$sql_update = "UPDATE money SET amount = amount + ".$post['nominal'];
				$query      = $this->db->query($sql_update);
			}else{
				$sql_update = "UPDATE money SET amount = amount - ".$post['nominal'];
				$query      = $this->db->query($sql_update);
			}
			
			if (!$query) {
				custom_notif("failed","103","Something went wrong with updating money");
			}
			custom_notif("success","Status","Success Add Kas");
			/*====== END UPDATE AMOUNT IN MONEY =====*/
			redirect("cms/bendahara/");
		}else{
			custom_notif("failed","101","Something went wrong inserting data");
			redirect("cms/bendahara/");
		}
	}

	function edit($id){
		$detailKas 				= $this->M_bendahara->getKasDetail("kas_id", $id);
		// debugCode($detailKas);
		$data['detailKas'] 		= $detailKas;
		$data['back_link']   	= base_url('cms/kelas');
		$data['web_title']   	= "Edit Kelas";
		$data['content']     	= "admin/bendahara/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		
		if(!empty($_FILES['photo']['name'])){
			$nama_file = 'AST-'.date('ydmhis').rand(10,99);

			// Configuration for file upload
			$config['upload_path']          = './images/bukti_pembayaran/';
			$config['allowed_types']        = 'gif|jpg|png|pdf';
			$config['max_size']             = 5000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$config['file_name']			= $nama_file;

			$this->load->library('upload', $config);
	 
			if ( ! $this->upload->do_upload('photo')){
				$error = array('error' => $this->upload->display_errors());
			}else{
				// unlink($post['nama_file']);
				$data = array('upload_data' => $this->upload->data());
			}
			
			$updateArray = array(
				"kas_name"  			=> $post['kas_name'],
				"kas_type"  			=> $post['kas_type'],
				"kas_transaction"  		=> $post['nominal'],
				"kas_bukti_pembayaran"	=> "images/bukti_pembayaran/".$this->upload->data('orig_name'),
				"updated_date"  		=> $post['tgl_trans'],
				"updated_by"    		=> $this->sessionData['user_id']
			);
		}else{

			$updateArray = array(
				"kas_name"  		=> $post['kas_name'],
				"kas_type"  		=> $post['kas_type'],
				"kas_transaction"  	=> $post['nominal'],
				"updated_date"  	=> $post['tgl_trans'],
				"updated_by"    	=> $this->sessionData['user_id']
			);
		}

		$update = $this->db->update("uang_kas", $updateArray, array("kas_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/bendahara");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/bendahara/edit/".$id);
		}
	}

	function doDelete($id){
		$currentMoney = $this->global->getCurrentMoney();
		$detail       = $this->M_bendahara->getKasDetail("kas_id",$id);
		if ($detail->kas_type == "IN") {
			$type="OUT";
			$amount_after = $currentMoney->amount - $detail->kas_transaction;
		}else{
			$type="IN";
			$amount_after = $currentMoney->amount + $detail->kas_transaction;
		}
		$newInsert = array(
			"kas_name"             => $detail->kas_name,
			"kas_type"             => $type,
			"kas_transaction"      => $detail->kas_transaction,
			"kas_bukti_pembayaran" => $detail->kas_bukti_pembayaran,
			"desc"                 => 'CANCEL TRANSACTION '.$detail->kas_type.' '.$detail->kas_name,
			"amount_after"         => $amount_after,
			"created_date"         => date("Y-m-d H:i:s"),
			"created_by"           => $this->sessionData['user_id'],
			"is_from_kas"          => 1,
			"status"               => 1,
			"isDeleted"            => 0
		);
		$insert	= $this->db->insert("uang_kas", $newInsert);
		if ($insert) {
			/*====== START UPDATE AMOUNT IN MONEY =====*/
			if ($type == "IN") {
				$sql_update = "UPDATE money SET amount = amount + ".$detail->kas_transaction;
				$query      = $this->db->query($sql_update);
			}else{
				$sql_update = "UPDATE money SET amount = amount - ".$detail->kas_transaction;
				$query      = $this->db->query($sql_update);
			}
			
			if (!$query) {
				custom_notif("failed","103","Something went wrong with updating money");
			}
			custom_notif("success","Status","Success Add new cancel kas");
			/*====== END UPDATE AMOUNT IN MONEY =====*/
			redirect("cms/bendahara/");
		}else{
			custom_notif("failed","101","Something went wrong inserting data");
			redirect("cms/bendahara/");
		}

	}

	function doDelete_OLD($id){
		
		$detail = $this->M_bendahara->getKasDetail("kas_id",$id);
		debugCode($detail);

		$updateArray = array(
			"isDeleted"    => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);
		$delete = $this->db->update("uang_kas", $updateArray, array("kas_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/bendahara");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/bendahara");
		}
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_kas(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_bendahara->get_list_kas ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_bendahara->get_list_kas ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_bendahara->get_list_kas ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_bendahara->get_list_kas ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			/*$button .= '
				<a href="'.base_url('cms/bendahara/edit/'.$value->kas_id).'" class="btn btn-primary btn-sm" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/bendahara/doDelete/'.$value->kas_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				';*/
			$button .= '
				<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/bendahara/doDelete/'.$value->kas_id).'\')" title="Delete"><i class="fa fa-trash"></i> Delete/Cancel</button>
				';
			/*========================================= END BUTTON STUFF =========================================*/
			// Data TP
			// $dataTP = $this->M_tahunpelajaran->getTahunPelajaraneDetail("tahun_pelajaran_id", $value->tahun_pelajaran_id);
			// End
			$type = "";
			if ($value->kas_type == "IN") {
				$type = "<b style='color:#2ecc71'>".$value->kas_type."</b>";
			}else{
				$type = "<b style='color:#e74c3c'>".$value->kas_type."</b>";
			}
			$rowData[] = $no++;
			$rowData[] = $value->kas_name;
			$rowData[] = $type;
			$rowData[] = number_format($value->kas_transaction);
			$rowData[] = $value->desc;
			$rowData[] = $value->created_date;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/


}