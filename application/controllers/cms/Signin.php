<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_signin");
	}

	public function index(){
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		
		if (!empty($sessionData)) {
			redirect('cms/dashboard');
		}
		$data['login'] = array("OK");
		$this->load->view('admin/signinup/signin',$data);
	}

	function doLogin(){
		$post     = $this->input->post();
		$proccess = $this->M_signin->checkLogin($post['email'],$post['password']);
		if ($proccess == 1) {
			redirect("cms/dashboard");
		}else{
			redirect("cms/signin");
		}
	}

	function signout(){
		session_destroy();
		redirect("cms/user");
	}
}