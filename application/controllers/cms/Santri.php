<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Santri extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_santri");
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataSantri = $this->M_santri->getSantri();

		$data['listData'] 	= $dataSantri;
		$data['web_title'] 	= "Santri";
		$data['content']   	= "admin/santri/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$provinsi	= $this->global->getProvince();

		$data['provinsi']	= $provinsi;
		$data['back_link'] 	= base_url('cms/santri');
		$data['web_title'] 	= "Add Santri";
		$data['content']   	= "admin/santri/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();

		$dataSantri = $this->M_santri->getSantriById("siswa_nomor_induk", $post['no_induk']);
		// debugCode($dataSantri);

		if (!empty($dataSantri)) {
			custom_notif("failed","Gagal","Nomor Induk : ".$post['no_induk']." sudah terdaftar.");
			redirect("cms/santri/add/");
		}else{
			// Membuat Json untuk additional siswa / santri
			$additional->nama_ayah 			= $post["additional"][0];
			$additional->pekerjaan_ayah 	= $post["additional"][1];
			$additional->pendidikan_ayah 	= $post["additional"][2];
			$additional->nama_ibu 			= $post["additional"][3];
			$additional->pekerjaan_ibu 		= $post["additional"][4];
			$additional->pendidikan_ibu 	= $post["additional"][5];
			$additional->kontak_ortu 		= $post["additional"][6];
			$additional->alamat_ortu 		= $post["additional"][7];
			$additional->nama_wali 			= $post["additional"][8];
			$additional->kontak_wali 		= $post["additional"][9];
			$additional->alamat_wali 		= $post["additional"][10];
			$additional 					= json_encode($additional);
			// debugCode($additional);

			$nama_file = 'SNT-'.date('ydmhis').rand(10,99);

			// Configuration for file upload
			$config['upload_path']          = './images/santri/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 5000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$config['file_name']			= $nama_file;

			$this->load->library('upload', $config);
	 
			if ( ! $this->upload->do_upload('photo')){
				// $error = array('error' => $this->upload->display_errors());
			}else{
				// $data = array('upload_data' => $this->upload->data());
			}

			$lama_pengabdian = date("Y") - $post['tmt'];
			
			$insertArray = array(
				"siswa_nomor_induk"  	=> $post['no_induk'],
				"siswa_nisn" 			=> $post['nisn'],
				"siswa_name" 			=> $post['nama'],
				"siswa_tempat_lahir" 	=> $post['tempat'],
				"siswa_tanggal_lahir" 	=> date("Y-m-d", strtotime($post['tgl_lahir'])),
				"siswa_no_hp" 			=> $post['no_hp'],
				"siswa_kewarganegaraan" => $post['kewarganegaraan'],
				"siswa_alamat" 			=> $post['alamat'],
				"siswa_province_id" 	=> $post['provinsi'],
				"siswa_kota_id" 		=> $post['kota'],
				"siswa_kecamatan" 		=> $post['kecamatan'],
				"siswa_kelurahan" 		=> $post['kelurahan'],
				"siswa_additional" 		=> $additional,
				"photo"					=> "images/santri/".$this->upload->data('orig_name'),
				"is_graduate"			=> "0",
				"status"				=> "1",
				"isDeleted"				=> "0",
				"created_date"  		=> date('Y-m-d H:i:s'),
				"created_by"    		=> $this->sessionData['user_id']

			);

			$insert	= $this->db->insert("siswa", $insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/santri/");
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/santri/add/");
			}
		}
	}

	function edit($id){
		$detailSantri 			= $this->M_santri->getSantriById("siswa_id", $id);
		$provinsi				= $this->global->getProvince();

		$data['provinsi']		= $provinsi;
		$data['detailSantri'] 	= $detailSantri;
		$data['back_link']   	= base_url('cms/santri');
		$data['web_title']   	= "Edit Santri";
		$data['content']     	= "admin/santri/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();

		$dataSantri = $this->M_santri->getSantriById("siswa_nomor_induk", $post['no_induk']);
		
		if (!empty($dataSantri)) {
			$no_induk = $dataSantri->siswa_nomor_induk;
		}else{
			$no_induk = $post['no_induk'];
		}

		// Membuat Json untuk additional siswa / santri
		$additional->nama_ayah 			= $post["additional"][0];
		$additional->pekerjaan_ayah 	= $post["additional"][1];
		$additional->pendidikan_ayah 	= $post["additional"][2];
		$additional->nama_ibu 			= $post["additional"][3];
		$additional->pekerjaan_ibu 		= $post["additional"][4];
		$additional->pendidikan_ibu 	= $post["additional"][5];
		$additional->kontak_ortu 		= $post["additional"][6];
		$additional->alamat_ortu 		= $post["additional"][7];
		$additional->nama_wali 			= $post["additional"][8];
		$additional->kontak_wali 		= $post["additional"][9];
		$additional->alamat_wali 		= $post["additional"][10];
		$additional 					= json_encode($additional);

		if(!empty($_FILES['photo']['name'])){
			$nama_file = 'SNT-'.date('ydmhis').rand(10,99);

			// Configuration for file upload
			$config['upload_path']          = './images/santri/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 5000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$config['file_name']			= $nama_file;

			$this->load->library('upload', $config);
	 
			if ( ! $this->upload->do_upload('photo')){
				$error = array('error' => $this->upload->display_errors());
			}else{
				// unlink($post['nama_file']);
				$data = array('upload_data' => $this->upload->data());
			}
			
			// Array for input to table Siswa
			$dataArray = array(
				"siswa_nomor_induk"  	=> $no_induk,
				"siswa_nisn" 			=> $post['nisn'],
				"siswa_name" 			=> $post['nama'],
				"siswa_tempat_lahir" 	=> $post['tempat'],
				"siswa_tanggal_lahir" 	=> date("Y-m-d", strtotime($post['tgl_lahir'])),
				"siswa_no_hp" 			=> $post['no_hp'],
				"siswa_kewarganegaraan" => $post['kewarganegaraan'],
				"siswa_alamat" 			=> $post['alamat'],
				"siswa_province_id" 	=> $post['provinsi'],
				"siswa_kota_id" 		=> $post['kota'],
				"siswa_kecamatan" 		=> $post['kecamatan'],
				"siswa_kelurahan" 		=> $post['kelurahan'],
				"siswa_additional" 		=> $additional,
				"photo"					=> "images/santri/".$this->upload->data('orig_name'),
				"is_graduate"			=> "0",
				"updated_date"  		=> date('Y-m-d H:i:s'),
				"updated_by"    		=> $this->sessionData['user_id']
			);
		}else{
			// Array for input to table Siswa
			$dataArray = array(
				"siswa_nomor_induk"  	=> $no_induk,
				"siswa_nisn" 			=> $post['nisn'],
				"siswa_name" 			=> $post['nama'],
				"siswa_tempat_lahir" 	=> $post['tempat'],
				"siswa_tanggal_lahir" 	=> date("Y-m-d", strtotime($post['tgl_lahir'])),
				"siswa_no_hp" 			=> $post['no_hp'],
				"siswa_kewarganegaraan" => $post['kewarganegaraan'],
				"siswa_alamat" 			=> $post['alamat'],
				"siswa_province_id" 	=> $post['provinsi'],
				"siswa_kota_id" 		=> $post['kota'],
				"siswa_kecamatan" 		=> $post['kecamatan'],
				"siswa_kelurahan" 		=> $post['kelurahan'],
				"siswa_additional" 		=> $additional,
				"is_graduate"			=> "0",
				"updated_date"  		=> date('Y-m-d H:i:s'),
				"updated_by"    		=> $this->sessionData['user_id']
			);
		}

		$update = $this->db->update("siswa", $dataArray, array("siswa_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/santri");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/santri/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("siswa", $updateArray, array("siswa_id" => $id));
		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/santri");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/santri");
		}
	}

	function upload(){
		$provinsi	= $this->global->getProvince();

		$data['provinsi']	= $provinsi;
		$data['back_link'] = base_url('cms/santri');
		$data['web_title'] = "Upload Santri";
		$data['content']   = "admin/santri/upload";
		$this->load->view('admin/layout',$data);
	}

	function uploadSantri(){
		$fileName = $this->input->post('file', TRUE);

		$config['upload_path'] = './upload/santri/'; 
		$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
		$config['max_size'] = 10000;

		$this->load->library('upload', $config);
		$this->upload->initialize($config); 

		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			// debugCode($error);
			$this->session->set_flashdata('msg','Ada kesalah dalam upload'); 
			redirect('cms/santri');
		} else {
			$media = $this->upload->data();
			$inputFileName = 'upload/santri/'.$media['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			if ($objPHPExcel->getActiveSheet()->getCell('F2')->getValue() <> "ayam") {
				$this->session->set_flashdata('is_success', 'No');
				redirect('cms/santri/upload');
			}else{
				$dataGagal = [];
				for ($row = 4; $row <= $highestRow; $row++){
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
						NULL,
						TRUE,
						FALSE);

					$dataSantri = $this->M_santri->getSantriById("siswa_nomor_induk", $rowData[0][0]);

					if (empty($dataSantri)) {
						$additional = [];
						$additional['nama_ayah'] 		= $rowData[0][112];
						$additional['pekerjaan_ayah'] 	= $rowData[0][13];
						$additional['pendidikan_ayah'] 	= $rowData[0][14];
						$additional['nama_ibu'] 		= $rowData[0][15];
						$additional['pekerjaan_ibu'] 	= $rowData[0][16];
						$additional['pendidikan_ibu'] 	= $rowData[0][17];
						$additional['kontak_ortu'] 		= "0".$rowData[0][18];
						$additional['alamat_ortu'] 		= $rowData[0][19];
						$additional['nama_wali'] 		= $rowData[0][20];
						$additional['kontak_wali'] 		= "0".$rowData[0][21];
						$additional['alamat_wali'] 		= $rowData[0][22];
						$additional 					= json_encode($additional);

						$data = array(
							"siswa_nomor_induk"  	=> $rowData[0][0],
							"siswa_nisn" 			=> $rowData[0][1],
							"siswa_name" 			=> $rowData[0][2],
							"siswa_tempat_lahir" 	=> $rowData[0][3],
							"siswa_tanggal_lahir" 	=> date("Y-m-d", strtotime($rowData[0][4])),
							"siswa_no_hp" 			=> "0".$rowData[0][5],
							"siswa_kewarganegaraan" => $rowData[0][6],
							"siswa_alamat" 			=> $rowData[0][7],
							"siswa_province_id" 	=> preg_replace("/[^0-9]/", '', $rowData[0][8]),
							"siswa_kota_id" 		=> preg_replace("/[^0-9]/", '', $rowData[0][9]),
							"siswa_kecamatan" 		=> $rowData[0][10]	,
							"siswa_kelurahan" 		=> $rowData[0][11],
							"siswa_additional" 		=> $additional,	
							"photo"					=> "images/img/default.png",
							"status"				=> "1",
							"isDeleted"				=> "0",
							"created_date"  		=> date('Y-m-d H:i:s'),
							"created_by"    		=> $this->sessionData['user_id'],
							"is_graduate"			=> "0"
						);
						
						$insert = $this->db->insert("siswa", $data);
					}else{
						$dataGagal[] = $rowData[0][0];
					}
				}

				if ($insert) {
					if (!empty($dataGagal)) {
						custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." sudah terdaftar.");
					}
					$this->session->set_flashdata('is_success', 'Yes');
					redirect("cms/santri/upload");	
				}else{
					if (!empty($dataGagal)) {
						custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." sudah terdaftar.");
					}
					$this->session->set_flashdata('is_success', 'No');
					redirect("cms/santri/upload");	
				}
			}
		}  
	}

	function getCityByProvince($id){
		$city = $this->global->getCityByProvince($id);
		$html = '<option value="">Pilih Kota</option>';
		foreach ($city as $key => $value) {
			$html.='<option value="'.$value->city_id.'">'.$value->city_name.'</option>';
		}
		die($html);
	}

	function card($id){
		$detsantri = $this->M_santri->getSantriByID("siswa_id",$id);
		generate_card_new($detsantri);
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_santri(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_santri->get_list_santri ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_santri->get_list_santri ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_santri->get_list_santri ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_santri->get_list_santri ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<a href="'.base_url('cms/santri/edit/'.$value->siswa_id).'" class="btn btn-primary btn-sm tbl-btn" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				<button class="btn btn-danger btn-sm tbl-btn" onClick="is_delete(\''.base_url('cms/santri/doDelete/'.$value->siswa_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				<a href="'.base_url('cms/santri/card/'.$value->siswa_id).'" target="_blank" class="btn btn-warning btn-sm tbl-btn" title="View Card"><i class="fa fa-user"></i> Card</a>
				';
			/*========================================= END BUTTON STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_tempat_lahir;
			$rowData[] = $value->siswa_alamat;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/

}