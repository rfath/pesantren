<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestasi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_prestasi");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Prestasi";
		$data['content']   = "admin/prestasi/listprestasi";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add Prestasi";
		$data['content']   = "admin/prestasi/addprestasi";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"prestasi_name"    	=> $post['prestasiname'],
			"created_date"     	=> date('Y-m-d H:i:s'),
			"created_by"       	=> $this->sessionData['user_id'],
			"status"   			=> 1,
			"isDeleted"   		=> 0,
		);
		$insert = $this->db->insert("prestasi",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/prestasi");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/prestasi/add");
		}
		
		
	}

	function edit($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$detailUser = $this->M_prestasi->getDetailPrestasi($user_id);
		//$data['access_group'] = $this->M_user->getAccessGroup();
		$data['id']	= "";
		if (!empty($detailUser)) {
			$data['id']	= $param;
		}

		$data["idnya"]	= $param;
		$data['detailUser'] = $detailUser;
		$data['web_title']  = "Edit Prestasi";
		$data['content']    = "admin/prestasi/editprestasi";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post    = $this->input->post();
		$user_id = encrypt_decrypt("decrypt", $id);
		$updateArray = array(
		"prestasi_name"    	=> $post['prestasiname'],
		"updated_date"     	=> date('Y-m-d H:i:s'),
		"updated_by"       	=> $this->sessionData['user_id'],
		);

		$update = $this->db->update("prestasi", $updateArray, array("prestasi_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/prestasi");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/prestasi/edit/".$id, 'refresh');
		}
	}

	function doDelete($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"isDeleted" => 1
		);
		$update = $this->db->update("prestasi",$updateArray,array("prestasi_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/prestasi");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/prestasi");
		}
	}

	public function get_list_prestasi(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_prestasi->get_list_prestasi ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_prestasi->get_list_prestasi ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_prestasi->get_list_prestasi ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_prestasi->get_list_prestasi ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->prestasi_id);

			$button = "";
			$button .= '
			<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/prestasi/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/prestasi/edit/'.$encrypt_id).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}
			$rowData[] = $no++;
			$rowData[] = $value->prestasi_name;
			// $rowData[] = $value->created_date;
			// $rowData[] = $this->sessionData['username'];
			// $rowData[] = $value->updated_date;
			// $rowData[] = $value->updated_by;
			// $rowData[] = $status;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}