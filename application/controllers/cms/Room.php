<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Room extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_room");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Room";
		$data['content']   = "admin/room/listroom";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$place = $this->global->getListPlace();

		$data['place']     = $place;
		$data['web_title'] = "Add Room";
		$data['content']   = "admin/room/addroom";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"room_name"        => $post['room_name'],
			"room_description" => $post['room_description'],
			"place_id"         => $post['room_place'],
			"created_date"     => date("Y-m-d H:i:s"),
			"created_by"       => $this->sessionData['user_id'],
			"is_active"        => 1,
			"is_deleted"       => 0
		);

		$doInsert = $this->db->insert("room",$insertArray);
		if ($doInsert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/room");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/room/add");
		}
	}

	function edit($param){
		$place = $this->global->getListPlace();

		$room_id  = encrypt_decrypt("decrypt",$param);
		$roomData = $this->M_room->getRoomDetail($room_id);

		$data['id']        = $param;
		$data['place']     = $place;
		$data['roomData']  = $roomData;
		$data['web_title'] = "Edit Room";
		$data['content']   = "admin/room/editroom";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post    = $this->input->post();
		$room_id = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"room_name"        => $post['room_name'],
			"room_description" => $post['room_description'],
			"place_id"         => $post['room_place'],
			"updated_date"     => date("Y-m-d H:i:s"),
			"updated_by"       => $this->sessionData['user_id']
		);

		$update = $this->db->update("room",$updateArray,array("room_id" => $room_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/room");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/room/edit/".$post['param']);
		}
	}

	function doDelete($id){
		$room_id = encrypt_decrypt("decrypt",$id);

		$updateArray = array(
			"is_deleted"        => 1,
			"updated_date"     => date("Y-m-d H:i:s"),
			"updated_by"       => $this->sessionData['user_id']
		);

		$update = $this->db->update("room",$updateArray,array("room_id" => $room_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/room");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/room");
		}
	}

	public function get_list_airlines(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_room->get_list_room ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_room->get_list_room ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_room->get_list_room ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_room->get_list_room ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$button = "";	
			$button .= '
			<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/room/doDelete/'.encrypt_decrypt('encrypt',$value->room_id)).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/room/edit/'.encrypt_decrypt('encrypt',$value->room_id)).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			

			$rowData[] = $no++;
			$rowData[] = $value->room_name;
			$rowData[] = $value->place_name;
			$rowData[] = $value->room_description;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}