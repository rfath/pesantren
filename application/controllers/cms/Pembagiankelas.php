<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PembagianKelas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_pembagiankelas");
		$this->load->model("M_kelas");
		$this->load->model("M_santri");
		$this->load->model("M_tahunpelajaran");
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataTP = $this->M_tahunpelajaran->getTahunPelajaran();
		
		$data['tp']			= $dataTP;
		$data['web_title'] 	= "Pembagian Kelas";
		$data['content']   	= "admin/pembagian_kelas/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$dataKelas = $this->M_kelas->getKelas();
		$dataTP    = $this->M_tahunpelajaran->getTahunPelajaran();
		
		$data['tp']			= $dataTP;
		$data['kelas']		= $dataKelas;
		$data['back_link'] 	= base_url('cms/pembagiankelas');
		$data['web_title'] 	= "Pembagian Kelas";
		$data['content']   	= "admin/pembagian_kelas/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post 		= $this->input->post();

		$infoSiswa 	= $this->M_pembagiankelas->getInfoSiswaKelas($post['tp'], $post['kelas'], $post['rombel'], encrypt_decrypt("decrypt", $post['siswa_id']));

		if (!empty($infoSiswa)) {
			$dataSiswa = $this->M_santri->getSantriDetail("siswa_id", encrypt_decrypt("decrypt", $post['siswa_id']));
			custom_notif("failed","Gagal","Nomor Induk : ".$dataSiswa->siswa_nomor_induk.", Nama : ".$post['siswa_nama']." sudah terdaftar di tahun pelajaran, kelas dan rombel yang dituju.");
			redirect("cms/pembagiankelas/add/");
		}else{
			$insertArray = array(
				"siswa_id"  			=> encrypt_decrypt("decrypt", $post['siswa_id']),
				"kelas_id"  			=> $post['kelas'],
				"kelas_rombel_id"  		=> $post['rombel'],
				"tahun_pelajaran_id"  	=> $post['tp'],
				"status" 				=> "1",
				"isDeleted" 			=> "0",
				"created_date"  		=> date('Y-m-d H:i:s'),
				"created_by"    		=> $this->sessionData['user_id']

			);

			$this->db->insert("siswa_kelas", $insertArray);
			$insert	= $this->db->insert("siswa_kelas_history", $insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/pembagiankelas/add");
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/pembagiankelas/add");
			}
		}
	}

	function edit($id){
		$detailKelas 			= $this->M_pembagiankelas->getPKelasDetail("siswa_id", $id);
		$dataKelas 				= $this->M_kelas->getKelas();
		$dataTP 				= $this->M_tahunpelajaran->getTahunPelajaran();

		$data['tp']				= $dataTP;
		$data['kelas']			= $dataKelas;
		$data['detailKelas'] 	= $detailKelas;
		$data['back_link']   	= base_url('cms/pembagiankelas');
		$data['web_title']   	= "Edit Pembagian Kelas";
		$data['content']     	= "admin/pembagian_kelas/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		$siswa_id = encrypt_decrypt("decrypt", $post['siswa_id']);

		$updateArray = array(
			"siswa_id"  			=> $siswa_id,
			"kelas_id"  			=> $post['kelas'],
			"kelas_rombel_id"  		=> $post['rombel'],
			"tahun_pelajaran_id"  	=> $post['tp'],
			"updated_date"  		=> date('Y-m-d H:i:s'),
			"updated_by"    		=> $this->sessionData['user_id']
		);

		$this->db->update("siswa_kelas", $updateArray, array("siswa_id" => $id));
		$update = $this->db->update("siswa_kelas_history", $updateArray, array("siswa_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembagiankelas");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembagiankelas/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("siswa_kelas", $updateArray, array("siswa_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembagiankelas");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembagiankelas");
		}
	}

	function upload(){
		$dataKelas = $this->M_kelas->getKelas();
		$dataTP 				= $this->M_tahunpelajaran->getTahunPelajaran();

		$data['tp']				= $dataTP;
		$data['kelas']			= $dataKelas;
		$data['back_link'] = base_url('cms/pembagiankelas');
		$data['web_title'] = "Upload Pembagian Kelas";
		$data['content']   = "admin/pembagian_kelas/upload";
		$this->load->view('admin/layout',$data);
	}

	function uploadProses(){
		$post 		= $this->input->post();
		$fileName = $this->input->post('file', TRUE);

		$config['upload_path'] = './upload/pembagian_kelas/'; 
		$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
		$config['max_size'] = 10000;

		$this->load->library('upload', $config);
		$this->upload->initialize($config); 

		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			debugCode($error);
			$this->session->set_flashdata('msg','Ada kesalah dalam upload'); 
			redirect('cms/pembagiankelas');
		} else {
			$media = $this->upload->data();
			$inputFileName = 'upload/pembagian_kelas/'.$media['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$sheet = $objPHPExcel->getSheet(0);
			// debugCode($sheet);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			if ($objPHPExcel->getActiveSheet()->getCell('F2')->getValue() <> "ayam") {
				$this->session->set_flashdata('is_success', 'No');
				redirect('cms/pembagiankelas/upload');
			}else{
				$dataGagal = [];
				$dataTerdaftar = [];
				for ($row = 4; $row <= $highestRow; $row++){  
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
						NULL,
						TRUE,
						FALSE);


					$dataSantri =  $this->M_santri->getSantriById("siswa_nomor_induk", $rowData[0][0]);
					// debugCode($dataSantri);
					if (!empty($dataSantri)) {

						// Kondisi ketika siswa sudah terdaftar dan memiliki kelas
						// $infoSiswa 	= $this->M_pembagiankelas->getInfoSiswaKelas($post['tp'], $post['kelas'], $post['rombel'], $dataSantri->siswa_id);
						$infoSiswa = $this->M_pembagiankelas->getPKelasDetail("siswa_id", $dataSantri->siswa_id);

						if (!empty($infoSiswa)) {
							$dataGagal[] = $dataSantri->siswa_nomor_induk;
							// $dataTerdaftar[] = $dataSantri->siswa_nomor_induk;
						}else{
							$data = array(
								"siswa_id"  		=> $dataSantri->siswa_id,
								"kelas_id" 			=> $post['kelas'],
								"kelas_rombel_id" 	=> $post['rombel'],
								"tahun_pelajaran_id"=> $post['tp'],
								"status"			=> "1",
								"isDeleted"			=> "0",
								"created_date"  	=> date('Y-m-d H:i:s'),
								"created_by"    	=> $this->sessionData['user_id']
							);
							
							$this->db->insert("siswa_kelas", $data);
							$insert = $this->db->insert("siswa_kelas_history", $data);	
						}
						// End
					}else{
						$dataGagal[] = $rowData[0][0];
					}
				}

				if ($insert) {
					
					if (!empty($dataGagal)) {
						custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." gagal diproses.");
					}

					// if (!empty($dataGagal)) {
					// 	custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." belum terdaftar.");
					// }
					// if (!empty($dataTerdaftar)) {
					// 	custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataTerdaftar)." sudah terdaftar di tahun pelajaran, kelas dan rombel yang dituju.");
					// }
					$this->session->set_flashdata('is_success', 'Yes');
					redirect("cms/pembagiankelas/upload");	
				}else{
					
					if (!empty($dataGagal)) {
						custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." gagal diproses.");
					}

					// if (!empty($dataGagal)) {
					// 	alert_terdaftar("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." belum terdaftar.");
					// }
					// if (!empty($dataTerdaftar)) {
					// 	custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataTerdaftar)." sudah terdaftar di tahun pelajaran, kelas dan rombel yang dituju.");
					// }
					$this->session->set_flashdata('is_success', 'No');
					redirect("cms/pembagiankelas/upload");	
				}
			}
		}  
	}

	function getRombelByKelas($id){
		$rombel = $this->M_kelas->getRombel("kelas_id", $id);
		$html = '<option value="">Pilih Rombel</option>';
		foreach ($rombel as $key => $value) {
			$html.='<option value="'.$value->kelas_rombel_id.'">'.$value->kelas_rombel_name.'</option>';
		}
		die($html);
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_pembagian_kelas(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_pembagiankelas->get_list_kelas_detial ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_pembagiankelas->get_list_kelas_detial ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_pembagiankelas->get_list_kelas_detial ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_pembagiankelas->get_list_kelas_detial ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<a href="'.base_url('cms/pembagiankelas/edit/'.$value->siswa_id).'" class="btn btn-primary btn-sm" title="Edit / Detail"><i class="fa fa-edit"></i></a>
				<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pembagiankelas/doDelete/'.$value->siswa_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				';
			/*========================================= END BUTTON STUFF =========================================*/
			// Data TP
			$dataTP = $this->M_tahunpelajaran->getTahunPelajaraneDetail("tahun_pelajaran_id", $value->tahun_pelajaran_id);
			// End
			
			$rowData[] = $no++;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->kelas_name;
			$rowData[] = $value->kelas_rombel_name;
			$rowData[] = $dataTP->tahun_pelajaran_name;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	/*==================================================== List Siswa ====================================================*/
	function get_list_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->global->get_list_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->global->get_list_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->siswa_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
			<button type="button" class="btn btn-outline-success btn-sm" onClick="selectsantri(\''.$encrypt_id.'\',\''.htmlspecialchars($value->siswa_name).'\')">Select</button>
			';
			/*========================================= END BUTTON STUFF =========================================*/

			// Get Detail siswa Kelas 
			$data_siswa_kelas = $this->M_pembagiankelas->getPKelasDetail("siswa_id", $value->siswa_id);
			// End

			if (empty($data_siswa_kelas)) {
			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $button;
			
				$listData[] = $rowData;
			}
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/

}