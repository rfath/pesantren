<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_master");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataRegion  = $this->M_master->getRegion();
		$data['regionData'] = $dataRegion;
		$data['web_title']  = "Region";
		$data['content']    = "admin/region/listRegion";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add Region";
		$data['back_link'] = base_url('cms/region');
		$data['content']   = "admin/region/addregion";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		$insertArray = array(
			"region_name"	=> $post['region_name'],
			"created_date"	=> date("Y-m-d H:i:s"),
			"created_by"	=> $this->sessionData['user_id']
		);
		$insert = $this->db->insert("region",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/region");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/region/add");
		}
	}

	function doDelete($param){
		$region_id = encrypt_decrypt("decrypt",$param);
		if ($region_id <> "") {
			$updateArray = array(
				"is_deleted"	=> 1,
				"updated_date"	=> date("Y-m-d H:i:s"),
				"updated_by"	=> $this->sessionData['user_id']
			);
			$update = $this->db->update("region",$updateArray, array("region_id" => $region_id));
			if ($update) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/region");
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/region");
			}
		}
	}

	function edit($param){
		$region_id = encrypt_decrypt("decrypt",$param);
		$regionDetail = $this->M_master->getRegionDetail($region_id);

		$data['id']			= $param;
		$data['regionDetail'] = $regionDetail;
		$data['web_title']    = "Edit Region";
		$data['back_link']    = base_url('cms/region');
		$data['content']      = "admin/region/editregion";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post      = $this->input->post();
		$region_id = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"region_name"	=> $post['region_name'],
			"updated_date"	=> date("Y-m-d H:i:s"),
			"updated_by"	=> $this->sessionData['user_id']
		);

		$update = $this->db->update("region",$updateArray, array("region_id" => $region_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/region");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/region/edit/".$post['param']);
		}
	}
}