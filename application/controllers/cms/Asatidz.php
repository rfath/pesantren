<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asatidz extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_asatidz");
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataAsatidz = $this->M_asatidz->getAsatidz();

		$data['listData'] 	= $dataAsatidz;
		$data['web_title'] 	= "Asatidz";
		$data['content']   	= "admin/asatidz/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['back_link'] = base_url('cms/asatidz');
		$data['web_title'] = "Add Asatidz";
		$data['content']   = "admin/asatidz/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();

		$detailAsatidz = $this->M_asatidz->getAsatidzDetail("asatidz_no_induk", $post['no_induk']);

		if (!empty($detailAsatidz)) {
			custom_notif("failed","Gagal","Nomor Induk : ".$post['no_induk']." sudah terdaftar.");
			redirect("cms/asatidz/add");
		}else{
			$nama_file = 'AST-'.date('ydmhis').rand(10,99);
			// Configuration for file upload
			$config['upload_path']          = './images/asatidz/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 5000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$config['file_name']			= $nama_file;

			$this->load->library('upload', $config);
	 
			if ( ! $this->upload->do_upload('photo')){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
			}

			$lama_pengabdian = date("Y") - date("Y", strtotime($post['tmt']));
			
			$insertArray = array(
				"asatidz_no_induk"  		=> $post['no_induk'],
				"asatidz_nuptk" 			=> $post['nuptk'],
				"asatidz_name" 				=> $post['nama'],
				"tempat" 					=> $post['tempat'],
				"tgl_lahir" 				=> date("Y-m-d", strtotime($post['tgl_lahir'])),
				"asatidz_address" 			=> $post['alamat'],
				"asatidz_phone" 			=> $post['no_hp'],
				"asatidz_tmt" 				=> date("Y-m-d", strtotime($post['tmt'])),
				"asatidz_lama_pengambian" 	=> $lama_pengabdian,
				"photo"						=> "images/asatidz/".$this->upload->data('orig_name'),
				"status"					=> "1",
				"isDeleted"					=> "0",
				"created_date"  			=> date('Y-m-d H:i:s'),
				"created_by"    			=> $this->sessionData['user_id']
			);

			$insert	= $this->db->insert("asatidz", $insertArray);
			if ($insert) {
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/asatidz/add");
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/asatidz/add/");
			}
		}
	}

	function edit($id){
		$detailAsatidz 			= $this->M_asatidz->getAsatidzDetail("asatidz_id", $id);
		
		$data['detailAsatidz'] 	= $detailAsatidz;
		$data['back_link']   	= base_url('cms/asatidz');
		$data['web_title']   	= "Edit Asatidz";
		$data['content']     	= "admin/asatidz/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		$lama_pengabdian = date("Y") - date("Y", strtotime($post['tmt']));

		// Kodisi jika Nomor Induk sudah ada
		$detailAsatidz 			= $this->M_asatidz->getAsatidzDetail("asatidz_no_induk", $post['no_induk']);
		if ($detailAsatidz) {
			$dataAsatidz 		= $this->M_asatidz->getAsatidzDetail("asatidz_id", $id);
			$no_induk = $dataAsatidz->asatidz_no_induk;
		}else{
			$no_induk = $post['no_induk'];
		}
		// End

		if(!empty($_FILES['photo']['name'])){
			$nama_file = 'AST-'.date('ydmhis').rand(10,99);

			// Configuration for file upload
			$config['upload_path']          = './images/asatidz/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 5000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$config['file_name']			= $nama_file;

			$this->load->library('upload', $config);
	 
			if ( ! $this->upload->do_upload('photo')){
				$error = array('error' => $this->upload->display_errors());
			}else{
				// unlink($post['nama_file']);
				$data = array('upload_data' => $this->upload->data());
			}
			
			// Array for input to table asatidz
			$dataArray = array(
				"asatidz_no_induk"  		=> $no_induk,
				"asatidz_nuptk" 			=> $post['nuptk'],
				"asatidz_name" 				=> $post['nama'],
				"tempat" 					=> $post['tempat'],
				"tgl_lahir" 				=> date("Y-m-d", strtotime($post['tgl_lahir'])),
				"asatidz_address" 			=> $post['alamat'],
				"asatidz_phone" 			=> $post['no_hp'],
				"asatidz_tmt" 				=> $post['tmt'],
				"asatidz_lama_pengambian" 	=> $lama_pengabdian,
				"photo"						=> "images/asatidz/".$this->upload->data('orig_name'),
				"updated_date"  			=> date('Y-m-d H:i:s'),
				"updated_by"    			=> $this->sessionData['user_id']
			);
		}else{
			// Array for input to table asatidz
			$dataArray = array(
				"asatidz_no_induk"  		=> $no_induk,
				"asatidz_nuptk" 			=> $post['nuptk'],
				"asatidz_name" 				=> $post['nama'],
				"tempat" 					=> $post['tempat'],
				"tgl_lahir" 				=> date("Y-m-d", strtotime($post['tgl_lahir'])),
				"asatidz_address" 			=> $post['alamat'],
				"asatidz_phone" 			=> $post['no_hp'],
				"asatidz_tmt" 				=> $post['tmt'],
				"asatidz_lama_pengambian" 	=> $lama_pengabdian,
				"updated_date"  			=> date('Y-m-d H:i:s'),
				"updated_by"    			=> $this->sessionData['user_id']
			);
		}

		$update = $this->db->update("asatidz", $dataArray, array("asatidz_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/asatidz");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/asatidz/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("asatidz", $updateArray, array("asatidz_id" => $id));
		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/asatidz");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/asatidz");
		}
	}

	function upload(){
		$data['back_link'] = base_url('cms/asatidz');
		$data['web_title'] = "Upload Asatidz";
		$data['content']   = "admin/asatidz/upload";
		$this->load->view('admin/layout',$data);
	}

	function uploadAsatidz(){
		$fileName = $this->input->post('file', TRUE);

		$config['upload_path'] = './upload/asatidz/'; 
		$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
		$config['max_size'] = 10000;

		$this->load->library('upload', $config);
		$this->upload->initialize($config); 

		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			// debugCode($error);
			$this->session->set_flashdata('msg','Ada kesalah dalam upload'); 
			redirect('cms/asatidz');
		} else {
			$media = $this->upload->data();
			$inputFileName = 'upload/asatidz/'.$media['file_name'];
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$sheet = $objPHPExcel->getSheet(0);
			// debugCode($sheet);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			if ($objPHPExcel->getActiveSheet()->getCell('F2')->getValue() <> "ayam") {
				$this->session->set_flashdata('is_success', 'No');
				redirect('cms/asatidz/upload');
			}else{
				$dataGagal = [];
				for ($row = 4; $row <= $highestRow; $row++){  
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
						NULL,
						TRUE,
						FALSE);

					$detailAsatidz 			= $this->M_asatidz->getAsatidzDetail("asatidz_no_induk", $rowData[0][0]);

					if (!empty($detailAsatidz)) {
						$dataGagal[] = $rowData[0][0];
					}else{
						$lama_pengabdian = date("Y") - date("Y", strtotime($rowData[0][7]));
						$data = array(
							"asatidz_no_induk"  		=> $rowData[0][0],
							"asatidz_nuptk" 			=> $rowData[0][1],
							"asatidz_name" 				=> $rowData[0][2],
							"tempat" 					=> $rowData[0][3],
							"tgl_lahir" 				=> date("Y-m-d", strtotime($rowData[0][4])),
							"asatidz_phone" 			=> "0".$rowData[0][5],
							"asatidz_address" 			=> $rowData[0][6],
							"asatidz_tmt" 				=> date("Y-m-d", strtotime($rowData[0][7])),
							"asatidz_lama_pengambian" 	=> $lama_pengabdian,
							"photo"						=> "images/img/default.png",
							"status"					=> "1",
							"isDeleted"					=> "0",
							"created_date"  			=> date('Y-m-d H:i:s'),
							"created_by"    			=> $this->sessionData['user_id']
						);
						
						$this->db->insert("asatidz", $data);
					}
				}
				
				if ($insert) {
					if (!empty($dataGagal)) {
						custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." sudah terdaftar.");
					}
					$this->session->set_flashdata('is_success', 'Yes');
					redirect("cms/asatidz/upload");
				}else{
					if (!empty($dataGagal)) {
						custom_notif("failed","Gagal","Nomor Induk : ".implode(", ", $dataGagal)." sudah terdaftar.");
					}
					$this->session->set_flashdata('is_success', 'No');
					redirect("cms/asatidz/upload");
				}
			}
		}  
	}

}