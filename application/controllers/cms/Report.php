<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_report");
		$this->load->model("M_member");
		$this->load->model("M_event");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title']  = "Report";
		$data['content']    = "admin/report/list_report";
		$this->load->view('admin/layout',$data);
	}

	/*=========================================== START SALES REPORT ===========================================*/
	function sales_report(){
		$get = $this->input->get();
		$member = $this->M_report->getMemberActive();
		$event  = $this->M_report->getEventActive();

		$dataGenerate = array();
		$dateReport = "";
		$excelHref = "";
		if (!empty($get)) {
			$dataGenerate = $this->generate_sales_report($get);
			$dateReport = $get['daterange'];
			$excelHref = base_url('cms/report/sales_report_excel?'.http_build_query($get));
		}

		$data['excelHref']	= $excelHref;
		$data['dateReport']	= $dateReport;
		$data['reportData'] = $dataGenerate;
		$data['member']     = $member;
		$data['event']      = $event;

		$data['web_title']  = "Sales Report";
		$data['back_link']  = base_url('cms/report/');
		$data['content']    = "admin/report/report_sales";
		$this->load->view('admin/layout',$data);
	}

	function sales_report_excel(){
		$get = $this->input->get();
		$member = $this->M_report->getMemberActive();
		$event  = $this->M_report->getEventActive();

		$dataGenerate = array();
		$dateReport = "";
		$excelHref = "";
		if (!empty($get)) {
			$dataGenerate = $this->generate_sales_report($get);
			$dateReport = $get['daterange'];
			$excelHref = base_url('cms/report/sales_report_excel?'.http_build_query($get));
		}

		$data['excelHref']	= $excelHref;
		$data['dateReport']	= $dateReport;
		$data['reportData'] = $dataGenerate;
		$data['member']     = $member;
		$data['event']      = $event;

		$this->load->view('admin/report/report_sales_excel',$data);
	}

	function generate_sales_report($get){
		$eplDate = explode(" - ", $get['daterange']);
		
		$date_start = date("Y-m-d", strtotime($eplDate[0]));
		$date_end   = date("Y-m-d", strtotime($eplDate[1]));

		$dataReport = $this->M_report->generate_sales_report($date_start, $date_end, $get['member'], $get['event']);
		return $dataReport;
	}
	/*=========================================== END SALES REPORT ===========================================*/

	/*=========================================== START MEMBER REPORT ===========================================*/
	function member_report(){
		$data['web_title']  = "Member Report";
		$data['back_link']  = base_url('cms/report/');
		$data['content']    = "admin/report/report_member";
		$this->load->view('admin/layout',$data);
	}

	function get_list_member(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_member->get_list_member_report ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_member->get_list_member_report ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_member->get_list_member_report ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_member->get_list_member_report ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->member_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '<a href="'.base_url("cms/report/mreport/".$encrypt_id).'" class="btn btn-primary btn-sm">Select</a>';
			/*========================================= END BUTTON STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->member_name;
			$rowData[] = $value->member_phone;
			$rowData[] = $value->member_email;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	function mreport($param){
		$get = $this->input->get();

		$member_id  = encrypt_decrypt("decrypt",$param);
		$dataMember = $this->global->getDetailMember($member_id);

		/*==== get data report ====*/
		$dataGenerate = array();
		$excelHref = "";
		if (!empty($get)) {

			$dataGenerate = $this->M_report->generate_member_report($get['year'],$member_id);
			$excelHref = base_url('cms/report/member_report_excel/'.$param."?".http_build_query($get));
		}

		$data['excelHref'] = $excelHref;
		$data['member_id'] = $param;

		$data['dataGenerate'] = $dataGenerate;
		$data['dataMember']   = $dataMember;
		$data['web_title']    = "Member Report";
		$data['back_link']    = base_url('cms/report/member_report');
		$data['content']      = "admin/report/report_member_generate";
		$this->load->view('admin/layout',$data);
	}

	function chinoutTrack(){
		$get = $this->input->get();

		$member_id = encrypt_decrypt("decrypt",$get['param1']);
		$event_id  = encrypt_decrypt("decrypt",$get['param2']);

		$listData = $this->M_report->get_check_inoutData($event_id, $member_id);
		
		$table="";
		foreach ($listData as $key => $value) {
			$type="";
			if ($value->type_att == 1) {
				$type = "IN";
			}else{
				$type = "OUT";
			}
			

			$table.="
			<tr class='trow'>
			<td colspan='2'>".$value->schedule_name."</td>
			<td>".$type."</td>
			<td>".date("d M Y H:i:s",strtotime($value->created_date))."</td>
			</tr>";
			
		}
		$emptyData = "<td class='trow' colspan='4' style='text-aligh:center;'>No Activity</td>";

		if ($table <> "") {
			die($table);
		}else{
			die($emptyData);
		}
	}
	function chinoutTrack2($param1,$param2){
		$member_id = encrypt_decrypt("decrypt",$param1);
		$event_id  = encrypt_decrypt("decrypt",$param2);

		$listData = $this->M_report->get_check_inoutData($event_id, $member_id);
		
		$table="";
		foreach ($listData as $key => $value) {
			$type="";
			if ($value->type_att == 1) {
				$type = "IN";
			}else{
				$type = "OUT";
			}
			

			$table.="
			<tr class='trow'>
			<td colspan='2'>".$value->schedule_name."</td>
			<td>".$type."</td>
			<td>".date("d M Y H:i:s",strtotime($value->created_date))."</td>
			</tr>";
			
		}
		$emptyData = "<td class='trow' colspan='4' style='text-aligh:center;'>No Activity</td>";

		if ($table <> "") {
			return $table;
		}else{
			return $emptyData;
		}
	}

	function member_report_excel($param){
		$get = $this->input->get();

		$member_id  = encrypt_decrypt("decrypt",$param);
		$dataMember = $this->global->getDetailMember($member_id);

		/*==== get data report ====*/
		$dataGenerate = array();
		if (!empty($get)) {

			$dataGenerate = $this->M_report->generate_member_report($get['year'],$member_id);
			$excelHref = base_url('cms/report/member_report_excel/'.$param."?".http_build_query($get));
		}

		$inoutArray = array();
		foreach ($dataGenerate as $key => $value) {
			$inoutArray[$value->event_id] = $this->chinoutTrack2($param, encrypt_decrypt("encrypt",$value->event_id));
		}
		

		$data['inoutArray']	= $inoutArray;
		$data['excelHref'] = $excelHref;
		$data['member_id'] = $param;

		$data['dataGenerate'] = $dataGenerate;
		$data['dataMember']   = $dataMember;
		$data['web_title']    = "Member Report";
		$this->load->view('admin/report/report_member_excel',$data);
	}
	/*=========================================== END MEMBER REPORT ===========================================*/

	/*=========================================== START EVENT REPORT ===========================================*/
	function event_report(){
		$data['web_title']  = "Event Report";
		$data['back_link']  = base_url('cms/report/');
		$data['content']    = "admin/report/report_event";
		$this->load->view('admin/layout',$data);
	}

	function get_list_event(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_event->get_list_event_report ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_event->get_list_event_report ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_event->get_list_event_report ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_event->get_list_event_report ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->event_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<a href="'.base_url("cms/report/event_report_detail/".$encrypt_id).'" class="btn btn-primary btn-sm">View Report</a>
				';
			/*========================================= END BUTTON STUFF =========================================*/

			/*========================================= BEGIN DATE STUFF =========================================*/
			$date_start = date("m/d/Y h:i A", strtotime($value->event_start_date));
			$date_end   = date("m/d/Y h:i A", strtotime($value->event_end_date));

			$open_date_start = date("m/d/Y h:i A", strtotime($value->event_open_registration_date));
			$close_date_end  = date("m/d/Y h:i A", strtotime($value->event_close_registration_date));
			/*========================================= END DATE STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->event_name;
			$rowData[] = $value->place_name;
			$rowData[] = '<span class="badge badge-bdr badge-success">'.$date_start.'</span>';
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	function event_report_detail($param){
		$event_id = encrypt_decrypt("decrypt",$param);

		$eventShortDetail = $this->global->getShortEventDetail($event_id);
		$totalEventData   = $this->M_report->totalEventReport($event_id);
		$er_listData      = $this->M_report->er_listPP($event_id);
		
		/*======= START TOTAL BY SCHEDULE ========*/

		$schHtml[] = "{y: ".$totalEventData['total_participant'].", name: 'Register', key: '".base_url('cms/report/event_bar_detail?data=reg&param='.$param)."', color: '#3498db'}";
		$schHtml[] = "{y: ".$totalEventData['total_attendance'].", name: 'Attendance', key: '".base_url('cms/report/event_bar_detail?data=att&param='.$param)."', color: '#34495e'}";
		$getAllSchedule = $this->global->getAllScheduleByEvent_short($event_id);
		$ttlPerSc = array();
		foreach ($getAllSchedule as $skey => $svalue) {

			$totalPerSchedule = 0;
			$totalPerSchedule = $this->M_report->er_totalParticipantBySchedule($svalue->event_schedule_id);

			$schHtml[] = "{y:".$totalPerSchedule.", name:'".str_replace('\'', '\\\'', $svalue->schedule_name)."', key:'".base_url('cms/report/event_bar_detail?data=sch&param='.$param.'&param2='.encrypt_decrypt('encrypt',$svalue->event_schedule_id))."', color: '#2ecc71'}";

			$ttlPerSc[$skey] = array(
				"schedule" => $svalue->schedule_name,
				"total"    => $totalPerSchedule,
				"id"       => $svalue->event_schedule_id
			);
		}
		//debugCode($schHtml);
		
		/*======= END TOTAL BY SCHEDULE ========*/

		/*======= START ATTENDANCE BY SCHEDULE =======*/
		$listAttBySch = array();
		/*foreach ($getAllSchedule as $pkey => $pvalue) {
			$listArray      = $this->M_report->er_getLIstAttendanceBySchedule($pvalue->event_schedule_id);
			$listAttendance = array();

			if (!empty($listArray)) {
				foreach ($listArray as $lkey => $lvalue) {
					$listAttendance[] = $lvalue->attendance_name;
				}
			}
			$listAttBySch[$pkey]['schedule_name']	= $pvalue->schedule_name;
			$listAttBySch[$pkey]['list_Attendance'] = $listAttendance;
		}*/


		/*======= END ATTENDANCE BY SCHEDULE =======*/
		//debugCode($listAttBySch);
		$data['id']             = $param;
		$data['dataChart']      = implode(", ", $schHtml);
		$data['listAttBySch']   = $listAttBySch;
		$data['detailEvent']    = $eventShortDetail;
		$data['ttlPerSc']       = $ttlPerSc;
		$data['pp_listData']    = $er_listData;
		$data['totalEventData'] = $totalEventData;

		$data['web_title']  = "Event Report";
		$data['back_link']  = base_url('cms/report/event_report');
		$data['content']    = "admin/report/report_event_detail";
		$this->load->view('admin/layout',$data);
	}

	function event_report_schedule(){
		$get          = $this->input->get();
		$event_id     = encrypt_decrypt("decrypt",$get['param']);
		$schedule_id  = encrypt_decrypt("decrypt",$get['param2']);
		$scheduleData = $this->global->getScheduleDetail($schedule_id);
		$listData     = $this->M_report->getAttendanceDataBYSchedule($schedule_id);
		
		$data['scheduleData'] = $scheduleData;
		$data['listData']     = $listData;
		$this->load->view('admin/report/report_event_detail_schedule_excel',$data);
	}

	function event_report_participant_excel($id){
		$event_id         = encrypt_decrypt("decrypt",$id);
		$listData         = $this->M_report->getAllListParticipantBYEvent($event_id);
		$eventShortDetail = $this->global->getShortEventDetail($event_id);

		$data['eventData'] = $eventShortDetail;
		$data['listData']  = $listData;
		$this->load->view('admin/report/report_event_detail_participant_excel',$data);
	}

	function event_report_attendance_excel($id){
		$event_id         = encrypt_decrypt("decrypt",$id);
		$listData         = $this->M_report->getAllListAttendanceBYEvent($event_id);
		$eventShortDetail = $this->global->getShortEventDetail($event_id);

		$data['eventData'] = $eventShortDetail;
		$data['listData']  = $listData;
		$this->load->view('admin/report/report_event_detail_attendance_excel',$data);
	}

	function event_bar_detail(){
		$get = $this->input->get();

		$event_id         = encrypt_decrypt("decrypt",$get['param']);
		$eventShortDetail = $this->global->getShortEventDetail($event_id);
		if ($get['data'] == "att") {
			$data['headCard'] = "Data Attendance - ".$eventShortDetail->event_name;
			$data['dataType'] = 1;
			$data['param']    = $get['param'];
			
			$data_result = $this->M_report->getListDataAttendance($event_id);
		}elseif($get['data'] == "reg"){

			$data['headCard'] = "Data Register - ".$eventShortDetail->event_name;
			$data['dataType'] = 2;
			$data['param']    = $get['param'];
		}elseif($get['data'] == "sch"){
			$schedule_id  = encrypt_decrypt("decrypt",$get['param2']);
			$scheduleData = $this->global->getScheduleDetail($schedule_id);
			
			$data['headCard'] = "Data ".$scheduleData->schedule_name." - ".$eventShortDetail->event_name;
			$data['dataType'] = 3;
			$data['param']    = $get['param'];
			$data['param2']    = $get['param2'];
		}else{
			$data['headCard'] = "";
			$data['dataType'] = 0;
			$data['param']    = $get['param'];
		}

		$data['web_title']  = "Event Report Detail";
		$data['back_link']  = base_url('cms/report/');
		$data['content']    = "admin/report/report_event_detail_bar";
		$this->load->view('admin/layout',$data);
	}

	function event_report_inout(){
		$get = $this->input->get();

		$event_id    = encrypt_decrypt("decrypt",$get['param']);
		$schedule_id = encrypt_decrypt("decrypt",$get['param2']);

		$listAttendance = $this->M_report->getAllListAttendanceBYEvent($event_id);
		$schedule       = $this->global->getScheduleDetail($schedule_id);
		
		$data['id']         = $schedule_id;
		$data['schedule']   = $schedule;
		$data['attendance'] = $listAttendance;
		$data['web_title']  = "Select Attendance for Download In/Out";
		$data['content']    = "admin/report/report_event_detail_inout_select";
		$this->load->view('admin/layout',$data);
	}

	function event_report_inout_excel($schedule_id,$attendance){
		$schedule   = $this->global->getScheduleDetail($schedule_id);
		$dataResult = $this->M_report->getListInoutExcel($schedule_id,$attendance);
		
		$data['schedule']	= $schedule;
		$data['dataResult']	= $dataResult;
		$this->load->view("admin/report/report_event_detail_inout_excel",$data);
	}

	public function get_list_report_event_attendance(){
		$requestParam             = $_REQUEST;
		$requestParam['event_id'] = encrypt_decrypt("decrypt",$requestParam['param']);
		
		$getData 				= $this->M_report->get_list_report_event_attendance ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_report->get_list_report_event_attendance ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_report->get_list_report_event_attendance ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_report->get_list_report_event_attendance ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$button = "";

			$rowData[] = $value->attendance_name;
			$rowData[] = date("d M Y H:i",strtotime($value->updated_date));
			$rowData[] = $value->region_name;
			$rowData[] = $value->dpw_name;
			$rowData[] = $value->satelit_church_name;
			$rowData[] = $value->church_role_name;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_report_event_register(){
		$requestParam             = $_REQUEST;
		$requestParam['event_id'] = encrypt_decrypt("decrypt",$requestParam['param']);
		
		$getData 				= $this->M_report->get_list_report_event_register ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_report->get_list_report_event_register ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_report->get_list_report_event_register ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_report->get_list_report_event_register ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$button = "";

			$rowData[] = $value->attendance_name;
			$rowData[] = date("d M Y H:i",strtotime($value->order_time));
			$rowData[] = $value->region_name;
			$rowData[] = $value->dpw_name;
			$rowData[] = $value->satelit_church_name;
			$rowData[] = $value->church_role_name;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_report_event_schedule(){
		$requestParam             = $_REQUEST;
		$requestParam['event_id']    = encrypt_decrypt("decrypt",$requestParam['param']);
		$requestParam['schedule_id'] = encrypt_decrypt("decrypt",$requestParam['param2']);
		
		$getData 				= $this->M_report->get_list_report_event_schedule ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_report->get_list_report_event_schedule ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_report->get_list_report_event_schedule ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_report->get_list_report_event_schedule ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$button = "";

			$rowData[] = $value->attendance_name;
			$rowData[] = date("d M Y H:i",strtotime($value->created_date));
			$rowData[] = $value->region_name;
			$rowData[] = $value->dpw_name;
			$rowData[] = $value->satelit_church_name;
			$rowData[] = $value->church_role_name;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}


	/*=========================================== END EVENT REPORT ===========================================*/
}