<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekappelanggaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_rekappelanggaran");
		$this->load->model("M_pelanggaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] 	= "Rekap Pelanggaran";
		$data['content']   	= "admin/rekap_pelanggaran/index";
		$this->load->view('admin/layout',$data);
	}

	function detailpelanggaran($id){
		// $dataPelanggaran = $this->M_rekappelanggaran->listSantriMelanggar("siswaId", $id);

		// $data['listData']	= $dataPelanggaran;
		$data['back_link'] 	= base_url('cms/rekappelanggaran');
		$data['web_title'] 	= "Detail Pelanggaran";
		$data['content']   	= "admin/rekap_pelanggaran/detail";
		$this->load->view('admin/layout',$data);
	}

	// public function printpelanggaran($siswa_id){
	// 	$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);
		
	// 	$dataPelanggaran 			= $this->M_rekappelanggaran->getListPelanggaran($siswa_id);
	// 	$data['listPelanggaran']	= $dataPelanggaran;

	// 	$filename = date("ymdHis")."_pelanggaran_siswa.pdf";
	// 	$view = $this->load->view('admin/rekap_pelanggaran/laporan_pelanggaran', $data, true);
	// 	$mpdf->WriteHTML($view);
	// 	$mpdf->Output($filename, "D");
	// 	// $mpdf->Output(FCPATH.'/upload/laporan/kwitansi/kwitansi_pembayaran_siswa/'.$filename, "F"); 
	// }

	public function printpelanggaran($siswa_id, $tgl_awal, $tgl_akhir){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);
		
		$dataPelanggaran 			= $this->M_rekappelanggaran->getListPelanggaranRange($siswa_id, $tgl_awal, $tgl_akhir);
		$data['listPelanggaran']	= $dataPelanggaran;

		$filename = date("ymdHis")."_pelanggaran_siswa.pdf";
		$view = $this->load->view('admin/rekap_pelanggaran/laporan_pelanggaran', $data, true);
		$mpdf->WriteHTML($view);
		// $mpdf->Output($filename, "D");
		$mpdf->Output(FCPATH.'/upload/laporan/pelanggaran/'.$filename, "F"); 
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_siswa_melanggar(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_rekappelanggaran->get_pelanggan_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_rekappelanggaran->get_pelanggan_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_rekappelanggaran->get_pelanggan_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_rekappelanggaran->get_pelanggan_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
				<a href="'.base_url('cms/rekappelanggaran/detailpelanggaran/'.$value->siswaId).'" class="btn btn-info btn-sm" title="Detial Pelanggaran">Detial <i class="fa fa-eye"></i></a>
				';
			/*========================================= END BUTTON STUFF =========================================*/
			$rowData[] = $no++;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_name;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}

	public function get_list_pelanggaran($id){
		$requestParam 			= $_REQUEST;
		// debugCode($requestParam);

		$getData 				= $this->M_rekappelanggaran->get_list_pelanggaran_siswa ( $requestParam, $id, 'nofilter' );
		$totalAllData 			= $this->M_rekappelanggaran->get_list_pelanggaran_siswa ( $requestParam, $id, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_rekappelanggaran->get_list_pelanggaran_siswa ( $requestParam, $id, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_rekappelanggaran->get_list_pelanggaran_siswa ( $requestParam, $id);
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();

			$hukuman = array();
			$hukuman = json_decode($value->hukuman,true);

			$detailhukuman = "";

			foreach ($hukuman['hukuman'] as $skey => $svalue) {
				// debugCode($value);
				$detailhukuman.="<b>Hukuman</b> : ".$svalue." (".$hukuman['status'][$skey].")<br/>";
			}

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->pelanggaran_name;
			$rowData[] = $detailhukuman;
			$rowData[] = $value->created_date;
			// $rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/

}