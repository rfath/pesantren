<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenaikanKelas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_penaikankelas");
		$this->load->model("M_tahunpelajaran");
		$this->load->model("M_kelas");
		$this->load->model("M_penaikankelas");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataTP = $this->M_tahunpelajaran->getTahunPelajaran();
		$dataKelas = $this->M_kelas->getKelas();

		$data['kelas']		= $dataKelas;
		$data['tp']			= $dataTP;
		$data['web_title'] 	= "Kelas";
		$data['content']   	= "admin/penaikan_kelas/index";
		$this->load->view('admin/layout',$data);
	}

	// function doAdd(){
	// 	$post     = $this->input->post();
		
	// 	// Update Status Siswa
	// 	$updateArray = array(
	// 		// "siswa_id"  			=> encrypt_decrypt("decrypt", $post['siswa_id']),
	// 		"kelas_id"  			=> $post['kelas'],
	// 		// "kelas_rombel_id"  		=> $post['rombel'],
	// 		"tahun_pelajaran_id"  	=> $post['tp'],
	// 		// "status" 				=> "1",
	// 		// "isDeleted" 			=> "0",
	// 		"updated_date"  		=> date('Y-m-d H:i:s'),
	// 		"updated_by"    		=> $this->sessionData['user_id']
	// 	);
	// 	$this->db->update("kelas", $updateArray, array("kelas_id" => $id));

	// 	// Insert new Status Siswa
	// 	$updateArray = array(
	// 		// "siswa_id"  			=> encrypt_decrypt("decrypt", $post['siswa_id']),
	// 		"kelas_id"  			=> $post['kelas'],
	// 		// "kelas_rombel_id"  		=> $post['rombel'],
	// 		"tahun_pelajaran_id"  	=> $post['tp'],
	// 		// "status" 				=> "1",
	// 		// "isDeleted" 			=> "0",
	// 		"updated_date"  		=> date('Y-m-d H:i:s'),
	// 		"updated_by"    		=> $this->sessionData['user_id']
	// 	);
	// 	$update = $this->db->update("kelas", $updateArray, array("kelas_id" => $id));

	// 	if ($update) {
	// 		$this->session->set_flashdata('is_success', 'Yes');
	// 		redirect("cms/kelas");
	// 	}else{
	// 		$this->session->set_flashdata('is_success', 'No');
	// 		redirect("cms/kelas/edit/".$id);
	// 	}
	// }

	// function add(){
	// 	$data['back_link'] = base_url('cms/kelas');
	// 	$data['web_title'] = "Posting Naik Kelas";
	// 	$data['content']   = "admin/kelas/add";
	// 	$this->load->view('admin/layout',$data);
	// }

	/*function doAdd(){
		$post = $this->input->post();
		debugCode($post);

		for ($i=0; $i < count($post['siswa_kelas_id']); $i++) { 
			$siswa_id = $post['siswa_kelas_id'][$i];
			
			$data_siswa_kelas = $this->M_penaikankelas->getSiswaKelasDetail('siswa_kelas_id', $siswa_id);
			// debugCode($data_siswa_kelas);
			if (!empty($post['naik_kelas'][$siswa_id])) {
				// kalo di check 
				$updateArray = array(
					"kelas_id"  			=> $post['kelas_siswa'],
					"tahun_pelajaran_id" 	=> $post['tp_siswa'],
					"updated_date"  		=> date('Y-m-d H:i:s'),
					"updated_by"    		=> $this->sessionData['user_id']

				);

				$insertHistory = array(
					"siswa_id"  			=> $siswa_id,
					"kelas_id" 				=> $post['kelas_siswa'],
					"kelas_rombel_id" 		=> $data_siswa_kelas->kelas_rombel_id,
					"tahun_pelajaran_id"	=> $post['tp_siswa'],
					"status" 				=> "1",
					"isDeleted" 			=> "0",
					"created_date"  		=> date('Y-m-d H:i:s'),
					"created_by"    		=> $this->sessionData['user_id']

				);
			}else{
				$updateArray = array(
					// "kelas_id"  			=> $post['kelas_siswa'],
					"tahun_pelajaran_id" 	=> $post['tp_siswa'],
					"updated_date"  		=> date('Y-m-d H:i:s'),
					"updated_by"    		=> $this->sessionData['user_id']

				);

				$insertHistory = array(
					"siswa_id"  			=> $siswa_id,
					"kelas_id" 				=> $data_siswa_kelas->kelas_id,
					"kelas_rombel_id" 		=> $data_siswa_kelas->kelas_rombel_id,
					"tahun_pelajaran_id"	=> $post['tp_siswa'],
					"status" 				=> "1",
					"isDeleted" 			=> "0",
					"created_date"  		=> date('Y-m-d H:i:s'),
					"created_by"    		=> $this->sessionData['user_id']

				);
			}

			$update = $this->db->update("siswa_kelas", $updateArray, array("siswa_kelas_id" => $siswa_id));
			if ($data_siswa_kelas->kelas_id == 3) {
				// Update is graduate siswa
				$this->db->update("siswa", 
					[
						"is_graduate"					=> "1",
						"garduate_tahun_pelajaran_id"	=> $post['tp_siswa'],
						"updated_date"  				=> date('Y-m-d H:i:s'),
						"updated_by"    				=> $this->sessionData['user_id']
					], 
					array("siswa" => $data_siswa_kelas->siswa_id));

				// Update status di history bahwa siswa sudah tidak aktif
				$insert = $this->db->update("siswa_kelas", 
					[
						"status"				=> "0",
						"created_date"  		=> date('Y-m-d H:i:s'),
						"created_by"    		=> $this->sessionData['user_id']
					], 
					array("siswa_id" => $siswa_id));
			}else{
				$insert	= $this->db->insert("siswa_kelas_history", $insertHistory);
			}
		}

		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/penaikankelas/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/penaikankelas/");
		}
	}*/

	function doAdd(){
		$post = $this->input->post();
		if ($post['type'] == "1") {
			$this->doPenaikan($post);
		}else{
			$this->doKelulusan($post);
		}
	}

	function doPenaikan($post){
		foreach ($post['naik_kelas'] as $key => $value) {
			$data_siswa_kelas = $this->M_penaikankelas->getSiswaKelasDetail('siswa_kelas_id', $key);
			$updateArray = array(
				"kelas_id"           => $post['kelas_siswa'],
				"tahun_pelajaran_id" => $post['tp_siswa'],
				"updated_date"       => date("Y-m-d H:i:s"),
				"updated_by"         => $this->sessionData['user_id']
			);
			$this->db->update("siswa_kelas", $updateArray, array("siswa_Id" => $data_siswa_kelas->siswa_id));
			$checkHistory = $this->M_penaikankelas->checkHistory($data_siswa_kelas->siswa_id, $post['kelas_siswa'], $post['tp_siswa']);
			if (empty($checkHistory)) {
				
				$insertArray = array(
					"siswa_id"           => $data_siswa_kelas->siswa_id,
					"kelas_id"           => $post['kelas_siswa'],
					"kelas_rombel_id"    => $data_siswa_kelas->kelas_rombel_id,
					"tahun_pelajaran_id" => $post["tp_siswa"],
					"created_date"       => date("Y-m-d H:i:s"),
					"created_by"         => $this->sessionData['user_id']
				);
				$doaction = $this->db->insert("siswa_kelas_history",$insertArray);
			}else{
				$updateArray = array(
					"updated_date"	=> date("Y-m-d H:i:s"),
					"updated_by"	=> $this->sessionData['user_id']
				);
				$where = array(
					"siswa_id"           => $data_siswa_kelas->siswa_id, 
					"kelas_id"           => $post['kelas_siswa'],
					"tahun_pelajaran_id" => $post['tp_siswa']
				);
				$doaction = $this->db->update("siswa_kelas_history",$updateArray, $where);
			}
		}
		if ($doaction) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/penaikankelas/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/penaikankelas/");
		}
	}

	function doKelulusan($post){
		foreach ($post['naik_kelas'] as $key => $value) {
			$data_siswa_kelas = $this->M_penaikankelas->getSiswaKelasDetail('siswa_kelas_id', $key);
			$insertAlumni = array(
				"siswa_id"           => $data_siswa_kelas->siswa_id,
				"tahun_pelajaran_id" => $post['tp_siswa'],
				"created_date"       => date("Y-m-d H:i:s"),
				"created_by"         => $this->sessionData['user_id']
			);
			$insert = $this->db->insert("alumni",$insertAlumni);
			if ($insert) {
				$updateSiswa = array(
					"is_graduate"                 => 1,
					"year_graduate"               => date("Y"),
					"date_graduate"               => date("Y-m-d H:i:s"),
					"graduate_tahun_pelajaran_id" => $post['tp_siswa']
				);
				$upate   = $this->db->update("siswa",$updateSiswa, array("siswa_id" => $data_siswa_kelas->siswa_id));
				$update2 = $this->db->update("siswa_kelas",array("status" => 2, "updated_date" => date("Y-m-d H:i:s"), "updated_by" => $this->sessionData['user_id']),array("siswa_kelas_id" => $key));
			}
		}
		$this->session->set_flashdata('is_success', 'Yes');
		redirect("cms/penaikankelas/");
	}

	public function get_list_siswa_rombel(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_penaikankelas->get_siswa_kelas_rombel ( $requestParam, 'nofilter');
		$totalAllData 			= $this->M_penaikankelas->get_siswa_kelas_rombel ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_penaikankelas->get_siswa_kelas_rombel ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_penaikankelas->get_siswa_kelas_rombel ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
					<button type="button" onClick="cardisantri(\''.$value->tahun_pelajaran_id.'\', \''.$value->kelas_rombel_id.'\', \''.$value->kelas_id.'\')" class="btn btn-info btn-sm" style="margin-top: 5px;"><i class="fa fa-users"></i> Cari Santri</button>
					<input type="hidden" value="'.$value->kelas_rombel_id.'">
					<input type="hidden" value="'.$value->tahun_pelajaran_id.'">
				';
			/*========================================= END BUTTON STUFF =========================================*/
			// Data TP
			$dataTP = $this->M_tahunpelajaran->getTahunPelajaraneDetail("tahun_pelajaran_id", $value->tahun_pelajaran_id);
			// End
			
			$rowData[] = $no++;
			$rowData[] = $dataTP->tahun_pelajaran_name;
			$rowData[] = $value->kelas_name;
			$rowData[] = $value->kelas_rombel_name;
			// $rowData[] = $check;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_siswa_kelas_detail(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_penaikankelas->get_siswa_kelas_detail ( $requestParam, 'nofilter');
		$totalAllData 			= $this->M_penaikankelas->get_siswa_kelas_detail ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_penaikankelas->get_siswa_kelas_detail ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_penaikankelas->get_siswa_kelas_detail ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			// debugCode($value);
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$check = "";
			if ($value->is_graduate == 1) {
				$check.="<b style='color:#2ecc71;'>LULUS</b>";
			}else{
				$check .='
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							<input type="hidden" name="siswa_kelas_id[]" value="'.$value->siswa_kelas_id.'">
							<input class="form-check-input" type="checkbox" name="naik_kelas['.$value->siswa_kelas_id.']" id="naik_kelas" value="1" checked>
						</label>
					</div>
				';
			}
			
			/*========================================= END BUTTON STUFF =========================================*/
			// Data TP
			$dataTP = $this->M_tahunpelajaran->getTahunPelajaraneDetail("tahun_pelajaran_id", $value->tahun_pelajaran_id);
			// End
			
			$rowData[] = $no++;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->kelas_name;
			$rowData[] = $value->kelas_rombel_name;
			$rowData[] = $dataTP->tahun_pelajaran_name;
			$rowData[] = $check;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
	/*==================================================== END DATA FOR DATATABLE ====================================================*/

}