<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Place extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_place");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Place";
		$data['content']   = "admin/place/listplace";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add place";
		$data['content']   = "admin/place/addplace";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		$insertParam = array(
			"place_name"   => $post['place_name'],
			"is_deleted"   => 0,
			"is_active"    => 1,
			"created_date" => date("Y-m-d H:i:s"),
			"creted_by"    => $this->sessionData['user_id'],
			"longlat"      => $post['lat'].",".$post['lng']
		);
		$insert = $this->db->insert("place", $insertParam);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/place");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/place/add");
		}
	}

	function edit($param){
		$place_id    = encrypt_decrypt("decrypt",$param);
		$detailPlace = $this->M_place->getDetailPlace($place_id);

		$data['id']	= "";
		if (!empty($detailPlace)) {
			$data['id']	= $param;
			$latlong = explode(",",$detailPlace->longlat);
			$data['lat']	= $latlong[0];
			$data['long']	= $latlong[1];
		}else{
			/*BEGIN DEFAULT LAT LONG*/
			$data['lat']	= -6.17511;
			$data['long']	= 106.86503949999997;
			/*END DEFAULT LAT LONG*/
		}

		$data['detailPlace'] = $detailPlace;
		$data['web_title']   = "Edit place";
		$data['content']     = "admin/place/editplace";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post = $this->input->post();

		$place_id    = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"place_name"   => $post['place_name'],
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
			"longlat"      => $post['lat'].",".$post['lng']
		);
		$update = $this->db->update("place",$updateArray, array("place_id"=>$place_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/place");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/place/edit".$post['param']);
		}
	}

	function doDelete($param){
		$place_id    = encrypt_decrypt("decrypt",$param);
		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("place", $updateArray, array("place_id" => $place_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/place");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/place");
		}
	}

	public function get_list_place(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_place->get_list_place ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_place->get_list_place ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_place->get_list_place ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_place->get_list_place ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$button = "";	
			$button .= '
			<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/place/doDelete/'.encrypt_decrypt('encrypt',$value->place_id)).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/place/edit/'.encrypt_decrypt('encrypt',$value->place_id)).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			

			$rowData[] = $no++;
			$rowData[] = $value->place_name;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}