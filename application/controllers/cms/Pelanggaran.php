<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_pelanggaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Prestasi";
		$data['content']   = "admin/pelanggaran/listpelanggaran";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add Prestasi";
		$data['content']   = "admin/pelanggaran/addpelanggaran";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"pelanggaran_name" => $post['pelanggaran'],
			"created_date"     => date('Y-m-d H:i:s'),
			"created_by"       => $this->sessionData['user_id'],
			"status"           => 1,
			"isDeleted"        => 0,
		);
		$insert = $this->db->insert("pelanggaran",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pelanggaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pelanggaran/add");
		}
		
		
	}

	function edit($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$detailUser = $this->M_pelanggaran->getDetailPelanggaran($user_id);
		//$data['access_group'] = $this->M_user->getAccessGroup();
		$data['id']	= "";
		if (!empty($detailUser)) {
			$data['id']	= $param;
		}

		$data["idnya"]      = $param;
		$data['detailUser'] = $detailUser;
		$data['web_title']  = "Edit Pelanggaran";
		$data['content']    = "admin/pelanggaran/editpelanggaran";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post    = $this->input->post();
		$user_id = encrypt_decrypt("decrypt", $id);
		$updateArray = array(
		"pelanggaran_name"    	=> $post['pelanggaran'],
		"updated_date"     	=> date('Y-m-d H:i:s'),
		"updated_by"       	=> $this->sessionData['user_id'],
		);

		$update = $this->db->update("pelanggaran", $updateArray, array("pelanggaran_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pelanggaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pelanggaran/edit/".$id, 'refresh');
		}
	}

	function doDelete($param){
		$user_id = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"isDeleted" => 1
		);
		$update = $this->db->update("pelanggaran",$updateArray,array("pelanggaran_id" => $user_id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pelanggaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pelanggaran");
		}
	}

	public function get_list_pelanggaran(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_pelanggaran->get_list_pelanggaran ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_pelanggaran->get_list_pelanggaran ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_pelanggaran->get_list_pelanggaran ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_pelanggaran->get_list_pelanggaran ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach ($getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt', $value->pelanggaran_id);

			$button = "";
			$button .= '
			<button class="btn btn-danger btn-sm" onClick="is_delete(\''.base_url('cms/pelanggaran/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
			$button .= '
			<a href="'.base_url('cms/pelanggaran/edit/'.$encrypt_id).'" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
			
			if ($value->status ==1){
				$status = "aktif";
			} else {
				$status = "tidak aktif";
			}
			$rowData[] = $no++;
			$rowData[] = $value->pelanggaran_name;
			// $rowData[] = $value->created_date;
			// $rowData[] = $this->sessionData['username'];
			// $rowData[] = $value->updated_date;
			// $rowData[] = $value->updated_by;
			// $rowData[] = $status;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}