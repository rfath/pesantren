<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekapKeamanan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_rekapkeamanan");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] 	= "Rekap Keamanan";
		$data['content']   	= "admin/rekap_keamanan/index";
		$this->load->view('admin/layout',$data);
	}

	// function printlaporanperizinan(){
		// $mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);

		// $dataKas 				= $this->M_bendahara->getKasDetail("kas_id", $id);
		// $data["dataKas"]		= $dataKas;


		// $filename = date("ymdHis")."_rekap_keamanan.pdf";
		// $view = $this->load->view('admin/pembayaran_santri/printkwitansi', $data, true);
		// $this->load->view('admin/pembayaran_santri/printkwitansi');

		// $mpdf->WriteHTML($view);
		// $mpdf->Output($filename, "D");
		// $mpdf->Output(FCPATH.'/upload/laporan/kwitansi/kwitansi_pembayaran_siswa/'.$filename, "F"); 
	// }

	public function printlaporanperizinan($status, $tgl_awal, $tgl_akhir){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);

		if ($status == "3") {
			$dataPerizinan 		= $this->M_rekapkeamanan->getListPerizinan($tgl_awal, $tgl_akhir);
		}else{
			if ($status == "0") {
				$status = NULL;
				$field_condition 	=  "return_date";
			}else{
				$status = '';
				$field_condition 	=  "return_date <>";
			}
			// debugCode($field_condition);
			$dataPerizinan 		= $this->M_rekapkeamanan->getListtPerizinanRange($field_condition, $status, $tgl_awal, $tgl_akhir);
		}
		
		$data['listData']	= $dataPerizinan;

		$filename = date("ymdHis")."_laporan_perizinan.pdf";

		$view = $this->load->view('admin/rekap_keamanan/laporan_keamanan', $data, true);
		$mpdf->WriteHTML($view);
		// $mpdf->Output($filename, "D");
		$mpdf->Output(FCPATH.'/upload/laporan/keamanan/'.$filename, "F"); 
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	function get_list_izin(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_rekapkeamanan->get_list_izinkeluar ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_rekapkeamanan->get_list_izinkeluar ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_rekapkeamanan->get_list_izinkeluar ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_rekapkeamanan->get_list_izinkeluar ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData    = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->izin_keluar_id);
			$status     = "";
			$status     = izin_status_html($value->return_date);

			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			if ($value->return_date > $value->time_limit){
				// $button.="Kembali Pada: <br><span class='badge badge-bdr badge-success'>".date("d M Y h:i A",strtotime($value->return_date))."</span>";
				$button.="<span class='badge badge-bdr badge-danger'>Telat</span>";
			}elseif(empty($value->return_date)){

			}else{
				$button.="<span class='badge badge-bdr badge-success'>Tepat</span>";
			}
			
			/*========================================= END BUTTON STUFF =========================================*/

			

			$rowData[]  = $no++;
			$rowData[]  = $value->siswa_name." / NIK: ".$value->siswa_nomor_induk;
			$rowData[]  = $value->keperluan;
			$rowData[]  = date("d M Y h:i A",strtotime($value->created_date));
			$rowData[]  = date("d M Y h:i A",strtotime($value->time_limit));
			$rowData[]  = $status;
			$rowData[]  = $button;
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}

}