<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PembayaranSiswa extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_pembayaransiswa");
		$this->load->model("M_pembayaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataPembayaranSiswa = $this->M_pembayaransiswa->getPembayaranSiswa();
		// debugCode($dataPembayaranSiswa);
		$data['listData'] 	= $dataPembayaranSiswa;
		$data['web_title'] 	= "Pembayaran Siswa";
		$data['content']   	= "admin/pembayaran_siswa/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$dataPembayaran  = $this->M_pembayaran->getPembayaran();
		$kelas           = $this->global->getKelas();
		$tahun_pelajaran = $this->global->getTahunPembelajaran();

		$data['kelas']           = $kelas;
		$data['dataPembayaran']  = $dataPembayaran;
		$data['tahun_pelajaran'] = $tahun_pelajaran;
		$data['back_link']       = base_url('cms/pembayaransiswa');
		$data['web_title']       = "Add Pembayaran Siswa";
		$data['content']         = "admin/pembayaran_siswa/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		if ($post['tipe'] == 1) {
			$this->pembayaranPerKelas($post);
		}else{
			$this->pembayaranPerSantri($post);
		}
	}

	function pembayaranPerKelas($post){
		$dataSiswa    = $this->M_pembayaran->getSiswaPerKelas($post['kelas']);
		$totaldata    = 0;
		$totalError   = 0;
		$totalErrorAl = 0;
		foreach ($dataSiswa as $key => $value) {
			$checkPembayaran = $this->M_pembayaran->checkPembayaranExist($post['title'],$value->siswa_id);
			if (empty($checkPembayaran)) {
				$insertArray = array(
					"pembayaran_id"      => $post['title'],
					"siswa_id"           => $value->siswa_id,
					"created_date"       => date("Y-m-d H:i:s"),
					"created_by"         => $this->sessionData['user_id'],
					"kelas_id"           => $value->kelas_id,
					"rombel_id"          => $value->kelas_rombel_id,
					"tahun_pelajaran_id" => $post['tahun_pelajaran'],
					"status"             => 1,
					"isDeleted"          => 0
				);
				$insert = $this->db->insert("pembayaran_install_siswa",$insertArray);
				if ($insert) {
					$totaldata  += 1;
				}else{
					$totalError +=1;
				}
			}else{
				$totalErrorAl += 1;
			}
		}
		/* ERROR HANDLING MESSAGE */
		if ($totaldata > 0) {
			custom_notif("success","Status","Success Enroll ".$totaldata." Data");
		}else{
			custom_notif("failed","Failed_01","No Data can be enrolled.");
		}
		if ($totalError <> 0) {
			custom_notif("failed","Failed_02","Failed Enroll ".$totalError." Data");
		}
		if ($totalErrorAl <> 0) {
			custom_notif("failed","Failed_03","Pembayaran already enrolled for ".$totalErrorAl." Data");
		}

		redirect("cms/pembayaransiswa");
	}

	function pembayaranPerSantri($post){
		$siswa_id        = encrypt_decrypt("decrypt", $post['idsantri']);
		$checkPembayaran = $this->M_pembayaran->checkPembayaranExist($post['title'],$siswa_id);
		$totaldata       = 0;
		$totalError      = 0;
		$totalErrorAl    = 0;
		$dataKelasDetail = $this->M_pembayaran->getSiswaPerKelas2($siswa_id);
		if (!empty($dataKelasDetail)) {
			if (empty($checkPembayaran)) {
				$insertArray = array(
					"pembayaran_id"      => $post['title'],
					"siswa_id"           => $siswa_id,
					"created_date"       => date("Y-m-d H:i:s"),
					"created_by"         => $this->sessionData['user_id'],
					"kelas_id"           => $dataKelasDetail->kelas_id,
					"rombel_id"          => $dataKelasDetail->kelas_rombel_id,
					"tahun_pelajaran_id" => $post['tahun_pelajaran'],
					"status"             => 1,
					"isDeleted"          => 0
				);
				$insert = $this->db->insert("pembayaran_install_siswa",$insertArray);
				if ($insert) {
					$totaldata  += 1;
				}else{
					$totalError +=1;
				}
			}else{
				$totalErrorAl += 1;
			}
			/*================= ERROR HANDLING MESSAGE =================*/
			if ($totaldata > 0) {
				custom_notif("success","Status","Success Enroll ".$totaldata." Data");
			}else{
				custom_notif("failed","Failed_01","No Data can be enrolled.");
			}
			if ($totalError <> 0) {
				custom_notif("failed","Failed_02","Failed Enroll ".$totalError." Data");
			}
			if ($totalErrorAl <> 0) {
				custom_notif("failed","Failed_03","Pembayaran already enrolled for ".$totalErrorAl." Data");
			}
		}else{
			custom_notif("failed","Failed_04","Santri tidak terdaftar di kelas mana pun");
		}

		redirect("cms/pembayaransiswa");
	}

	function backupaddwanda(){
		$insertArray = array(
			"pembayaran_id" => $post['title'],
			"siswa_id"  	=> encrypt_decrypt("decrypt", $post['idsantri']),
			"status" 		=> "1",
			"isDeleted" 	=> "0",
			"created_date"  => date('Y-m-d H:i:s'),
			"created_by"    => $this->sessionData['user_id']

		);

		$insert	= $this->db->insert("pembayaran_install_siswa", $insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaransiswa/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaransiswa/add/");
		}
	}

	function edit($id){
		$detailPembayaranSiswa 			= $this->M_pembayaransiswa->getPembayaranSiswaDetail("pembayaran_install_siswa_id", $id);
		$dataPembayaran    				= $this->M_pembayaran->getPembayaran();
		// debugCode($detailPembayaranSiswa);
		$data['dataPembayaran']	= $dataPembayaran;
		$data['detailPemSiswa'] 	= $detailPembayaranSiswa;
		$data['back_link']   		= base_url('cms/pembayaransiswa');
		$data['web_title']   		= "Edit Pembayaran Siswa";
		$data['content']     		= "admin/pembayaran_siswa/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		// debugCode($post);
		$updateArray = array(
			"pembayaran_id" => $post['title'],
			"siswa_id"  	=> encrypt_decrypt("decrypt", $post['idsantri']),
			"updated_date"  => date('Y-m-d H:i:s'),
			"updated_by"    => $this->sessionData['user_id']
		);

		$update = $this->db->update("pembayaran_install_siswa", $updateArray, array("pembayaran_install_siswa_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaransiswa");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaransiswa/edit/".$id);
		}
	}

	function doDelete($id){
		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("pembayaran_install_siswa", $updateArray, array("pembayaran_install_siswa_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaransiswa");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaransiswa");
		}
	}

	function gobayar($id){
		$detailData      = $this->M_pembayaran->getDetailPembayaran($id);
		$pembayaran_item = $this->M_pembayaran->getPembayaranInstallItem("pii.pembayaran_id",$detailData->pembayaran_id);
		$data['id']                = $id;
		$data['dataPembayaran']    = $detailData;
		$data['detail_pembayaran'] = $pembayaran_item;
		$data['back_link']         = base_url('cms/pembayaransiswa');
		$data['web_title']         = "Detail Pembayaran Siswa";
		$data['content']           = "admin/pembayaran_siswa/gobayar";
		$this->load->view('admin/layout',$data);
	}

	function list_pembayaran_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_pembayaran->get_list_pembayaran ( $requestParam, 'nofilter');
		$totalAllData 			= $this->M_pembayaran->get_list_pembayaran ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_pembayaran->get_list_pembayaran ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_pembayaran->get_list_pembayaran ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		$listSub = [];
		foreach( $getData->result () AS $value){
			$rowData = array();
			$button  = "";
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			/*$button .= '
				<button class="btn btn-danger btn-sm tbl-btn" onClick="is_delete(\''.base_url('cms/pembayaransiswa/doDelete/'.$value->pembayaran_install_siswa_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
				<a href="'.base_url('cms/pembayaransiswa/gobayar/'.$value->pembayaran_install_siswa_id).'" class="btn btn-success btn-sm tbl-btn">Bayar</a>
				<a target="_blank" href="'.base_url('cms/pembayaransantri/printkwitansi/'.$value->pembayaran_install_siswa_id).'" class="btn btn-warning btn-sm tbl-btn">Kwitansi</a>';*/
			
			/*========================================= END BUTTON STUFF =========================================*/			
			$status = "";
			if ($value->status == 1) {
				$status = "<b style='color:#e74c3c;'>Belum Lunas</b>";
				$button .= '
					<button class="btn btn-danger btn-sm tbl-btn" onClick="is_delete(\''.base_url('cms/pembayaransiswa/doDelete/'.$value->pembayaran_install_siswa_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>
					<a href="'.base_url('cms/pembayaransiswa/gobayar/'.$value->pembayaran_install_siswa_id).'" class="btn btn-success btn-sm tbl-btn">Bayar</a>';
			}elseif($value->status == 2){
				$status = "<b style='color:#2ecc71;'>Sudah Lunas</b>";
				$button.='<a target="_blank" href="'.base_url('cms/pembayaransantri/rfprintkwitansi/'.$value->pembayaran_install_siswa_id).'" class="btn btn-warning btn-sm tbl-btn">Kwitansi</a>';
			}

			$rowData[] = $no++;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->pembayaran_title;
			$rowData[] = date("d M Y H:i",strtotime($value->created_date));
			$rowData[] = $status;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}

	function doBayar($id){
		$post = $this->input->post();
		$currentMoney           = $this->global->getCurrentMoney();
		$tanggal_bayar_kwitansi = date("dmY");
		$tanggal_bayar_real     = date("Y-m-d H:i:s");
		$no_kwitansi            = $post['noinduk']."/".$tanggal_bayar_kwitansi."/".$id;

		/*============ INSERT TO UANG KAS ============*/
		$amount_after = $currentMoney->amount + $post['totalz'];
		$iUangKasArray = array(
			"kas_name"            => "PEMBAYARAN ".$post['pembayaran'],
			"kas_type"            => "IN",
			"kas_from"            => "PEMBAYARAN ".$post['pembayaran']." ".$post['nama_siswa'],
			"kas_transaction"     => $post['totalz'],
			"pembayaran_id"       => $id,
			"pembayaran_siswa_id" => $post['sidz'],
			"amount_after"        => $amount_after,
			"created_date"        => date("Y-m-d H:i:s"),
			"created_by"          => $this->sessionData['user_id'],
			"status"              => 1,
			"isDeleted"           => 0,
			"is_from_kas"         => 0
		);
		$insert_uang_kas = $this->db->insert("uang_kas",$iUangKasArray);
		if ($insert_uang_kas) {
			/*====== START UPDATE AMOUNT IN MONEY =====*/
			$sql_update = "UPDATE money SET amount = amount + ".$post['totalz'];
			$query      = $this->db->query($sql_update);
			if (!$query) {
				custom_notif("failed","103","Something went wrong with updating money");
			}
			/*====== END UPDATE AMOUNT IN MONEY =====*/
			/*====== START UPDATE DATA PEMBAYARAN INSTALL SISWA ======*/
			$updateArray = array(
				"no_kwitansi"  => $no_kwitansi,
				"date_pay"     => $tanggal_bayar_real,
				"updated_date" => $tanggal_bayar_real,
				"updated_by"   => $this->sessionData['user_id'],
				"status"       => 2
			);
			$updateData = $this->db->update("pembayaran_install_siswa", $updateArray,array("pembayaran_install_siswa_id" => $id));
			if ($updateData) {
				custom_notif("success","Status","Success change status to BAYAR");
			}else{
				custom_notif("failed","102","Something went wrong with updating data 1");
			}
			/*====== END UPDATE DATA PEMBAYARAN INSTALL SISWA ======*/
		}else{
			custom_notif("failed","101","Something went wrong with insert 1");
		}
		redirect("cms/pembayaransiswa");
	}
}