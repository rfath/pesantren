<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_order");
		$this->load->model("M_event");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$listEvent = $this->global->getActiveEventAndUpcomming();
		
		$data['lsitEvent'] = $listEvent;
		$data['web_title'] = "Attendance - Select Event";
		$data['content']   = "admin/attendance/listevent";
		$this->load->view('admin/layout',$data);
	}

	function aevent($param){
		$event_id      = encrypt_decrypt("decrypt",$param);
		$listAttendant = $this->M_order->getAttendance($event_id);
		$eventDetail   = $this->global->getShortEventDetail($event_id);
		$getReligion   = $this->global->getListRegion();
		$getDPW        = $this->global->getListDPW();
		$getSatelit    = $this->global->getSatelitChurch();
		$getChurchR    = $this->global->getChurchRole();

		$data['listChurchR']   = $getChurchR;
		$data['listSatelit']   = $getSatelit;
		$data['listDPW']       = $getDPW;
		$data['listReligion']  = $getReligion;
		$data['eventDetail']   = $eventDetail;
		$data['listAttendant'] = $listAttendant;
		$data['id']            = $param;
		$data['web_title']     = "Attendance Proccess";
		$data['back_link']     = base_url('cms/attendance');
		$data['content']       = "admin/attendance/attendance";
		$this->load->view('admin/layout',$data);
	}

	function checkCode($event_id,$orderNumber){
		$event_id       = encrypt_decrypt("decrypt",$event_id);
		$getOrderNumber = explode("-", $orderNumber);

		$check = $this->M_order->checkOrderCode($event_id,$getOrderNumber[2]);
		
		if (!empty($check)) {
			
			/*============= DATA FOR JSON =============*/
			$datajson = array();
			$notValid = 0;
			foreach ($check as $key => $value) {
				if ($value->card_number <> "") { //If member already register
					$notValid = $notValid + 1;
				}else{
					$datajson[] = array(
						"eticket"   => $value->eticket,
						"name"      => $value->attendance_name,
						"email"     => $value->attendance_email,
						"phone"     => $value->attendance_number,
						"param"     => $value->attendance_id,
						"region"    => $value->region_id,
						"dpw"       => $value->dpw_id,
						"sateilite" => $value->satelit_church_id,
						"role"      => $value->church_role_id,
						"remarks"   => $value->payment_remarks,
						"gender"    => $value->gender,
						"age"       => $value->age
					);
				}
			}
			if ($notValid == 0) {
				echo json_encode(array("is_valid" => "OK", "attd" => $datajson));
			}else{
				echo json_encode(array("is_valid" => "NOT_OK", "msg" => "This attendance already exchange for their card."));
			}
			
		}else{
			echo json_encode(array("is_valid" => "NOT_OK", "msg" => "This attendance is not in this event."));
		}
	}

	function doProccess($event_id){
		$post = $this->input->post();
		$ev_id = encrypt_decrypt("decrypt",$event_id);
		
		/*======== START CHECK DUPLICATE CARD ========*/
		$dataCard = array();
		$dupCard = 0;
		foreach ($post['datapart'] as $dpkey => $dpvalue) {
			$check = $this->M_order->checkCardNumber($ev_id,$dpvalue['card']);
			if (!empty($check)) {
				$this->session->set_flashdata('is_success', 'You have Duplicate Card Number: '.$dpvalue['card'].', please Check again.');
				redirect("cms/attendance/aevent/".$event_id);
			}
			$dataCard[$dpkey] = $dpvalue['card'];
		}
		$newArr = array_unique($dataCard, SORT_REGULAR);
		$countData = array(
			"data1"	=> count($post['datapart']),
			"data2"	=> count($newArr)
		);
		/*======== START CHECK DUPLICATE CARD ========*/

		if ($countData['data1'] == $countData['data2']) {
			foreach ($post['datapart'] as $key => $value) {
				$updateArray = array(
					"attendance_name"   => $value['name'],
					"attendance_number" => $value['phone'],
					"attendance_email"  => $value['email'],
					"card_number"       => $value['card'],
					"updated_date"      => date("Y-m-d H:i:s"),
					"updated_by"        => $this->sessionData['user_id'],
					"is_active"         => 1,
					"region_id"         => $value['region'],
					"dpw_id"            => $value['dpw'],
					"satelit_church_id" => $value['satelit_church'],
					"church_role_id"    => $value['church_role'],
					"eticket"           => $value['eticket'],
					"gender"            => $value['gender'],
					"age"               => $value['age']
				);
				$update = $this->db->update("attendance",$updateArray, array("attendance_id" => $value['param']));
			}

			if($this->db->trans_status() === FALSE){	
				$this->db->trans_rollback();
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/attendance/aevent/".$event_id);
			}else{
				$this->db->trans_commit();
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/attendance/aevent/".$event_id);
			}
		}else{
			$this->session->set_flashdata('is_success', 'You have Duplicate Card Number on Input, please Check again.');
			redirect("cms/attendance/aevent/".$event_id);
		}		
	}

	function chinout($param){
		$get = $this->input->get();
		$event_id    = encrypt_decrypt("decrypt",$param);
		//$packageData = $this->global->getPackageByEvent($event_id);
		$scheduleData = $this->global->getAllScheduleByEvent($event_id);
		
		$eventDetail = $this->global->getShortEventDetail($event_id);
		$data['session'] = "";
		$data['sessionDetail']	= "";
		if ($eventDetail->is_recurring == 1) {
			if (empty($get['session']) <> "") {
				redirect("cms/attendance/session/".$param);
			}else{
				$data['session'] = $get['session'];
				$data['sessionDetail'] = $this->M_event->getRecurringSessionById(encrypt_decrypt('decrypt',$get['session']));
			}
		}
		
		$data['scheduleData'] = $scheduleData;
		$data['eventDetail']  = $eventDetail;
		$data['id']           = $param;
		$data['web_title']    = "Check In/Out";
		$data['back_link']    = base_url('cms/attendance');
		$data['content']      = "admin/attendance/checkinout";
		$this->load->view('admin/layout',$data);
	}

	function session($param){
		$event_id    = encrypt_decrypt("decrypt",$param);
		$eventDetail = $this->global->getShortEventDetail($event_id);

		$data['eventDetail']  = $eventDetail;
		$data['id']           = $param;
		$data['web_title']    = "Select Recurring Date";
		$data['back_link']    = base_url('cms/attendance');
		$data['content']      = "admin/attendance/recurring_session";
		$this->load->view('admin/layout',$data);
	}

	function checkrecurring($param){
		$event_id = encrypt_decrypt("decrypt",$param);
		$post     = $this->input->post();
		$dateData = date("Y-m-d",strtotime($post['date_start']));
		$check    = $this->M_event->checkrecurring($dateData,$event_id);

		if (!empty($check)) {
			$recurring_id = encrypt_decrypt('encrypt',$check->recurring_session_id);
		}else{
			$insertArray = array(
				"event_id"       => $event_id,
				"recurring_date" => $dateData
			);
			$insert       = $this->db->insert("recurring_session",$insertArray);
			$insert_id    = $this->db->insert_id();
			$recurring_id = encrypt_decrypt('encrypt',$insert_id);
		}
		
		redirect("cms/attendance/chinout/".$param."?session=".$recurring_id);
	}

	function getSchedule($package_id = ""){
		if ($package_id == "") {
			$schedule = array();
		}else{
			$schedule = $this->M_event->getScheduleByPackage($package_id);
		}
		
		$html = '<option value="">Select Schedule</option>';
		foreach ($schedule as $key => $value) {
			$html.='<option value="'.$value->event_schedule_id.'">'.$value->schedule_name.'</option>';
		}
		die($html);
	}

	function chprocess($schedule_id){
		$get = $this->input->get();
		$getDataSchedule = $this->M_event->getShordSchedule($schedule_id);
		$data['scheduleData'] = $getDataSchedule;
		$data['schedule_id']  = $schedule_id;
		$data['session']      = $get['session'];
		$this->load->view("admin/attendance/checkinout_process",$data);
	}

	function dochinout(){
		$get = $this->input->get();
		$checkData = $this->M_event->checkAttendanceIO($get['evi'], $get['cn']);
		
		if (!empty($checkData)) {
			$checkSchedule = $this->M_event->checkScheduleINOUT($checkData->package_id, $get['sci']);
			
			if (!empty($checkSchedule)) {
				if ($get['ses'] <> "") {
					$session = encrypt_decrypt("decrypt",$get['ses']);
				}
				$insertArray = array(
					"attendance_id"        => $checkData->attendance_id,
					"schedule_id"          => $get['sci'],
					"type_att"             => $get['io'],
					"created_date"         => date("Y-m-d H:i:s"),
					"created_by"           => $this->sessionData['user_id'],
					"recurring_session_id" => $session
				);
				$insert = $this->db->insert("attendance_schedule",$insertArray);
				if ($insert) {
					if ($get['io'] == 1) {
						echo json_encode(array("status" => "ok", "msg" => "Check in Success", "par2" => $get['sci']));
					}else{
						echo json_encode(array("status" => "ok", "msg" => "Check out Success"));
					}
				}else{
					if ($get['io'] == 1) {
						echo json_encode(array("status" => "not_ok", "msg" => "Check out Failed"));
					}else{
						echo json_encode(array("status" => "not_ok", "msg" => "Check out Failed"));
					}
				}
			}else{
				echo json_encode(array("status" => "not_ok", "msg" => "Attendance not registered on this schedule."));
			}
		}else{
			echo json_encode(array("status" => "not_ok", "msg" => "Attendance not registered on this event."));
		}
	}

	function eattendance($param){
		$attendance_id = encrypt_decrypt("decrypt",$param);
		$getData       = $this->M_order->getDetailAttendance($attendance_id);
		$getReligion   = $this->global->getListRegion();
		$getDPW        = $this->global->getListDPW();
		$getSatelit    = $this->global->getSatelitChurch();
		$getChurchR    = $this->global->getChurchRole();
		$listPacakge   = $this->global->getPackageByEvent($getData->event_id);		

		$data['listPackage']  = $listPacakge;
		$data['listChurchR']  = $getChurchR;
		$data['listSatelit']  = $getSatelit;
		$data['listDPW']      = $getDPW;
		$data['listReligion'] = $getReligion;
		$data['id']           = $param;
		$data['detailData']   = $getData;
		$this->load->view("admin/attendance/editattendance",$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		$event_id      = encrypt_decrypt("decrypt",$id);
		$attendance_id = encrypt_decrypt("decrypt",$post['param']);

		
		$check = $this->M_order->checkCardNumber($event_id,$post['att_card'],$attendance_id);
		
		if (empty($check)) {
			$order_id = $this->global->getOrderIDbyAttendance($attendance_id);
			
			$updateArray = array(
				"attendance_name"   => $post['att_name'],
				"attendance_number" => $post['att_phone'],
				"attendance_email"  => $post['att_email'],
				"card_number"       => $post['att_card'],
				"last_updated_date" => date("Y-m-d H:i:s"),
				"last_updated_by"   => $this->sessionData['user_id'],
				"region_id"         => $post['region'],
				"dpw_id"            => $post['dpw'],
				"satelit_church_id" => $post['satelit_church'],
				"church_role_id"    => $post['church_role'],
				"eticket"           => $post['eticket'],
				"gender"            => $post['gender'],
				"age"               => $post['age']
			);
			$update = $this->db->update("attendance",$updateArray, array("attendance_id" => $attendance_id));
			if ($update) {
				$this->db->update("order",array("package_id" => $post['package']), array("order_id" => $order_id));
				$this->session->set_flashdata('is_success', 'Yes');
				redirect("cms/attendance/aevent/".$id);
			}else{
				$this->session->set_flashdata('is_success', 'No');
				redirect("cms/attendance/aevent/".$id);
			}
		}else{
			$this->session->set_flashdata('is_success', 'You have Duplicate Card Number on Input, please Check again.');
			redirect("cms/attendance/aevent/".$id);
		}		
	}

	function searchOrder(){
		$get = $this->input->get();

		$event_id   = encrypt_decrypt("decrypt",$get['param']);
		$searchData = $this->M_order->searchOrder($event_id, $get['searchKey']);

		$data['dataOrder']	= $searchData;
		$this->load->view("admin/attendance/searchresult",$data);
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function list_attendance_schedule(){
		$requestParam 			= $_REQUEST;
		
		if ($requestParam['session'] <> "") {
			$requestParam['session'] = encrypt_decrypt('decrypt',$requestParam['session']);
		}

		$getData 				= $this->M_event->get_list_attendance_schedule ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_event->get_list_attendance_schedule ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_event->get_list_attendance_schedule ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_event->get_list_attendance_schedule ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();

			$inout = "";
			if ($value->type_att == 1) {
				$inout = '<span class="badge badge-bdr badge-success">IN</span>';
			}else{
				$inout = '<span class="badge badge-bdr badge-danger">OUT</span>';
			}

			$rowData[] = $no++;
			$rowData[] = $value->attendance_name;
			$rowData[] = date("d M h:i A",strtotime($value->created_date));
			$rowData[] = $inout;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}