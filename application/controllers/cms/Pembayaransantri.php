<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PembayaranSantri extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_pembayaransantri");
		$this->load->model("M_pembayaran");
		$this->load->model("M_bendahara");
		$this->load->model("M_santri");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	// function index(){
	// 	$dataPembayaranSiswa = $this->M_pembayaransiswa->getPembayaranSiswa();
	// 	// debugCode($dataPembayaranSiswa);
	// 	$data['listData'] 	= $dataPembayaranSiswa;
	// 	$data['web_title'] 	= "Pembayaran Siswa";
	// 	$data['content']   	= "admin/pembayaran_santri/index";
	// 	$this->load->view('admin/layout',$data);
	// }

	function add(){
		// $dataPembayaran    		= $this->M_pembayaransantri->getInstallSiswaByCondition();

		// $data['dataPembayaran']	= $dataPembayaran;
		$data['back_link'] 		= base_url('cms/pembayaransantri');
		$data['web_title'] 		= "Pembayaran Santri";
		$data['content']   		= "admin/pembayaran_santri/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();

		$dataItem = $this->M_pembayaran->getPembayaranInstallItem("pembayaran_id", $post['title']);
		$priceAllItem = [];
		foreach ($dataItem as $value) {
			$priceAllItem[] = $value->price;
		}
		
		$insertArray = array(
			"kas_from"  			=> "siswa",
			"pembayaran_id" 		=> $post['title'],
			"pembayaran_siswa_id" 	=> $post['idsantri'],
			"kas_transaction"		=> array_sum($priceAllItem),
			"status" 				=> "1",
			"isDeleted" 			=> "0",
			// "created_date"  		=> date('Y-m-d H:i:s'),
			"created_date"  		=> date('Y-m-d H:i:s', strtotime($post['tgl_trans'])),
			"created_by"    		=> $this->sessionData['user_id']

		);

		$insert	= $this->db->insert("uang_kas", $insertArray);
		$insert_id = $this->db->insert_id();
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/pembayaransantri/printkwitansi/".$insert_id);
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/pembayaransantri/add");
		}
	}

	function printkwitansi($id){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);

		$dataKas 				= $this->M_bendahara->getKasDetail("kas_id", $id);
		$dataInstallItem 		= $this->M_pembayaran->getPembayaranInstallItem("pembayaran_id", $dataKas->pembayaran_id);
		$dataSiswa 				= $this->M_santri->getSantriDetail("siswa_id", $dataKas->pembayaran_siswa_id);
		$dataPembayaran 		= $this->M_pembayaran->getPembayaranDetail("pembayaran_id", $dataKas->pembayaran_id);

		$data['pembayaran']		= $dataPembayaran;
		$data['detailSiswa']	= $dataSiswa;
		$data["installItem"]	= $dataInstallItem;
		$data["dataKas"]		= $dataKas;


		$filename = date("ymdHis")."_kwitasi_pembayaran.pdf";
		// debugCode($dataSiswa);

		$view = $this->load->view('admin/pembayaran_santri/printkwitansi', $data);


		/*$mpdf->WriteHTML($view);
		$mpdf->Output($filename, "F");*/
		//$mpdf->Output(FCPATH.'/upload/laporan/kwitansi/kwitansi_pembayaran_siswa/'.$filename, "F"); 
	}

	function rfprintkwitansi($id){
		$mpdf = new \Mpdf\Mpdf(['tempDir' => FCPATH . '/upload/pdf']);

		$detailData = $this->M_pembayaran->getDetailPembayaran($id);
		$pembayaran_item = $this->M_pembayaran->getPembayaranInstallItem("pii.pembayaran_id",$detailData->pembayaran_id);
		
		$data['detailData']       = $detailData;
		$data['detailPembayaran'] = $pembayaran_item;

		$view = $this->load->view('admin/pembayaran_santri/printkwitansi', $data,true);
		$mpdf->WriteHTML($view);
		$mpdf->Output();
	}

	function getPembayaran($id){
		$pembayaran = $this->M_pembayaransantri->getInstallSiswaByCondition("pis.siswa_id", $id);
		$html = '<option value="">Pilih Title Pembayaran</option>';
		foreach ($pembayaran as $key => $value) {
			$html.='<option value="'.$value->pembayaran_id.'">'.$value->pembayaran_title.'</option>';
		}
		die($html);
	}

	/*==================================================== DATA FOR DATATABLE ====================================================*/
	function get_list_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->global->get_list_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->global->get_list_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
			<button type="button" class="btn btn-outline-success btn-sm" id="select" onClick="selectsantri(\''.$value->siswa_id.'\',\''.htmlspecialchars($value->siswa_name).'\')">Select</button>
			';
			/*========================================= END BUTTON STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}
}