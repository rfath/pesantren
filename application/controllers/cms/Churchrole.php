<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Churchrole extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_master");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$listData  = $this->M_master->getChurchRole();
		$data['listData']  = $listData;
		$data['web_title'] = "Church Role";
		$data['content']   = "admin/church_role/listrole";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Add Church Role";
		$data['back_link'] = base_url('cms/churchroles');
		$data['content']   = "admin/church_role/addrole";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"church_role_name" => $post['church_role_name'],
			"created_date"     => date("Y-m-d H:i:s"),
			"created_by"       => $this->sessionData['user_id']
		);
		$insert = $this->db->insert("church_role",$insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/churchrole");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/churchrole/add");
		}
	}

	function edit($param){
		$id         = encrypt_decrypt("decrypt",$param);
		$detailData = $this->M_master->getDetailChurchRole($id);

		$data['id']         = $param;
		$data['detailData'] = $detailData;
		$data['web_title']  = "Edit Church Role";
		$data['back_link']  = base_url('cms/churchroles');
		$data['content']    = "admin/church_role/editrole";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate(){
		$post = $this->input->post();
		$id   = encrypt_decrypt("decrypt",$post['param']);

		$updateArray = array(
			"church_role_name" => $post['church_role_name'],
			"updated_date"     => date("Y-m-d H:i:s"),
			"updated_by"       => $this->sessionData['user_id']
		);
		$update = $this->db->update("church_role",$updateArray,array("church_role_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/churchrole");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/churchrole/edit/".$post['param']);
		}
	}

	function doDelete($param){
		$post = $this->input->post();
		$id   = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"is_deleted"   => 1,
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id']
		);
		$update = $this->db->update("church_role",$updateArray,array("church_role_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/churchrole/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/churchrole/");
		}
	}
}