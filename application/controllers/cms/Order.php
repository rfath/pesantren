<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_order");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		/*$inputFileName = './assets/excel/'.$newname;
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);*/
		$listEvent = $this->global->getAllEventShort();
		
		$data['listEvent'] = $listEvent;
		$data['web_title'] = "Order";
		$data['content']   = "admin/order/listorder";
		$this->load->view('admin/layout',$data);
	}

	function detail($param){
		$order_id    = encrypt_decrypt("decrypt", $param);
		$detailOrder = $this->M_order->orderDetail($order_id);

		/*================= START STATUS PAYMENT =================*/
		//0: Waiting Payment, 1: Waiting Admin Approve , 2: Accept, 3: Reject
		if ($detailOrder->payment_status == 0) {
			$status_payment = "Waiting For Payment";
		}elseif($detailOrder->payment_status == 1){
			$status_payment = "Waiting Admin Approve";
		}elseif($detailOrder->payment_status == 2){
			$status_payment = "Accepted";
		}else{
			$status_payment = "Rejected";
		}

		$additional = array();
		$seat = 1;
		if ($detailOrder->additional_participant <> "") {
			$additional = json_decode($detailOrder->additional_participant);
			$count      = count($additional);
			$seat       = $seat + $count;
		}
		
		$pay = json_decode($detailOrder->payment_type);
		
		if ($pay->pay_type == 2) {
			$payment = $pay->pay_type_info." / ".$pay->pay_detail[0]." (".$pay->pay_detail[1].":".$pay->pay_detail[2].")";
		}else{
			$payment = $pay->pay_type_info;
		}
		
		$orderNumber = "OD-".date('Ymd',strtotime($detailOrder->order_time))."-".$detailOrder->order_id;
		$data['id']             = $param;
		$data['order_number']   = $orderNumber;
		$data['payment']        = $payment;
		$data['seat']           = $seat;
		$data['additional']     = $additional;
		$data['status_payment'] = $status_payment;
		$data['detailOrder']    = $detailOrder;
		$data['payData']        = $pay;
		$data['web_title'] = "Detail Order";
		$data['back_link'] = base_url('cms/order');
		$data['content']   = "admin/order/detailOrder";
		$this->load->view('admin/layout',$data);
	}

	function doProccess(){
		$post = $this->input->post();
		if (isset($post['accept'])) {
			$this->doAccept($post);
		}elseif(isset($post['reject'])){
			$this->doReject($post);
		}
	}

	function doAccept($post){
		$order_id    = encrypt_decrypt("decrypt", $post['param']);
		/*insert Main Attendance*/
		$insertArray = array(
			"attendance_name"   => $post['part_name'],
			"attendance_email"  => $post['part_mail'],
			"attendance_number" => $post['part_phone'],
			"order_id"          => $order_id,
			"created_date"      => date("Y-m-d H:i:s"),
			"created_by"        => $this->sessionData['user_id'],
			"is_active"         => 0,
			"is_deleted"        => 0,
			"eticket"           => $post['eticket'],
			"gender"            => $post['gender'],
			"age"               => $post['age']
		);
		
		$insert = $this->db->insert("attendance",$insertArray);
		if ($insertArray) {
			$arrayAdditional = array();
			if (!empty($post['additional'])) {
				foreach ($post['additional'] as $key => $value) {
					$insertAttendat = array(
						"attendance_name"   => $value['name'],
						"attendance_email"  => $value['email'],
						"attendance_number" => $value['phpone'],
						"order_id"          => $order_id,
						"created_date"      => date("Y-m-d H:i:s"),
						"created_by"        => $this->sessionData['user_id'],
						"is_active"         => 0,
						"is_deleted"        => 0,
						"eticket"           => $value['eticket'],
						"gender"            => $value['gender'],
						"age"               => $value['age']
					);
					$insert2 = $this->db->insert("attendance",$insertAttendat);

					$arrayAdditional[] = array(
						"name"    => $value['name'],
						"email"   => $value['email'],
						"phone"   => $value['phpone'],
						"gender"  => $value['gender'],
						"eticket" => $value['eticket'],
						"age"     => $value['age']
					);
				}
			}

			$updateOrderArray = array(
				"payment_status" => 2,
				"eticket"        => $post['eticket']
			);
			if (!empty($arrayAdditional)) {
				$updateOrderArray['additional_participant'] = json_encode($arrayAdditional);
			}
			$updateOrder = $this->db->update("order",$updateOrderArray,array("order_id" => $order_id));
			
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/order/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/order/");
		}
	}

	function doReject($post){
		$order_id    = encrypt_decrypt("decrypt", $post['param']);
		$updateOrder = $this->db->update("order",array("payment_status" => 3),array("order_id" => $order_id));
		if ($updateOrder) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/order/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/order/");
		}
	}

	function add(){
		$dataMember = $this->M_order->getMemberActive();
		$dataUpcomingEvent = $this->global->getListUpcomingEvent();

		$data['dataEvent']  = $dataUpcomingEvent;
		$data['dataMember'] = $dataMember;
		$data['web_title']  = "Add Manual Order";
		$data['back_link']  = base_url('cms/order');
		$data['content']    = "admin/order/addorder";
		$this->load->view('admin/layout',$data);
	}

	function downloadExample($param){
		$event_id = encrypt_decrypt("decrypt",$param);
		$eventDetail   = $this->global->getEventDetail($event_id);
		$packageDetail = $this->global->getPackageByEvent($eventDetail->event_id);

		$data['dataEvent']   = $eventDetail;
		$data['packageData'] = $packageDetail;
		$this->load->view("admin/order/excelexample",$data);
	}

	function packageData($param){
		$event_id = encrypt_decrypt("decrypt",$param);
		$packageDetail = $this->global->getPackageByEvent($event_id);

		$html= '<option value="">Select Package</option>';
		foreach ($packageDetail as $key => $value) {
			$html.='<option value="'.$value->event_package_id.'">'.$value->package_name.'</option>';
		}
		die($html);
	}

	function doImportData(){
		$post = $this->input->post();
		
		$event_id      = encrypt_decrypt("decrypt",$post['eventData']);
		$package_id    = $post['packageData'];
		$packageDetail = $this->global->getPackageDetail($package_id);
		
		/*UPLOAD FILE*/
		$ext     = pathinfo($_FILES['data_file']['name'], PATHINFO_EXTENSION);
		$newname = strtotime(date("Y-m-d H:i:s")).".".$ext;

		$config['upload_path']          = './assets_ticketing/excel/';
		$config['allowed_types']        = 'xls';
		$config['file_name']			= $newname;
		$config['overwrite']			= TRUE;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('data_file')){
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
			die();
		}else{

			$inputFileName = './assets_ticketing/excel/'.$newname;
			//$inputFileName = './assets_ticketing/excel/1531803336.xls';
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
			$worksheet = $spreadsheet->getActiveSheet();
			$rows = [];
			foreach ($worksheet->getRowIterator() AS $row) {
			    $cellIterator = $row->getCellIterator();
			    $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
			    $cells = [];
			    foreach ($cellIterator as $cell) {
			        $cells[] = $cell->getValue();
			    }
			    $rows[] = $cells;
			}
			
			//Start input2 data
			$dataError = 0;
			$eticketError = array();
			$successData = 0;
			foreach ($rows as $key => $value) {
				if ($key == 0 ) {
					continue;
				}

				$howMuch = count(array_filter(array_values($value)));
				if ($howMuch == 0) {
					continue;
				}

				$value[11] = str_replace(" ", "", $value[11]);
				
				$checkEticket = array();
				if ($value[11] <> "") {
					$checkEticket = $this->M_order->checkAvailableEticket($event_id,$value[11]);
					if (!empty($checkEticket)) {
						$eticketError[] = $checkEticket->eticket;
					}
				}
				
				if ($value[0] <> "" AND $value[1] <> "" AND empty($checkEticket)) {
					/*====== START CHECK REGION ======*/
					$region_id = "";
					if ($value[5] <> "") {
						$checkRegion = $this->M_order->checkRegion($value[5]);
						if (empty($checkRegion)) {
							$region_id = $this->save_RDC($value[5], "region_name", "region");
						}else{
							$region_id = $checkRegion->region_id;
						}
					}
					
					/*====== END CHECK REGION ======*/

					/*====== START CHECK REGION ======*/
					$dpw_id = "";
					if ($value[6] <> "") {
						$checkDPW = $this->M_order->checkDPW($value[6]);
						if (empty($checkDPW)) {
							$dpw_id = $this->save_RDC($value[6], "dpw_name", "dpw");
						}else{
							$dpw_id = $checkDPW->dpw_id;
						}
					}
					/*====== END CHECK REGION ======*/

					/*====== START CHECK CHURCH ======*/
					$satelit_church_id = "";
					if ($value[7] <> "") {
						$checkSatelit = $this->M_order->checkSatelitChurch($value[7]);
						if (empty($checkSatelit)) {
							$satelit_church_id = $this->save_RDC($value[7], "satelit_church_name", "satelit_church");
						}else{
							$satelit_church_id = $checkSatelit->satelit_church_id;
						}
					}
					/*====== END CHECK CHURCH ======*/

					/*====== START CHECK ROLE ======*/
					$church_role_id = "";
					if ($value[8] <> "") {
						$checkChurchRole = $this->M_order->checkChurchRole($value[8]);
						if (empty($checkChurchRole)) {
							$church_role_id = $this->save_RDC($value[8], "church_role_name", "church_role");
						}else{
							$church_role_id = $checkChurchRole->church_role_id;
						}
					}
					/*====== END CHECK ROLE ======*/

					/*====== START CHECK ROLE ======*/
					$gender = "MALE";
					if ($value[13] <> "") {
						$gender = $value[13];
					}
					/*====== END CHECK ROLE ======*/


					/*====== start check if member exoist or not ======*/
					$checkMember = $this->M_order->checkMemberByEmail($value[1]);
					if (empty($checkMember)) {
						$newMemberArray = array(
							"member_name"     => $value[0],
							"member_email"    => $value[1],
							"member_phone"    => $value[2],
							"member_password" => md5("123456"),
							"created_date"    => date("Y-m-d H:i:s"),
							"is_active"       => 1,
							"is_deleted"      => 0,
							"gender"          => $gender
						);						

						$insertMember = $this->db->insert("member", $newMemberArray);
						$memberID     = $this->db->insert_id();
					}else{
						$memberID = $checkMember->member_id;
						/*$updateMemberArray = array(
							"member_name"	=> $value[0],
							"member_phone"	=> $value[2]
						);
						$updateMember = $this->db->update("member",$updateMemberArray, array("member_id" => $member_id));*/
					}
					/*====== end check if member exoist or not ======*/

					/*====== start insert to order ======*/
					$payment_type_tmp = strtolower($value[4]);
					if ($payment_type_tmp <> "") {
						if ($payment_type_tmp == "cash") {
							$payarr['pay_type']      = "3";
							$payarr['pay_type_info'] = "Cash";
							$payarr['pay_detail']    = "";
						}elseif ($payment_type_tmp == "transfer") {
							$payarr['pay_type']      = "2";
							$payarr['pay_type_info'] = "Transfer";
							$payarr['pay_detail']    = array($value[5],$value[6], $value[7]);
						}elseif ($payment_type_tmp == "doku"){
							$payarr['pay_type']      = "1";
							$payarr['pay_type_info'] = "Doku";
							$payarr['pay_detail']    = array($value[5],$value[6], $value[7]);
						}else{
							$payarr['pay_type']      = "99";
							$payarr['pay_type_info'] = $value[4];
							$payarr['pay_detail']    = "";
						}
					}else{
						$payarr['pay_type']      = "3";
						$payarr['pay_type_info'] = "Cash";
						$payarr['pay_detail']    = "";
					}
					
					
					$dateRegister = date("Y-m-d",strtotime($value[3]));
					if ($value[3] == "") {
						$dateRegister = date("Y-m-d H:i:s");
					}
					
					$payment_status = strtolower($value[9]);
					$p_status = 2;
					if ($payment_status == "paid") {
						$p_status = 2;
					}elseif ($payment_status == "outstanding"){
						$p_status = 1;
					}elseif ($payment_status == ""){
						$p_status = 2; // Default PAID here
					}

					$orderInsertArray = array(
						"event_id"               => $event_id,
						"package_id"             => $package_id,
						"qty_seat"               => 1,
						"order_price"            => $packageDetail->package_price,
						"member_id"              => $memberID,
						"order_time"             => $dateRegister,
						"payment_type"           => json_encode($payarr),
						"payment_status"         => $p_status,
						"payment_remarks"        => $value[10],
						"eticket"                => $value[11],
						"order_participant_name" => $value[0],
						"gender"                 => $gender,
						"age"                    => $value[14]
					);

					if ($p_status == 2) {
						$orderInsertArray['payment_time'] = $dateRegister;
					}

					$insertOrder = $this->db->insert("order",$orderInsertArray);
					$orderID     = $this->db->insert_id();
					/*====== end insert to order ======*/
					if ($p_status == 2) {
						/*====== start insert to attendance ======*/
						$attInsertArray = array(
							"attendance_name"   => $value[0],
							"attendance_number" => $value[2],
							"attendance_email"  => $value[1],
							"order_id"          => $orderID,
							"created_date"      => date("Y-m-d H:i:s"),
							"is_active"         => 0,
							"is_deleted"        => 0,
							"eticket"           => $value[11],
							"region_id"         => $region_id,
							"dpw_id"            => $dpw_id,
							"satelit_church_id" => $satelit_church_id,
							"church_role_id"    => $church_role_id,
							"gender"            => $gender,
							"age"               => $value[14]
						);
						if ($value[12] <> "") {
							$attInsertArray['is_active']    = 1;
							$attInsertArray['updated_date'] = date("Y-m-d H:i:s");
							$attInsertArray['card_number']  = $value[12];
						}
						$insertAttendance = $this->db->insert("attendance",$attInsertArray);
						/*====== end insert to attendance ======*/
					}
					$successData = $successData + 1;
					$this->send_email($orderID);
				}else{
					$dataError1 = $dataError1 + 1;
				}
			}
			if ($dataError1 <> 0) {
				custom_notif("failed","Failed with email","There is ".$dataError1." data failed to insert. Please check Name, Email, or Eticket");
			}
			if (!empty($eticketError)) {
				custom_notif("failed","Failed with eticket","E-ticket already exist in this event : ".implode(", ", $eticketError));
			}
			if ($successData <> 0) {
				custom_notif("success","ok");
			}
			redirect("cms/order/");
		}
	}

	function save_RDC($name, $field, $table){
		$insertArray = array(
			$field         => $name,
			"created_date" => date("Y-m-d H:i:s"),
			"created_by"   => $this->sessionData['user_id'],
			"is_active"    => 1,
			"is_deleted"   => 0
		);
		
		$insert = $this->db->insert($table, $insertArray);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	/*SEND EMAIL NOTIFICATION REGISTER*/
	function send_email($id){
		$dataOrder = $this->M_order->orderDetail($id);
		
		$additional = "";
		$seat = 1;
		if ($dataOrder->additional_participant <> "") {
			$additionalData = json_decode($dataOrder->additional_participant);
			foreach ($additionalData as $key => $value) {
				$additionalArray[] = $value->name." (E-ticket : <i>".$value->eticket."</i>)";
			}
			$count      = count($additionalArray);
			$seat       = $seat + $count;
			$additional = implode(", " , $additionalArray);
		}

		$pay = json_decode($dataOrder->payment_type);
		
		if ($pay->pay_type == 2) {
			$payment = $pay->pay_type_info." / ".$pay->pay_detail[0]." (".$pay->pay_detail[1].":".$pay->pay_detail[2].")";
		}else{
			$payment = $pay->pay_type_info;
		}

		$data['payment']     = $payment;
		$data['seat']        = $seat;
		$data['additional']  = $additional;
		$data['detailOrder'] = $dataOrder;
		
		$attachments = array();
		$subject     = "Thank you";
		$template    = $this->load->view("front/front_page/eventEmailFormat",$data,true);
		$this->global->send_email ( defaultFormEmail(), defaultFormName(), $dataOrder->member_email, "", $subject, $template,$attachments );
	}
	/*==================================================== DATA FOR DATATABLE ====================================================*/
	public function get_list_order(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_order->get_list_order ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_order->get_list_order ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_order->get_list_order ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_order->get_list_order ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->order_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = '
			<a href="'.base_url("cms/order/detail/".$encrypt_id).'" class="btn btn-info btn-sm ed" title="Detail"><i class="fa fa-info-circle"></i></a>
			';
			$button_proccess= '
				<button class="btn btn-success btn-sm ed" title="Approve"><i class="fa fa-check"></i></button>
				<button class="btn btn-danger btn-sm ed" onClick="#" title="Reject"><i class="fa fa-times"></i></button>
				';
			/*========================================= END BUTTON STUFF =========================================*/
			/*========================================= START STATUS PAYMENT =========================================*/
			//0: Waiting Payment, 1: Waiting Admin Approve , 2: Accept, 3: Reject
			$status_payment = "";
			if ($value->payment_status == 0) {
				$status_payment = '<span class="badge badge-bdr badge-warning">Waiting for Payment</span>';
			}elseif($value->payment_status == 1){
				$status_payment = '<span class="badge badge-bdr badge-info">Waiting Admin to Approve</span>';
			}elseif($value->payment_status == 2){
				$status_payment = '<span class="badge badge-bdr badge-success">Accepted</span>';
			}else{
				$status_payment = '<span class="badge badge-bdr badge-danger">Rejected</span>';
			}
			/*========================================= END STATUS PAYMENT =========================================*/

			/*========================================= START STATUS ORDER =========================================*/
			// 0: Order Place , 1: On Progress, 2: Valid, 3: Invalid

			/*========================================= END STATUS ORDER =========================================*/


			$rowData[] = $no++;
			$rowData[] = $value->eticket;
			$rowData[] = "OD-".date("Ymd",strtotime($value->order_time))."-".$value->order_id;
			$rowData[] = $value->order_participant_name;
			$rowData[] = $value->event_name;
			$rowData[] = $value->package_name;
			$rowData[] = $status_payment;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		
		die();
	}
}