<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunPelajaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_tahunpelajaran");
		$this->sessionData = $this->session->sessionData;
		$sessionData = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$dataTapel = $this->M_tahunpelajaran->getTahunPelajaran();
		// debugCode($dataTapel);

		$data['listData'] 	= $dataTapel;
		$data['web_title'] 	= "Group Module";
		$data['content']   	= "admin/tahun_pelajaran/index";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['back_link'] = base_url('cms/tahunpelajaran');
		$data['web_title'] = "Add Group Module";
		$data['content']   = "admin/tahun_pelajaran/add";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){
		$post = $this->input->post();
		
		$insertArray = array(
			"tahun_pelajaran_name"  => $post['tahun_pelajaran'],
			"status" 				=> "1",
			"isDeleted" 			=> "0",
			"created_date"     		=> date('Y-m-d H:i:s'),
			"created_by"       		=> $this->sessionData['user_id']

		);

		$insert	= $this->db->insert("tahun_pelajaran", $insertArray);
		if ($insert) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/tahunpelajaran/");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/tahunpelajaran/add/");
		}
	}

	function edit($id){
		$detailTP 		= $this->M_tahunpelajaran->getTahunPelajaraneDetail("tahun_pelajaran_id", $id);
		// debugCode($detailTP);
		$data['detailTP'] 	 = $detailTP;
		$data['back_link']   = base_url('cms/tahunpelajaran');
		$data['web_title']   = "Edit Module & Submodule";
		$data['content']     = "admin/tahun_pelajaran/edit";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post     = $this->input->post();
		// debugCode($post);
		$updateArray = array(
			"tahun_pelajaran_name"  => $post['tahun_pelajaran'],
			"updated_date"     		=> date('Y-m-d H:i:s'),
			"updated_by"       		=> $this->sessionData['user_id']
		);

		$update = $this->db->update("tahun_pelajaran", $updateArray, array("tahun_pelajaran_id" => $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/tahunpelajaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/tahunpelajaran/edit/".$id);
		}
	}

	function doDelete($id){

		$updateArray = array(
			"isDeleted"   => "1",
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
		);

		$delete = $this->db->update("tahun_pelajaran", $updateArray, array("tahun_pelajaran_id" => $id));

		if ($delete) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("cms/tahunpelajaran");
		}else{
			$this->session->set_flashdata('is_success', 'No');
			redirect("cms/tahunpelajaran");
		}
	}

}