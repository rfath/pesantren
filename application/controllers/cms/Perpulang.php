<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perpulang extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("M_perizinan");
		$this->sessionData = $this->session->sessionData;
		$sessionData       = $this->sessionData;
		if (empty($sessionData)) {
			redirect('cms/signin');
		}
	}

	function index(){
		$data['web_title'] = "Perizinan Pulang";
		$data['content']   = "admin/keamanan/perizinan_pulang";
		$this->load->view('admin/layout',$data);
	}

	function add(){
		$data['web_title'] = "Tambah Perizinan Pulang";
		$data['back_link'] = base_url('cms/perpulang');
		$data['content']   = "admin/keamanan/addperizinan_pulang";
		$this->load->view('admin/layout',$data);
	}

	function doAdd(){

		$post = $this->input->post();
		$id = encrypt_decrypt("decrypt",$post['idsantri']);
		$insertArray = array(
			"siswa_id"     => $id,
			"keperluan"    => $post['alasan'],
			"time_limit"   => date("Y-m-d H:i:s",strtotime($post['kembalipada'])),
			"created_date" => date("Y-m-d H:i:s"),
			"created_by"   => $this->sessionData['user_id'],
			"status"       => 1,
			"isDeleted"    => 0,
			"izin_type"    => 2
		);
		$insert = $this->db->insert("izin_keluar",$insertArray);
		if ($insert) {
			custom_notif("success","Success");
			redirect("cms/perpulang");
		}else{
			custom_notif("failed");
			redirect("cms/perpulang/add");
		}
	}
	function edit($param){
		$izin_keluar_id   = encrypt_decrypt("decrypt",$param);
		$detailIzinKeluar = $this->M_perizinan->getDetailIzinkeluar($izin_keluar_id);
		$data['id']       = "";

		if (!empty($detailIzinKeluar)) {
			$data['id']	= $param;
		}

		$data["idnya"]            = $param;
		$data['detailIzinKeluar'] = $detailIzinKeluar;
		$data['web_title']        = "Edit Perizinan Pulang";
		$data['content']          = "admin/keamanan/editperizinan_pulang";
		$this->load->view('admin/layout',$data);
	}

	function doUpdate($id){
		$post           = $this->input->post();
		$id_izin_keluar = encrypt_decrypt("decrypt", $id);
		$idSantri       = encrypt_decrypt("decrypt",$post['idsantri']);
		$updateArray = array(
			"siswa_id"     => $idSantri,
			"keperluan"    => $post['alasan'],
			"time_limit"   => date("Y-m-d H:i:s",strtotime($post['kembalipada'])),
			"updated_date" => date("Y-m-d H:i:s"),
			"updated_by"   => $this->sessionData['user_id'],
			"status"       => 1,
			"isDeleted"    => 0,
			"izin_type"    => 2
		);
		
		$update = $this->db->update("izin_keluar", $updateArray, array("izin_keluar_id" => $id_izin_keluar));
		if ($update) {
			custom_notif("success","Success");
			redirect("cms/perpulang");
		}else{
			custom_notif("failed");
			redirect("cms/perpulang/edit/".$id, 'refresh');
		}
	}

	function doDelete($param){
		$id_izin_keluar = encrypt_decrypt("decrypt",$param);

		$updateArray = array(
			"isDeleted" => 1
		);
		$update = $this->db->update("izin_keluar",$updateArray,array("izin_keluar_id" => $id_izin_keluar));
		if ($update) {
			custom_notif("success","Success");
			redirect("cms/perpulang");
		}else{
			custom_notif("failed");
			redirect("cms/perpulang");
		}
	}

	function kembali($param){
		$izin_keluar_id = encrypt_decrypt("decrypt",$param);

		$detailIzinKeluar = $this->M_perizinan->getDetailIzinkeluar($izin_keluar_id);
		$data['id']	= "";
		if (!empty($detailIzinKeluar)) {
			$data['id']	= $param;
		}

		$data["idnya"]            = $param;
		$data['detailIzinKeluar'] = $detailIzinKeluar;
		$data['web_title']        = "Edit Kembali Masuk";
		$data['content']          = "admin/keamanan/kembali";
		$this->load->view('admin/layout',$data);
	}

	function getDetailIzin(){
		$get            = $this->input->get();
		$izin_keluar_id = encrypt_decrypt("decrypt",$get['param']);
		$detailKembali  = $this->M_perizinan->getDetailIzinkeluar($izin_keluar_id);
		$idnya          = $get['param'];

		$data['idnya']      = $idnya;
		$data['detailData'] = $detailKembali;
		$this->load->view('admin/keamanan/detailpulangmodal',$data);
	}

	function doKembali($param){
		$post           = $this->input->post();
		$izin_keluar_id = encrypt_decrypt("decrypt",$param);
		$updateArray = array(
			"updated_date"	=> date("Y-m-d H:i:s"),
			"updated_by"	=> $this->sessionData['user_id'],
			"return_date"	=> date("Y-m-d H:i:s",strtotime($post['tanggalkembali']))
		);
		$update = $this->db->update("izin_keluar",$updateArray,array("izin_keluar_id" => $izin_keluar_id));
		if ($update) {
			custom_notif("success","Success");
		}else{
			custom_notif("failed");
		}
		redirect('cms/perpulang');
	}
	/*==================================================== DATA FOR DATATABLE ====================================================*/
	function get_list_siswa(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->global->get_list_siswa ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->global->get_list_siswa ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->global->get_list_siswa ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->siswa_id);
			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			$button .= '
			<button type="button" class="btn btn-outline-success btn-sm" onClick="selectsantri(\''.$encrypt_id.'\',\''.htmlspecialchars($value->siswa_name).'\')">Select</button>
			';
			/*========================================= END BUTTON STUFF =========================================*/

			$rowData[] = $no++;
			$rowData[] = $value->siswa_name;
			$rowData[] = $value->siswa_nomor_induk;
			$rowData[] = $value->siswa_nisn;
			$rowData[] = $button;
			
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}

	function get_list_izin(){
		$requestParam 			= $_REQUEST;

		$getData 				= $this->M_perizinan->get_list_izinpulang ( $requestParam, 'nofilter' );
		$totalAllData 			= $this->M_perizinan->get_list_izinpulang ( $requestParam, 'nofilter', 'all' )->num_rows ();
		$totalDataFiltered 		= $this->M_perizinan->get_list_izinpulang ( $requestParam, 'nofilter', 'all' )->num_rows ();
		
		if (empty ( $requestParam ['search'] ['value'] ) > 1) {
			$getData 			= $this->M_perizinan->get_list_izinpulang ( $requestParam );
			$totalDataFiltered 	= $getData->num_rows ();
		}
		
		$listData = array ();
		$no = ($requestParam['start']+1);
		
		foreach( $getData->result () AS $value){
			$rowData    = array();
			$encrypt_id = encrypt_decrypt('encrypt',$value->izin_keluar_id);
			$status     = "";
			$status     = izin_status_html($value->return_date);

			/*========================================= BEGIN BUTTON STUFF =========================================*/
			$button = "";
			if ($value->return_date == "") {
				$button .= '
				<button class="btn btn-danger btn-sm tbl-btn" onClick="is_delete(\''.base_url('cms/perpulang/doDelete/'.$encrypt_id).'\')" title="Delete"><i class="fa fa-trash"></i></button>';
				$button .= '
				<a href="'.base_url('cms/perpulang/edit/'.$encrypt_id).'" class="btn btn-primary btn-sm tbl-btn" title="Edit"><i class="fa fa-edit"></i></a>';
				$button .= '
				<button class="btn btn-success btn-sm tbl-btn" id="kembali" onClick="dokembali(\''.$encrypt_id.'\')" title="Kembali">Kembali</button>';
			}else{
				$button.="Kembali Pada: <br><span class='badge badge-bdr badge-success'>".date("d M Y h:i A",strtotime($value->return_date))."</span>";
			}
			
			/*========================================= END BUTTON STUFF =========================================*/

			

			$rowData[]  = $no++;
			$rowData[]  = $value->siswa_name." / NIK: ".$value->siswa_nomor_induk;
			$rowData[]  = $value->keperluan;
			$rowData[]  = date("d M Y h:i A",strtotime($value->created_date));
			$rowData[]  = date("d M Y h:i A",strtotime($value->time_limit));
			$rowData[]  = $status;
			$rowData[]  = $button;
			$listData[] = $rowData;
			
			$json_data = array (
				"draw"            => intval ( $requestParam ['draw'] ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => intval ( $totalAllData ), // total number of records
				"recordsFiltered" => intval ( $totalDataFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $listData 
			); // total data array
		}
		if(empty($json_data)){
			$json_data = array (
				"draw"            => 0, // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
				"recordsTotal"    => 0, // total number of records
				"recordsFiltered" => 0, // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => ""
			); // total data array
		}
		header ( 'Content-Type: application/json;charset=utf-8' );
		echo json_encode ($json_data);
		die();
	}

}