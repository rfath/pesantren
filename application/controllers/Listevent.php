<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listevent extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//$this->sessionData = $this->session->sessionData;
	}

	function index(){
		$get = $this->input->get();
		$date = "";
		if (isset($get['date'])) {
			$date = $get['date'];
		}
		$getListEvent = $this->frontpage->getListEvent(0,$date);

		$data['date']       = $date;
		$data['list_event'] = $getListEvent;
		$data['content']    = "front/front_page/eventList";
		$this->load->view('front/layout',$data);
	}

	function loadmore($page){
		$get = $this->input->get();
		$date = "";
		if (isset($get['date'])) {
			$date = $get['date'];
		}
		$getListEvent = $this->frontpage->getListEvent($page,$date);
		$html = "";
		foreach ($getListEvent as $key => $levalue) {
			$html.='<div class="event-item">
				<span class="date"><strong>'.date("d",strtotime($levalue->event_start_date)).'</strong>'.date("M",strtotime($levalue->event_start_date)).'</span>
				<span class="time"><span class="h">'.date("d M Y",strtotime($levalue->event_start_date)).'<span class="sep"></span> '.date("d M Y",strtotime($levalue->event_end_date)).'</span></span>
				<div class="frame">
					<h2><a href="'.base_url('event/detail/'.$levalue->event_id).'">'.$levalue->event_name.'</a></h2>
					<span class="meta"><a href="#">'.$levalue->place_name.'</a></span>
				</div>
				<a href="'.base_url('event/detail/'.$levalue->event_id).'" class="more"><i class="fa fa-angle-right"></i></a>
			</div>';
		}
		die($html);
	}
}