<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//$this->sessionData = $this->session->sessionData;
	}

	public function index(){
		$getListEvent = $this->frontpage->getListEvent();

		$data['list_event']	= $getListEvent;
		$data['front_slider'] = array("OK");
		$data['image_post']   = array(); // Image post in layout.php
		$data['content']      = "front/front_page/front_page";
		$this->load->view('front/layout',$data);
	}
}