<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->sessionMember = $this->session->sessionMember;
		$this->load->model("M_event");
		$this->load->model("M_order");
	}

	function index(){
		redirect();
	}

	function detail($id){
		$detailEvent  = $this->frontpage->getDetailEvent($id);
		$featureImage = base_url($detailEvent->event_featured_image);
		$listSchedule = $this->global->getScheduleIsShowFrontByEvent($id);

		$latlong = $detailEvent->longlat;
		$latlong = explode(",", $latlong);

		$data['listSchedule'] = $listSchedule;
		$data['loginData']	  = $this->sessionMember;
		$data['latlong']      = $latlong;
		$data['detailEvent']  = $detailEvent;
		$data['front_slider'] = array();
		$data['image_post']   = array("url" => $featureImage); // Image post in layout.php
		$data['content']      = "front/front_page/eventDetail";
		$this->load->view('front/layout',$data);
	}

	function register($id){
		checkMemberLogin();
		$detailEvent       = $this->frontpage->getDetailEvent($id);
		$featureImage      = base_url($detailEvent->event_featured_image);
		$dataPackage       = $this->M_event->getEventPackage($detailEvent->event_id);
		
		$dataUser          = $this->global->getDetailMember($this->sessionMember['member_id']);
		
		$offline_bank      = json_decode($detailEvent->offline_bank_transfer_destination);
		
		$checkIsRegistered = $this->frontpage->isUserRegisteredEvent($id, $this->sessionMember['member_id']);		
		

		$data['isRegistered']= $checkIsRegistered;
		$data['dataUser']    = $dataUser;
		$data['package']     = $dataPackage;
		$data['loginData']   = $this->sessionMember;
		$data['detailEvent'] = $detailEvent;
		$data['offlineBank'] = $offline_bank;

		$data['front_slider'] = array();
		$data['image_post']   = array(); // Image post in layout.php
		$data['content']      = "front/front_page/eventRegister";
		$this->load->view('front/layout',$data);
	}

	function manualregister($id, $iduser){
		$sessionData = $this->session->sessionData;
		if (empty($sessionData)) {
			redirect();
		}

		$detailEvent       = $this->frontpage->getDetailEvent($id);
		if (empty($detailEvent)) {
			redirect();
		}
		$featureImage      = base_url($detailEvent->event_featured_image);
		$dataPackage       = $this->M_event->getEventPackage($detailEvent->event_id);
		
		$dataUser          = $this->global->getDetailMember($iduser);
		if (empty($dataUser)) {
			redirect();
		}
		
		$offline_bank      = json_decode($detailEvent->offline_bank_transfer_destination);
		
		$checkIsRegistered = $this->frontpage->isUserRegisteredEvent($id, $iduser);		
		

		$data['isRegistered']= $checkIsRegistered;
		$data['dataUser']    = $dataUser;
		$data['package']     = $dataPackage;
		$data['detailEvent'] = $detailEvent;
		$data['offlineBank'] = $offline_bank;

		$data['front_slider'] = array();
		$data['image_post']   = array(); // Image post in layout.php
		$data['content']      = "front/front_page/eventRegisterManual";
		$this->load->view('front/layout',$data);
	}

	function detailPackage($id = ""){
		if ($id == "") {
			echo "";
		}else{
			$packageDetail = $this->M_event->getPackageDetail($id);
			$schedule = $this->M_event->getScheduleByPackage($packageDetail->event_package_id);

			$data['schedule']      = $schedule;
			$data['packageDetail'] = $packageDetail;
			$this->load->view('front/front_page/eventPackageDetail',$data);
		}
	}

	function doRegister(){
		checkMemberLogin();
		$post = $this->input->post();
		$dataMember = $this->global->getShortDetailMember($this->session->sessionMember['member_id']);

		$seat = 1;
		$additional_participant = "";
		if ($post['additionalp']) {
			$countAdditional = count($post['additionalp']);
			$seat            = $seat + $countAdditional;
			$newVal = array();
			foreach ($post['additionalp'] as $adkey => $advalue) {
				$advalue['eticket']	= "";
				$newVal[] = $advalue;
			}
			$additional_participant = json_encode(array_values($newVal));
		}
		
		/*=================== GET DETAIL DATA LIKE EVENT, PACKAGE ETC ===================*/
		$detailEvent   = $this->frontpage->getDetailEvent($post['param']);
		$packageDetail = $this->M_event->getPackageDetail($post['package']);
		/*=================== #### ===================*/


		/*=================== COUNT PRICE ===================*/
		$price = $packageDetail->package_price;
		$totalPrice = $price * $seat;
		/*=================== #### ===================*/
		$paymentArray = array();
		if ($post['payment'] == 2) {
			$bank = "";
			$bank = explode("|", $post['bank']);
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Manual Transfer",
				"pay_detail"    => $bank
			);
		}else{
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Online Payment Channel",
				"pay_detail"    => ''
			);
		}
		$pay_type = json_encode($paymentArray);

		$insertArray = array(
			"event_id"               => $post['param'],
			"package_id"             => $post['package'],
			"qty_seat"               => $seat,
			"order_price"            => $totalPrice,
			"member_id"              => $this->sessionMember['member_id'],
			"additional_participant" => $additional_participant,
			"order_time"             => date("Y-m-d H:i:s"),
			"payment_type"           => $pay_type,
			"payment_status"         => 0,
			"payment_time"           => "",
			"payment_remarks"        => $post['remarks'],
			"order_status"           => 0,
			"eticket"                => "",
			"order_participant_name" => $dataMember->member_name,
			"gender"                 => $post['gender'],
			"age"                    => $post['age']
		);
		$insert = $this->db->insert("order",$insertArray);
		$insert_id = $this->db->insert_id();
		if ($insert) {
			$updateSeat = "UPDATE event_package SET package_remaining_seat = package_remaining_seat - ".$seat." WHERE event_package_id = ".$post['package'];
			$query     = $this->db->query($updateSeat);
			$this->send_email($insert_id);
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("event/detailOrder/".$insert_id);
		}else{
			$this->session->set_flashdata('is_success', 'Failed to register');
			redirect("event/register/".$post['param']);
		}
	}

	function doManualRegister(){
		$post = $this->input->post();
		$dataMember = $this->global->getShortDetailMember($post['member_param']);

		$seat = 1;
		$additional_participant = "";
		if ($post['additionalp']) {
			$countAdditional = count($post['additionalp']);
			$seat            = $seat + $countAdditional;

			$newVal = array();
			foreach ($post['additionalp'] as $adkey => $advalue) {
				$advalue['eticket']	= "";
				$newVal[] = $advalue;
			}
			$additional_participant = json_encode(array_values($newVal));
		}
		
		/*=================== GET DETAIL DATA LIKE EVENT, PACKAGE ETC ===================*/
		$detailEvent   = $this->frontpage->getDetailEvent($post['param']);
		$packageDetail = $this->M_event->getPackageDetail($post['package']);
		/*=================== #### ===================*/


		/*=================== COUNT PRICE ===================*/
		$price = $packageDetail->package_price;
		$totalPrice = $price * $seat;
		/*=================== #### ===================*/
		$paymentArray = array();
		if ($post['payment'] == 2) {
			$bank = "";
			$bank = explode("|", $post['bank']);
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Manual Transfer",
				"pay_detail"    => $bank
			);
		}elseif($post['payment'] == 3){
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Offline Payment (Cash)",
				"pay_detail"    => ''
			);
		}else{
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Online Payment Channel",
				"pay_detail"    => ''
			);
		}
		$pay_type = json_encode($paymentArray);

		$insertArray = array(
			"event_id"               => $post['param'],
			"package_id"             => $post['package'],
			"qty_seat"               => $seat,
			"order_price"            => $totalPrice,
			"member_id"              => $post['member_param'],
			"additional_participant" => $additional_participant,
			"order_time"             => date("Y-m-d H:i:s"),
			"payment_type"           => $pay_type,
			"payment_status"         => 0,
			"payment_time"           => "",
			"payment_remarks"        => $post['remarks'],
			"order_status"           => 0,
			"eticket"                => "",
			"order_participant_name" => $dataMember->member_name,
			"gender"                 => $post['gender'],
			"age"                    => $post['age']
		);
		$insert = $this->db->insert("order",$insertArray);
		$insert_id = $this->db->insert_id();
		if ($insert) {
			$updateSeat = "UPDATE event_package SET package_remaining_seat = package_remaining_seat - ".$seat." WHERE event_package_id = ".$post['package'];
			$query      = $this->db->query($updateSeat);
			$this->send_email($insert_id);
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("event/detailOrderManual/".$insert_id);
		}else{
			$this->session->set_flashdata('is_success', 'Failed to register');
			redirect("event/manualregister/".$post['param']."/".$post['member_param']);
		}
	}

	function send_email($id){
		$dataOrder = $this->M_order->orderDetail($id);
		
		$additional = "";
		$seat = 1;
		if ($dataOrder->additional_participant <> "") {
			$additionalData = json_decode($dataOrder->additional_participant);
			foreach ($additionalData as $key => $value) {
				$additionalArray[] = $value->name." (E-ticket : <i>".$value->eticket."</i>)";
			}
			$count      = count($additionalArray);
			$seat       = $seat + $count;
			$additional = implode(", " , $additionalArray);
		}

		$pay = json_decode($dataOrder->payment_type);
		
		if ($pay->pay_type == 2) {
			$payment = $pay->pay_type_info." / ".$pay->pay_detail[0]." (".$pay->pay_detail[1].":".$pay->pay_detail[2].")";
		}else{
			$payment = $pay->pay_type_info;
		}

		$data['payment']     = $payment;
		$data['seat']        = $seat;
		$data['additional']  = $additional;
		$data['detailOrder'] = $dataOrder;
		
		$attachments = array();
		$subject     = "Thank you";
		$template    = $this->load->view("front/front_page/eventEmailFormat",$data,true);

		$this->global->send_email ( defaultFormEmail(), defaultFormName(), $dataOrder->member_email, "", $subject, $template,$attachments );
	}

	function detailOrder($id){
		checkMemberLogin();
		$dataOrder = $this->M_order->orderDetail($id);
		if ($this->sessionMember['member_id'] <> $dataOrder->member_id) {
			redirect();
		}
		/*================= START STATUS PAYMENT =================*/
		//0: Waiting Payment, 1: Waiting Admin Approve , 2: Accept, 3: Reject
		if ($dataOrder->payment_status == 0) {
			$status_payment = "Waiting For Payment";
		}elseif($dataOrder->payment_status == 1){
			$status_payment = "Waiting Admin Approve";
		}elseif($dataOrder->payment_status == 2){
			$status_payment = "Accepted";
		}else{
			$status_payment = "Rejected";
		}

		$additional = array();
		$seat = 1;
		if ($dataOrder->additional_participant <> "") {
			$additional = json_decode($dataOrder->additional_participant);
			$count      = count($additional);
			$seat       = $seat + $count;
		}
		
		$pay = json_decode($dataOrder->payment_type);
		
		if ($pay->pay_type == 2) {
			$payment = $pay->pay_type_info." / ".$pay->pay_detail[0]." (".$pay->pay_detail[1].":".$pay->pay_detail[2].")";
		}else{
			$payment = $pay->pay_type_info;
		}

		$data['payment']        = $payment;
		$data['seat']           = $seat;
		$data['additional']     = $additional;
		$data['status_payment'] = $status_payment;
		$data['dataOrder']      = $dataOrder;
		$data['front_slider']   = array();
		$data['image_post']     = array(); // Image post in layout.php
		$data['content']        = "front/front_page/eventDetailOrder";
		$this->load->view('front/layout',$data);
	}

	function detailOrderManual($id){
		$dataOrder = $this->M_order->orderDetail($id);
		/*================= START STATUS PAYMENT =================*/
		//0: Waiting Payment, 1: Waiting Admin Approve , 2: Accept, 3: Reject
		if ($dataOrder->payment_status == 0) {
			$status_payment = "Waiting For Payment";
		}elseif($dataOrder->payment_status == 1){
			$status_payment = "Waiting Admin Approve";
		}elseif($dataOrder->payment_status == 2){
			$status_payment = "Accepted";
		}else{
			$status_payment = "Rejected";
		}

		$additional = array();
		$seat = 1;
		if ($dataOrder->additional_participant <> "") {
			$additional = json_decode($dataOrder->additional_participant);
			$count      = count($additional);
			$seat       = $seat + $count;
		}
		
		$pay = json_decode($dataOrder->payment_type);
		
		if ($pay->pay_type == 2) {
			$payment = $pay->pay_type_info." / ".$pay->pay_detail[0]." (".$pay->pay_detail[1].":".$pay->pay_detail[2].")";
		}else{
			$payment = $pay->pay_type_info;
		}
		//debugCode($dataOrder);
		$data['payment']        = $payment;
		$data['seat']           = $seat;
		$data['additional']     = $additional;
		$data['status_payment'] = $status_payment;
		$data['dataOrder']      = $dataOrder;
		$data['front_slider']   = array();
		$data['image_post']     = array(); // Image post in layout.php
		$data['content']        = "front/front_page/eventDetailOrderManual";
		$this->load->view('front/layout',$data);
	}

	function payment($id){
		checkMemberLogin();
		$dataOrder = $this->M_order->orderDetail($id);
		if (($this->sessionMember['member_id'] <> $dataOrder->member_id) OR empty($dataOrder) ) {
			redirect();
		}
		
		$payment_type = json_decode($dataOrder->payment_type,true);
		
		$data['id']           = $id;
		$data['payment_type'] = $payment_type;
		$data['dataOrder']    = $dataOrder;
		$data['member']       = $this->global->getDetailMember($dataOrder->member_id);
		$data['front_slider'] = array();
		$data['image_post']   = array(); // Image post in layout.php
		if ($dataOrder->payment_status == 0) {
			if ($payment_type['pay_type'] <> 1) {
				$data['content']        = "front/front_page/eventPayment";
			}else{
				$data['doku_config']	= $this->global->get_doku_config();
				$data['content']        = "front/front_page/eventPaymentOnline";
			}
		}else{
			redirect();
		}
		$this->load->view('front/layout',$data);
	}

	function uploadpayment($id){
		$post = $this->input->post();
		checkMemberLogin();
		$dataOrder = $this->M_order->orderDetail($id);
		
		$dir = './assets_ticketing/confirmation/';
		if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}

		if (empty($dataOrder) AND ($dataOrder->payment_status <> 0)) {
			$this->session->set_flashdata('is_success', 'Payment Error.');
			redirect('order/historyorder/');
		}

		/*==== START UPLOADING FILE ====*/
		$ext           = pathinfo($_FILES['fileconfirm']['name'], PATHINFO_EXTENSION);
		$file_name = "confirm".date("dmYHis").rand(100,999).".".$ext;
		
		$config['upload_path']   = $dir;
		$config['allowed_types'] = '*';
		$config['file_name']     = $file_name;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('fileconfirm')){
			$error = 'error: '. $this->upload->display_errors();
			echo $error;
			die();
		}else{
			$file_name = $dir.$file_name;
			$file_name = substr($file_name, 1); //remove "."
		}
		/*==== END UPLOADING FILE ====*/

		/*==== START UPDATE DATA ====*/
		$updateArray = array(
			"payment_time"    => date("Y-m-d H:i:s"),
			"payment_remarks" => $post['remarks'],
			"payment_status"  => 1,
			"payment_file"    => $file_name
		);
		$update = $this->db->update("order", $updateArray, array("order_id"	=> $id));
		if ($update) {
			$this->session->set_flashdata('is_success', 'Yes');
			redirect('order/historyorder/');
		}else{
			$this->session->set_flashdata('is_success', 'Payment Failed, Something went wrong');
			redirect('order/historyorder/');
		}
	}

	function manualregisterAttendance($event_id){
		$sessionData = $this->session->sessionData;
		if (empty($sessionData)) {
			redirect();
		}

		$detailEvent       = $this->frontpage->getDetailEvent($event_id);
		if (empty($detailEvent)) {
			redirect();
		}
		$dataPackage       = $this->M_event->getEventPackage($event_id);
		$offline_bank      = json_decode($detailEvent->offline_bank_transfer_destination);

		$region        = $this->global->getListRegion();
		$dpw           = $this->global->getListDPW();
		$satelitChurch = $this->global->getSatelitChurch();
		$getChurchRole = $this->global->getChurchRole();


		$data['region']        = $region;
		$data['dpw']           = $dpw;
		$data['satelitChurch'] = $satelitChurch;
		$data['getChurchRole'] = $getChurchRole;
		$data['package']       = $dataPackage;
		$data['detailEvent']   = $detailEvent;
		$data['offlineBank']   = $offline_bank;

		$data['front_slider'] = array();
		$data['image_post']   = array(); // Image post in layout.php
		$data['content']      = "front/front_page/eventRegisterManualAttendance";
		$this->load->view('front/layout',$data);
	}

	function doManualRegisterAttendance(){
		$post = $this->input->post();

		/*======== START CHECK IF PARTICIPANT EMAIL ALREADY EXIST ========*/
		$checkEmailParticipant = $this->M_order->checkMemberByEmail($post['email_participant']);
		if (empty($checkEmailParticipant)) {
			$insertMemberArray = array(
				"member_name"     => $post['name_participant'],
				"member_email"    => $post['email_participant'],
				"member_phone"    => $post['phone_participant'],
				"member_password" => md5("123456"),
				"created_date"    => date("Y-m-d H:i:s"),
				"is_active"       => 1,
				"is_deleted"      => 0
			);

			$doInsertMember = $this->db->insert("member", $insertMemberArray);
			$memberinsertID = $this->db->insert_id();
			$member_id = $memberinsertID;
		}else{
			$member_id = $checkEmailParticipant->member_id;
		}
		/*======== END CHECK IF PARTICIPANT EMAIL ALREADY EXIST ========*/ 

		$seat = 1;
		$additional_participant = "";
		if ($post['additionalp']) {
			$countAdditional = count($post['additionalp']);
			$seat            = $seat + $countAdditional;

			$newVal = array();
			foreach ($post['additionalp'] as $adkey => $advalue) {
				$newVal[] = $advalue;
			}
			$additional_participant = json_encode(array_values($newVal));
		}
		
		/*=================== GET DETAIL DATA LIKE EVENT, PACKAGE ETC ===================*/
		$detailEvent   = $this->frontpage->getDetailEvent($post['param']);
		$packageDetail = $this->M_event->getPackageDetail($post['package']);
		/*=================== #### ===================*/

		/*=================== COUNT PRICE ===================*/
		$price      = $packageDetail->package_price;
		$totalPrice = $price * $seat;
		/*=================== #### ===================*/

		/*=================== START INSERT TO ORDER ===================*/
		$paymentArray = array();
		if ($post['payment'] == 2) {
			$bank = "";
			$bank = explode("|", $post['bank']);
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Manual Transfer",
				"pay_detail"    => $bank
			);
		}elseif($post['payment'] == 3){
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Offline Payment (Cash)",
				"pay_detail"    => ''
			);
		}else{
			$paymentArray = array(
				"pay_type"      => $post['payment'],
				"pay_type_info" => "Online Payment Channel",
				"pay_detail"    => ''
			);
		}
		$pay_type = json_encode($paymentArray);

		$insertArray = array(
			"event_id"               => $post['param'],
			"package_id"             => $post['package'],
			"qty_seat"               => $seat,
			"order_price"            => $totalPrice,
			"member_id"              => $member_id,
			"additional_participant" => $additional_participant,
			"order_time"             => date("Y-m-d H:i:s"),
			"payment_type"           => $pay_type,
			"payment_status"         => 2,
			"payment_time"           => "",
			"payment_remarks"        => $post['remarks'],
			"order_status"           => 0,
			"eticket"                => $post['eticket'],
			"order_participant_name" => $post['name_participant'],
			"gender"                 => $post['gender'],
			"age"                    => $post['age']
		);
		$insert    = $this->db->insert("order",$insertArray);
		$insert_id = $this->db->insert_id();
		/*=================== END INSERT TO ORDER ===================*/

		/*=================== START INSERT TO ATTENDANCE ===================*/
		$att[] = array(
			"attendance_name"   => $post['name_participant'],
			"attendance_number" => $post['phone_participant'],
			"attendance_email"  => $post['email_participant'],
			"order_id"          => $insert_id,
			"region_id"         => $post['region'],
			"dpw_id"            => $post['dpw'],
			"satelit_church_id" => $post['satelit'],
			"church_role_id"    => $post['church'],
			"created_date"      => date("Y-m-d H:i:s"),
			"is_active"         => 0,
			"is_deleted"        => 0,
			"eticket"           => $insertArray['eticket'],
			"gender"            => $post['gender'],
			"age"               => $post['age']
		);
		if (!empty($post['additionalp'])) {
			foreach ($newVal as $adkey => $advalue) {
				$att[] = array(
					"attendance_name"   => $advalue['name'],
					"attendance_number" => $advalue['phone'],
					"attendance_email"  => $advalue['email'],
					"order_id"          => $insert_id,
					"region_id"         => $post['region'],
					"dpw_id"            => $post['dpw'],
					"satelit_church_id" => $post['satelit'],
					"church_role_id"    => $post['church'],
					"created_date"      => date("Y-m-d H:i:s"),
					"is_active"         => 0,
					"is_deleted"        => 0,
					"eticket"           => $advalue['eticket'],
					"gender"            => $post['gender'],
					"age"               => $post['age']
				);
			}
		}
		
		foreach ($att as $atskey => $atsvalue) {			
			$insertAttendance = $this->db->insert("attendance",$atsvalue);
		}
		/*=================== END INSERT TO ORDER ===================*/
		if ($insert) {
			$updateSeat = "UPDATE event_package SET package_remaining_seat = package_remaining_seat - ".$seat." WHERE event_package_id = ".$post['package'];
			$query     = $this->db->query($updateSeat);
			//$this->send_email($insert_id);
			$this->session->set_flashdata('is_success', 'Yes');
			redirect("event/detailOrderManual/".$insert_id);
		}else{
			$this->session->set_flashdata('is_success', 'Failed to register');
			redirect("event/manualregisterAttendance/".$post['param']);
		}
	}	
}